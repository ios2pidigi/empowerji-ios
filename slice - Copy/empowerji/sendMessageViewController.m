//
//  moListViewController.m
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "sendMessageViewController.h"
#import "detailViewController.h"
#import "Constants.h"
#import "moTableViewCell.h"
#import "UIImageView+JMImageCache.h"
#import "searchViewController.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "connectivity.h"

#import "homeViewController.h"

@interface sendMessageViewController ()
{
    NSArray *mArray;
}
@end

@implementation sendMessageViewController
@synthesize isfrmHelp;

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
     adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    [self.adVw loadRequest:[GADRequest request]];
    
    mArray = [[NSArray alloc]init];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
  
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}

# pragma mark WebService Calls

-(void)sendMessageWebservice
{
    
  //  click_from=ask_question
  // help = help
    

    
    NSString *click_from = @"";
    
    if(isfrmHelp)
    {
        click_from = @"help";
    }
    else
    {
      click_from = @"ask_question";
        
        //plan_name
    }
    
    
     [self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *feedTxtVwText = [_txtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    if(feedTxtVwText.length>0)
    {
        
    }
    else
    {
        NSString *lang = langGlobal;
        
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"messageEmpty" value:@"" table:@"strings-english"];
        
        
        [self.view makeToast:message
                    duration:3.0
                    position:CSToastPositionBottom];
        
        //[SVProgressHUD dismiss];
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           
                           [self.view hideToastActivity];
                           
                       });
        
        return ;
    }
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@feedback_submit.php?user_id=%@&feedback=%@&lang_id=%ld&click_from=%@",baseUrl,userid,feedTxtVwText,langidd,click_from];
    
    if(isfrmHelp)
    {
        click_from = @"help";
        
        wUrl=[NSString stringWithFormat:@"%@feedback_submit.php?user_id=%@&feedback=%@&lang_id=%ld&click_from=%@",baseUrl,userid,feedTxtVwText,langidd,click_from];
    }
    else
    {
        click_from = @"ask_question";
        
        NSString *plan_name = @"";
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"planName"])
          plan_name =  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"planName"]];
        
        wUrl=[NSString stringWithFormat:@"%@feedback_submit.php?user_id=%@&feedback=%@&lang_id=%ld&click_from=%@&plan_name=%@",baseUrl,userid,feedTxtVwText,langidd,click_from,plan_name];
        
        //plan_name
    }
    
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self sendMessageWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            _txtVw.text = @"";
            // [SVProgressHUD dismiss];
            
            NSArray *result = [[json objectForKey:@"result"]objectAtIndex:0];
            
            if([[result valueForKey:@"status"] isEqualToString:@"success"])
            {
                [self.view makeToast:[result valueForKey:@"message"]
                            duration:3.0
                            position:CSToastPositionBottom];
                
            }
            else
            {
                [self.view makeToast:@"Oops..Something went wrong"
                            duration:3.0
                            position:CSToastPositionBottom];
            }
            
           // [self backFeed:nil];
            
            dispatch_async(dispatch_get_main_queue(),
                           ^{
                               
                               [self.view hideToastActivity];
                               
                           });
            
        });
    }];
    
    [dataTask resume];
    
}



# pragma mark Button Actions

- (IBAction)btnSearchActn:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)homeAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)submitAction:(id)sender
{
    [self.view endEditing:YES];
    
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
      [self sendMessageWebservice];
    }
                  
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
    
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
   /* NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    */
    
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
    
}

@end
