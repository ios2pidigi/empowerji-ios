//
//  moListViewController.h
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface sendMessageViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UITableView *mTblVw;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@property(nonatomic) BOOL isfrmHelp;

@property (strong, nonatomic) IBOutlet UITextView *txtVw;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

@end
