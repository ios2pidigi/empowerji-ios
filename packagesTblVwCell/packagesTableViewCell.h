//
//  packagesTableViewCell.h
//  ssc
//
//  Created by sarath sb on 7/5/19.
//  Copyright © 2019 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface packagesTableViewCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UIImageView *plusImg;
@property (strong, nonatomic) IBOutlet UIButton *getTrialBtn;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UIImageView *imgVid;
@property (strong, nonatomic) IBOutlet UILabel *unlimTechLbl;
@property (strong, nonatomic) IBOutlet UIImageView *imgTec;

@property (strong, nonatomic) IBOutlet UIButton *aboutBtn;
@property (strong, nonatomic) IBOutlet UILabel *unlimVidLbl;
@property (strong, nonatomic) IBOutlet UILabel *planLbl;
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UIButton *btnClick;
@property (strong, nonatomic) IBOutlet UILabel *proLbl;

@end

NS_ASSUME_NONNULL_END
