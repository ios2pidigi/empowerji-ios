//
//  packagesTableViewCell.h
//  ssc
//
//  Created by sarath sb on 7/5/19.
//  Copyright © 2019 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface basicpackagesTableViewCell : UITableViewCell

{
    
}
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UIImageView *unlimImg;
@property (strong, nonatomic) IBOutlet UIButton *abtBtn;
@property (strong, nonatomic) IBOutlet UILabel *unlimLbl;
@property (strong, nonatomic) IBOutlet UILabel *planLbl;
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UIButton *btnClick;
@property (strong, nonatomic) IBOutlet UILabel *proLbl;
@property (strong, nonatomic) IBOutlet UIImageView *img;
@end

NS_ASSUME_NONNULL_END
