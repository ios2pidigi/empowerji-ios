//
//  loginViewController.m
//  ssc
//
//  Created by swaroop on 05/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "languagesViewController.h"
#import "signupViewController.h"
#import "forgotPassViewController.h"
#import "homeViewController.h"
#import "Constants.h"
#import "SVProgressHUD.h"
#import "connectivity.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

#import "GAI.h"
#import "GAIDictionaryBuilder.h"

#import "lngTableViewCell.h"
#import "loginViewController.h"

@interface languagesViewController ()
{
    //NSArray *bArray;
    
    NSArray *lnArr;
    
    NSInteger setRadioSelectedIndex;
    
    NSMutableArray *arraySelectObject;
    
    BOOL isFirst;
}
@end

@implementation languagesViewController
@synthesize isFromAppdelegate,isFrmHome;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isFirst = NO;
    
     NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    [self lnWebservice];
    
    [_btnContinue addTarget:self action:@selector(continueAction) forControlEvents:UIControlEventTouchUpInside];
    
    if(isFromAppdelegate)
    {
        _backImg.hidden = YES;
        
        
    }
    else
    {
      [_backBtn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        _backImg.hidden = NO;
        
    }
    
    //token for push services
    
  //  if([[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"])
   //     [self saveGuestTokenWS]; //save token
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
    UITouch *touch = [touches anyObject];
    if(![touch.view isKindOfClass:[UITextField class]])
    {
        [self.view endEditing:YES];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    _btnContinue.layer.cornerRadius = _btnContinue.frame.size.height/2;

    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}

# pragma mark WebService Calls

-(void)lnWebservice
{
    [SVProgressHUD show];
   
    
    NSString *wUrl=[NSString stringWithFormat:@"%@languages.php?from=lang_chooser",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self lnWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           lnArr = [json objectForKey:@"result"];
          
            arraySelectObject = [[NSMutableArray alloc]init];
            
            for ( int i=0; i < lnArr.count; i++ )
            {
                [arraySelectObject addObject:@"0"];
            }
            
            [_lTblVw reloadData];
            
            NSString *lid;
            
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"languageIDMain"])
                lid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"languageIDMain"]];
            else
                lid = @"0";
            
            
            for(int i=0; i< lnArr.count ; i++)
            {
                NSString *langID = [[lnArr objectAtIndex:i]valueForKey:@"id"];
                if([lid caseInsensitiveCompare:langID]==NSOrderedSame)
                {
                    NSString *langText = [[lnArr objectAtIndex:i]valueForKey:@"language"];
                    
                    NSLog(@"%d %@",i,langText);
                    
                    setRadioSelectedIndex = i;
                }
            }
            
            
            [SVProgressHUD dismiss];
        });
    }];
    
    [dataTask resume];
    
}

-(void)saveGuestTokenWS
{
    int randIntgr = arc4random_uniform(10000);
    
    //[self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
   // NSString *userid = [NSString stringWithFormat:@"%@",[[arrUser objectAtIndex:0] valueForKey:@"user_id"]];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"];
    
    token = [token stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    
    /* save_token.php?user_id=150&token=d0p_RE9zG_o:APA91bFPDu-tbWoeaSNZTTSkLtjelz7wYgJq6uL9XXkqqFubkvHq8mSP9pfCF4fZv9doazkWItnLP7Pk97mD8raacu_A8NAlHt2ZsSAL-46GWcQPLCzyk19Elp24OkKPY83b9HJK5GLB&device_id=9e9e11f98bb75e23&device=android
     */
    
    
    NSString *langid;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"])
        langid = [[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"];
    else
        langid = @"5";
    
    //app.empowerji.com/webservice4/save_guest.php?&lang_id=5&device_id=7439aeb9fcd7aaa9
    
    NSString *wUrl=[NSString stringWithFormat:@"%@save_guest.php?token=%@&device_id=%@&device=ios&counts=%d&lang_id=%@",baseUrl,token,currentDeviceId,randIntgr,langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self saveGuestTokenWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json token save:%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)saveTokenWS
{
    int randIntgr = arc4random_uniform(10000);
    
    //[self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *userid = [NSString stringWithFormat:@"%@",[[arrUser objectAtIndex:0] valueForKey:@"user_id"]];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"];
    
    token = [token stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"useridForToken"])
        userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"useridForToken"]];
    
    NSString *langid;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"langidEdit"])
        langid = [[NSUserDefaults standardUserDefaults]objectForKey:@"langidEdit"];
    else
        langid = @"5";
    
    NSString *wUrl=[NSString stringWithFormat:@"%@save_token.php?user_id=%@&token=%@&device_id=%@&device=ios&counts=%d&lang_id=%@",baseUrl,userid,token,currentDeviceId,randIntgr,langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
   {
       NSDictionary *json;
       
       if(data ==nil)
       {
           [self saveTokenWS];
           return ;
       }
       
       json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
       
       NSLog(@"json token save:%@", json);
       
       
       
       dispatch_async(dispatch_get_main_queue(), ^{
           
           
       });
   }];
    
    [dataTask resume];
    
}

# pragma mark Button Actions

-(void)radioBtnAction:(UIButton*)sender
{
    [[NSUserDefaults standardUserDefaults]setInteger:sender.tag forKey:@"selectedRadioBtn"];
    
    isFirst = YES;
    
    for(int i=0; i< lnArr.count ; i++)
    {
        if(sender.tag == i)
        {
          [arraySelectObject replaceObjectAtIndex:i withObject:@"1"];
        }
        else
        {
            [arraySelectObject replaceObjectAtIndex:i withObject:@"0"];
        }
    }
    
    [_lTblVw reloadData];
    
    
   /* for (UITableViewCell *cell in _lTblVw.subviews)
    {
        if ([cell isKindOfClass:[UITableViewCell class]])
        {
            for (UIView *button in cell.contentView.subviews)
            {
                if ([button isKindOfClass:[DLRadioButton class]])
                {
                    [(UIButton *)button setSelected:NO];
                }
            }
            
        }
        else
        {
            if ([NSStringFromClass([cell class]) isEqualToString:@"UITableViewWrapperView"])
            {
                
                    for (UIView *button in cell.contentView.subviews)
                    {
                        if ([button isKindOfClass:[DLRadioButton class]])
                        {
                            [(UIButton *)button setSelected:NO];
                        }
                    }
                   
                
            }
        }
    }
    */
    
   // [sender setSelected:YES];
    
    //lang_tblvw
    NSString *langid = [NSString stringWithFormat:@"%@",[[lnArr objectAtIndex:sender.tag]valueForKey:@"id"]];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    [[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"dummyLang"];
    else
    {
      //[[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"dummyLang"];
    
    [[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"langidEdit"];
    
    [[NSUserDefaults standardUserDefaults]setInteger:[langid integerValue] forKey:@"languageIDMain"];
    }
    
    
    //save language selected locally
    NSString *selLang;
    
    NSInteger i = [langid integerValue];
    switch (i) {
        case 0:
            selLang = @"en";
            break;
        case 1:
            selLang = @"en";
            break;
        case 2:
            selLang = @"hi";
            break;
        case 3:
            selLang = @"mr-IN";
            break;
        case 4:
            selLang = @"gu-IN";
            break;
        case 5:
            selLang = @"en";
            break;
        default:
            break;
    }
    if(selLang)
        [[NSUserDefaults standardUserDefaults] setObject:selLang forKey:@"Dlanguage"];
    
   
}

- (IBAction)back:(id)sender
{
    if(isFrmHome)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)continueAction
{
    
    
    //[[NSUserDefaults standardUserDefaults]removeObjectForKey:@"supportClickedHereButton"];
   // [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"keepgoingClickedHereButton"];
   // [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"keepmeloggedin"];
   // [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userEmail"];
    
    
    /////////////
    NSString *langid;
    
    {
        
        
        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
        {
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"])
                langid =  [[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"];
            
            [[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"langidFromLanguages"];
        }
        else
        {
            //[[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"dummyLang"];
            
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"langidEdit"])
                langid =  [[NSUserDefaults standardUserDefaults]objectForKey:@"langidEdit"];
            
            [[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"langidEdit"];
            
            [[NSUserDefaults standardUserDefaults]setInteger:[langid integerValue] forKey:@"languageIDMain"];
            
            [self saveTokenWS];
        }
        

    }
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"Dlanguage"])
    {
      NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"Dlanguage"];
        
        [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"language"];
    }
    
      if([[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"])
         [self saveGuestTokenWS];
    
    if(isFromAppdelegate)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"firstInstallLANG" forKey:@"firstInstallLANG"];
    }
    
    homeViewController *homeVC = [[homeViewController alloc]initWithNibName:@"homeViewController" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:homeVC];
    homeVC.isFrmLangScreen = YES;
    navigationController.navigationBarHidden=YES;
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
///////
    
    //commented by sarath
 /*   loginViewController *lgVC = [[loginViewController alloc]init];
    
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:lgVC];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
    
    navigationController.navigationBarHidden=YES;
  */
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return lnArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIFont *font = [UIFont systemFontOfSize:16.0];
    
    NSString *title = [[lnArr objectAtIndex:indexPath.section]valueForKey:@"title"];
    NSString *description = [[lnArr objectAtIndex:indexPath.section]valueForKey:@"description"];
    
    NSString *combinedStr = [NSString stringWithFormat:@"%@ %@",title,description];
    
    CGRect textRect = [combinedStr boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    
    CGSize size = textRect.size;
    
    {
        if(IS_IPAD)
        {
            return 120 + size.height;
        }
        else
            return 90 + size.height;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView.tag == 10)
        return 10;
    else
        return 20;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier =@"l";
    
    lngTableViewCell *cell = (lngTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"lngTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
  // lnArr
    NSString *lang = [[lnArr objectAtIndex:section] valueForKey:@"reg_language"];
    NSString *language = [NSString stringWithFormat:@"(%@)",[[lnArr objectAtIndex:section] valueForKey:@"language"]];
    
    if([lang caseInsensitiveCompare:@"english"]==NSOrderedSame)
    {
        language = @"";
    }
    
    cell.lnLbl.text = [NSString stringWithFormat:@"%@ %@",lang,language];
    
    cell.radioBtn.tag = section;
    
    cell.cellRadioBtn.tag = section;
    
    [cell.cellRadioBtn addTarget:self action:@selector(radioBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.radioBtn addTarget:self action:@selector(radioBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    if([[arraySelectObject objectAtIndex:section] isEqual:@"1"])
    {
        
        [cell.radioBtn setSelected:YES];
    }
    else
    {
       [cell.radioBtn setSelected:NO];
    }
    
    if(!isFirst)
    {
      if(section == setRadioSelectedIndex)
      {
        [cell.radioBtn setSelected:YES];
      }
    }
    
    
    if(IS_IPAD)
        cell.bg.layer.cornerRadius = 5.0;
    else
        cell.bg.layer.cornerRadius = 5.0;
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return;
}


# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
