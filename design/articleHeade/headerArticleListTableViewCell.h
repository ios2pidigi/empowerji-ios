//
//  moTableViewCell.h
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface headerArticleListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *cellTitle;
@property (strong, nonatomic) IBOutlet UIImageView *cellImg;
@end
