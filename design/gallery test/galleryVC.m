//
//  moListViewController.m
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "galleryVC.h"
#import "detailViewController.h"
#import "Constants.h"
#import "moTableViewCell.h"
#import "UIImageView+JMImageCache.h"
#import "searchViewController.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "galleryListTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "galleryCollectionViewCell.h"
#import "NHBalancedFlowLayout.h"

#import "popUpVideoViewController.h"
#import "loginViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "UIButton+WebCache.h"

#import "homeViewController.h"

@interface galleryVC ()<UICollectionViewDataSource,NHBalancedFlowLayoutDelegate>
{
    NSArray *mArray;
    
    NSMutableArray *gArray;
    
    CGRect frameCollectVw;
    
    CGRect imgVwFrame;
    
    BOOL once;
    
}
@end

@implementation galleryVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    once = NO;
    
    _imageDetailw.frame = [UIScreen mainScreen].bounds;
    
    
    gArray = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
        gArray = [plistDict objectForKey:@"galleryBookmark"];
        
        if ([gArray count] == 0)
        {
            
        }
    }
    
    
    
    NSString *admobId,*adStatus;
    
    NSString *headerLblText = [[NSUserDefaults standardUserDefaults]objectForKey:@"listHeader"];
    
    _headerLbl.text = [NSString stringWithFormat:@"%@",headerLblText];
    
    NSString *subHeaderText = [[NSUserDefaults standardUserDefaults]objectForKey:@"subCatName"];
    
    _subHeaderLbl.text = [NSString stringWithFormat:@"%@ - %@",subHeaderText,headerLblText];
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
     adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus caseInsensitiveCompare:@"Yes"]==NSOrderedSame)
    [self.adVw loadRequest:[GADRequest request]];
    
    
    mArray = [[NSArray alloc]init];
    
    _collectionVwGallery.dataSource= self;
    _collectionVwGallery.delegate = self;
    
    if(_isFrmBkmd)
    {
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            mArray = [plistDict objectForKey:@"rateArray"];
        }
        
        [_collectionVwGallery reloadData];
    }
    else
    [self listMWebservice];
    
    [_btnClose addTarget:self action:@selector(closeDetail) forControlEvents:UIControlEventTouchUpInside];
    
    [_helpBtn addTarget:self action:@selector(helpAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Gallery Detail Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    [_bgImg addGestureRecognizer:singleTap];

}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    NSLog(@"image clicked");
    
     [_imageDetailw removeFromSuperview];
}

-(void)viewWillAppear:(BOOL)animated
{
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        mArray = [[NSArray alloc]init];
        [_collectionVwGallery reloadData];
    }
    else
    {
        if(_isFrmBkmd)
        {
            mArray = [[NSArray alloc]init];
            
            NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            NSFileManager * fileManager = [NSFileManager defaultManager];
            //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                mArray = [plistDict objectForKey:@"rateArray"];
            }
            
            [_collectionVwGallery reloadData];
            
        }
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    imgVwFrame = _detailImgVw.frame;
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
        
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
        UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
       
    }
    //_detailImgVw.image=iv.image;
    
   // if(once)
   // _detailImgVw.frame = CGRectMake(0, 0, imgWidth, heightImage);
   // _detailImgVw.image=iv.image;
    
    //_detailImgVw.center = self.view.center;
}

# pragma mark WebService Calls

-(void)listMWebservice
{
   // [SVProgressHUD show];
    
    // categories.php?cat_id=
    NSString *cat_id;
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    cat_id =  [[NSUserDefaults standardUserDefaults]objectForKey:@"galleryVCId"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@gallery.php?cat_id=%@&lang_id=%ld",baseUrl,cat_id,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self listMWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            mArray = [json objectForKey:@"result"];
            
            if([mArray count]>0)
            {
                if([[mArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    mArray = [[NSArray alloc]init];
                    
                    [_collectionVwGallery reloadData];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
                
                
                
                 ////////
                 NSMutableArray *arrRate = [[NSMutableArray alloc]init];
                 NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                 NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                 NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                 NSFileManager * fileManager = [NSFileManager defaultManager];
                 //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                 if([fileManager fileExistsAtPath:finalPath])
                 {
                 NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                 arrRate = [plistDict objectForKey:@"moreArray"];
                 
                 if ([arrRate count] == 0)
                 {
                 arrRate = [NSMutableArray new];
                 
                 for(NSDictionary *dict in mArray)
                 {
                 NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                 
                 NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                 [dictToSave removeObjectsForKeys:keysForNullValues];
                 
                 [arrRate addObject:dictToSave];
                 }
                 NSLog(@"arrRate :%@",arrRate);

                 [plistDict setValue:arrRate forKey:@"moreArray"];
                 [plistDict writeToFile:finalPath atomically:YES];
                 
                 }
                 else
                 {
                 arrRate = [NSMutableArray new];
                 
                 for(NSDictionary *dict in mArray)
                 {
                 NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                 
                 NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                 [dictToSave removeObjectsForKeys:keysForNullValues];
                 
                 [arrRate addObject:dictToSave];
                 }
                 NSLog(@"arrRate :%@",arrRate);
                 
                 [plistDict setValue:arrRate forKey:@"moreArray"];
                 [plistDict writeToFile:finalPath atomically:YES];
                 }
                 }
                 
                 
                 ///////
                 
                
                
            }
            
            
            [_collectionVwGallery reloadData];
            
            CGRect frameC = _collectionVwGallery.frame;
            frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
            _collectionVwGallery.frame = frameC;
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)listReloadWS
{
    // [SVProgressHUD show];
    
    // categories.php?cat_id=
    NSString *cat_id;
    
    cat_id =  [[NSUserDefaults standardUserDefaults]objectForKey:@"galleryVCId"];
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@gallery.php?cat_id=%@&lang_id=%ld",baseUrl,cat_id,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self listReloadWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            mArray = [json objectForKey:@"result"];
            
            if([mArray count]>0)
            {
                if([[mArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    mArray = [[NSArray alloc]init];
                    
                    [_collectionVwGallery reloadData];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
                
                
                
                ////////
                NSMutableArray *arrRate = [[NSMutableArray alloc]init];
                NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                NSFileManager * fileManager = [NSFileManager defaultManager];
                //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                if([fileManager fileExistsAtPath:finalPath])
                {
                    NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                    arrRate = [plistDict objectForKey:@"moreArray"];
                    
                    if ([arrRate count] == 0)
                    {
                        arrRate = [NSMutableArray new];
                        
                        for(NSDictionary *dict in mArray)
                        {
                            NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                            
                            NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                            [dictToSave removeObjectsForKeys:keysForNullValues];
                            
                            [arrRate addObject:dictToSave];
                        }
                        NSLog(@"arrRate :%@",arrRate);
                        
                        [plistDict setValue:arrRate forKey:@"moreArray"];
                        [plistDict writeToFile:finalPath atomically:YES];
                        
                    }
                    else
                    {
                        arrRate = [NSMutableArray new];
                        
                        for(NSDictionary *dict in mArray)
                        {
                            NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                            
                            NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                            [dictToSave removeObjectsForKeys:keysForNullValues];
                            
                            [arrRate addObject:dictToSave];
                        }
                        NSLog(@"arrRate :%@",arrRate);
                        
                        [plistDict setValue:arrRate forKey:@"moreArray"];
                        [plistDict writeToFile:finalPath atomically:YES];
                    }
                }
                
                
                ///////
                
                
                
            }
            
            
            [_collectionVwGallery reloadData];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)favWS:(NSString*) gid :(NSString*) isSel
{
    //add_likes.php?id=4
    
   // action=yes/no
    
    NSString *wUrl=[NSString stringWithFormat:@"%@add_likes.php?id=%@&action=%@",baseUrl,gid,isSel];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self favWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //[self listMWebservice];
            
            [self listReloadWS];
            
        });
    }];
    
    [dataTask resume];
}

#pragma mark UICollectionView methods

- (CGSize)itemSize
{
    NSInteger numberOfColumns = 2;
    
   // CGFloat itemWidth = (CGRectGetWidth(self.collectionVw.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(150, 150);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return [mArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"g";
    
    UINib *nib = [UINib nibWithNibName:@"galleryCollectionViewCell" bundle: nil];
    
    [collectionView registerNib:nib forCellWithReuseIdentifier:simpleTableIdentifier];
    
    NSInteger section = indexPath.row;
    
    galleryCollectionViewCell *cell = (galleryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    
    [cell.imageThumb sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"thumb"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    cell.btnFav.tag = section;
    
    [cell.btnFav addTarget:self action:@selector(favAction:) forControlEvents:UIControlEventTouchUpInside];
    //ico-star-selected
    
    NSString *countLbl = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.row]valueForKey:@"likes"]];
    cell.countLbl.text = countLbl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        
    if([gArray containsObject:[[mArray objectAtIndex:indexPath.row]valueForKey:@"id"]])
    {
        [cell.btnFav setImage:[UIImage imageNamed:@"ico-star-selected"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnFav setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
    }
    }
    else
    {
      [cell.btnFav setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
    }
    
    cell.btnFav.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4,4);
    
    [cell layoutSubviews];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    once = YES;
    _detailImgVw.frame = imgVwFrame;
    
  // [_detailImgVw sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.row]valueForKey:@"image"]]] placeholderImage:nil];
    
    [_detailImgVw sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.row]valueForKey:@"image"]]]
                      placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]
                               options:SDWebImageRefreshCached //SDWebImageProgressiveDownload
                            progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL)
                         {
                                
                            }
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                        {
                            _detailImgVw.image = image;
                            
                            UIImageView *iv = _detailImgVw;
                            
                            float imgWidth = _detailImgVw.frame.size.width;
                            float heightImage = ((iv.image.size.height*imgWidth)/iv.image.size.width) ;//- ( _detailLbl.frame.size.height + _detailTitleLbl.frame.size.height +60);
                            
                            if(heightImage > [UIScreen mainScreen].bounds.size.height - (_closeBtn.frame.size.height + _detailLbl.frame.size.height + _detailTitleLbl.frame.size.height))
                            {
                                if(IS_IPAD)
                                {
                                    _detailImgVw.contentMode = UIViewContentModeScaleAspectFill;
                                    
                                    imgWidth = _detailImgVw.frame.size.width - 250;
                                    
                                    heightImage =[UIScreen mainScreen].bounds.size.height - (_closeBtn.frame.size.height + _detailLbl.frame.size.height + _detailTitleLbl.frame.size.height);
                                }
                                else
                               
                                {
                                imgWidth = _detailImgVw.frame.size.width - 60;
                                
                                heightImage =((iv.image.size.height*imgWidth)/iv.image.size.width);
                                }
                            }
                                
                            _detailImgVw.image=iv.image;
                            
                            _detailImgVw.frame = CGRectMake(0, 0, imgWidth, heightImage);
                            
                            _detailImgVw.center = self.view.center;
                            
                           /* if(IS_IPAD)
                            {
                                if(_detailImgVw.frame.size.height > [UIScreen mainScreen].bounds.size.height)
                                {
                                    CGRect frameHI = _detailImgVw.frame;
                                    frameHI.origin.y = _closeBtn.frame.size.height;
                                    frameHI.size.height = [UIScreen mainScreen].bounds.size.height - 150;
                                    
                                    _detailImgVw.frame = frameHI;
                                }
                            }
                            */
                            
                            CGRect frameI = _closeImg.frame ;
                            frameI.origin.x = (_detailImgVw.frame.origin.x + _detailImgVw.frame.size.width) - frameI.size.width;
                            frameI.origin.y = _detailImgVw.frame.origin.y - frameI.size.height;
                            _closeImg.frame = frameI;
                            
                            
                            CGRect frameB = _closeBtn.frame ;
                            frameB.origin.x = (_detailImgVw.frame.origin.x + _detailImgVw.frame.size.width) - frameB.size.width;
                            frameB.origin.y = _detailImgVw.frame.origin.y - frameB.size.height;
                            _closeBtn.frame = frameB;
                            
                            CGRect fr2 = _detailFav.frame;
                            fr2.origin.x = (_detailImgVw.frame.origin.x + _detailImgVw.frame.size.width) - (fr2.size.width+5);
                            fr2.origin.y = (_detailImgVw.frame.origin.y + _detailImgVw.frame.size.height) - (fr2.size.height + 5);
                            _detailFav.frame = fr2;
                            
                            CGRect fr1 = _likesLbl.frame;
                            fr1.origin.x = (_detailImgVw.frame.origin.x + _detailImgVw.frame.size.width) - (fr1.size.width + fr2.size.width + 5);
                            fr1.origin.y = (_detailImgVw.frame.origin.y + _detailImgVw.frame.size.height) - (fr1.size.height + 5);
                            _likesLbl.frame = fr1;
                            
                            
                            CGRect frameLbl1 = _detailTitleLbl.frame;
                            frameLbl1.origin.y = _detailImgVw.frame.origin.y + _detailImgVw.frame.size.height + 10;
                            _detailTitleLbl.frame = frameLbl1;
                            
                            CGRect frameLbl2 = _detailLbl.frame;
                            frameLbl2.origin.y = _detailImgVw.frame.origin.y + _detailImgVw.frame.size.height + frameLbl1.size.height + 10;
                            _detailLbl.frame = frameLbl2;
                            
                            if((frameLbl2.origin.y+ frameLbl2.size.height) > [UIScreen mainScreen].bounds.size.height)
                            {
                                CGRect frameLbl1 = _detailTitleLbl.frame;
                                
                                CGRect frameLbl2 = _detailLbl.frame;
                                
                                frameLbl1.size.height = 25;
                                frameLbl2.size.height = 25;
                                
                                frameLbl1.origin.y = [UIScreen mainScreen].bounds.size.height - (frameLbl1.size.height + frameLbl2.size.height +10);
                                _detailTitleLbl.frame = frameLbl1;
                                
                                
                                frameLbl2.origin.y = [UIScreen mainScreen].bounds.size.height - (frameLbl1.size.height + 10);
                                _detailLbl.frame = frameLbl2;
                            }
                            
                            
                            
                             _detailImgVw.layer.borderColor = [UIColor whiteColor].CGColor;
                              _detailImgVw.layer.borderWidth = 1.0;
                            _detailImgVw.clipsToBounds = YES;
                            
                            if(IS_IPAD)
                                _detailImgVw.layer.cornerRadius = 10.0;
                            else
                                _detailImgVw.layer.cornerRadius = 5.0;
                            
                           }];
    
    NSString *user = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.row]valueForKey:@"user"]];
    
    NSString *title = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.row]valueForKey:@"title"]];
    
    NSString *lang = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    NSString *submitted = [languageBundle localizedStringForKey:@"submitted" value:@"" table:@"strings-english"];
    
    _detailLbl.text = [NSString stringWithFormat:@"%@ : %@",submitted,user];
    
    _detailTitleLbl.text = [NSString stringWithFormat:@"%@",title];
    
    NSString *countLbl = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.row]valueForKey:@"likes"]];
    
    _likesLbl.text = countLbl;
    
    _detailFav.tag = indexPath.row;
    
    NSString *imgId = [[mArray objectAtIndex:indexPath.row]valueForKey:@"id"];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
      [_detailFav setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
    }
    else
    {
    if([gArray containsObject:imgId])
    {
      [_detailFav setImage:[UIImage imageNamed:@"ico-star-selected"] forState:UIControlStateNormal];
    }
    else
        [_detailFav setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
    
    }
    
    [_detailFav addTarget:self action:@selector(favActionDetail:) forControlEvents:UIControlEventTouchUpInside];
    
   // _detailImgVw.layer.borderColor = [UIColor whiteColor].CGColor;
  //  _detailImgVw.layer.borderWidth = 1.0;
    _detailImgVw.clipsToBounds = YES;
    
    if(IS_IPAD)
    _detailImgVw.layer.cornerRadius = 10.0;
    else
    _detailImgVw.layer.cornerRadius = 5.0;
    
    
    
 /*   if(_detailImgVw.frame.size.height > _detailImgVw.frame.size.width)
    {
      // CGRect framImg = _detailImgVw.frame ;
      //  framImg.size.height = [UIScreen mainScreen].bounds.size.height;
      //  framImg.size.width = [UIScreen mainScreen].bounds.size.width;
      //  _detailImgVw.frame = framImg;
        
        _detailImgVw.image = [self imageWithImage:_detailImgVw.image scaledToSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        
        _detailImgVw.contentMode = UIViewContentModeScaleAspectFill;
    }
    else
        _detailImgVw.contentMode = UIViewContentModeScaleAspectFit;
    
    */
    
    NSLog(@"imageFrame :%@",NSStringFromCGRect(_detailImgVw.frame));
    
    [self.view addSubview:_imageDetailw];
    
    
    return;
    
    NSString *catid =[[mArray objectAtIndex:indexPath.row]valueForKey:@"id"];
    
    [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidFromHomeCat"];
    
    NSString *catName =[[mArray objectAtIndex:indexPath.row]valueForKey:@"name"];
    
    [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"catName"];
    
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 10.0;
    }
    else
        return 10.0; //0.01;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 40.0;
    }
    else
        return 15.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 3.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    
    CGSize size ;
    
   
    {
        if(IS_IPAD)
        {
            size = CGSizeMake(cellWidth -20, cellWidth -20);//cellWidth-84
        }
        else
            size = CGSizeMake(cellWidth-25, cellWidth-25); //162
    }
    
    
    return size;
}


- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return UIEdgeInsetsMake(0, 10, 0, 10);
    }
    else
        return UIEdgeInsetsMake(0, 15, 0, 15); // top, left, bottom, right
}


#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return mArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 250;
    }
    else
        return 200;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSString *descptn;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
    descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
    
    descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    
    UIFont *font = [UIFont systemFontOfSize:16.0];
    
    CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    
    CGSize size = textRect.size;
    
    if(section == 0)
        return 0;//size.height;
    else
    return 20;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        NSString *descptn;
        NSInteger height;
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
            descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
        
        descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        
        UIFont *font = [UIFont systemFontOfSize:16.0];
        
        CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        
        CGSize size = textRect.size;
        
            height = size.height;
        
        UILabel *headrLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, _mTblVw.frame.size.width-10, height)];
        headrLbl.text = descptn;
        headrLbl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
        headrLbl.textAlignment = NSTextAlignmentLeft;
        //headrLbl.backgroundColor = pinkLightColorBG;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        view.backgroundColor = [UIColor whiteColor];
        [view addSubview:headrLbl];
       
        return view ;
    }
    else
    {
      UIView *v = [UIView new];
      [v setBackgroundColor:[UIColor clearColor]];
      return v;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier =@"gl";
    
    galleryListTableViewCell *cell = (galleryListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"galleryListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.titleLbl.text = [[mArray objectAtIndex:section]valueForKey:@"name"];
    
    cell.descptnLbl.text = [[mArray objectAtIndex:section]valueForKey:@"description"];
    
    [cell.descptnLbl sizeToFit];
    
    //cell.cellImg = [[mArray objectAtIndex:section]valueForKey:@"image"];
    [cell.imageThumb sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
     NSString *idval = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"id"]];
    
    [[NSUserDefaults standardUserDefaults]setInteger:indexPath.section forKey:@"moreArrayId"];
    
    detailViewController *dVC = [[detailViewController alloc]init];
    [dVC getDictionaryDetailid:idval];
    if(_isFrmBkmd)
    {
        dVC.isfrmBkMore = YES;
    }
    else
    dVC.isFrmMore=YES;
    [self.navigationController pushViewController:dVC animated:YES];
}

# pragma mark Button Actions

- (IBAction)btnSearchActn:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)homeAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)favAction:(UIButton*)sender
{
    //[sender setImage:[UIImage imageNamed:@"ico-star-selected"] forState:UIControlStateNormal];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                [rvc presentViewController: lgVC animated: NO completion:nil];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        //[vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [rvc presentViewController: alert animated: NO completion:nil];
    }
    else
    {
    
    NSString *gid = [[mArray objectAtIndex:sender.tag]valueForKey:@"id"];
    
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    NSMutableDictionary *plistDict;
    if([fileManager fileExistsAtPath:finalPath])
    {
       plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"galleryBookmark"];
        
        if ([arrRate count] == 0)
        {
            arrRate = [NSMutableArray new];
            
            
            [arrRate addObject:gid];
        }
        else
        {
            if([arrRate containsObject:gid])
            {
                //[arrRate removeObjectIdenticalTo:gid];
                [arrRate removeObject: gid];
            }
            else
             [arrRate addObject:gid];
        }
    }
    [plistDict setValue:arrRate forKey:@"galleryBookmark"];
    [plistDict writeToFile:finalPath atomically:YES];
   
    
    gArray = [[NSMutableArray alloc]init];
    
    NSString *isSeleted;
    
    if([sender.currentImage isEqual:[UIImage imageNamed:@"ico-star-selected"]])
    {
        isSeleted = @"no";
       // [sender setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
        
    }
    else
    {
        isSeleted = @"yes";
        //[sender setImage:[UIImage imageNamed:@"ico-star-selected"] forState:UIControlStateNormal];
    }
    
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
        gArray = [plistDict objectForKey:@"galleryBookmark"];
       
    }
    
     [_collectionVwGallery reloadData];
    
    [self favWS:gid :isSeleted];
        
    }
}

-(void)favActionDetail:(UIButton*)sender
{
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                [rvc presentViewController: lgVC animated: NO completion:nil];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        //[vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [rvc presentViewController: alert animated: NO completion:nil];
    }
    else
    {
        
    NSString *gid = [[mArray objectAtIndex:sender.tag]valueForKey:@"id"];
    
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    NSMutableDictionary *plistDict;
    if([fileManager fileExistsAtPath:finalPath])
    {
        plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"galleryBookmark"];
        
        if ([arrRate count] == 0)
        {
            arrRate = [NSMutableArray new];
            
            
            [arrRate addObject:gid];
        }
        else
        {
            if([arrRate containsObject:gid])
            {
                [arrRate removeObject: gid];
            }
            else
                [arrRate addObject:gid];
        }
    }
    [plistDict setValue:arrRate forKey:@"galleryBookmark"];
    [plistDict writeToFile:finalPath atomically:YES];
    
    
    gArray = [[NSMutableArray alloc]init];
    
    NSString *isSeleted;
    
    if([sender.currentImage isEqual:[UIImage imageNamed:@"ico-star-selected"]])
    {
        isSeleted = @"no";
         [sender setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
        
        NSInteger count = [_likesLbl.text integerValue];
        if(count != 0)
        count -- ;
        _likesLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
    }
    else
    {
        isSeleted = @"yes";
        [sender setImage:[UIImage imageNamed:@"ico-star-selected"] forState:UIControlStateNormal];
        
        NSInteger count = [_likesLbl.text integerValue] ;
        count ++ ;
        _likesLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
    }
    
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
        gArray = [plistDict objectForKey:@"galleryBookmark"];
        
    }
    
    [_collectionVwGallery reloadData];
    
    [self favWS:gid :isSeleted];
        
    }
}

-(void)closeDetail
{
    [_imageDetailw removeFromSuperview];
}

-(void)helpAction
{
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
  /*  NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
   */
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
    
}

@end
