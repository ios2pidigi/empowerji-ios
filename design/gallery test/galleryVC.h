//
//  moListViewController.h
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface galleryVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UITableView *mTblVw;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionVwGallery;
@property (strong, nonatomic) IBOutlet UIView *imageDetailw;
@property (strong, nonatomic) IBOutlet UIImageView *detailImgVw;
@property (strong, nonatomic) IBOutlet UILabel *detailLbl;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UILabel *subHeaderLbl;
@property (strong, nonatomic) IBOutlet UIButton *detailFav;
@property (strong, nonatomic) IBOutlet UILabel *likesLbl;
@property (strong, nonatomic) IBOutlet UIButton *helpBtn;
@property (strong, nonatomic) IBOutlet UIImageView *helpImg;
@property (strong, nonatomic) IBOutlet UILabel *helpLbl;

@property (strong, nonatomic) IBOutlet UIImageView *closeImg;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bgImg;


@property (strong, nonatomic) IBOutlet UILabel *detailTitleLbl;

@property(nonatomic) BOOL isFrmBkmd;

@end
