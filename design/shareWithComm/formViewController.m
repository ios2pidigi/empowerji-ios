//
//  homeViewController.m
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "formViewController.h"
#import "homeCollectionViewCell.h"
#import "sideMenuViewController.h"
#import "Constants.h"
#import "categoryViewController.h"
#import "SVProgressHUD.h"
#import "searchViewController.h"
#import "listViewController.h"
#import "myFeedViewController.h"
#import "sendMessageViewController.h"
#import "supportViewController.h"
#import "connectivity.h"
#import "UIView+Toast.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "UIImageView+WebCache.h"
#import "moListViewController.h"
#import "galleryListingViewController.h"

#import "homeCategoryViewController.h"
#import "articleListViewController.h"
#import "ASIFormDataRequest.h"
#import "FCFileManager.h"

#import "popUpVideoViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "UIButton+WebCache.h"

#import "homeViewController.h"

@import GoogleMobileAds;

@interface formViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentMenuDelegate,UIDocumentPickerDelegate>
{
   // NSArray *homeArray;
    NSArray *subCatCheckArr;
    
    NSMutableArray *catArr;
    
    NSMutableArray *catArr2;
    
    AVAudioPlayer *_audioPlayer;
    
    NSString *filetype;
    NSString *filename;
    NSMutableArray *data;
    NSData *docData;
    NSString *docName;
    NSString *imageName;
    
    NSString *imageFileName;
    
    BOOL once;
    CGRect frameFields;
    
    CGRect catTfFrame;
    CGRect despTfFrame;
    CGRect commentsFrame;
    CGRect catTblFrame;
    CGRect btnSelCatFrame;
    
    CGRect langTfFrame;
    CGRect langTblFrame;
    CGRect langSelFrame;
    
    UIImage * img;
    
    NSArray *langArr;
}
@end

@implementation formViewController
@synthesize isFrmCommunity;

- (void)viewDidLoad {
    [super viewDidLoad];
    //230, 176, 80a
    
    once = YES;
    
    _popUpVw.frame = [UIScreen mainScreen].bounds;
    
    subCatCheckArr = [[NSArray alloc]init];
    
    [SVProgressHUD setDefaultStyle:(SVProgressHUDStyleDark)];
    [SVProgressHUD setDefaultMaskType:(SVProgressHUDMaskTypeCustom)];
    
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    
    [SVProgressHUD setMaximumDismissTimeInterval:10.0];
    [SVProgressHUD setMinimumDismissTimeInterval:3];
    
   // [self settingsWebservice];
    
    [self categoryWebService];
    
    //[self subCategoryCheckWebService];
    
   if([[NSUserDefaults standardUserDefaults]objectForKey:@"catName"])
   _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
    else
    _headerLbl.text = @"";
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
    }
    else
    {
        [self staticPagesWS];
    }
        
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];

//    https://app.empowerji.com/webservice/static_pages.php
    
    [_playBtn addTarget:self action:@selector(playAudio) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnBack addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    _searchBarTF.layer.borderColor = [UIColor whiteColor].CGColor;
    _searchBarTF.layer.borderWidth = 1.0;
    
    _btnshareWithComm.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnshareWithComm.layer.borderWidth = 1.0;
    _btnshareWithComm.layer.cornerRadius = 10.0;
    
    _btnSubmit.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnSubmit.layer.borderWidth = 1.0;
    _btnSubmit.layer.cornerRadius = 10.0;
    
    _uploadFileTF.layer.borderColor = [UIColor whiteColor].CGColor;
    _uploadFileTF.layer.borderWidth = 1.0;
   
    
    [_radioBtn1 setSelected:YES];
    
    _radioBtn1.tag = 0;
    _radioBtn2.tag = 1;
    
    [_radioBtn1 addTarget:self action:@selector(radioButtonSelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_radioBtn2 addTarget:self action:@selector(radioButtonSelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnOk addTarget:self action:@selector(closePopUpVw) forControlEvents:UIControlEventTouchUpInside];
    
   
    
    
    NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    NSString *commentsPlaceHolder = [languageBundle localizedStringForKey:@"commentsPlaceholder" value:@"" table:@"strings-english"];
    
    _commentsTxtVw.text = commentsPlaceHolder;
    
    _commentsTxtVw.textColor = [UIColor lightGrayColor];
    
    [_btnSubmit addTarget:self action:@selector(submitAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSelCat addTarget:self action:@selector(selectCategoty) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSelLang addTarget:self action:@selector(selectLanguage) forControlEvents:UIControlEventTouchUpInside];
    
    [_helpBtn addTarget:self action:@selector(helpAction) forControlEvents:UIControlEventTouchUpInside];
    
    //
   // UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didRecognizeTapGesture:)];
   // [self.titleTF.superview addGestureRecognizer:tapGesture];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Form Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    
    [_homeBtn addTarget:self action:@selector(homeActn:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) didRecognizeTapGesture:(UITapGestureRecognizer*) gesture {
    CGPoint point = [gesture locationInView:gesture.view];
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        if (CGRectContainsPoint(self.titleTF.frame, point))
        {
           // [_titleTF becomeFirstResponder];
        }
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
   // gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor blackColor].CGColor];
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    _supportBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    //Rgb2UIColor(0, 93, 155);
    _supportBtn.layer.borderWidth= 2.0f;
    
    _keepGoingBtn.layer.borderColor=[UIColor whiteColor].CGColor;//Rgb2UIColor(0, 93, 155);
    _keepGoingBtn.layer.borderWidth= 2.0f;
    
    
    _uploadBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    _uploadBtn.layer.borderWidth= 1.0f;
    
    int i =0;
    if(IS_IPAD)
    {
        i = 50;
    }
    else
        i = 30;
    
    CGRect frame = _categoryTblVw.frame;
    
    if(_radioBtn1.isSelected)
        frame.size.height = i * catArr.count;
    else
        frame.size.height = i * catArr2.count;
    
    _categoryTblVw.frame = frame;
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
       // _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
      //  _helpBtn.frame = frameB;
        
      //  [self.helpLbl setCenter:_helpBtn.center];
        
        
       // [self.adVw loadRequest:[GADRequest request]];
        
        CGRect frameBtmVw = _bottomView.frame;
        frameBtmVw.size.height = frameBtmVw.size.height - _helpBtn.frame.size.height;
        //_bottomView.frame = frameBtmVw;
        
        
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
        UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
        if(!once)
        {
            once = YES;
            CGRect frameC = _collectionVw.frame;
            frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
            _collectionVw.frame = frameC;
        }
    }
    
    NSString *lang = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    NSString *poptext = [languageBundle localizedStringForKey:@"popUpText" value:@"" table:@"strings-english"];
    
    
    UIFont *font = _lblPopUp.font;
    
    CGRect textRect = [poptext boundingRectWithSize:CGSizeMake(_lblPopUp.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    
    CGSize size = textRect.size;
    
    CGRect framP =  _scrollPopVw.frame;
    framP.size.height = size.height + _btnOk.frame.size.height +120;
    _scrollPopVw.frame = framP;
    
    self.scrollPopVw.center = self.view.superview.center;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if(once)
    {
        frameFields = _fieldViews.frame;
        
        catTfFrame = _catTF.frame;
        despTfFrame = _descTF.frame;
        commentsFrame = _commentsTxtVw.frame;
        catTblFrame = _categoryTblVw.frame;
        btnSelCatFrame = _btnSelCat.frame;
        
        langTfFrame = _langTF.frame;
        langTblFrame = _langTblVw.frame;
        langSelFrame = _btnSelLang.frame;
        
      [self.view addSubview:_popUpVw];
        once = NO;
    }
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
    
    UITouch *touch = [touches anyObject];
    if(![touch.view isKindOfClass:[UITextField class]])
    {
        [self.view endEditing:YES];
    }
}

- (void)hideKeyboard:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    NSLog(@"%ld", (long)view.tag);
    
    [self.view endEditing:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark WebService Calls

-(void)settingsWebservice
{
  
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid =@"";
    }
    else
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@",baseUrl,(long)langid,userid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self settingsWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
                [def synchronize];
            
            NSString *admobId; NSString *adStatus;
            admobId = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"admob_id"];
            
             adStatus = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"admob_status"];
            
            //[GADMobileAds configureWithApplicationID:admobId];
            
            _adVw.adSize = kGADAdSizeSmartBannerPortrait;
            self.adVw.adUnitID = admobId;//@"ca-app-pub-8214158784518238/4464482105";
            self.adVw.rootViewController = self;
            
            if([adStatus isEqualToString:@"Yes"])
            {
                CGRect frameH = _helpImg.frame;
                frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
                _helpImg.frame = frameH;
                
                CGRect frameB = _helpBtn.frame;
                frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
                _helpBtn.frame = frameB;
                
                [self.helpLbl setCenter:_helpBtn.center];
                
                
                [self.adVw loadRequest:[GADRequest request]];
            }
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)categoryWebService
{
    //http://listlinkz.com/ssc/webservice/categories.php?cat_id=4
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@community_categories.php?&lang_id=%ld",baseUrl,(long)langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
   {
       NSDictionary *json;
       
       if(data ==nil)
       {
           [self categoryWebService];
           return ;
       }
       
       json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
       
       NSLog(@"json :%@", json);
       
       dispatch_async(dispatch_get_main_queue(), ^{
           
           catArr = [[NSMutableArray alloc]init]; catArr2 = [[NSMutableArray alloc]init];
           langArr = [[NSArray alloc]init];
           
           catArr = [json valueForKey:@"read_category"];
           
           catArr = [catArr mutableCopy];
           
           
           NSString *lang = langGlobal;
           NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
           NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
           NSString *selCat = [languageBundle localizedStringForKey:@"selCat" value:@"" table:@"strings-english"];
           
           
           NSDictionary *dict = @{ @"id" : @"0", @"name" : selCat};
           [catArr insertObject:dict atIndex:0];
           
           catArr2 = [json valueForKey:@"pic_category"];
           
           catArr2 = [catArr2 mutableCopy];
           
           NSDictionary *dict2 = @{ @"id" : @"0", @"name" : selCat};
           [catArr2 insertObject:dict2 atIndex:0];
           
           langArr = [json valueForKey:@"languages"];
           
           [_categoryTblVw reloadData];
           
            [_langTblVw reloadData];
           
           NSString *catname;
           if(_radioBtn1.isSelected)
               catname = [[catArr objectAtIndex:0]valueForKey:@"name"];
           else
               catname = [[catArr2 objectAtIndex:0]valueForKey:@"name"];
           
           _catTF.text = catname;
           
       });
   }];
    
    [dataTask resume];
}

-(void)subCategoryCheckWebService
{
  //http://listlinkz.com/ssc/webservice/check_sub_exists.php
    
    NSString *wUrl=[NSString stringWithFormat:@"%@check_sub_exists.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self subCategoryCheckWebService];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            subCatCheckArr = [[json objectForKey:@"result"]objectAtIndex:0];
            
           //result
            
        });
    }];
    
    [dataTask resume];
    
    
}

-(void)staticPagesWS
{
    NSString *wUrl=[NSString stringWithFormat:@"%@static_pages.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self staticPagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
          //  NSArray *staticArr = [[json objectForKey:@"result"]objectAtIndex:0];
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"result"]] forKey:@"staticArrContents"];
            [def synchronize];
            //result
            
          /*  NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            NSData *data = [def objectForKey:@"staticArrContents"];
            NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
           */
            
        });
    }];
    
    [dataTask resume];
}

/*
-(void)testUploadWS
{
     NSString *urlString = @"http://app.empowerji.com/webservice3/community_upload.php";
    
   
    //__weak
    ASIFormDataRequest *asiRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [asiRequest setRequestMethod:@"POST"];
    [asiRequest setTimeOutSeconds:10000];
    
    
    NSString *folderLinkId;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"1ParentFolderId"])
        folderLinkId = [[NSUserDefaults standardUserDefaults]objectForKey:@"1ParentFolderId"];
    
    
    [asiRequest addRequestHeader:[NSString stringWithFormat:@"multipart/form-data"] value:@"Content-Type"];
    [asiRequest setPostValue:[NSString stringWithFormat:@"uploadFile"] forKey:@"Type"];
    
    if(!filename || (filename == nil) || ([filename isEqualToString:@""]))
    {
        filename = @"picture.jpg";
    }
    if(!docName || (docName == nil) || ([docName isEqualToString:@""]))
    {
        docName = @"doc.doc";
    }
    
    if([filetype isEqualToString:@"Documents"])
    {
        [asiRequest addData:docData withFileName:@"doc.doc" andContentType:@"application/octet-stream" forKey:@"file"];
    }
    else
    {
        if(!img || img == nil)
        {
            [self.view makeToast:@"Please select image"
                        duration:1.5
                        position:CSToastPositionBottom];
            
            return;
        }
        
        NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
        NSString *base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
        [asiRequest setPostValue:[NSString stringWithFormat:@"%@",base64] forKey:@"file"];
    }
    
    //user_id=%@&comment=comment&title=title&cat_id=%@&file=%@
    
    [asiRequest setPostValue:@"153" forKey:@"user_id"];
    
    [asiRequest setPostValue:@"test" forKey:@"comment"];
    
    [asiRequest setPostValue:@"ttitle" forKey:@"title"];
    
    [asiRequest setPostValue:@"63" forKey:@"cat_id"];
   
    
    
    [asiRequest setCompletionBlock:^{
        
        [SVProgressHUD dismiss];
        
        NSError *error = [asiRequest error];
        if (!error) {
            NSString *response = [asiRequest responseString];
            
            NSLog(@" Response upload file = %@",response);
            
           
            
            [self.view makeToast:@"File added successfully"
                        duration:1.5
                        position:CSToastPositionBottom];
            
            
        }else {
            
            [self.view makeToast:@"Something went wrong"
                        duration:1.5
                        position:CSToastPositionBottom];
            
           
        }
    }];
    
    
    [asiRequest startAsynchronous];
}

-(void)testingWS
{
    {
         NSString *urlString = [NSString stringWithFormat:@"http://app.empowerji.com/webservice3/community_upload.php"];
        
        NSMutableString *stringPass = [[NSMutableString alloc] init];
        
        
        NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
        
        NSString *base64;
        
        if(_radioBtn2.isSelected)
        {
            if(!img || img == nil)
            {
                [self.view makeToast:@"Please select image"
                            duration:1.5
                            position:CSToastPositionBottom];
                
                return;
            }
            
        base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        }
        else
        base64 = [docData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
         NSString *catid = @"69";
        
        NSString *userid = @"153";
        
        //postParams = [NSString stringWithFormat:@"user_id=%@&comment=comment&title=title&cat_id=%@&file=%@&type=reads&extn=pdf",userid,catid,base64];
        
        
        
         [stringPass appendString:[NSString stringWithFormat:@"cat_id=%@",catid]];
        
        [stringPass appendString:[NSString stringWithFormat:@"&file=%@&type=reads&extn=pdf",base64]];
        
       [stringPass appendString:[NSString stringWithFormat:@"&user_id=%@&comment=comment&title=title&lang_id=5",userid]];
        
        NSData *postData = [stringPass dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",urlString]]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            if(data ==nil)
            {
                return ;
            }
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"json response : %@",json);
            
        }] resume];
        
    }
}

-(void)uploadImageWS
{
    
    NSString *urlString = [NSString stringWithFormat:@"http://app.empowerji.com/webservice3/community_upload.php"];
    
    NSMutableString *stringPass = [[NSMutableString alloc] init];
    
    
    NSString *base64 = [docData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString *catid = @"69";
    
    NSString *userid = @"153";
    
    //postParams = [NSString stringWithFormat:@"user_id=%@&comment=comment&title=title&cat_id=%@&file=%@&type=reads&extn=pdf",userid,catid,base64];
    
    [stringPass appendString:[NSString stringWithFormat:@"user_id=%@&comment=comment&title=title",userid]];
    
    [stringPass appendString:[NSString stringWithFormat:@"&cat_id=%@",catid]];
    
    [stringPass appendString:[NSString stringWithFormat:@"&file=%@&type=reads&extn=pdf",base64]];
    
    //create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    //Populate a dictionary with all the regular values you would like to send.
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:userid forKey:@"user_id"];
    
    [parameters setValue:@"comm" forKey:@"comment"];
    
    [parameters setValue:@"test" forKey:@"pic_title"];
    
     [parameters setValue:catid forKey:@"cat_id"];
    
    [parameters setValue:@"picture" forKey:@"type"];
    
    // add params (all params are strings)
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *FileParamConstant = @"file";
    
    NSData *imageData = UIImageJPEGRepresentation(img, 1);
    
    //Assuming data is not nil we add this to the multipart form
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:urlString]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        
        if ([httpResponse statusCode] == 200)
        {
            
            NSLog(@"success");
        }
        
                               
        
    }];
    
}

-(void)uploadImageASI
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [SVProgressHUD show];
    
   
    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
    
    NSString *str = [NSString stringWithFormat:@"http://app.empowerji.com/webservice3/community_upload.php"];
    
    if(img)
        [self uploadImage:str data:imageData];
}
-(void)uploadImage:(NSString*)urlString data:(NSData*)imageData
{
   
    NSString *base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    
    //__weak
    ASIFormDataRequest *asiRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [asiRequest setRequestMethod:@"POST"];
    [asiRequest setTimeOutSeconds:10000];
    
    NSString *catid = @"69";
    
    NSString *userid = @"153";
    
    [asiRequest addRequestHeader:[NSString stringWithFormat:@"multipart/form-data"] value:@"Content-Type"];
    //[asiRequest setPostValue:[NSString stringWithFormat:@"image_update"] forKey:@"action"];
    [asiRequest setPostValue:[NSString stringWithFormat:@"%@", catid] forKey:@"cat_id"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"%@", userid] forKey:@"user_id"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"commt"] forKey:@"comment"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"test"] forKey:@"pic_title"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"picture"] forKey:@"type"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"%@",base64] forKey:@"file"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"5"] forKey:@"lang_id"];
    
    NSLog(@"URL :PostReq:%@",asiRequest);
    
    [asiRequest setCompletionBlock:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
        });
        
        
        NSError *error = [asiRequest error];
        if (!error)
        {
            NSString *response = [asiRequest responseString];
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            NSLog(@" Response  = %@",response);
            
        }
        else
        {
            
            
        }
    }];
    
    
    [asiRequest startAsynchronous];
    
}

 */

-(void)multiPartUpload:(NSString *)url
{
   
    NSString *prefixString = @"Audio";
    
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
    NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@", prefixString, guid];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"_187934598797439873422234";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.14 (KHTML, like Gecko) Version/6.0.1 Safari/536.26.14" forHTTPHeaderField:@"User-Agent"];
    [request setValue:@"http://google.com" forHTTPHeaderField:@"Origin"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"audio\"; filename=\"%@.mp3\"\r\n", uniqueFileName] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:docData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"%@", returnString);
    
}

-(void)uploadFileTest
{
     NSString *urlString = [NSString stringWithFormat:@"%@community_upload.php",baseUrl];
    
    NSString *comment , *title ;
    
    NSString *catid =  [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
    
    NSString *extn;
    NSString *docPath = [FCFileManager pathForTemporaryDirectoryWithPath:docName];
    
    NSString *langid;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"readLangid"])
        langid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"readLangid"]];
    
    
    comment = [_commentsTxtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    title = [_descTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    catid = @"66";
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catidUploadFile"])
        catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidUploadFile"];
    
    extn = [docPath pathExtension];
    
    NSString *base64 = [docData base64EncodedStringWithOptions:0];
    
    if(!langid)
    {
        langid = @"5";
    }
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *userid  = [[retrievedDictionary objectAtIndex:0]valueForKey:@"user_id"];
    
  //  postParams = [NSString stringWithFormat:@"user_id=%@&comment=%@&title=%@&cat_id=%@&file=%@&type=reads&extn=%@&lang_id=%@",userid,comment,title,catid,base64,extn,langid];
    
    //__weak
    ASIFormDataRequest *asiRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [asiRequest setRequestMethod:@"POST"];
    [asiRequest setTimeOutSeconds:10000];
    
    [asiRequest addRequestHeader:[NSString stringWithFormat:@"multipart/form-data"] value:@"Content-Type"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"%@",userid] forKey:@"user_id"];
    
    [asiRequest setPostValue:comment forKey:@"comment"];
    [asiRequest setPostValue:title  forKey:@"title"];
    [asiRequest setPostValue:catid forKey:@"cat_id"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"%@",base64] forKey:@"file"];
    
    [asiRequest setPostValue:@"reads" forKey:@"type"];
    
    [asiRequest setPostValue:extn forKey:@"extn"];
    
    [asiRequest setPostValue:langid forKey:@"lang_id"];
    
    
    
    //create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    //Populate a dictionary with all the regular values you would like to send.
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    /*
     [asiRequest setPostValue:[NSString stringWithFormat:@"%@",userid] forKey:@"user_id"];
     [asiRequest setPostValue:comment forKey:@"comment"];
     [asiRequest setPostValue:title  forKey:@"title"];
     [asiRequest setPostValue:catid forKey:@"cat_id"];
     [asiRequest setPostValue:[NSString stringWithFormat:@"%@",base64] forKey:@"file"];
     [asiRequest setPostValue:@"reads" forKey:@"type"];
     [asiRequest setPostValue:extn forKey:@"extn"];
     [asiRequest setPostValue:langid forKey:@"lang_id"];
     */
    [parameters setValue:userid forKey:@"user_id"];
    
    [parameters setValue:comment forKey:@"comment"];
    
    [parameters setValue:title forKey:@"title"];
    
    [parameters setValue:catid forKey:@"cat_id"];
    
    [parameters setValue:[NSString stringWithFormat:@"%@",base64] forKey:@"file"];
    
    [parameters setValue:@"reads" forKey:@"type"];
    
    [parameters setValue:extn forKey:@"extn"];
    
    [parameters setValue:langid forKey:@"lang_id"];
    
    // add params (all params are strings)
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *FileParamConstant = @"file";
    
   // NSData *imageData = UIImageJPEGRepresentation(img, 1);
    
    //Assuming data is not nil we add this to the multipart form
    if (docData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:docData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the request
    [request setHTTPBody:body];
    
    // set URL
    [request setURL:[NSURL URLWithString:urlString]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
         
         if ([httpResponse statusCode] == 200)
         {
             
             NSLog(@"success");
         }
         
         dispatch_async(dispatch_get_main_queue(), ^{
             //[self.view makeToastActivity:CSToastPositionCenter];
             [SVProgressHUD dismiss];
         });
     }];
    
}

-(void)uploadFileWS
{
    
    [SVProgressHUD show];
    
    NSString *urlString = [NSString stringWithFormat:@"%@community_upload.php",baseUrl];
    
   NSString *catid =  [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
    
    NSString *extn;
    NSString *docPath = [FCFileManager pathForTemporaryDirectoryWithPath:docName];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
   NSString *userid  = [[retrievedDictionary objectAtIndex:0]valueForKey:@"user_id"];
    
    
    
    {
        // Create the URLSession on the default configuration
        NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
        
        // Setup the request with URL
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
        
      // NSString *base64 = [docData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
        /*
         type = reads/picture
         extn=file extension
         pic_title=picture title
         lang_id=language id
         */
        
        // Convert POST string parameters to data using UTF8 Encoding
        NSString *postParams;
        
        NSInteger langidd = 5;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
            langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
        
        NSLog(@"docPath :%@",docPath);
        
        if(_radioBtn2.isSelected)
        {
            //picture upload
            catid = @"69";
            
            extn = imageName;
            
            NSString * lngid;
            
            NSString *picTitle;
            
            picTitle = [_titleTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
           // extn = @"jpg";
            //[[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidUploadFile"];
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"catidUploadPic"])
            catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidUploadPic"];
            
            if(!img || img == nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    
                    NSString *lang = langGlobal;
                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    NSString *imgEmpty = [languageBundle localizedStringForKey:@"imgEmpty" value:@"" table:@"strings-english"];
                    
                [self.view makeToast:imgEmpty
                            duration:1.5
                            position:CSToastPositionBottom];
                
                });
                
                return;

            }
            
            NSData *imageData = UIImageJPEGRepresentation(img, 1);
            NSString *base64 = [imageData base64EncodedStringWithOptions:0];
          
            if(!lngid)
            {
                lngid = @"5";
            }
            
            postParams = [NSString stringWithFormat:@"user_id=%@&pic_title=%@&cat_id=%@&file=%@&type=picture&extn=%@&lang_id=%@&language_id=%ld",userid,picTitle,catid,base64,extn,lngid,(long)langidd];
        }
        else
        {
            //file upload
            
            NSString *comment , *title ;
            
            NSString *langid;
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"readLangid"])
                langid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"readLangid"]];
            
            
            comment = [_commentsTxtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            title = [_descTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            
            
            if(!docData || docData == nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    
                    NSString *lang = langGlobal;
                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    NSString *docEmpty = [languageBundle localizedStringForKey:@"docEmpty" value:@"" table:@"strings-english"];
                    
                [self.view makeToast:docEmpty
                            duration:1.5
                            position:CSToastPositionBottom];
                
                });
                
                return;
            }
            
            NSString *docPath = [FCFileManager pathForTemporaryDirectoryWithPath:docName];
            
            unsigned long long size = [[NSFileManager defaultManager] attributesOfItemAtPath:docPath error:nil].fileSize;
            
            NSLog(@"fileSize :%lld",size);
            
            
            catid = @"66";
            
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"catidUploadFile"])
                catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidUploadFile"];
            
            extn = [docPath pathExtension];
            
             NSString *base64 = [docData base64EncodedStringWithOptions:0];
            
            if(!langid)
            {
                langid = @"5";
            }
            
            postParams = [NSString stringWithFormat:@"user_id=%@&comment=%@&title=%@&cat_id=%@&file=%@&type=reads&extn=%@&lang_id=%@&language_id=%ld",userid,comment,title,catid,base64,extn,langid,langidd];
        }
        
        //testing multipart
        //[self uploadFileTest];
        
       // return;
       // NSData *postData = [postParams dataUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"postParams :%@",postParams);
        
        NSData *postData = [postParams dataUsingEncoding:NSUTF8StringEncoding];
        
       // NSData *postData = [postParams dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        
        
        // Convert POST string parameters to data using UTF8 Encoding
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setHTTPBody:postData];
        
        // Create dataTask
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            if(data == nil)
            {
                NSString *lang = langGlobal;
                
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                
                NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[self.view makeToastActivity:CSToastPositionCenter];
                    [SVProgressHUD dismiss];
                    
                    [self.view makeToast:error
                                duration:1.5
                                position:CSToastPositionBottom];
                    
                });
                
                return ;
            }
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
             NSLog(@"json response : %@",json);
            
            if([[[json valueForKey:@"result"]objectAtIndex:0] valueForKey:@"message"])
            {
                NSString *message = [[[json valueForKey:@"result"]objectAtIndex:0] valueForKey:@"message"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[self.view makeToastActivity:CSToastPositionCenter];
                    
                    _descTF.text = @"";
                    _commentsTxtVw.text = @"";
                    _titleTF.text = @"";
                    
                    docData = nil;
                    
                    img = nil;
                    
                    imageFileName = @"";
                    docName = @"";
                    
                    _uploadFileTF.text = @"";
                    
                    [SVProgressHUD dismiss];
                    
                    [self.view makeToast:message
                                duration:1.5
                                position:CSToastPositionBottom];
                    
                });
                
               
            }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 //[self.view makeToastActivity:CSToastPositionCenter];
                 [SVProgressHUD dismiss];
                 
                 NSString *lang = langGlobal;
                 NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                 NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                 NSString *uploadError = [languageBundle localizedStringForKey:@"uploadError" value:@"" table:@"strings-english"];
                 
                 [self.view makeToast:uploadError
                             duration:1.5
                             position:CSToastPositionBottom];
                 
             });
         }
           
            
           
        }];
        
        // Fire the request
        [dataTask resume];
    }
    
    
    /*
    {
        
        
        
       
        NSMutableString *stringPass = [[NSMutableString alloc] init];
        
        
        //[stringPass appendString:[NSString stringWithFormat:@"&userId=%@",userID]];
        
        
        //[stringPass appendString:[NSString stringWithFormat:@"&token=%@",token]];
        
        //[stringPass addRequestHeader:[NSString stringWithFormat:@"multipart/form-data"] value:@"Content-Type"];
        
        [stringPass setValue:[NSString stringWithFormat:@"testing Title"] forKey:@"title"];
        
         [stringPass setValue:[NSString stringWithFormat:@"1"] forKey:@"cat_id"];
        
        {
            [stringPass setValue:docData forKey:@"file"];
        }
        
        
        NSData *postData = [stringPass dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",urlString]]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            if(data ==nil)
            {
                return ;
            }
            
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            
            
            NSLog(@"json response : %@",json);
            
        }] resume];
        
    }
    
    return;
    
   //app.empowerji.com/webservice3/community_upload.php
    
   // NSString *urlString = @"https://web.koolkampus.co.in/koolkampus-androidservice/MFcontroller?Type=uploadFile";  //ratebase;  //?Type=uploadFile
    
   // NSString *urlString = @"http://app.empowerji.com/webservice3/community_upload.php";
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *array1 = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSArray *array = [[array1 valueForKey:@"User"]objectAtIndex:0];
    
    
    NSInteger selectedIndex;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"selectedIndex"])
        selectedIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"selectedIndex"];
    else
        selectedIndex = 0;
    
   // array = [array objectAtIndex:selectedIndex];
    
    
  //  NSMutableArray *arra = [[NSUserDefaults standardUserDefaults]valueForKey:@"AuthorDetails"];
    
    
    //__weak
    ASIFormDataRequest *asiRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [asiRequest setRequestMethod:@"POST"];
    [asiRequest setTimeOutSeconds:10000];
    
  //  NSString *authrName;NSString *authrId;
    
    NSString *folderLinkId;
    
   // if([[NSUserDefaults standardUserDefaults]objectForKey:@"1ParentFolderId"])
     //   folderLinkId = [[NSUserDefaults standardUserDefaults]objectForKey:@"1ParentFolderId"];
    
    NSLog(@"folderLinkId :%@",folderLinkId);
    //Type=uploadFile&access=public&authorId=196608&authorName&userId=196608&instituteId=4&folderLinkId=63&dtype=teacher_profile
    
    //comment, title, cat_id, file
    
    [asiRequest addRequestHeader:[NSString stringWithFormat:@"multipart/form-data"] value:@"Content-Type"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"testing Title"] forKey:@"title"];
    
    [asiRequest setPostValue:[NSString stringWithFormat:@"1"] forKey:@"cat_id"];
    
    {
        [asiRequest addData:docData withFileName:@"doc.doc" andContentType:@"application/octet-stream" forKey:@"file"];
    }
    
//    if(!filename || (filename == nil) || ([filename isEqualToString:@""]))
//    {
//        filename = @"picture.jpg";
//    }
//    if(!docName || (docName == nil) || ([docName isEqualToString:@""]))
//    {
//        docName = @"doc.doc";
//    }
//
//    if([filetype isEqualToString:@"Camera"])
//    {
//        [asiRequest addData:UIImageJPEGRepresentation(img, .1) withFileName:filename andContentType:@"application/octet-stream" forKey:@"upload"];
//    }
//    else if([filetype isEqualToString:@"Gallery"])
//    {
//        [asiRequest addData:UIImageJPEGRepresentation(img, .1) withFileName:filename andContentType:@"application/octet-stream" forKey:@"upload"];
//    }
//    else if([filetype isEqualToString:@"Documents"])
//    {
//        [asiRequest addData:docData withFileName:@"doc.doc" andContentType:@"application/octet-stream" forKey:@"upload"];
//    }
//    else if([filetype isEqualToString:@""])
//    {
//
//    }
//    else
//    {
//
//    }
    
    
    
  //  [asiRequest setPostValue:@"public" forKey:@"access"];
    //[asiRequest setPostValue:authrId  forKey:@"authorId"];
  //  [asiRequest setPostValue:authrName forKey:@"authorName"];
    
 //   [asiRequest setPostValue:[NSString stringWithFormat:@"%@",[array valueForKey:@"id"]] forKey:@"userId"];
    
   // [asiRequest setPostValue:[NSString stringWithFormat:@"%@",[array valueForKey:@"instituteid"]] forKey:@"instituteId"];
   
   // [asiRequest setPostValue:folderLinkId forKey:@"folderLinkId"];
    
    
    //[asiRequest setPostValue:@"teacher_profile" forKey:@"dtype"];
    
    
    [asiRequest setCompletionBlock:^{
        
        [SVProgressHUD dismiss];
        
        NSError *error = [asiRequest error];
        if (!error) {
            NSString *response = [asiRequest responseString];
            
            NSLog(@" Response upload file = %@",response);
            
           // [self mfWebservice];
            
            //[self closeUploadVw:nil];
            
            [self.view makeToast:@"File added successfully"
                        duration:1.5
                        position:CSToastPositionBottom];
            
            
        }else {
            
            [self.view makeToast:@"Something went wrong"
                        duration:1.5
                        position:CSToastPositionBottom];
            
            //[self closeUploadVw:nil];
        }
    }];
    
    
    [asiRequest startAsynchronous];
     
     */
}

# pragma mark Button Actions

- (IBAction)menuBtnAction:(id)sender
{
    sideMenuViewController *svc=[[sideMenuViewController alloc]init];
    svc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    svc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:svc animated:YES completion:nil];
}

- (IBAction)searchAction:(id)sender
{
    //_searchBar.hidden=NO;
    //[_searchBar becomeFirstResponder];
    
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnSelectionAction:(id)sender
{
    myFeedViewController *mfVC = [[myFeedViewController alloc]initWithNibName:@"myFeedViewController" bundle:nil];
    mfVC.isfrmHome = YES;
    [self.navigationController pushViewController:mfVC animated:YES];
}
- (IBAction)homeActn:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)supportBtnAction:(id)sender
{
    supportViewController *smVC = [[supportViewController alloc]initWithNibName:@"supportViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}
- (IBAction)keepGoingbtnAction:(id)sender
{
    supportViewController *smVC = [[supportViewController alloc]initWithNibName:@"supportViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}

-(void)playAudio
{
    // Construct URL to sound file
    NSString *path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    [_audioPlayer play];
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)uploadBtnAction:(UIButton*)sender
{
    [self.view endEditing:YES];
    [self chooseFile:sender];
    
   // [self uploadFileWS];
}

- (IBAction)chooseFile:(id)sender
{
    
    NSString *lang = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    NSString *gallery = [languageBundle localizedStringForKey:@"gallery" value:@"" table:@"strings-english"];
    NSString *documents = [languageBundle localizedStringForKey:@"documents" value:@"" table:@"strings-english"];
    
    
  /*  UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
       
       alert.popoverPresentationController.sourceView = self.view;
       
       [alert addAction:[UIAlertAction actionWithTitle:gallery style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
   */
    if(_radioBtn2.isSelected)
    {
           UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
           pickerView.allowsEditing = YES;
           pickerView.delegate = self;
           [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
           [self presentViewController:pickerView animated:YES completion:nil];
        
    }
    else
      // }]];
    /*   [alert addAction:[UIAlertAction actionWithTitle:documents style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           */
           
           //UIDocumentMenuViewController
        
    {
           UIDocumentPickerViewController *documentProviderMenu =
           [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.text", @"com.apple.iwork.pages.pages", @"public.data"] inMode:UIDocumentPickerModeImport];
           
           documentProviderMenu.delegate = self;
           
           
           NSLog(@"%f",floor(NSFoundationVersionNumber));
           if ((floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_9_x_Max) && !(IS_IPAD)) {
               
               [self presentViewController:documentProviderMenu animated:YES completion:nil];
           }
           else
           {
               
               UIPopoverController *popOverController = [[UIPopoverController alloc]
                                                         initWithContentViewController:documentProviderMenu] ;
               CGRect rect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 1, 1);
               [popOverController presentPopoverFromRect:rect
                                                  inView:self.view
                                permittedArrowDirections:UIPopoverArrowDirectionAny
                                                animated:YES];
               
               documentProviderMenu.popoverPresentationController.sourceView = self.view;
           }
        
    }
     //  }]];
     /*  [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
           
           //filetype = @"";
           
       }]];
       [alert setModalPresentationStyle:UIModalPresentationPopover];
      
       
       CGRect rect = CGRectMake(0, 0, 1, 1);
       
       UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
       popPresenter.sourceView = sender;
       popPresenter.sourceRect = rect; // You can set position of popover
       [self presentViewController:alert animated:TRUE completion:nil];
       */
    
       data= [[NSMutableArray alloc] init];
       NSString *documentsPath= [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
       NSError *error;
       NSArray* files= [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
       NSRange range;
       
       for (NSString* file in files) {
           range = [file rangeOfString:@".doc"
                               options:NSCaseInsensitiveSearch];
           if (range.location!= NSNotFound) {
               [data addObject:file];
           }
       }
       
       NSLog(@"data array :%@",data);
    
}

-(void)radioButtonSelAction:(DLRadioButton*)sender
{
    _categoryTblVw.hidden = YES;
    
    if(sender.tag == 1)
    {
        if(imageFileName)
        _uploadFileTF.text = imageFileName;
        else
         _uploadFileTF.text = @"";
        
        _downArrow1.hidden = NO;
        _downArrow2.hidden = YES;
        
        _radioBtn2.selected = YES;
        _radioBtn1.selected = NO;
        
        CGRect frame = _fieldViews.frame;
        frame.size.height = 0;
        _fieldViews.frame = frame;
        
        CGRect frame2 = _bottomView.frame;
        frame2.origin.y = _fieldViews.frame.origin.y + _fieldViews.frame.size.height;
        _bottomView.frame = frame2;
        
       // _catTF.hidden = YES;
        _descTF.hidden = YES;
        _commentsTxtVw.hidden = YES;
        
        _langTF.hidden = YES;
        
        _titleTF.hidden = NO;
        
        _catTF.frame = langTfFrame;
        
        _btnSelCat.frame = _catTF.frame;
        
         CGRect frameCatTblVw = _categoryTblVw.frame ;
        //frameCatTblVw
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *placeholderTxt = [languageBundle localizedStringForKey:@"uploadImages" value:@"" table:@"strings-english"];
        
         _uploadFileTF.placeholder = placeholderTxt;
        
    }
    else if(sender.tag == 0)
    {
        if(docName)
        _uploadFileTF.text = docName;
        else
            _uploadFileTF.text = @"";
        
        _downArrow1.hidden = NO;
        _downArrow2.hidden = NO;
        
        _radioBtn2.selected = NO;
        _radioBtn1.selected = YES;
        
        _titleTF.hidden = YES;
        
        _catTF.frame = catTfFrame;
        _catTF.hidden = NO;
        _descTF.hidden = NO;
        _commentsTxtVw.hidden = NO;
        _langTF.hidden = NO;
        
        CGRect frame = _fieldViews.frame;
        frame.size.height = frameFields.size.height;
        _fieldViews.frame = frame;
        
        CGRect frame2 = _bottomView.frame;
        frame2.origin.y = _fieldViews.frame.origin.y + _fieldViews.frame.size.height;
        _bottomView.frame = frame2;
        
        
        _catTF.frame = catTfFrame;
        _descTF.frame = despTfFrame;
        _commentsTxtVw.frame = commentsFrame;
        _categoryTblVw.frame = catTblFrame;
        _btnSelCat.frame =  btnSelCatFrame;
        
        _btnSelLang.frame = langSelFrame;
        _langTblVw.frame = langTblFrame;
        _langTF.frame = langTfFrame;
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *placeholderTxt = [languageBundle localizedStringForKey:@"uploadFiles" value:@"" table:@"strings-english"];
        
        _uploadFileTF.placeholder = placeholderTxt;
    }
    
    [_categoryTblVw reloadData];
    
    int i=0;
    if(IS_IPAD)
    {
        i = 50;
    }
    else
        i = 30;
    
    CGRect frame = _categoryTblVw.frame;
    
    if(_radioBtn1.isSelected)
    frame.size.height = i * catArr.count;
    else
        frame.size.height = i * catArr2.count;
    
    _categoryTblVw.frame = frame;
    
    NSString *catname , *catid;
    
   
    if(_radioBtn1.isSelected)
    {
        catname = [[catArr objectAtIndex:0]valueForKey:@"name"];
        
        catid = [[catArr objectAtIndex:0]valueForKey:@"id"];
        
      [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidUploadFile"];
    }
    else
    {
        catname = [[catArr2 objectAtIndex:0]valueForKey:@"name"];
        
        catid = [[catArr2 objectAtIndex:0]valueForKey:@"id"];
        
        [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidUploadPic"];
    }
    
    _catTF.text = catname;
}

-(void)closePopUpVw
{
    [self.popUpVw removeFromSuperview];
}

-(void)submitAction
{
   // [self uploadImageASI];
    
   // [self uploadImageWS];
    
   // [self testingWS];
    
    [self.view endEditing:YES];
    
    if(_radioBtn1.isSelected)
    {
        NSString *lanStr =[[langArr objectAtIndex:0]valueForKey:@"language"];
        
        NSString *catStr =[[catArr objectAtIndex:0]valueForKey:@"name"];
        
      if((_langTF.text.length==0) || ([_langTF.text caseInsensitiveCompare:lanStr]==NSOrderedSame) || ([_langTF.text caseInsensitiveCompare:lanStr]==NSOrderedSame) )
      {
          
          NSString *lang = langGlobal;
          NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
          NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
          NSString *alert = [languageBundle localizedStringForKey:@"selLangEmpty" value:@"" table:@"strings-english"];
          
          [self.view makeToast:alert
                      duration:1.5
                      position:CSToastPositionBottom];
      }
      else if((_catTF.text.length==0) || ([_catTF.text caseInsensitiveCompare:catStr]==NSOrderedSame) || ([_catTF.text caseInsensitiveCompare:catStr]==NSOrderedSame) )
      {
          
          NSString *lang = langGlobal;
          NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
          NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
          NSString *alert = [languageBundle localizedStringForKey:@"selCatEmpty" value:@"" table:@"strings-english"];
          
          [self.view makeToast:alert
                      duration:1.5
                      position:CSToastPositionBottom];
      }
        else
        {
            
            if(_descTF.text.length==0)
            {
                
                NSString *lang = langGlobal;
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                NSString *alert = [languageBundle localizedStringForKey:@"articleEmpty" value:@"" table:@"strings-english"];
                
                [self.view makeToast:alert
                            duration:1.5
                            position:CSToastPositionBottom];
            }
            else
            {
                [self uploadFileWS];
            }
        }
    }
    else
    {
         NSString *catStr =[[catArr2 objectAtIndex:0]valueForKey:@"name"];
        
         if((_catTF.text.length==0) || ([_catTF.text caseInsensitiveCompare:catStr]==NSOrderedSame) || ([_catTF.text caseInsensitiveCompare:catStr]==NSOrderedSame) )
        {
            
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *alert = [languageBundle localizedStringForKey:@"selCatEmpty" value:@"" table:@"strings-english"];
            
            [self.view makeToast:alert
                        duration:1.5
                        position:CSToastPositionBottom];
        }
        
           else if(_titleTF.text.length==0)
            {
                
                NSString *lang = langGlobal;
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                NSString *alert = [languageBundle localizedStringForKey:@"titleEmpty" value:@"" table:@"strings-english"];
                
                [self.view makeToast:alert
                            duration:1.5
                            position:CSToastPositionBottom];
            }
            else
            {
                if(_titleTF.text.length > 100)
                {
                    NSString *lang = langGlobal;
                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    NSString *alert = [languageBundle localizedStringForKey:@"titleLength" value:@"" table:@"strings-english"];
                    
                    [self.view makeToast:alert
                                duration:1.5
                                position:CSToastPositionBottom];
                }
                else
                [self uploadFileWS];
            }
    }
    
    
}

-(void)selectCategoty
{
    _categoryTblVw.hidden = NO;
    
    _langTblVw.hidden = YES;
    
    int i =0;
    if(IS_IPAD)
    {
        i = 50;
    }
    else
        i = 30;
    
    CGRect frame = _categoryTblVw.frame;
    
    if(_radioBtn1.isSelected)
        frame.size.height = i * catArr.count;
    else
        frame.size.height = i * catArr2.count;
    
    _categoryTblVw.frame = frame;
}

-(void)selectLanguage
{
    _langTblVw.hidden = NO;
    
    int i =0;
    if(IS_IPAD)
    {
        i = 50;
    }
    else
        i = 30;
    
    CGRect frame = _langTblVw.frame;
    
    if(_radioBtn1.isSelected)
        frame.size.height = i * langArr.count;
    else
        frame.size.height = 0;//i * catArr2.count;
    
    _langTblVw.frame = frame;
}

-(void)helpAction
{
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
    
  /*  NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
   */
    
}

#pragma mark SearchBar Cancel Action

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    _searchBar.hidden=YES;
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == 0)
    {
        if(_radioBtn1.isSelected)
            return [catArr count];
        else
            return [catArr2 count];
    }
    else //if(tableView.tag == 1)
    {
        return langArr.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 50;
    }
    else
        return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    
    
    NSInteger section = indexPath.section;
    
    if(tableView.tag == 0)
    {
    if(_radioBtn1.isSelected)
        cell.textLabel.text = [[catArr objectAtIndex:section]valueForKey:@"name"];
    else
       cell.textLabel.text = [[catArr2 objectAtIndex:section]valueForKey:@"name"];
    }
    else
    {
        NSString *lanID = [NSString stringWithFormat:@"%@",[[langArr objectAtIndex:section]valueForKey:@"id"]];
        
        NSString *langText = [[langArr objectAtIndex:section]valueForKey:@"language"];
        
        if([lanID caseInsensitiveCompare:@"0"]==NSOrderedSame)
        {
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *placeholderTxt = [languageBundle localizedStringForKey:@"language_preference" value:@"" table:@"strings-english"];
            
            langText = placeholderTxt;
        }
        else
        {
            
        }
        
        cell.textLabel.text = langText;
        
       // cell.textLabel.text = [[langArr objectAtIndex:section]valueForKey:@"language"];
    }
    //cell.textLabel.textColor = [UIColor whiteColor];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    NSString *catName , *catid , *langid;
    
    if(tableView.tag == 0)
    {
    if(_radioBtn1.isSelected)
    {
        catName = [[catArr objectAtIndex:section]valueForKey:@"name"];
        catid = [[catArr objectAtIndex:section]valueForKey:@"id"];
        
         [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidUploadFile"];
    }
    else
    {
        catName = [[catArr2 objectAtIndex:section]valueForKey:@"name"];
        catid = [[catArr2 objectAtIndex:section]valueForKey:@"id"];
        
         [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidUploadPic"];
    }
    
    _catTF.text = catName;
    
    _categoryTblVw.hidden = YES;
        
      
        
    }
    else
    {
        catName = [[langArr objectAtIndex:section]valueForKey:@"language"];
        
        langid = [[langArr objectAtIndex:section]valueForKey:@"id"];
        
        [[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"readLangid"];
        
        _langTF.text = catName;
        
        _langTblVw.hidden = YES;
    }
}
#pragma mark status bar color

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}


#pragma mark UICollectionView methods

- (CGSize)itemSize
{
    NSInteger numberOfColumns = 2;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionVw.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   if(isFrmCommunity)
       return [catArr count] ;
    else
    return [catArr count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"home";
    
    UINib *nib = [UINib nibWithNibName:@"homeCollectionViewCell" bundle: nil];
    
    [collectionView registerNib:nib forCellWithReuseIdentifier:simpleTableIdentifier];
    
    NSInteger section = indexPath.row;
    
   homeCollectionViewCell *cell = (homeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    cell.homeLbl.text = [[catArr objectAtIndex:section]valueForKey:@"name"];
  
    [cell.homeImg1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[catArr objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    //cell.homeImg1.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[catArr objectAtIndex:section]valueForKey:@"image"]]];
    
   // cell.tag = [[[catArr objectAtIndex:section]valueForKey:@"tag"]integerValue];
    
    [cell.homeLbl setText:[cell.homeLbl.text uppercaseString]];
    
    [cell layoutSubviews];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    
    cell.homeImg.layer.cornerRadius = 10;
    cell.homeImg1.layer.cornerRadius = 10;
    
    [cell.homeImg.layer setBorderColor:[UIColor whiteColor].CGColor];
    [cell.homeImg.layer setBorderWidth:2.0f];
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
//    if(pathToFirstRow==indexPath)
//    {
//        CGRect frame;
//        frame = cell.homeImg1.frame;
//        if(IS_IPAD)
//        frame.origin.x = cell.homeImg.frame.origin.x+3;
//        else
//           frame.origin.x = cell.homeImg.frame.origin.x+2;
//
//        cell.homeImg1.frame = frame;
//
//        cell.homeImg1.contentMode = UIViewContentModeScaleAspectFill;
//    }
//    else
    {
        CGRect frame;
        frame = cell.homeImg1.frame;
        frame.size.width = frame.size.width - 2;
        
        cell.homeImg1.frame = frame;
    }
    
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *catid =[[catArr objectAtIndex:indexPath.row]valueForKey:@"id"];
    
    [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidFromHomeCat"];
    
    NSString *catName =[[catArr objectAtIndex:indexPath.row]valueForKey:@"name"];
    
    if(!([catid caseInsensitiveCompare:@"64"]==NSOrderedSame))
    [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"catName"];
    
    NSString *subExist = [[catArr objectAtIndex:indexPath.row]valueForKey:@"sub_exists"];
    
    NSString *descrptn = [[catArr objectAtIndex:indexPath.row]valueForKey:@"description"];
    
     [[NSUserDefaults standardUserDefaults]setObject:descrptn forKey:@"catDescription"];
    
    if([catid caseInsensitiveCompare:@"64"]==NSOrderedSame)
    {
        NSString *subCatName =[[catArr objectAtIndex:indexPath.row]valueForKey:@"name"];
        
        [[NSUserDefaults standardUserDefaults]setObject:subCatName forKey:@"subCatName"];
        
        galleryListingViewController *glVC = [[galleryListingViewController alloc]initWithNibName:@"galleryListingViewController" bundle:nil];
        [self.navigationController pushViewController:glVC animated:YES];
    }
    else if([catid caseInsensitiveCompare:@"63"]==NSOrderedSame)
    {
        homeCategoryViewController *hcVC = [[homeCategoryViewController alloc]initWithNibName:@"homeCategoryViewController" bundle:nil];
        hcVC.isFrmCommunity = YES;
        [self.navigationController pushViewController:hcVC animated:YES];
    }
    else
    {
    if([subExist caseInsensitiveCompare:@"yes"]==NSOrderedSame)
    {
        //homeCategoryViewController *hcVC = [[homeCategoryViewController alloc]initWithNibName:@"homeCategoryViewController" bundle:nil];
       // [self.navigationController pushViewController:hcVC animated:YES];
    }
    else
    {
        moListViewController *liVC = [[moListViewController alloc]initWithNibName:@"moListViewController" bundle:nil];
        [self.navigationController pushViewController:liVC animated:YES];
    }
    }
    return;
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    
   // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              
              
             
                  
    if([urlData length] == 0)
    {
        NSLog(@"internet is not connected");
        
       
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
       NSLog(@"internet is connected");
        
       
        
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable )
        {
            //connection unavailable
            
            [self showMessage:errorMessage withTitle:nil];
        }
        else
        {
            
            NSInteger ctag;
          //  ctag = [[[homeArray objectAtIndex:indexPath.row]valueForKey:@"tag"]integerValue];
            
           // [[NSUserDefaults standardUserDefaults]setInteger:ctag forKey:@"catTag"];
            
           // [[NSUserDefaults standardUserDefaults]setObject:[[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"] forKey:@"HomeClickedCellName"];
            
            NSString *subexist;
        
            switch (ctag) {
                    
                case 2:
                    //experience
                {
                    subexist = @"experience_sub";
                    
                    break;
                }
                case 3:
                    //quick learn
                {
                    subexist = @"quicklearn_sub";
                    
                    break;
                }
                case 4:
                    //news
                {
                    subexist = @"news_sub_exists";
                    
                    break;
                }
                case 5:
                    //health
                {
                    subexist = @"health_sub_exists";
                    
                    break;
                }
                default:
                    break;
            }
            
            NSLog(@"%@",subCatCheckArr);
            //subCatCheckArr
            
            @try {
                
                
                if([[subCatCheckArr valueForKey:subexist] isEqualToString:@"yes"])
                {
                    categoryViewController *catVC = [[categoryViewController alloc]initWithNibName:@"categoryViewController" bundle:nil];
                    [self.navigationController pushViewController:catVC animated:YES];
                }
                else
                {
                   // NSString *name;
                    
                  //  name = [[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"];
                    
                   // [[NSUserDefaults standardUserDefaults]setInteger:ctag forKey:@"listid"];
                    
                   // [[NSUserDefaults standardUserDefaults]setObject:name forKey:@"listname"];
                    
                    
                    listViewController *lVC = [[listViewController alloc]initWithNibName:@"listViewController" bundle:nil];
                    [self.navigationController pushViewController:lVC animated:YES];
                }
                
            }
            @catch (NSException *exception)
            {
                [self showMessage:errorMessage withTitle:nil];
            }
            
            
        }
    }
    
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
          
                         
                         });
          
          
      }] resume];
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
     if(IS_IPAD)
     {
        return 40.0;
     }
    else
    return 15.0; //0.01;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 40.0;
    }
    else
        return 15.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 2.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    
    CGSize size ;
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
//    if(pathToFirstRow==indexPath)
//    {
//       float cellWidth2 = screenWidth / 1;
//
//
//        if(IS_IPAD)
//        {
//            size = CGSizeMake(555, 250);//cellWidth-84
//        }
//        else
//            size = CGSizeMake(cellWidth2-35, cellWidth-25); //162
//    }
//    else
    {
        if(IS_IPAD)
        {
            size = CGSizeMake(258, 250);//cellWidth-84
        }
        else
            size = CGSizeMake(cellWidth-25, cellWidth-25); //162
    }
    
    
    return size;
}


- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return UIEdgeInsetsMake(0, 105, 0, 105);
    }
    else
    return UIEdgeInsetsMake(0, 15, 0, 15); // top, left, bottom, right
}

# pragma mark TextView Delegates

- (BOOL)textViewShouldReturn:(UITextView *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textField {
    
    NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    NSString *placeholderTxt = [languageBundle localizedStringForKey:@"anyComments" value:@"" table:@"strings-english"];
    
    
    NSString *textVw_text = textField.text;
    
    NSString *trimmedString = [textVw_text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    if ([trimmedString caseInsensitiveCompare:placeholderTxt]==NSOrderedSame)
    {
        textField.text = @"";
        
        textField.textColor = [UIColor blackColor];
    }
}


- (void)textViewDidEndEditing:(UITextView *)textField {
    
    NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    NSString *placeholderTxt = [languageBundle localizedStringForKey:@"anyComments" value:@"" table:@"strings-english"];
    
    if ([textField.text isEqualToString:@""])
    {
        textField.text = placeholderTxt;
        
        textField.textColor = [UIColor lightGrayColor];
        
    }
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    NSString *lang = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    NSString *ok = [languageBundle localizedStringForKey:@"ok" value:@"" table:@"strings-english"];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

# pragma mark check for internet Ping

-(BOOL)iConnected
{
   __block BOOL connectin;
    NSString *wUrl=[NSString stringWithFormat:@"%@static_pages.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self staticPagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
    
         if(json == nil)
         {
             connectin = NO;
         }
        else
        {
            connectin = YES;
        }
    }];
    
    [dataTask resume];
    
    return connectin;
}


#pragma mark document picker

- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu
didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker
{
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    if (controller.documentPickerMode == UIDocumentPickerModeImport)
    {
        
        // Condition called when user download the file
        docData = [NSData dataWithContentsOfURL:url];
        // NSData of the content that was downloaded - Use this to upload on the server or save locally in directory
        
        
        //Showing alert for success
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *alertMessage = [NSString stringWithFormat:@"Successfully downloaded file %@", [url lastPathComponent]];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"UIDocumentView" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
            
            docName = [url lastPathComponent];
            NSString *docPath = [FCFileManager pathForTemporaryDirectoryWithPath:docName];
            NSLog(@"docPath :%@",docPath);
            
            NSError *error = nil;
            NSFileCoordinator *coordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
            
            [coordinator coordinateReadingItemAtURL:url options:NSFileCoordinatorReadingImmediatelyAvailableMetadataOnly error:&error byAccessor:^(NSURL *newURL) {
                
                NSError *err = nil;
                
                NSNumber * fileSize;
                if(![url getPromisedItemResourceValue:&fileSize forKey:NSURLFileSizeKey error:&err])
                {
                    NSLog(@"Failed error: %@", error);
                    return ;
                }
                else
                {
                    NSLog(@"fileSize %f",fileSize.doubleValue);
                    
                  NSString *sizeInKB = [NSByteCountFormatter stringFromByteCount:fileSize.doubleValue countStyle:NSByteCountFormatterCountStyleFile];
                    
                    NSLog(@"file size :%@",sizeInKB);
                    
                    int fileSizKB = [sizeInKB intValue];
                    if( (fileSizKB >= 750) || ([sizeInKB containsString:@"MB"]) )
                    {
                        docData = nil;
                        
                        NSString *lang = langGlobal;
                        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                        NSString *maxSize = [languageBundle localizedStringForKey:@"maxSize" value:@"" table:@"strings-english"];
                        
                        [self showMessage:maxSize withTitle:nil];
                        
                    }
                    
                    _uploadFileTF.text = docName;
                }
            }];
             
            
           // _pathLblText.hidden = NO;
           // _pathLblText.text = docPath;
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            // [self presentViewController:alertController animated:YES completion:nil];
            
        });
    }
    
}

- (void)documentMenuWasCancelled:(UIDocumentMenuViewController *)documentMenu
{
    docData = nil;
    
    _uploadFileTF.text = @"";
}

#pragma mark - PickerDelegates

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
     img = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    NSURL *imagePath = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
    NSString *imageiName = [imagePath lastPathComponent];
    
    imageFileName = imageiName;
    
    imageName = [imageiName pathExtension];
    
    
    
    ///change img size
    CGRect rect = CGRectMake(0,0,img.size.width/4,img.size.height/6);
    UIGraphicsBeginImageContext( rect.size );
    [img drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageDataForResize = UIImageJPEGRepresentation(picture1,0.4);
    UIImage *img2=[UIImage imageWithData:imageDataForResize];
    img = img2; // reassigning to original image as to prevent changing things later in the code
    
    _uploadFileTF.text = imageFileName;
    
}

- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}

@end
