//
//  homeViewController.h
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLRadioButton.h"
@import GoogleMobileAds;

@interface formViewController : UIViewController
@property (strong, nonatomic) IBOutlet UICollectionView *collectionVw;
//@property (strong, nonatomic) IBOutlet UIView *adVw;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIButton *supportBtn;
@property (strong, nonatomic) IBOutlet UIButton *keepGoingBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@property (strong, nonatomic) IBOutlet UITextField *searchBarTF;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UIButton *btnshareWithComm;
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UITextField *uploadFileTF;
@property (strong, nonatomic) IBOutlet UITextField *catTF;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutlet UITextField *descTF;
@property (strong, nonatomic) IBOutlet UITextView *commentsTxtVw;
@property (strong, nonatomic) IBOutlet UIButton *uploadBtn;
@property (strong, nonatomic) IBOutlet DLRadioButton *radioBtn2;
@property (strong, nonatomic) IBOutlet DLRadioButton *radioBtn1;
@property (strong, nonatomic) IBOutlet UIView *popUpVw;
@property (strong, nonatomic) IBOutlet UIButton *btnOk;
@property (strong, nonatomic) IBOutlet UILabel *lblPopUp;
@property (strong, nonatomic) IBOutlet UIButton *btnSelCat;
@property (strong, nonatomic) IBOutlet UITableView *categoryTblVw;
@property (strong, nonatomic) IBOutlet UIView *fieldViews;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIButton *btnSelLang;
@property (strong, nonatomic) IBOutlet UITableView *langTblVw;
@property (strong, nonatomic) IBOutlet UITextField *langTF;
@property (strong, nonatomic) IBOutlet UITextField *titleTF;
@property (strong, nonatomic) IBOutlet UIButton *helpBtn;
@property (strong, nonatomic) IBOutlet UIImageView *helpImg;
@property (strong, nonatomic) IBOutlet UILabel *helpLbl;
@property (strong, nonatomic) IBOutlet UIButton *homeBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollPopVw;
@property (strong, nonatomic) IBOutlet UIView *popBgVw;
@property (strong, nonatomic) IBOutlet UIImageView *downArrow1;
@property (strong, nonatomic) IBOutlet UIImageView *downArrow2;

@property(nonatomic)BOOL isFrmCommunity;

@end
