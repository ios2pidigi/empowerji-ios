//
//  moListViewController.m
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "galleryListingViewController.h"
#import "detailViewController.h"
#import "Constants.h"
#import "moTableViewCell.h"
#import "UIImageView+JMImageCache.h"
#import "searchViewController.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "galleryListTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "galleryVC.h"

#import "popUpVideoViewController.h"
#import "sideMenuViewController.h"
#import "sendMessageViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "loginViewController.h"

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#import "UIButton+WebCache.h"

#import "homeViewController.h"

@interface galleryListingViewController ()<AVAudioPlayerDelegate>
{
    NSArray *mArray;
    
    BOOL playedOnce;
    
    BOOL once;
    
    UIButton *hLbl;
    
    UIButton *btn;
}
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@end

@implementation galleryListingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    once = NO;
    
    [_playBtn addTarget:self action:@selector(playAudio) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *admobId,*adStatus;
    
    NSString *headerLblText = [[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
    
    _headerLbl.text = [NSString stringWithFormat:@"%@",headerLblText];
    
    NSString *subHeaderText = [[NSUserDefaults standardUserDefaults]objectForKey:@"subCatName"];
    
    _subHeaderLbl.text = [NSString stringWithFormat:@"%@",subHeaderText];
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
     adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        
        [self.adVw loadRequest:[GADRequest request]];
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        //[self.view addSubview:customAdBanner];
        
    }
    
    mArray = [[NSArray alloc]init];
    
    if(_isFrmBkmd)
    {
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            mArray = [plistDict objectForKey:@"rateArray"];
        }
        
        [self.mTblVw reloadData];
    }
    else
    [self listMWebservice];
    
    
    _headerLbl.text = @"Community";
    
    [_helpBtn addTarget:self action:@selector(helpAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnMenu addTarget:self action:@selector(menuBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnContactUs addTarget:self action:@selector(contactUs:) forControlEvents:UIControlEventTouchUpInside];
   
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Gallery List Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catName"])
        _headerLblT.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"subCatName"])
        _headerSubLblT.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"subCatName"];
    
    
    //show help view
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"firstInstallGalleryList"])
    {
        // [[NSUserDefaults standardUserDefaults]setObject:@"firstInstall" forKey:@"firstInstall"];
        
        // get window screen size for frame
        CGRect screensize = [[UIScreen mainScreen] bounds];
        //create a new UIview with the same size for same view
       // TransprentView = [[UIView alloc] initWithFrame:screensize];
        
        // change the background color to black and the opacity to 0.6 for Transparent
        // TransprentView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        
        // here add your Label, Image, etc, what ever you want
        
        hLbl = [[UIButton alloc]initWithFrame:CGRectMake(((screensize.size.width/2)-(screensize.size.width/2)), ((screensize.size.height/2)-(50/2)), screensize.size.width, 50)];
        [hLbl setTitle:@"Click Here for Help" forState:UIControlStateNormal]; //↪ ⤻ ⇧
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *helpText = [languageBundle localizedStringForKey:@"helpText" value:@"" table:@"strings-english"];
        [hLbl setTitle:helpText forState:UIControlStateNormal];
        
        
        hLbl.titleLabel.textColor = [UIColor whiteColor];
        hLbl.backgroundColor = [UIColor clearColor];
        hLbl.titleLabel.textAlignment = NSTextAlignmentCenter;
        hLbl.titleLabel.numberOfLines = 0;
        if(IS_IPAD)
        {
            [hLbl setBackgroundImage:[UIImage imageNamed:@"bubble-2"] forState:UIControlStateNormal];
            hLbl.titleLabel.font = [UIFont systemFontOfSize:18.0 weight:UIFontWeightBold];
        }
        else
        {
            [hLbl setBackgroundImage:[UIImage imageNamed:@"bubble-2"] forState:UIControlStateNormal];
            hLbl.titleLabel.font = [UIFont systemFontOfSize:16.0 weight:UIFontWeightBold];
        }
        
        [hLbl addTarget:self action:@selector(bubbleClicked) forControlEvents:UIControlEventTouchUpInside];
        
        hLbl.titleLabel.alpha = 1;
        [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
            hLbl.titleLabel.alpha = 0;
        } completion:nil];
        
        [self.view addSubview:hLbl];
        
        //add a button above hlbl to play audio
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor = [UIColor clearColor];
        btn.frame = _playBtn.frame;
        [btn addTarget:self action:@selector(bubbleClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationIsActive:)name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnteredForeground:)name:UIApplicationWillEnterForegroundNotification object:nil];
    
    
}

- (void)applicationIsActive:(NSNotification *)notification
{
    NSLog(@"Application Did Become Active");
    
    hLbl.titleLabel.alpha = 1;
}

- (void)applicationEnteredForeground:(NSNotification *)notification
{
    NSLog(@"Application Entered Foreground");
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_audioPlayer pause];
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
    
}

static NSInteger countG;

-(void)viewWillAppear:(BOOL)animated
{
  if(_isFrmBkmd)
  {
    mArray = [[NSArray alloc]init];
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        mArray = [plistDict objectForKey:@"rateArray"];
    }
    
    [self.mTblVw reloadData];
      
   }
    
    playedOnce = YES;
    
    if(countG > 5)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"firstInstallGalleryList" forKey:@"firstInstallGalleryList"];
        
         [hLbl removeFromSuperview];
    }
    else
    {
        [self.view addSubview:hLbl];
    }
    
    countG ++;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    _btnMenu.layer.cornerRadius = _btnMenu.frame.size.height/2;
    _btnContactUs.layer.cornerRadius = _btnContactUs.frame.size.height/2;
    
    //make menu btn background gradient
    CAGradientLayer *gradientbtn = [CAGradientLayer layer];
    gradientbtn.frame = _btnMenu.bounds;
    gradientbtn.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnMenu.layer insertSublayer:gradientbtn atIndex:0];
    
    //make contactUs btn background gradient
    CAGradientLayer *gradientbtn2 = [CAGradientLayer layer];
    gradientbtn2.frame = _btnContactUs.bounds;
    gradientbtn2.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnContactUs.layer insertSublayer:gradientbtn2 atIndex:0];
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
        
        if(!once)
        {
            once = YES;
        CGRect frameC = _mTblVw.frame;
        frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
        _mTblVw.frame = frameC;
        }
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
        UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
    }
    
    
    CGRect frameHelpLbl = hLbl.frame;
    if(IS_IPAD)
        frameHelpLbl.origin.x = _speakerImg.frame.origin.x - ((([UIScreen mainScreen].bounds.size.width - _playBtn.frame.origin.x)/2)+20);
    else
        frameHelpLbl.origin.x = (([UIScreen mainScreen].bounds.size.width - _speakerImg.frame.origin.x));
    
    frameHelpLbl.origin.y = _playBtn.frame.origin.y + _playBtn.frame.size.height;
    
    if(IS_IPAD)
        frameHelpLbl.size.width = [UIScreen mainScreen].bounds.size.width - frameHelpLbl.origin.x;
    else
        frameHelpLbl.size.width = [UIScreen mainScreen].bounds.size.width - (frameHelpLbl.origin.x);
    
    frameHelpLbl.size.height = 500;
    hLbl.frame = frameHelpLbl;
    [hLbl sizeToFit];
    
    hLbl.titleEdgeInsets = UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f);
    
    btn.frame = _playBtn.frame;
   
    
    hLbl.titleLabel.alpha = 1;
    [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
        hLbl.titleLabel.alpha = 0;
    } completion:nil];
    
    
    NSString *catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
    
    if(([catid caseInsensitiveCompare:@"1"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"3"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"4"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"5"]==NSOrderedSame))
    {
        
    }
    else
    {
        _speakerImg.hidden = YES;
        _playBtn.hidden = YES;
        btn.hidden = YES;
        [hLbl removeFromSuperview];
    }
    
    [_helpLbl sizeToFit];
}

# pragma mark WebService Calls

-(void)listMWebservice
{
    [SVProgressHUD show];
    
    // categories.php?cat_id=
    NSString *cat_id;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"])
        cat_id = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"]];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@categories.php?cat_id=%@&lang_id=%ld",baseUrl,cat_id,(long)langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self listMWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            mArray = [json objectForKey:@"categories"];
            
            if([mArray count]>0)
            {
                if([[mArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    [_mTblVw reloadData];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
                
                
                
                 ////////
                 NSMutableArray *arrRate = [[NSMutableArray alloc]init];
                 NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                 NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                 NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                 NSFileManager * fileManager = [NSFileManager defaultManager];
                 //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                 if([fileManager fileExistsAtPath:finalPath])
                 {
                 NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                 arrRate = [plistDict objectForKey:@"moreArray"];
                 
                 if ([arrRate count] == 0)
                 {
                 arrRate = [NSMutableArray new];
                 
                 for(NSDictionary *dict in mArray)
                 {
                 NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                 
                 NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                 [dictToSave removeObjectsForKeys:keysForNullValues];
                 
                 [arrRate addObject:dictToSave];
                 }
                 NSLog(@"arrRate :%@",arrRate);

                 [plistDict setValue:arrRate forKey:@"moreArray"];
                 [plistDict writeToFile:finalPath atomically:YES];
                 
                 }
                 else
                 {
                 arrRate = [NSMutableArray new];
                 
                 for(NSDictionary *dict in mArray)
                 {
                 NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                 
                 NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                 [dictToSave removeObjectsForKeys:keysForNullValues];
                 
                 [arrRate addObject:dictToSave];
                 }
                 NSLog(@"arrRate :%@",arrRate);
                 
                 [plistDict setValue:arrRate forKey:@"moreArray"];
                 [plistDict writeToFile:finalPath atomically:YES];
                 }
                 }
                 
                 
                 ///////
                 
                
                
            }
            
            
            [_mTblVw reloadData];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return mArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 250;
    }
    else
        return 150;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSString *descptn;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
    descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
    
    descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    
    UIFont *font = [UIFont systemFontOfSize:16.0];
    
    CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    
    CGSize size = textRect.size;
    
    if(section == 0)
        return 0;//size.height;
    else
    return 20;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        NSString *descptn;
        NSInteger height;
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
            descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
        
        descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        
        UIFont *font = [UIFont systemFontOfSize:16.0];
        
        CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        
        CGSize size = textRect.size;
        
            height = size.height;
        
        UILabel *headrLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, _mTblVw.frame.size.width-10, height)];
        headrLbl.text = descptn;
        headrLbl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
        headrLbl.textAlignment = NSTextAlignmentLeft;
        //headrLbl.backgroundColor = pinkLightColorBG;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        view.backgroundColor = [UIColor whiteColor];
        [view addSubview:headrLbl];
       
        return view ;
    }
    else
    {
      UIView *v = [UIView new];
      [v setBackgroundColor:[UIColor clearColor]];
      return v;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier =@"gl";
    
    galleryListTableViewCell *cell = (galleryListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"galleryListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.titleLbl.text = [[mArray objectAtIndex:section]valueForKey:@"name"];
    
    cell.descptnLbl.text = [[mArray objectAtIndex:section]valueForKey:@"description"];
    
    [cell.descptnLbl sizeToFit];
    
    cell.descptnLbl.lineBreakMode = NSLineBreakByClipping;
    
    //cell.cellImg = [[mArray objectAtIndex:section]valueForKey:@"image"];
    [cell.imageThumb sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    cell.imageThumb.layer.cornerRadius = 10.0;
    cell.imageThumb.clipsToBounds = YES;
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *idval = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"id"]];
    
    [[NSUserDefaults standardUserDefaults]setObject:idval forKey:@"galleryVCId"];
    
    NSString *listHeader = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"name"]];
    
    [[NSUserDefaults standardUserDefaults]setObject:listHeader forKey:@"listHeader"];
    
    galleryVC *dVC = [[galleryVC alloc]initWithNibName:@"galleryVC" bundle:nil];
    [self.navigationController pushViewController:dVC animated:YES];
}

# pragma mark Button Actions

- (IBAction)btnSearchActn:(id)sender
{
     [self stopAudio];
    
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnBackAction:(id)sender
{
     [self stopAudio];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)homeAction:(id)sender
{
     [self stopAudio];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)helpAction
{
     [self stopAudio];
    
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
}

- (IBAction)menuBtnAction:(id)sender
{
     [self stopAudio];
    
    sideMenuViewController *svc=[[sideMenuViewController alloc]init];
    svc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    svc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:svc animated:YES completion:nil];
}

- (IBAction)contactUs:(id)sender
{
     [self stopAudio];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                [rvc presentViewController: lgVC animated: NO completion:nil];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        //[vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [rvc presentViewController: alert animated: NO completion:nil];
    }
    else
    {
    sendMessageViewController *smVC = [[sendMessageViewController alloc]initWithNibName:@"sendMessageViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
    }
}

-(void)playAudio
{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    // if(isFrmLogin || isFrmUpdate)
    {
        // playedOnce = YES;
        
        //  isFrmUpdate = NO;
        
    }
    
    if(playedOnce)
    {
        _speakerImg.hidden = YES;
        
        playedOnce = NO;
        // CGFloat halfButtonHeight = _playBtn.bounds.size.height ;
        // CGFloat buttonWidth = _playBtn.bounds.size.width;
        indicator.center = _playBtn.center;
        [self.view addSubview:indicator];
        [indicator startAnimating];
        
        NSInteger langid = 5;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
            langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
        
        int r = arc4random_uniform(10000);
        
        NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&counts=%d",baseUrl,(long)langid,r];
        
        NSLog(@"URL :%@", wUrl);
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = 100.0;
        sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
       {
           NSDictionary *json;
           
           if(data ==nil)
           {
               [self playAudio];
               return ;
           }
           
           json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
           
           NSLog(@"json :%@", json);
           
           dispatch_async(dispatch_get_main_queue(), ^{
               
               
               NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
               [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
               [def synchronize];
               
               
               //download audio
               NSArray *audioUrlArr = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"audio"];
               
               if([audioUrlArr isKindOfClass:[NSString class]])
               {
                   
               }
               else
               {
                   if(audioUrlArr.count >0)
                   {
                       NSString *url;
                       
                       if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                           url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                       
                       NSString *mainId;
                       
                       if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
                       {
                           mainId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
                       }
                       
                       for(NSString *page in audioUrlArr)
                       {
                           if([[page valueForKey:@"page"]caseInsensitiveCompare:mainId]==NSOrderedSame)
                           {
                               
                               url = [page valueForKey:@"file"];
                               
                               NSURL *urlT = [[NSURL alloc] initWithString:url];
                               NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                               NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                               NSString *filePath = [documents stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",mainId]];
                               BOOL success = [soundData writeToFile:filePath atomically:YES];
                               
                               if(success)
                               {
                                   _speakerImg.hidden = NO;
                                   
                                   [indicator removeFromSuperview];
                                   [indicator stopAnimating];
                                   
                                   NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                                   NSData *data = [def objectForKey:@"settings"];
                                   NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                                   
                                   NSArray *audioUrlArr = [[retrievedDictionary valueForKey:@"audio"]objectAtIndex:0];
                                   NSString *path;
                                   
                                   NSString *url;
                                   
                                   NSString *mainId;
                                   
                                   if([audioUrlArr isKindOfClass:[NSString class]])
                                   {
                                       return;
                                   }
                                   else
                                   {
                                       if(audioUrlArr.count >0)
                                       {
                                           if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
                                           {
                                               mainId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
                                           }
                                           
                                           if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                                               url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                                           
                                           for(NSString *page in audioUrlArr)
                                           {
                                               if([[page valueForKey:@"page"]caseInsensitiveCompare:mainId]==NSOrderedSame)
                                               {
                                                   url = [page valueForKey:@"file"];
                                               }
                                           }
                                           
                                           path = [NSString stringWithFormat:@"%@", url];
                                       }
                                       
                                       else
                                           path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
                                   }
                                   
                                   NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                                   NSString *soundFilePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",mainId]];
                                   
                                   
                                   NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
                                   NSError *error = nil;
                                   
                                   _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
                                   _audioPlayer.delegate=self;
                                   
                                   
                                   if(!(_audioPlayer) || (_audioPlayer == nil))
                                   {
                                       [_audioPlayer play];
                                       
                                       [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                                   }
                                   
                                   else
                                   {
                                       
                                       {
                                           if((_speakerImg.image !=nil) && [_speakerImg.image isEqual:[UIImage imageNamed:@"ico-speaker-close.png"]])
                                           {
                                               NSLog(@"player is playing");
                                               
                                               [_audioPlayer pause];
                                               [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
                                           }
                                           else
                                           {
                                               NSLog(@"player not playing");
                                               
                                               [_audioPlayer play];
                                               
                                               [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                                           }
                                           
                                       }
                                   }
                                   
                               }
                               break;
                           }
                       }
                   }
                   
               }
               
           });
       }];
        
        [dataTask resume];
        
        
    }
    else
    {
        _speakerImg.hidden = NO;
        
        [indicator removeFromSuperview];
        [indicator stopAnimating];
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:@"settings"];
        NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        NSArray *audioUrlArr = [[retrievedDictionary valueForKey:@"audio"]objectAtIndex:0];
        NSString *path;
        
        NSString *url;
        
        NSString *mainId;
        
        if([audioUrlArr isKindOfClass:[NSString class]])
        {
            return;
        }
        else
        {
            if(audioUrlArr.count >0)
            {
                if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
                {
                    mainId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
                }
                
                if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                    url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                
                for(NSString *page in audioUrlArr)
                {
                    if([[page valueForKey:@"page"]caseInsensitiveCompare:mainId]==NSOrderedSame)
                    {
                        url = [page valueForKey:@"file"];
                    }
                }
                
                path = [NSString stringWithFormat:@"%@", url];
            }
            
            else
                path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
        }
        
        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *soundFilePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",mainId]];
        
        
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
        NSError *error = nil;
        
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
        _audioPlayer.delegate=self;
        
        
        if(!(_audioPlayer) || (_audioPlayer == nil))
        {
            [_audioPlayer play];
            
            [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
        }
        
        else
        {
            
            {
                if((_speakerImg.image !=nil) && [_speakerImg.image isEqual:[UIImage imageNamed:@"ico-speaker-close.png"]])
                {
                    NSLog(@"player is playing");
                    
                    [_audioPlayer pause];
                    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
                }
                else
                {
                    NSLog(@"player not playing");
                    
                    [_audioPlayer play];
                    
                    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                }
                
            }
        }
        
    }
    
}

-(void)stopAudio
{
    [_audioPlayer pause];
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
}

-(void)bubbleClicked
{
    [self playAudio];
    //[TransprentView removeFromSuperview];
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
   /*
    NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    */
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
    
}

@end
