//
//  moListViewController.h
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface galleryListingViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UITableView *mTblVw;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UILabel *subHeaderLbl;
@property (strong, nonatomic) IBOutlet UIButton *helpBtn;
@property (strong, nonatomic) IBOutlet UIImageView *helpImg;
@property (strong, nonatomic) IBOutlet UILabel *helpLbl;

@property (strong, nonatomic) IBOutlet UIImageView *speakerImg;
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnContactUs;

@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@property (strong, nonatomic) IBOutlet UILabel *headerLblT;
@property (strong, nonatomic) IBOutlet UILabel *headerSubLblT;

@property(nonatomic) BOOL isFrmBkmd;

@end
