//
//  moListViewController.m
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "articleListViewController.h"
#import "detailViewController.h"
#import "Constants.h"
#import "moTableViewCell.h"
#import "UIImageView+JMImageCache.h"
#import "searchViewController.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"

#import "JMImageCache.h"
#import "UIImageView+WebCache.h"

#import "articleListTableViewCell.h"
#import "articledetailViewController.h"
#import "popUpVideoViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "UIButton+WebCache.h"

#import "homeViewController.h"

@interface articleListViewController ()
{
    NSArray *mArray;
}
@end

@implementation articleListViewController
@synthesize isfrmDetail;

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *admobId,*adStatus;
    
    isfrmDetail = NO;
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isfrmDetail"];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
     adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        
        [self.adVw loadRequest:[GADRequest request]];
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
        UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
      //  [self.view addSubview:customAdBanner];
        
       
    }
    
    mArray = [[NSArray alloc]init];
    
    if(_isFrmBkmd)
    {
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            mArray = [plistDict objectForKey:@"rateArray"];
        }
        
        [self.mTblVw reloadData];
    }
    else
    [self listMWebservice];
    
    
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"articlelistMainHeader"])
        _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"articlelistMainHeader"];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"articlelistHeader"])
    _subHeaderLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"articlelistHeader"];
    
    
    [_helpBtn addTarget:self action:@selector(helpAction) forControlEvents:UIControlEventTouchUpInside];
    
    
   /* id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Article List Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    */
}

-(void)viewWillAppear:(BOOL)animated
{
    NSString *str ;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"isfrmDetail"])
    {
        str = [[NSUserDefaults standardUserDefaults]objectForKey:@"isfrmDetail"];
    }
    
    if([str caseInsensitiveCompare:@"yes"]==NSOrderedSame)
    {
      [self listMWebservice];
    }
    
    if(_isFrmBkmd)
  {
    mArray = [[NSArray alloc]init];
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        mArray = [plistDict objectForKey:@"rateArray"];
    }
    
    [self.mTblVw reloadData];
      
   }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
        
        CGRect frameC = _mTblVw.frame;
        frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
        _mTblVw.frame = frameC;
    }
    
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
        UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
       
    }
}

# pragma mark WebService Calls

-(void)listMWebservice
{
    // article_full_list.php
   // NSInteger listid;
   // listid = [[NSUserDefaults standardUserDefaults]integerForKey:@"articlelistid"];
    
    NSInteger listid;
    listid = [[NSUserDefaults standardUserDefaults]integerForKey:@"listid"];
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@com_article_list.php?cat_id=%ld&lang_id=%ld",baseUrl,(long)listid,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self listMWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            mArray = [[NSArray alloc]init];
            
            if(!([[[json valueForKey:@"result"]objectAtIndex:0]valueForKey:@"error"]))
            {
                mArray = [json objectForKey:@"result"];
            }
            else
            {
                
            }
            
            
            
           // if([mArray count]>0)
            {
                if([[[json valueForKey:@"result"]objectAtIndex:0]valueForKey:@"error"])
                {
                    [self.view makeToast:[[[json valueForKey:@"result"]objectAtIndex:0]valueForKey:@"error"]
                                duration:1.0
                                position:CSToastPositionBottom];
                    
                    [_mTblVw reloadData];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
                
                
                
                 ////////
                 NSMutableArray *arrRate = [[NSMutableArray alloc]init];
                 NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                 NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                 NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                 NSFileManager * fileManager = [NSFileManager defaultManager];
                 //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                 if([fileManager fileExistsAtPath:finalPath])
                 {
                 NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                 arrRate = [plistDict objectForKey:@"moreArray"];
                 
                 if ([arrRate count] == 0)
                 {
                 arrRate = [NSMutableArray new];
                 
                 for(NSDictionary *dict in mArray)
                 {
                 NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                 
                 NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                 [dictToSave removeObjectsForKeys:keysForNullValues];
                 
                 [arrRate addObject:dictToSave];
                 }
                 NSLog(@"arrRate :%@",arrRate);

                 [plistDict setValue:arrRate forKey:@"moreArray"];
                 [plistDict writeToFile:finalPath atomically:YES];
                 
                 }
                 else
                 {
                 arrRate = [NSMutableArray new];
                 
                 for(NSDictionary *dict in mArray)
                 {
                 NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                 
                 NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                 [dictToSave removeObjectsForKeys:keysForNullValues];
                 
                 [arrRate addObject:dictToSave];
                 }
                 NSLog(@"arrRate :%@",arrRate);
                 
                 [plistDict setValue:arrRate forKey:@"moreArray"];
                 [plistDict writeToFile:finalPath atomically:YES];
                 }
                 }
                 
                 
                 ///////
                 
                
                
            }
            
            
            [_mTblVw reloadData];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return mArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 80;
    }
    else
        return 80;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(section == 0)
        return 0;
    else
        return 10;
    
    NSString *descptn;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
    descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
    
    descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    
    UIFont *font = [UIFont systemFontOfSize:16.0];
    
    CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    
    CGSize size = textRect.size;
    
    if(section == 0)
    return size.height;
    else
    return 20;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        NSString *descptn;
        NSInteger height;
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
            descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
        
        descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        
        UIFont *font = [UIFont systemFontOfSize:16.0];
        
        CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        
        CGSize size = textRect.size;
        
            height = size.height;
        
        UILabel *headrLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, _mTblVw.frame.size.width-10, height)];
        headrLbl.text = @"   Status Updates";
        headrLbl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
        headrLbl.textAlignment = NSTextAlignmentLeft;
        //headrLbl.backgroundColor = pinkLightColorBG;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        view.backgroundColor = [UIColor whiteColor];
        [view addSubview:headrLbl];
       
        return view ;
    }
    else
    {
      UIView *v = [UIView new];
      [v setBackgroundColor:[UIColor clearColor]];
      return v;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier =@"al";
    
    articleListTableViewCell *cell = (articleListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"articleListTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.cellTitle.text = [[mArray objectAtIndex:section]valueForKey:@"title"];
    
    NSString *lang =[[mArray objectAtIndex:section]valueForKey:@"language"];
    
    NSString *authr;
    if([[mArray objectAtIndex:section]valueForKey:@"submitted_by"])
    authr = [[mArray objectAtIndex:section]valueForKey:@"submitted_by"];
    
    NSString *langg = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:langg ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    NSString *by = [languageBundle localizedStringForKey:@"by" value:@"" table:@"strings-english"];
    
    
    cell.authorLbl.text = [NSString stringWithFormat:@"%@ %@",by,authr];
    
    [cell.authorLbl setText:[cell.authorLbl.text uppercaseString]];
    
    cell.langLbl.text = lang;
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger inde = indexPath.section;
    
     [[NSUserDefaults standardUserDefaults]setInteger:inde forKey:@"detsilDidSelIndex"];
    
    
     NSString *idval = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"id"]];
    
    [[NSUserDefaults standardUserDefaults]setInteger:indexPath.section forKey:@"moreArrayId"];
    
     NSString *articleName = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"title"]];
    
    [[NSUserDefaults standardUserDefaults]setObject:articleName forKey:@"articleName"];
    
    
    articledetailViewController *dVC = [[articledetailViewController alloc]init];
    [dVC getDictionaryDetailid:idval :mArray];
    if(_isFrmBkmd)
    {
        dVC.isfrmBkMore = YES;
    }
    else
    dVC.isFrmMore=YES;
    [self.navigationController pushViewController:dVC animated:YES];
}

# pragma mark Button Actions

- (IBAction)btnSearchActn:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)homeAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)helpAction
{
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
  /*  NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
   
   */
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
    
}

@end
