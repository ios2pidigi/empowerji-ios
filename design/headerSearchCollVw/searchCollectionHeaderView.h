//
//  homeCollectionHeaderView.h
//  LiiMR-Technician
//
//  Created by sarath sb on 5/28/18.
//  Copyright © 2018 pidigi. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface searchCollectionHeaderView : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;

@end
