//
//  myFeedViewController.h
//  ssc
//
//  Created by swaroop on 18/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

@interface notificationsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *bkTblVw;

@property (strong, nonatomic) IBOutlet UIView *detailVw;

@property (strong, nonatomic) IBOutlet UILabel *detailHeadr;
@property (nonatomic, strong) IBOutlet YTPlayerView *playerVw;
@property (strong, nonatomic) IBOutlet UILabel *scrollArrowLbl;
@property (strong, nonatomic) IBOutlet UIButton *btnPrev;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UITextView *txtVw;
@property (strong, nonatomic) IBOutlet UIButton *btnBookMk;
@property (strong, nonatomic) IBOutlet UIView *readMoreVw;
@property (strong, nonatomic) IBOutlet UIButton *btnReadArticle;
@property (strong, nonatomic) IBOutlet UILabel *lblReadArticle;
@property (strong, nonatomic) IBOutlet UIWebView *webVw;

@property (strong, nonatomic) IBOutlet UIButton *btnComments;
@property (strong, nonatomic) IBOutlet UIView *commentDetailVw;
@property (strong, nonatomic) IBOutlet UITextView *commentTxtVw;
@property (strong, nonatomic) IBOutlet UILabel *commentDetailHeader;
@property (strong, nonatomic) IBOutlet UITableView *commentsTblVw;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;

@property(nonatomic) BOOL isfrmHome;
@property(nonatomic) BOOL isfrmAppdelegate;

@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@end
