//
//  homeViewController.m
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "cSearchViewController.h"
#import "homeCollectionViewCell.h"
#import "sideMenuViewController.h"
#import "Constants.h"
#import "categoryViewController.h"
#import "SVProgressHUD.h"
#import "searchViewController.h"
#import "listViewController.h"
#import "myFeedViewController.h"
#import "sendMessageViewController.h"
#import "supportViewController.h"
#import "connectivity.h"
#import "UIView+Toast.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "UIImageView+WebCache.h"
#import "moListViewController.h"
#import "galleryListingViewController.h"
@import GoogleMobileAds;

#import "articleListViewController.h"
#import "searchCollectionHeaderView.h"

#import "headerArticleListTableViewCell.h"
#import "moTableViewCell.h"
#import "detailViewController.h"
#import "popUpVideoViewController.h"

#import "loginViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "articleListTableViewCell.h"
#import "articledetailViewController.h"

#import "UIButton+WebCache.h"

#import "homeViewController.h"

@interface cSearchViewController ()<UITableViewDelegate,UITableViewDataSource>
{
   // NSArray *homeArray;
    NSArray *subCatCheckArr;
    
    NSArray *catArr;
    
    AVAudioPlayer *_audioPlayer;
    
    NSMutableArray *firstArray;
    
    NSMutableArray *secondArray;
    
    NSArray *newArray;
    
    UIButton *customAdBanner;
    
    int index;
    
}
@end

@implementation cSearchViewController
@synthesize isFrmCommunity,isfrmHome,isfrmCommunityAndMore;

- (void)viewDidLoad {
    [super viewDidLoad];
    //230, 176, 80a
    
    subCatCheckArr = [[NSArray alloc]init];
    
    [SVProgressHUD setDefaultStyle:(SVProgressHUDStyleDark)];
    [SVProgressHUD setDefaultMaskType:(SVProgressHUDMaskTypeCustom)];
    
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    
    [SVProgressHUD setMaximumDismissTimeInterval:10.0];
    [SVProgressHUD setMinimumDismissTimeInterval:3];
    
    //[self searchWS];
    
   // [self settingsWebservice];
    
    //[self categoryWebService];
    
   // [self subCategoryCheckWebService];
    
   if([[NSUserDefaults standardUserDefaults]objectForKey:@"catName"])
   _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
    else
    _headerLbl.text = @"";
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
    }
    else
    {
        //[self staticPagesWS];
    }
        
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];

//    https://app.empowerji.com/webservice/static_pages.php
    
    [_playBtn addTarget:self action:@selector(playAudio) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnBack addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    _searchBarTF.layer.borderColor = [UIColor whiteColor].CGColor;
    _searchBarTF.layer.borderWidth = 1.0;
    
    [_btnMenu addTarget:self action:@selector(menuBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnContactUs addTarget:self action:@selector(homeActn:) forControlEvents:UIControlEventTouchUpInside];
    
    _collectionVw.scrollEnabled = NO;
    _mTblVw.scrollEnabled = NO;
    
    [_helpBtn addTarget:self action:@selector(helpAction) forControlEvents:UIControlEventTouchUpInside];
    
    _mTblVw.scrollEnabled = NO;
    _collectionVw.scrollEnabled = NO;
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Search Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if(isfrmHome)
    {
        _headerLbl.text = @"";
    }
    
    customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
   // gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor blackColor].CGColor];
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    _supportBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    //Rgb2UIColor(0, 93, 155);
    _supportBtn.layer.borderWidth= 2.0f;
    
    _keepGoingBtn.layer.borderColor=[UIColor whiteColor].CGColor;//Rgb2UIColor(0, 93, 155);
    _keepGoingBtn.layer.borderWidth= 2.0f;
    
    
    _btnMenu.layer.cornerRadius = _btnMenu.frame.size.height/2;
    _btnContactUs.layer.cornerRadius = _btnContactUs.frame.size.height/2;
    
    //make menu btn background gradient
    CAGradientLayer *gradientbtn = [CAGradientLayer layer];
    gradientbtn.frame = _btnMenu.bounds;
    gradientbtn.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnMenu.layer insertSublayer:gradientbtn atIndex:0];
    
    //make contactUs btn background gradient
    CAGradientLayer *gradientbtn2 = [CAGradientLayer layer];
    gradientbtn2.frame = _btnContactUs.bounds;
    gradientbtn2.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnContactUs.layer insertSublayer:gradientbtn2 atIndex:0];
    
    
    
    ////
    int heig; int tHeig;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 2.0;
    
    if(IS_IPAD)
    {
        heig = 250;
    }
    else
        heig = cellWidth-25;
    
    
    if(IS_IPAD)
    {
        tHeig = 120;
    }
    else
        tHeig = 90;
    
    NSInteger j=0;
    if(firstArray.count > 0)
    {
        if(firstArray.count == 1)
            j = 1;
        else
            j = (firstArray.count/2);
        
    }
    
    CGRect cFrame = _collectionVw.frame;
    if(firstArray.count>0)
        cFrame.size.height = (heig * j)+80;
    else
        cFrame.size.height = heig * j;
    _collectionVw.frame = cFrame;
    
    _collectionVw.bounces = NO;
    _mTblVw.bounces = NO;
    
    CGRect tFrame = _mTblVw.frame;
    
    if(secondArray.count>0)
        tFrame.size.height = tHeig * secondArray.count+80;
    else
        tFrame.size.height = tHeig * secondArray.count;
    
    tFrame.origin.y = cFrame.origin.y + cFrame.size.height;
    _mTblVw.frame = tFrame;
    
    _scrollVw.contentSize = CGSizeMake(_scrollVw.frame.size.width, cFrame.size.height + tFrame.size.height + 70 + 70 );
    
    _mTblVw.scrollEnabled = NO;
    _collectionVw.scrollEnabled = NO;
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
        
        CGRect frameC = _scrollVw.frame;
        frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
        _scrollVw.frame = frameC;
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
      //  UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        customAdBanner.frame = _adVw.frame;
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
       // [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
   //[self setStatusBarBackgroundColor:[UIColor colorWithRed:(230/255.0) green:(176/255.0) blue:(80/255.0) alpha:1.0]];
    
    
    ////////
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count > 0)
    {
        // "custom_banner_arr"
        
        //prefetch advw images and interstitial
        
        NSMutableArray *custom_banner_arr = [[NSMutableArray alloc]init];
        
        
        NSArray *custom_banner_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_banner_arr"];
        
        for(NSString *arr in custom_banner_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_banner"];
            [custom_banner_arr addObject:url];
        }
        
        // uint32_t rnd = arc4random_uniform([custom_banner_arr count]);
        
        index = arc4random_uniform(custom_banner_arr2.count);
        
        NSString *randomObject = [custom_banner_arr objectAtIndex:index];
        
        NSLog(@"rand:%u, rnd:%@",index,randomObject);
        
        NSLog(@"custom_banner url :%@",randomObject);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject forKey:@"custom_banner_url"];
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:randomObject] forState:UIControlStateNormal];
        
        /////
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark WebService Calls

-(void)searchWS :(NSString*)catid :(NSString*)searchStr
{
    //app.empowerji.com/webservice3/search.php?cat_id=3&text=a
    
    searchStr = [searchStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSString *mainCatId;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
        mainCatId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
    
    catid = mainCatId;
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@search.php?cat_id=%@&text=%@&lang_id=%ld",baseUrl,catid,searchStr,langidd];
    
    //app.empowerji.com/webservice4/home_search.php?cat_id=&text=how+to+book&lang_id=5
    
    if(isfrmHome)
    {
      wUrl=[NSString stringWithFormat:@"%@home_search.php?cat_id=%@&text=%@&lang_id=%ld",baseUrl,catid,searchStr,langidd];
    }
    
    if(isfrmCommunityAndMore)
    {
        //more_search.php?cat_id=59,60,1&text=silver&lang_id=5
        wUrl=[NSString stringWithFormat:@"%@more_search.php?cat_id=59,60,1&text=%@&lang_id=%ld",baseUrl/*,catid*/,searchStr,langidd];
    }
        
    [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidFrmSearch"];
    
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self searchWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            firstArray = [[NSMutableArray alloc]init];
            secondArray = [[NSMutableArray alloc]init];
            
            if(!([[[json valueForKey:@"categories"]objectAtIndex:0]valueForKey:@"message"]))
            {
                firstArray = [json valueForKey:@"categories"];
            }
            else
            {
                
            }
            
            if(!([[[json valueForKey:@"articles"]objectAtIndex:0]valueForKey:@"message"]))
            {
              secondArray = [json valueForKey:@"articles"];
            }
            else
            {
                
            }
            
            
          newArray = firstArray?[firstArray arrayByAddingObjectsFromArray:secondArray]:[[NSArray alloc] initWithArray:secondArray];
            
            NSLog(@"newArray :%@",newArray);
            
            NSLog(@"first count :%lu",(unsigned long)firstArray.count);
            NSLog(@"second count :%lu",(unsigned long)secondArray.count);
            
            if(newArray.count == 0)
            {
                [self.view makeToast:[[[json valueForKey:@"categories"]objectAtIndex:0]valueForKey:@"message"]
                            duration:1.0
                            position:CSToastPositionBottom];
                
                [SVProgressHUD dismiss];
                
                double delayInSeconds = 1.5;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                   
                    [self back];
                });
                
                return ;
            }
            
            [_collectionVw reloadData];
            
            [_mTblVw reloadData];
            
            
            ////
            int heig; int tHeig;
            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width;
            float cellWidth = screenWidth / 2.0;
            
            if(IS_IPAD)
            {
                heig = 250;
            }
            else
                heig = cellWidth-25;
            
            
            if(IS_IPAD)
            {
                tHeig = 120;
            }
            else
                tHeig = 90;
            
            NSInteger j=0;
            if(firstArray.count > 0)
            {
                if(firstArray.count == 1)
                  j = 1;
                else
                  j = (firstArray.count/2);
                
            }
            
            ///collectionView cell interspace
            int interSpace;
            if(IS_IPAD)
            {
                interSpace = 40.0;
            }
            else
                interSpace = 15.0;
            ///
            
            CGRect cFrame = _collectionVw.frame;
            if(firstArray.count>0)
            {
                if(firstArray.count == 1)
                {
                    cFrame.size.height = ((heig * (j))+80+(interSpace*(j+1)));
                }
                else
                {
                 if((firstArray.count)%2 == 0)
                 {
                  cFrame.size.height = ((heig * (j))+80+(interSpace*(j+1)));
                 }
                 else
                 cFrame.size.height = ((heig * (j+1))+80+(interSpace*(j+1)));
                 }
            }
            else
                cFrame.size.height = heig * j;
            
            _collectionVw.frame = cFrame;
            
            _collectionVw.bounces = NO;
            _mTblVw.bounces = NO;
            
            CGRect tFrame = _mTblVw.frame;
            
            if(secondArray.count>0)
            tFrame.size.height = tHeig * secondArray.count+80 +(20 * secondArray.count) ;
            else
                tFrame.size.height = tHeig * secondArray.count;
            
            tFrame.origin.y = cFrame.origin.y + cFrame.size.height;
            _mTblVw.frame = tFrame;
            
            _scrollVw.contentSize = CGSizeMake(_scrollVw.frame.size.width, (cFrame.size.height + tFrame.size.height));
            
            CGRect frameS = _scrollVw.frame;
            frameS.size.height = _helpBtn.frame.origin.y;
            //_scrollVw.frame = frameS;
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)settingsWebservice
{
  
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid =@"";
    }
    else
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@",baseUrl,(long)langid,userid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self settingsWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
                [def synchronize];
            
            NSString *admobId; NSString *adStatus;
            admobId = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"admob_id"];
            
             adStatus = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"admob_status"];
            
            //[GADMobileAds configureWithApplicationID:admobId];
            
            _adVw.adSize = kGADAdSizeSmartBannerPortrait;
            self.adVw.adUnitID = admobId;//@"ca-app-pub-8214158784518238/4464482105";
            self.adVw.rootViewController = self;
            
            if([adStatus isEqualToString:@"Yes"])
            {
                CGRect frameH = _helpImg.frame;
                frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
                _helpImg.frame = frameH;
                
                CGRect frameB = _helpBtn.frame;
                frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
                _helpBtn.frame = frameB;
                
                [self.helpLbl setCenter:_helpBtn.center];
                
                
                [self.adVw loadRequest:[GADRequest request]];
            }
            
            else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
            {
                CGRect frameH = _helpImg.frame;
                frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
                _helpImg.frame = frameH;
                
                CGRect frameB = _helpBtn.frame;
                frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
                _helpBtn.frame = frameB;
                
                [self.helpLbl setCenter:_helpBtn.center];
                
                //[self.adVw loadRequest:[GADRequest request]];
                
                NSString *custom_banner = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"custom_banner"]];
                
                
                customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
                
                [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
                
                
                [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
                
                
               // [self.view addSubview:customAdBanner];
                
                
            }
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)categoryWebService
{
    //http://listlinkz.com/ssc/webservice/categories.php?cat_id=4
    
    [SVProgressHUD show];
    
    NSString *catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@categories.php?cat_id=%@",baseUrl,catid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
   {
       NSDictionary *json;
       
       if(data ==nil)
       {
           [self categoryWebService];
           dispatch_async(dispatch_get_main_queue(), ^{
               //[self.view makeToastActivity:CSToastPositionCenter];
               [SVProgressHUD dismiss];
           });
           return ;
       }
       
       json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
       
       NSLog(@"json :%@", json);
       
       dispatch_async(dispatch_get_main_queue(), ^{
           
           catArr = [[NSArray alloc]init];
           
           catArr = [json objectForKey:@"categories"];
           
           [_collectionVw reloadData];
           
           dispatch_async(dispatch_get_main_queue(), ^{
               //[self.view makeToastActivity:CSToastPositionCenter];
               [SVProgressHUD dismiss];
           });
       });
   }];
    
    [dataTask resume];
}

-(void)subCategoryCheckWebService
{
  //http://listlinkz.com/ssc/webservice/check_sub_exists.php
    
    NSString *wUrl=[NSString stringWithFormat:@"%@check_sub_exists.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self subCategoryCheckWebService];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            subCatCheckArr = [[json objectForKey:@"result"]objectAtIndex:0];
            
           //result
            
        });
    }];
    
    [dataTask resume];
    
    
}

-(void)staticPagesWS
{
    NSString *wUrl=[NSString stringWithFormat:@"%@static_pages.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self staticPagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
          //  NSArray *staticArr = [[json objectForKey:@"result"]objectAtIndex:0];
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"result"]] forKey:@"staticArrContents"];
            [def synchronize];
            //result
            
          /*  NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            NSData *data = [def objectForKey:@"staticArrContents"];
            NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
           */
            
        });
    }];
    
    [dataTask resume];
}
# pragma mark Button Actions

- (IBAction)menuBtnAction:(id)sender
{
    sideMenuViewController *svc=[[sideMenuViewController alloc]init];
    svc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    svc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:svc animated:YES completion:nil];
}

- (IBAction)searchAction:(id)sender
{
    //_searchBar.hidden=NO;
    //[_searchBar becomeFirstResponder];
    
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnSelectionAction:(id)sender
{
    myFeedViewController *mfVC = [[myFeedViewController alloc]initWithNibName:@"myFeedViewController" bundle:nil];
    mfVC.isfrmHome = YES;
    [self.navigationController pushViewController:mfVC animated:YES];
}
- (IBAction)homeActn:(id)sender
{
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                [rvc presentViewController: lgVC animated: NO completion:nil];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        //[vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [rvc presentViewController: alert animated: NO completion:nil];
    }
    else
    {
        
    sendMessageViewController *smVC = [[sendMessageViewController alloc]initWithNibName:@"sendMessageViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
    }
}

- (IBAction)supportBtnAction:(id)sender
{
    supportViewController *smVC = [[supportViewController alloc]initWithNibName:@"supportViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}
- (IBAction)keepGoingbtnAction:(id)sender
{
    supportViewController *smVC = [[supportViewController alloc]initWithNibName:@"supportViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}

-(void)playAudio
{
    // Construct URL to sound file
    NSString *path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    [_audioPlayer play];
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)helpAction
{
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
  /*  NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
   */
    
}

#pragma mark SearchBar Cancel Action

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    _searchBar.hidden=YES;
}
#pragma mark status bar color

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}


#pragma mark UICollectionView methods

- (CGSize)itemSize
{
    NSInteger numberOfColumns = 2;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionVw.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return firstArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"home";
    
    UINib *nib = [UINib nibWithNibName:@"homeCollectionViewCell" bundle: nil];
    
    [collectionView registerNib:nib forCellWithReuseIdentifier:simpleTableIdentifier];
    
    NSInteger section = indexPath.row;
    
   homeCollectionViewCell *cell = (homeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    NSString *title;
    
    if([[newArray objectAtIndex:section]valueForKey:@"name"])
       title = [[newArray objectAtIndex:section]valueForKey:@"name"];
    else if([[newArray objectAtIndex:section]valueForKey:@"title"])
            title = [[newArray objectAtIndex:section]valueForKey:@"title"];
    
    cell.homeLbl.text = title;
  
    [cell.homeImg1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[newArray objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    
    [cell.homeLbl setText:[cell.homeLbl.text uppercaseString]];
    
    [cell layoutSubviews];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    
    cell.homeImg.layer.cornerRadius = 10;
   // cell.homeImg1.layer.cornerRadius = 10;
    
   // [cell.homeImg.layer setBorderColor:[UIColor whiteColor].CGColor];
   // [cell.homeImg.layer setBorderWidth:2.0f];
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
    {
        CGRect frame;
        frame = cell.homeImg1.frame;
        frame.size.width = frame.size.width ;//-2
        
        cell.homeImg1.frame = frame;
        
        cell.homeImg.hidden = YES;
        
       // [[cell.homeImg1 layer] setBorderWidth:2.0f];
        //[[cell.homeImg1 layer] setBorderColor:[UIColor whiteColor].CGColor];
        
        ///corner radius to imageview top left and top right
        UIBezierPath *maskPath = [UIBezierPath
                                  bezierPathWithRoundedRect:cell.homeImg1.bounds
                                  byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                  cornerRadii:CGSizeMake(10, 10)
                                  ];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        
        maskLayer.frame = self.view.bounds;
        maskLayer.path = maskPath.CGPath;
        
        cell.homeImg1.layer.mask = maskLayer;
        /////
        // [[cell.homeImg1 layer] setBorderWidth:2.0f];
        // [[cell.homeImg1 layer] setBorderColor:[UIColor whiteColor].CGColor];
        
        
        /////corner radius to label bottom left and bottom right
        UIBezierPath *maskPath2 = [UIBezierPath
                                   bezierPathWithRoundedRect:cell.paddingLbl.bounds
                                   byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                   cornerRadii:CGSizeMake(10, 10)
                                   ];
        CAShapeLayer *maskLayer2 = [CAShapeLayer layer];
        maskLayer2.frame = self.view.bounds;
        maskLayer2.path = maskPath2.CGPath;
        cell.paddingLbl.layer.mask = maskLayer2;
        ///
    }
    
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *catid =[[newArray objectAtIndex:indexPath.row]valueForKey:@"id"];
    
    [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidFromHomeCat"];
    
    NSString *catName =[[newArray objectAtIndex:indexPath.row]valueForKey:@"name"];
    
    if(!([catid caseInsensitiveCompare:@"64"]==NSOrderedSame))
    [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"catName"];
    
    NSString *subExist = [[newArray objectAtIndex:indexPath.row]valueForKey:@"sub_exists"];
    
    NSString *descrptn = [[newArray objectAtIndex:indexPath.row]valueForKey:@"description"];
    
     [[NSUserDefaults standardUserDefaults]setObject:descrptn forKey:@"catDescription"];
    
    if([catid caseInsensitiveCompare:@"64"]==NSOrderedSame)
    {
        NSString *subCatName =[[newArray objectAtIndex:indexPath.row]valueForKey:@"name"];
        
        [[NSUserDefaults standardUserDefaults]setObject:subCatName forKey:@"subCatName"];
        
        galleryListingViewController *glVC = [[galleryListingViewController alloc]initWithNibName:@"galleryListingViewController" bundle:nil];
        [self.navigationController pushViewController:glVC animated:YES];
    }
    else if([catid caseInsensitiveCompare:@"63"]==NSOrderedSame)
    {
        
    }
    else
    {
    if([subExist caseInsensitiveCompare:@"yes"]==NSOrderedSame)
    {
       
    }
    else
    {
        if(isFrmCommunity)
        {
            [[NSUserDefaults standardUserDefaults] setInteger:[catid integerValue] forKey:@"articlelistid"];
            
            articleListViewController *atVC = [[articleListViewController alloc]initWithNibName:@"articleListViewController" bundle:nil];
            [self.navigationController pushViewController:atVC animated:YES];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setInteger:[catid integerValue] forKey:@"listid"];
            
            [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"MOcatName"];
            
         moListViewController *liVC = [[moListViewController alloc]initWithNibName:@"moListViewController" bundle:nil];
         [self.navigationController pushViewController:liVC animated:YES];
        }
    }
    }
    return;
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    
   // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              
              
             
                  
    if([urlData length] == 0)
    {
        NSLog(@"internet is not connected");
        
       
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
       NSLog(@"internet is connected");
        
       
        
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable )
        {
            //connection unavailable
            
            [self showMessage:errorMessage withTitle:nil];
        }
        else
        {
            
            NSInteger ctag;
          //  ctag = [[[homeArray objectAtIndex:indexPath.row]valueForKey:@"tag"]integerValue];
            
           // [[NSUserDefaults standardUserDefaults]setInteger:ctag forKey:@"catTag"];
            
           // [[NSUserDefaults standardUserDefaults]setObject:[[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"] forKey:@"HomeClickedCellName"];
            
            NSString *subexist;
        
            switch (ctag) {
                    
                case 2:
                    //experience
                {
                    subexist = @"experience_sub";
                    
                    break;
                }
                case 3:
                    //quick learn
                {
                    subexist = @"quicklearn_sub";
                    
                    break;
                }
                case 4:
                    //news
                {
                    subexist = @"news_sub_exists";
                    
                    break;
                }
                case 5:
                    //health
                {
                    subexist = @"health_sub_exists";
                    
                    break;
                }
                default:
                    break;
            }
            
            NSLog(@"%@",subCatCheckArr);
            //subCatCheckArr
            
            @try {
                
                
                if([[subCatCheckArr valueForKey:subexist] isEqualToString:@"yes"])
                {
                    categoryViewController *catVC = [[categoryViewController alloc]initWithNibName:@"categoryViewController" bundle:nil];
                    [self.navigationController pushViewController:catVC animated:YES];
                }
                else
                {
                   // NSString *name;
                    
                  //  name = [[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"];
                    
                   // [[NSUserDefaults standardUserDefaults]setInteger:ctag forKey:@"listid"];
                    
                   // [[NSUserDefaults standardUserDefaults]setObject:name forKey:@"listname"];
                    
                    
                    listViewController *lVC = [[listViewController alloc]initWithNibName:@"listViewController" bundle:nil];
                    [self.navigationController pushViewController:lVC animated:YES];
                }
                
            }
            @catch (NSException *exception)
            {
                [self showMessage:errorMessage withTitle:nil];
            }
            
            
        }
    }
    
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
          
                         
                         });
          
          
      }] resume];
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
     if(IS_IPAD)
     {
        return 40.0;
     }
    else
    return 15.0; //0.01;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 40.0;
    }
    else
        return 0.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 2.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    
    CGSize size ;
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
//    if(pathToFirstRow==indexPath)
//    {
//       float cellWidth2 = screenWidth / 1;
//
//
//        if(IS_IPAD)
//        {
//            size = CGSizeMake(555, 250);//cellWidth-84
//        }
//        else
//            size = CGSizeMake(cellWidth2-35, cellWidth-25); //162
//    }
//    else
    {
        if(IS_IPAD)
        {
            size = CGSizeMake(258, 250);//cellWidth-84
        }
        else
        {
            if(screenWidth  == 320 )
            {
                //iphone 5s
                size = CGSizeMake(cellWidth-15, cellWidth-15);
            }
            else
                size = CGSizeMake(cellWidth-50, cellWidth-20); //162
            
           // size = CGSizeMake(cellWidth-25, cellWidth+15); //162
        }
    }
    
    
    return size;
}


- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    
    if(IS_IPAD)
    {
        return UIEdgeInsetsMake(0, 105, 0, 105);
    }
    else
    {
        if(screenWidth  == 320 )
            return UIEdgeInsetsMake(0, 5, 0, 5);
        else
            return UIEdgeInsetsMake(0, 35, 0, 35); // top, left, bottom, right
        
      //return UIEdgeInsetsMake(0, 15, 0, 15); // top, left, bottom, right
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    
    return CGSizeMake(self.collectionVw.frame.size.width, 50.f);
    
    if((section == 0) && (firstArray.count >0))
    {
        return CGSizeMake(self.collectionVw.frame.size.width, 50.f);
    }
    else
    {
        if((firstArray.count ==0))
        {
            if((section == 0) && (secondArray.count >0))
            {
                return CGSizeMake(self.collectionVw.frame.size.width, 50.f);
            }
            else
                return CGSizeMake(0,0);
        }
        else if((firstArray.count >0))
        {
            if((secondArray.count >0) && ([[newArray objectAtIndex:section]valueForKey:@"title"]))
            {
                return CGSizeMake(self.collectionVw.frame.size.width, 50.f);
            }
            else
                return CGSizeMake(0,0);
        }
        else
        {
             return CGSizeMake(0,0);
        }
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    UINib *headerNib = [UINib nibWithNibName:@"searchCollectionHeaderView" bundle:nil];
    
    [collectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"searchHead"];
    
    
    searchCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"searchHead" forIndexPath:indexPath];
    
    
    if (kind == UICollectionElementKindSectionHeader)
    {
        reusableview = headerView;
        
    }
    
    if (kind == UICollectionElementKindSectionFooter)
    {
        
        return nil;
        
    }
    
    return reusableview;
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return secondArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    NSString *lId = [NSString stringWithFormat:@"%@",[[secondArray objectAtIndex:section]valueForKey:@"id"]];
    
    if([lId caseInsensitiveCompare:@"0"]==NSOrderedSame)
    {
        return 20;
    }
    else
    {
        if(IS_IPAD)
        {
            return 120;
        }
        else
            return 90;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(section == 0)
        return 60;
    else
        return 20;
    
    
    NSString *descptn;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
        descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
    
    descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    
    UIFont *font = [UIFont systemFontOfSize:16.0];
    
    CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    
    CGSize size = textRect.size;
    
    if(section == 0)
        return size.height;
    else
        return 20;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    if(section == 0)
    {
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *articles = [languageBundle localizedStringForKey:@"articles" value:@"" table:@"strings-english"];
        
        UILabel *headrLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, _mTblVw.frame.size.width-10, 50)];
        headrLbl.text = articles;
        headrLbl.textColor = [UIColor whiteColor];
        headrLbl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
        headrLbl.textAlignment = NSTextAlignmentCenter;
        headrLbl.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(61/255.0) blue:(102/255.0) alpha:1.0];//(51, 163, 255)
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        view.backgroundColor = [UIColor clearColor];
        [view addSubview:headrLbl];
        
        return view ;
    }
    else
    {
        UIView *v = [UIView new];
        [v setBackgroundColor:[UIColor clearColor]];
        return v;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    NSString *lId = [NSString stringWithFormat:@"%@",[[secondArray objectAtIndex:section]valueForKey:@"id"]];
    
    if([lId caseInsensitiveCompare:@"0"]==NSOrderedSame)
    {
        static NSString *CellIdentifier = @"header";
        headerArticleListTableViewCell *cellH = (headerArticleListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cellH.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cellH == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"headerArticleListTableViewCell" owner:self options:nil];
            cellH = [nib objectAtIndex:0];
        }
        
        //NSInteger section = indexPath.section;
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *otherLng = [languageBundle localizedStringForKey:@"otherLanguage" value:@"" table:@"strings-english"];
        
        cellH.cellTitle.text = otherLng;
        
        cellH.clipsToBounds = NO;
        cellH.layer.masksToBounds = NO;
        
        return cellH;
        
    }
    else
    {
        NSString *catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFrmSearch"];
        catid = @"";
        if([catid caseInsensitiveCompare:@"1"]==NSOrderedSame)
        {
            
            static NSString *CellIdentifier =@"al";
            
            articleListTableViewCell *cell = (articleListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"articleListTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            NSInteger section = indexPath.section;
            
            cell.cellTitle.text = [[secondArray objectAtIndex:section]valueForKey:@"title"];
            
            NSString *lang =[[secondArray objectAtIndex:section]valueForKey:@"language"];
            
            NSString *authr;
            if([[secondArray objectAtIndex:section]valueForKey:@"submitted_by"])
                authr = [[secondArray objectAtIndex:section]valueForKey:@"submitted_by"];
            
            NSString *langg = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:langg ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *by = [languageBundle localizedStringForKey:@"by" value:@"" table:@"strings-english"];
            
            
            cell.authorLbl.text = [NSString stringWithFormat:@"%@ %@",by,authr];
            
            [cell.authorLbl setText:[cell.authorLbl.text uppercaseString]];
            
            cell.langLbl.text = lang;
            
            cell.clipsToBounds = NO;
            cell.layer.masksToBounds = NO;
            
            return cell;
            
        }
        
        else
        {
            static NSString *CellIdentifier =@"more";
            
            moTableViewCell *cell = (moTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"moTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            NSInteger section = indexPath.section;
            
            cell.cellTitle.text = [[secondArray objectAtIndex:section]valueForKey:@"title"];
            
            [cell.cellImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[secondArray objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
            
            //[cell.cellImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"image"]]] placeholder:[UIImage imageNamed:@"logo"]];
            
            NSString *lang =[[secondArray objectAtIndex:section]valueForKey:@"language"];
            
            cell.langLbl.text = lang;
            
            CGRect frame = cell.langLbl.frame;
            frame.size.width = cell.langLbl.intrinsicContentSize.width;
            //cell.langLbl.frame = frame;
            
            cell.clipsToBounds = NO;
            cell.layer.masksToBounds = NO;
            
            return cell;
        }
        
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *idval = [NSString stringWithFormat:@"%@",[[secondArray objectAtIndex:indexPath.section] valueForKey:@"id"]];
    
    [[NSUserDefaults standardUserDefaults]setInteger:indexPath.section forKey:@"moreArrayId"];
    
    NSString *catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFrmSearch"];
    
  /*  if([catid caseInsensitiveCompare:@"1"]==NSOrderedSame)
    {
        NSString *name = [NSString stringWithFormat:@"%@",[[secondArray objectAtIndex:indexPath.section] valueForKey:@"title"]];
        
         [[NSUserDefaults standardUserDefaults]setObject:@"Community" forKey:@"articlelistHeader"];
        
        articledetailViewController *dVC = [[articledetailViewController alloc]init];
        dVC.isFrmCSearch = YES;
        [dVC getDictionaryDetailid:idval :secondArray];
        [self.navigationController pushViewController:dVC animated:YES];
    }
    else
        */
    {
     detailViewController *dVC = [[detailViewController alloc]init];
     [dVC getDictionaryDetailid:idval];
     [self.navigationController pushViewController:dVC animated:YES];
    }
}


# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

# pragma mark check for internet Ping

-(BOOL)iConnected
{
   __block BOOL connectin;
    NSString *wUrl=[NSString stringWithFormat:@"%@static_pages.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self staticPagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
    
         if(json == nil)
         {
             connectin = NO;
         }
        else
        {
            connectin = YES;
        }
    }];
    
    [dataTask resume];
    
    return connectin;
}

@end
