//
//  homeViewController.h
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface cSearchViewController : UIViewController
@property (strong, nonatomic) IBOutlet UICollectionView *collectionVw;
//@property (strong, nonatomic) IBOutlet UIView *adVw;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIButton *supportBtn;
@property (strong, nonatomic) IBOutlet UIButton *keepGoingBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@property (strong, nonatomic) IBOutlet UITextField *searchBarTF;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnContactUs;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (strong, nonatomic) IBOutlet UITableView *mTblVw;
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UIButton *helpBtn;
@property (strong, nonatomic) IBOutlet UIImageView *helpImg;
@property (strong, nonatomic) IBOutlet UILabel *helpLbl;

@property(nonatomic)BOOL isFrmCommunity;
@property(nonatomic)BOOL isfrmHome;
@property(nonatomic)BOOL isfrmCommunityAndMore;

-(void)searchWS :(NSString*)catid :(NSString*)searchStr;

@end
