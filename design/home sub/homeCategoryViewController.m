//
//  homeViewController.m
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "homeCategoryViewController.h"
#import "homeCollectionViewCell.h"
#import "sideMenuViewController.h"
#import "Constants.h"
#import "categoryViewController.h"
#import "SVProgressHUD.h"
#import "searchViewController.h"
#import "listViewController.h"
#import "myFeedViewController.h"
#import "sendMessageViewController.h"
#import "supportViewController.h"
#import "connectivity.h"
#import "UIView+Toast.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "UIImageView+WebCache.h"
#import "moListViewController.h"
#import "galleryListingViewController.h"
@import GoogleMobileAds;

#import "articleListViewController.h"
#import "cSearchViewController.h"
#import "popUpVideoViewController.h"

#import "loginViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "communityViewController.h"

#import "UIButton+WebCache.h"

#import "homeViewController.h"

@interface homeCategoryViewController ()<AVAudioPlayerDelegate>
{
   // NSArray *homeArray;
    NSArray *subCatCheckArr;
    
    NSArray *catArr;
    
   // AVAudioPlayer *_audioPlayer;
    AVPlayer *player;
    
    UIView* TransprentView;
    UIButton *hLbl;
    
    UIButton *btn;
    
    UIButton *dummyBackBtn;
    
    BOOL playedOnce;
    
    BOOL once;
    
    int index;
    
    UIButton *customAdBanner;
    
}
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@end

@implementation homeCategoryViewController
@synthesize isFrmCommunity,isFrmHome,isFrmCommunityNMore;

- (void)viewDidLoad {
    [super viewDidLoad];
    //230, 176, 80a
    once = NO;
    subCatCheckArr = [[NSArray alloc]init];
    
    [SVProgressHUD setDefaultStyle:(SVProgressHUDStyleDark)];
    [SVProgressHUD setDefaultMaskType:(SVProgressHUDMaskTypeCustom)];
    
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    
    [SVProgressHUD setMaximumDismissTimeInterval:10.0];
    [SVProgressHUD setMinimumDismissTimeInterval:3];
    
   // [self settingsWebservice];
    
    [self categoryWebService];
    
   // [self subCategoryCheckWebService];
    
   if([[NSUserDefaults standardUserDefaults]objectForKey:@"catName"])
   _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
    else
    _headerLbl.text = @"";
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
    }
    else
    {
        //[self staticPagesWS];
    }
        
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];

//    https://app.empowerji.com/webservice/static_pages.php
    
    [_playBtn addTarget:self action:@selector(playAudio) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnBack addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    _searchBarTF.layer.borderColor = [UIColor whiteColor].CGColor;
    _searchBarTF.layer.borderWidth = 1.0;
    
    [_btnMenu addTarget:self action:@selector(menuBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnContactUs addTarget:self action:@selector(homeActn:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSearch addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_helpBtn addTarget:self action:@selector(helpAction) forControlEvents:UIControlEventTouchUpInside];
    
    if(isFrmHome)
    {
        _subHeaderLbl.text = @"";
    }
    else
    _subHeaderLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
    
    _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCatName"];
    
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"HomeCategory Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    //comment this to show help bubble
   // [[NSUserDefaults standardUserDefaults]setObject:@"firstInstallHomeCategory" forKey:@"firstInstallHomeCategory"];
    
    //show help view
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"firstInstallHomeCategory"])
    {
        // [[NSUserDefaults standardUserDefaults]setObject:@"firstInstall" forKey:@"firstInstall"];
        
        // get window screen size for frame
        CGRect screensize = [[UIScreen mainScreen] bounds];
        //create a new UIview with the same size for same view
        TransprentView = [[UIView alloc] initWithFrame:screensize];
        
        // change the background color to black and the opacity to 0.6 for Transparent
        // TransprentView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        
        // here add your Label, Image, etc, what ever you want
        
        hLbl = [[UIButton alloc]initWithFrame:CGRectMake(((screensize.size.width/2)-(screensize.size.width/2)), ((screensize.size.height/2)-(50/2)), screensize.size.width, 50)];
        [hLbl setTitle:@"Click Here for Help" forState:UIControlStateNormal]; //↪ ⤻ ⇧
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *helpText = [languageBundle localizedStringForKey:@"helpText" value:@"" table:@"strings-english"];
        [hLbl setTitle:helpText forState:UIControlStateNormal];
        
        
        hLbl.titleLabel.textColor = [UIColor whiteColor];
        hLbl.backgroundColor = [UIColor clearColor];
        hLbl.titleLabel.textAlignment = NSTextAlignmentCenter;
        hLbl.titleLabel.numberOfLines = 0;
        if(IS_IPAD)
        {
            /*[hLbl setBackgroundImage:[UIImage imageNamed:@"bubble-2"] forState:UIControlStateNormal];
            hLbl.titleLabel.font = [UIFont systemFontOfSize:18.0 weight:UIFontWeightBold];
             */
            
            hLbl.backgroundColor = blueColorBtn;
            hLbl.titleLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightBold];
            hLbl.layer.cornerRadius = 5.0;
            hLbl.clipsToBounds = YES;
        }
        else
        {
            //[hLbl setBackgroundImage:[UIImage imageNamed:@"bubble-2"] forState:UIControlStateNormal];
           // hLbl.titleLabel.font = [UIFont systemFontOfSize:16.0 weight:UIFontWeightBold];
            
            hLbl.backgroundColor = blueColorBtn;
            hLbl.titleLabel.font = [UIFont systemFontOfSize:10.0 weight:UIFontWeightBold];
            hLbl.layer.cornerRadius = 5.0;
            hLbl.clipsToBounds = YES;
        }
        
        [hLbl addTarget:self action:@selector(bubbleClicked) forControlEvents:UIControlEventTouchUpInside];
        
        hLbl.titleLabel.alpha = 1;
        [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
            hLbl.titleLabel.alpha = 0;
        } completion:nil];
        
        [self.view addSubview:hLbl];
        
        //add a button above hlbl to play audio
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor = [UIColor clearColor];
        btn.frame = _playBtn.frame;
        [btn addTarget:self action:@selector(bubbleClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        
        // add this TransprentView to your main view
        //[self.view addSubview:TransprentView];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationIsActive:)name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnteredForeground:)name:UIApplicationWillEnterForegroundNotification object:nil];
    
    _collectionVw.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    
    _counterLbl.textAlignment = NSTextAlignmentCenter;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cacheUpdated:) name:@"MyCacheUpdatedNotification2" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification2" object:self];
    
    
    customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
    
}

- (void)cacheUpdated:(NSNotification *)notification
{
    [self resetCounter];
}

- (void)applicationIsActive:(NSNotification *)notification
{
    NSLog(@"Application Did Become Active");
    
    hLbl.titleLabel.alpha = 1;
}

- (void)applicationEnteredForeground:(NSNotification *)notification
{
    NSLog(@"Application Entered Foreground");
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGRect frameHeader1 = _headerLbl.frame;
    frameHeader1.size.width = _headerLbl.intrinsicContentSize.width + 20;
    frameHeader1.origin.x = ([UIScreen mainScreen].bounds.size.width/2) - (frameHeader1.size.width/2);
    _headerLbl.frame = frameHeader1;
    
    CGRect frameSpeaker = _speakerImg.frame;
    frameSpeaker.origin.x = frameHeader1.origin.x + frameHeader1.size.width;
    _speakerImg.frame = frameSpeaker;
    
    CGRect frameSpeakerBtn = _playBtn.frame;
    frameSpeakerBtn.origin.x = frameHeader1.origin.x + frameHeader1.size.width;
    _playBtn.frame = frameSpeakerBtn;
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
   // gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor blackColor].CGColor];
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    _supportBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    //Rgb2UIColor(0, 93, 155);
    _supportBtn.layer.borderWidth= 2.0f;
    
    _keepGoingBtn.layer.borderColor=[UIColor whiteColor].CGColor;//Rgb2UIColor(0, 93, 155);
    _keepGoingBtn.layer.borderWidth= 2.0f;
    
    
    _btnMenu.layer.cornerRadius = _btnMenu.frame.size.height/2;
    _btnContactUs.layer.cornerRadius = _btnContactUs.frame.size.height/2;
    
    //make menu btn background gradient
    CAGradientLayer *gradientbtn = [CAGradientLayer layer];
    gradientbtn.frame = _btnMenu.bounds;
    gradientbtn.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnMenu.layer insertSublayer:gradientbtn atIndex:0];
    
    //make contactUs btn background gradient
    CAGradientLayer *gradientbtn2 = [CAGradientLayer layer];
    gradientbtn2.frame = _btnContactUs.bounds;
    gradientbtn2.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnContactUs.layer insertSublayer:gradientbtn2 atIndex:0];
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
        
        if(!once)
        {
            once = YES;
        CGRect frameC = _collectionVw.frame;
        frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
        _collectionVw.frame = frameC;
        }
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
        //UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
       // [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
        
    }
    
    CGRect frameHelpLbl = hLbl.frame;
    if(IS_IPAD)
        frameHelpLbl.origin.x = _playBtn.frame.origin.x;//_speakerImg.frame.origin.x - ((([UIScreen mainScreen].bounds.size.width - _playBtn.frame.origin.x)/2)+20);
    else
        frameHelpLbl.origin.x = _playBtn.frame.origin.x; //(([UIScreen mainScreen].bounds.size.width - _speakerImg.frame.origin.x));
    
    frameHelpLbl.origin.y = _playBtn.frame.origin.y + _playBtn.frame.size.height;
    
    if(IS_IPAD)
        frameHelpLbl.size.width = [UIScreen mainScreen].bounds.size.width - frameHelpLbl.origin.x;
    else
        frameHelpLbl.size.width = [UIScreen mainScreen].bounds.size.width - (frameHelpLbl.origin.x);
    
    frameHelpLbl.size.height = 500;
    hLbl.frame = frameHelpLbl;
    [hLbl sizeToFit];
    
    hLbl.titleEdgeInsets = UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f);
    
    btn.frame = _playBtn.frame;
    dummyBackBtn.frame = _btnBack.frame;
    
    hLbl.titleLabel.alpha = 1;
    [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
        hLbl.titleLabel.alpha = 0;
    } completion:nil];

    
    NSString *catid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"]];
    
    
    if(([catid caseInsensitiveCompare:@"1"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"3"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"4"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"5"]==NSOrderedSame))
    {
        
    }
    else
    {
        _speakerImg.hidden = YES;
        _playBtn.hidden = YES;
        btn.hidden = YES;
        [hLbl removeFromSuperview];
    }
    
    
    [_helpLbl sizeToFit];
    
     _counterLbl.layer.cornerRadius = _counterLbl.frame.size.height/2;
    
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_audioPlayer pause];
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
    [self removeTransparentView];
}

static NSInteger countG;

-(void)viewWillAppear:(BOOL)animated
{
   //[self setStatusBarBackgroundColor:[UIColor colorWithRed:(230/255.0) green:(176/255.0) blue:(80/255.0) alpha:1.0]];
    
    playedOnce = YES;
    
    if(countG > 5)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"firstInstallHomeCategory" forKey:@"firstInstallHomeCategory"];
        
        [hLbl removeFromSuperview];
    }
    else
    {
        [self.view addSubview:hLbl];
    }
    
    countG ++;
    
    
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger count = 0;
    
    if([userDefaults integerForKey:@"notificationCount"])
    {
        count = [userDefaults integerForKey:@"notificationCount"];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0; //count;
    
    
    // NSInteger badgeNum;
    //badgeNum = [UIApplication sharedApplication].applicationIconBadgeNumber;
    // _counterLbl.text = [NSString stringWithFormat:@"%ld",(long)badgeNum];
    
    _counterLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
    
    if(([_counterLbl.text caseInsensitiveCompare:@"0"]==NSOrderedSame))
    {
        _counterLbl.hidden = YES;
    }
    else
    {
        _counterLbl.hidden = NO;
    }
    
    
    
    ////////
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count > 0)
    {
        // "custom_banner_arr"
        
        //prefetch advw images and interstitial
        
        NSMutableArray *custom_banner_arr = [[NSMutableArray alloc]init];
        
        
        NSArray *custom_banner_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_banner_arr"];
        
        for(NSString *arr in custom_banner_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_banner"];
            [custom_banner_arr addObject:url];
        }
        
        // uint32_t rnd = arc4random_uniform([custom_banner_arr count]);
        
        index = arc4random_uniform(custom_banner_arr2.count);
        
        NSString *randomObject = [custom_banner_arr objectAtIndex:index];
        
        NSLog(@"rand:%u, rnd:%@",index,randomObject);
        
        NSLog(@"custom_banner url :%@",randomObject);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject forKey:@"custom_banner_url"];
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:randomObject] forState:UIControlStateNormal];
        
        /////
        NSMutableArray *custom_inter_arr = [[NSMutableArray alloc]init];
        
        NSArray *custom_inter_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_inter_arr"];
        
        for(NSString *arr in custom_inter_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_inter"];
            [custom_inter_arr addObject:url];
        }
        
        // uint32_t rnd2 = arc4random_uniform([custom_inter_arr count]);
        
        // NSUInteger randomIndex = arc4random() % custom_inter_arr.count;
        
        //  index = arc4random_uniform(custom_inter_arr2.count);
        
        
        NSString *randomObject2 = [custom_inter_arr objectAtIndex:index];
        
        NSLog(@"custom_inter_arr url :%@",randomObject2);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject2 forKey:@"custom_inter"];
        
        
        /////
    }
}

-(void)resetCounter
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger count = 0;
    
    if([userDefaults integerForKey:@"notificationCount"])
    {
        count = [userDefaults integerForKey:@"notificationCount"];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;//count;
    
    
    // NSInteger badgeNum;
    //badgeNum = [UIApplication sharedApplication].applicationIconBadgeNumber;
    // _counterLbl.text = [NSString stringWithFormat:@"%ld",(long)badgeNum];
    
    _counterLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
    
    if(([_counterLbl.text caseInsensitiveCompare:@"0"]==NSOrderedSame))
    {
        _counterLbl.hidden = YES;
    }
    else
    {
        _counterLbl.hidden = NO;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark WebService Calls

-(void)settingsWebservice
{
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
  
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid =@"";
    }
    else
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@",baseUrl,(long)langid,userid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self settingsWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
                [def synchronize];
            
            NSString *admobId; NSString *adStatus;
            admobId = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"admob_id"];
            
             adStatus = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"admob_status"];
            
            //[GADMobileAds configureWithApplicationID:admobId];
            
            _adVw.adSize = kGADAdSizeSmartBannerPortrait;
            self.adVw.adUnitID = admobId;//@"ca-app-pub-8214158784518238/4464482105";
            self.adVw.rootViewController = self;
            
            if([adStatus isEqualToString:@"Yes"])
            {
                CGRect frameH = _helpImg.frame;
                frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
                _helpImg.frame = frameH;
                
                CGRect frameB = _helpBtn.frame;
                frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
                _helpBtn.frame = frameB;
                
                [self.helpLbl setCenter:_helpBtn.center];
                
                
                [self.adVw loadRequest:[GADRequest request]];
            }
            
            else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
            {
                CGRect frameH = _helpImg.frame;
                frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
                _helpImg.frame = frameH;
                
                CGRect frameB = _helpBtn.frame;
                frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
                _helpBtn.frame = frameB;
                
                [self.helpLbl setCenter:_helpBtn.center];
                
                //[self.adVw loadRequest:[GADRequest request]];
                
                NSString *custom_banner = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"custom_banner"]];
                
                
                UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
                
                [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
                
                
                [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
                
                
                [self.view addSubview:customAdBanner];
                
                if(!once)
                {
                    once = YES;
                    CGRect frameC = _collectionVw.frame;
                    frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
                    _collectionVw.frame = frameC;
                }
            }
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)categoryWebService
{
    //http://listlinkz.com/ssc/webservice/categories.php?cat_id=4
    
    [SVProgressHUD show];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@categories.php?cat_id=%@&lang_id=%ld",baseUrl,catid,(long)langid];
    
    if(isFrmCommunityNMore)
    {
        //categories.php?cat_id=59,60,1&lang_id=5
        
        wUrl=[NSString stringWithFormat:@"%@categories.php?cat_id=59,60,1&lang_id=%ld",baseUrl/*,catid*/,(long)langid];
    }
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
   {
       NSDictionary *json;
       
       if(data ==nil)
       {
           [self categoryWebService];
           dispatch_async(dispatch_get_main_queue(), ^{
               //[self.view makeToastActivity:CSToastPositionCenter];
               [SVProgressHUD dismiss];
           });
           return ;
       }
       
       json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
       
       NSLog(@"json :%@", json);
       
       dispatch_async(dispatch_get_main_queue(), ^{
           
           catArr = [[NSArray alloc]init];
           
           if([[[json valueForKey:@"categories"]objectAtIndex:0]valueForKey:@"message"])
           {
               [self.view makeToast:[[[json valueForKey:@"categories"]objectAtIndex:0]valueForKey:@"message"]
                           duration:1.0
                           position:CSToastPositionBottom];
               
               
               //[self back];
               
               [SVProgressHUD dismiss];
               return ;
           }
           
           catArr = [json objectForKey:@"categories"];
           
           [_collectionVw reloadData];
           
           dispatch_async(dispatch_get_main_queue(), ^{
               //[self.view makeToastActivity:CSToastPositionCenter];
               [SVProgressHUD dismiss];
           });
       });
   }];
    
    [dataTask resume];
}

-(void)subCategoryCheckWebService
{
  //http://listlinkz.com/ssc/webservice/check_sub_exists.php
    
    NSString *wUrl=[NSString stringWithFormat:@"%@check_sub_exists.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self subCategoryCheckWebService];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            subCatCheckArr = [[json objectForKey:@"result"]objectAtIndex:0];
            
           //result
            
        });
    }];
    
    [dataTask resume];
    
    
}

-(void)staticPagesWS
{
    NSString *wUrl=[NSString stringWithFormat:@"%@static_pages.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self staticPagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
          //  NSArray *staticArr = [[json objectForKey:@"result"]objectAtIndex:0];
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"result"]] forKey:@"staticArrContents"];
            [def synchronize];
            //result
            
          /*  NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            NSData *data = [def objectForKey:@"staticArrContents"];
            NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
           */
            
        });
    }];
    
    [dataTask resume];
}
# pragma mark Button Actions

-(void)stopAudio
{
    [_audioPlayer pause];
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
}

- (IBAction)menuBtnAction:(id)sender
{
    [self stopAudio];
    
    sideMenuViewController *svc=[[sideMenuViewController alloc]init];
    svc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    svc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:svc animated:YES completion:nil];
}

- (IBAction)searchAction:(id)sender
{
    //_searchBar.hidden=NO;
    //[_searchBar becomeFirstResponder];
    [self stopAudio];
    
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnSelectionAction:(id)sender
{
    [self stopAudio];
    
    myFeedViewController *mfVC = [[myFeedViewController alloc]initWithNibName:@"myFeedViewController" bundle:nil];
    mfVC.isfrmHome = YES;
    [self.navigationController pushViewController:mfVC animated:YES];
}
- (IBAction)homeActn:(id)sender
{
    [self stopAudio];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                [rvc presentViewController: lgVC animated: NO completion:nil];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        //[vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [rvc presentViewController: alert animated: NO completion:nil];
    }
    else
    {
    sendMessageViewController *smVC = [[sendMessageViewController alloc]initWithNibName:@"sendMessageViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
    }
}

- (IBAction)supportBtnAction:(id)sender
{
    [self stopAudio];
    
    supportViewController *smVC = [[supportViewController alloc]initWithNibName:@"supportViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}
- (IBAction)keepGoingbtnAction:(id)sender
{
    [self stopAudio];
    
    supportViewController *smVC = [[supportViewController alloc]initWithNibName:@"supportViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}

-(void)playAudio
{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
   // if(isFrmLogin || isFrmUpdate)
    {
       // playedOnce = YES;
        
      //  isFrmUpdate = NO;
        
    }
    
    if(_audioPlayer.isPlaying)
    {
        [_audioPlayer pause];
        [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
        return;
    }
    
    NSString *str1; NSString *str2;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"langStrHomeCat"])
      str1 = [[NSUserDefaults standardUserDefaults]objectForKey:@"langStrHomeCat"];
    else
        str1 = @"5";
    
    NSInteger langidHomeCat = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidHomeCat = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    str2 = [NSString stringWithFormat:@"%ld",(long)langidHomeCat];
    
    
    
    
    
    if([str2 caseInsensitiveCompare:str1]==NSOrderedSame)
    {
        playedOnce = NO;
    }
    
    
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *langStr;
    langStr = [NSString stringWithFormat:@"%ld",(long)langid];
    
    
    /////
    NSString *fileNames;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
    {
        fileNames = [NSString stringWithFormat:@"%@lang%@.mp3",[[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"],langStr];
    }
    
    if(isFrmCommunity)
    {
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"])
        {
            fileNames = [NSString stringWithFormat:@"communitylang%@.mp3",langStr];
        }
    }
    
    if(isFrmCommunityNMore)
    {
        fileNames = [NSString stringWithFormat:@"morelang%@.mp3",langStr];
    }
    
    
    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    NSString* foofile = [documentsPath stringByAppendingPathComponent:fileNames];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    
    if(fileExists)
    {
        NSLog(@"file exists");
    }
    else
    {
        playedOnce = YES;
    }
    ///////
    
    if(playedOnce)
    {
        _speakerImg.hidden = YES;
        
        playedOnce = NO;
        // CGFloat halfButtonHeight = _playBtn.bounds.size.height ;
        // CGFloat buttonWidth = _playBtn.bounds.size.width;
        indicator.center = _playBtn.center;
        [self.view addSubview:indicator];
        [indicator startAnimating];
        
        NSInteger langid = 5;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
            langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
        
        int r = arc4random_uniform(10000);
        
        NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
        NSData *datauser = [defuser objectForKey:@"userData"];
        NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
        
        NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
        
        NSString *userid;
        
        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
        {
            userid =@"";
        }
        else
            userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
        
        NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
        
        NSString *langStr;
        langStr = [NSString stringWithFormat:@"%ld",(long)langid];
        [[NSUserDefaults standardUserDefaults]setObject:langStr forKey:@"langStrHomeCat"];
        
        NSLog(@"URL :%@", wUrl);
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = 200.0;
        sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
       {
           NSDictionary *json;
           
           if(data ==nil)
           {
               [self playAudio];
               return ;
           }
           
           json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
           
           NSLog(@"json :%@", json);
           
           dispatch_async(dispatch_get_main_queue(), ^{
               
               
               NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
               [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
               [def synchronize];
               
               
               //download audio
               NSArray *audioUrlArr = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"audio"];
               
    if([audioUrlArr isKindOfClass:[NSString class]])
    {
                   
    }
    else
    {
        if(audioUrlArr.count >0)
        {
            NSString *url;
            
            if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
            url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
            
            NSString *mainId;
            
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
            {
                mainId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
            }
            
            if(isFrmCommunity)
            {
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"])
            {
                mainId = @"community"; //[[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
            }
            }
            
            if(isFrmCommunityNMore)
            {
                mainId = @"more";
            }
            
            
          
            BOOL isthere = NO;
            
    for(NSString *page in audioUrlArr)
    {
        if([[page valueForKey:@"page"]caseInsensitiveCompare:mainId]==NSOrderedSame)
        {
          
            isthere = YES;
            
            url = [page valueForKey:@"file"];
                               
            NSURL *urlT = [[NSURL alloc] initWithString:url];
            NSData *soundData = [NSData dataWithContentsOfURL:urlT];
            NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *filePath = [documents stringByAppendingPathComponent:[NSString stringWithFormat:@"%@lang%@.mp3",mainId,langStr]];
    BOOL success = [soundData writeToFile:filePath atomically:YES];
                               
    if(success)
    {
        _speakerImg.hidden = NO;
                                   
        [indicator removeFromSuperview];
        [indicator stopAnimating];
                                                                      
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *audioUrlArr = [[retrievedDictionary valueForKey:@"audio"]objectAtIndex:0];
    NSString *path;
    
    NSString *url;
    
    NSString *mainId;
    
    if([audioUrlArr isKindOfClass:[NSString class]])
    {
        return;
    }
    else
    {
    if(audioUrlArr.count >0)
    {
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
        {
            mainId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
        }
        
        if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
            url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
        
        
        if(isFrmCommunity)
        {
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"])
            {
                mainId = @"community"; //[[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
            }
        }
        
        if(isFrmCommunityNMore)
        {
            mainId = @"more";
        }
        
        for(NSString *page in audioUrlArr)
        {
            if([[page valueForKey:@"page"]caseInsensitiveCompare:mainId]==NSOrderedSame)
            {
                url = [page valueForKey:@"file"];
            }
        }
        
        path = [NSString stringWithFormat:@"%@", url];
    }
    
    else
        path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
    }
    
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *soundFilePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@lang%@.mp3",mainId,langStr]];
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
            NSError *error = nil;
            
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
            _audioPlayer.delegate=self;
            
       
        
   
    
  
    if(!(_audioPlayer) || (_audioPlayer == nil))
    {
        [_audioPlayer play];
        
        [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
    }
    
    else
    {
      
        {
            if((_speakerImg.image !=nil) && [_speakerImg.image isEqual:[UIImage imageNamed:@"ico-speaker-close.png"]])
            {
                NSLog(@"player is playing");
                
                [_audioPlayer pause];
                [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
            }
            else
            {
                NSLog(@"player not playing");
                
                [_audioPlayer play];
                
                [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
            }
            
        }
    }
   
             });
        
    }
    break;
        }
}
            
     if(isthere)
     {
         
     }
     else
     {
         _speakerImg.hidden = NO;
         
         [indicator removeFromSuperview];
         [indicator stopAnimating];
         
         [_audioPlayer pause];
         [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
     }
            
    }
                   
        }
               
    });
       }];
        
        [dataTask resume];
        
        
    }
    else
    {
        _speakerImg.hidden = NO;
        
        [indicator removeFromSuperview];
        [indicator stopAnimating];
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:@"settings"];
        NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        NSArray *audioUrlArr = [[retrievedDictionary valueForKey:@"audio"]objectAtIndex:0];
        NSString *path;
        
        NSString *url;
        
        NSString *mainId;
        
        if([audioUrlArr isKindOfClass:[NSString class]])
        {
            return;
        }
        else
        {
            if(audioUrlArr.count >0)
            {
                if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
                {
                    mainId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
                }
                
                if(isFrmCommunity)
                {
                    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"])
                    {
                        mainId = @"community"; //[[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
                    }
                }
                
                if(isFrmCommunityNMore)
                {
                    mainId = @"more";
                }
                
                if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                    url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                
                for(NSString *page in audioUrlArr)
                {
                    if([[page valueForKey:@"page"]caseInsensitiveCompare:mainId]==NSOrderedSame)
                    {
                        url = [page valueForKey:@"file"];
                    }
                }
                
                path = [NSString stringWithFormat:@"%@", url];
            }
            
            else
                path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
        }
        
        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *soundFilePath = [documentDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@lang%@.mp3",mainId,langStr]];
        
        
       
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
            NSError *error = nil;
            
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
            _audioPlayer.delegate=self;
        
            
            
        
            
        
        
        
        
        if(!(_audioPlayer) || (_audioPlayer == nil))
        {
            [_audioPlayer play];
            
            [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
        }
        
        else
        {
            
            {
                if((_speakerImg.image !=nil) && [_speakerImg.image isEqual:[UIImage imageNamed:@"ico-speaker-close.png"]])
                {
                    NSLog(@"player is playing");
                    
                    [_audioPlayer pause];
                    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
                }
                else
                {
                    NSLog(@"player not playing");
                    
                    [_audioPlayer play];
                    
                    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                }
                
            }
        }
        
            });
        
    }
    
}
    
-(void)back
{
     [self stopAudio];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)helpAction
{
    [self stopAudio];
    
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
  //[self.navigationController pushViewController:homeVC animated:YES];

    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];

}

-(void)searchAction
{
    [self stopAudio];
    
    [self.view endEditing:YES];
    
    NSString *searchStr; NSString *catid;
    
    searchStr = _searchBarTF.text;
    
    if(([searchStr isEqualToString:@""]) || (searchStr.length == 0) )
    {
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *message = [languageBundle localizedStringForKey:@"searchEmpty" value:@"" table:@"strings-english"];
        
        [self.view makeToast:message
                    duration:1.5
                    position:CSToastPositionBottom];
    }
    else
    {
    catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
    
    cSearchViewController *seVC = [[cSearchViewController alloc]initWithNibName:@"cSearchViewController" bundle:nil];
        if(isFrmCommunityNMore)
        seVC.isfrmCommunityAndMore=YES;
    [seVC searchWS :catid :searchStr];
    [self.navigationController pushViewController:seVC animated:YES];
    }
}

-(void)removeTransparentView
{
    [TransprentView removeFromSuperview];
    
}

-(void)bubbleClicked
{
    [self playAudio];
    [TransprentView removeFromSuperview];
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
   /* NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    */
    
}

#pragma mark avaudio delegate
-(void)itemDidFinishPlaying:(NSNotification *) notification
{
    // Will be called when AVPlayer finishes playing playerItem
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
}


#pragma mark SearchBar Cancel Action

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    _searchBar.hidden=YES;
}
#pragma mark status bar color

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}


#pragma mark UICollectionView methods

- (CGSize)itemSize
{
    NSInteger numberOfColumns = 2;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionVw.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   if(isFrmCommunity)
       return [catArr count] ;
    else
    return [catArr count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"home";
    
    UINib *nib = [UINib nibWithNibName:@"homeCollectionViewCell" bundle: nil];
    
    [collectionView registerNib:nib forCellWithReuseIdentifier:simpleTableIdentifier];
    
    NSInteger section = indexPath.row;
    
   homeCollectionViewCell *cell = (homeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    cell.homeLbl.text = [[catArr objectAtIndex:section]valueForKey:@"name"];
  
    [cell.homeImg1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[catArr objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    //cell.homeImg1.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[catArr objectAtIndex:section]valueForKey:@"image"]]];
    
   // cell.tag = [[[catArr objectAtIndex:section]valueForKey:@"tag"]integerValue];
    
    [cell.homeLbl setText:[cell.homeLbl.text uppercaseString]];
    
    [cell layoutSubviews];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    
    cell.homeImg.layer.cornerRadius = 10;
    
    // cell.homeImg1.layer.cornerRadius = 10;
    
    //    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.homeImg1.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(10, 10) ];
    //
    //    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    //
    //    maskLayer.frame = cell.homeImg1.bounds;
    //    maskLayer.path = maskPath.CGPath;
    //    cell.homeImg1.layer.mask = maskLayer;
    
    
    // [cell.homeImg.layer setBorderColor:[UIColor whiteColor].CGColor];
    // [cell.homeImg.layer setBorderWidth:2.0f];
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
    //    if(pathToFirstRow==indexPath)
    //    {
    //        CGRect frame;
    //        frame = cell.homeImg1.frame;
    //        if(IS_IPAD)
    //        frame.origin.x = cell.homeImg.frame.origin.x+3;
    //        else
    //           frame.origin.x = cell.homeImg.frame.origin.x+2;
    //
    //        cell.homeImg1.frame = frame;
    //
    //        cell.homeImg1.contentMode = UIViewContentModeScaleAspectFill;
    //    }
    //    else
    {
        CGRect frame;
        frame = cell.homeImg1.frame;
        frame.size.width = frame.size.width ;//- 2;
        // frame.size.height = frame.size.width;
        
        cell.homeImg1.frame = frame;
        
        // cell.homeImg.hidden  = YES;
        
        
        
        
        ///corner radius to imageview top left and top right
        UIBezierPath *maskPath = [UIBezierPath
                                  bezierPathWithRoundedRect:cell.homeImg1.bounds
                                  byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                  cornerRadii:CGSizeMake(10, 10)
                                  ];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        
        maskLayer.frame = self.view.bounds;
        maskLayer.path = maskPath.CGPath;
        
        cell.homeImg1.layer.mask = maskLayer;
        /////
        // [[cell.homeImg1 layer] setBorderWidth:2.0f];
        // [[cell.homeImg1 layer] setBorderColor:[UIColor whiteColor].CGColor];
        
        
        /////corner radius to label bottom left and bottom right
        UIBezierPath *maskPath2 = [UIBezierPath
                                   bezierPathWithRoundedRect:cell.paddingLbl.bounds
                                   byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                   cornerRadii:CGSizeMake(10, 10)
                                   ];
        CAShapeLayer *maskLayer2 = [CAShapeLayer layer];
        maskLayer2.frame = self.view.bounds;
        maskLayer2.path = maskPath2.CGPath;
        cell.paddingLbl.layer.mask = maskLayer2;
        ///
        
        
        CGRect padFrame = cell.paddingLbl.frame;
        padFrame.size.height = 40;
        // cell.paddingLbl.frame = padFrame;
        
        //cell.paddingLbl.hidden = NO;
        
        /* cell.layer.masksToBounds = NO;
         cell.layer.shadowOffset = CGSizeMake(-15, 20);
         cell.layer.shadowRadius = 5;
         cell.layer.shadowOpacity = 0.5;
         */
        
        
        cell.homeImg.layer.borderColor = [UIColor whiteColor].CGColor; //cellBorderColor;
        cell.homeImg.layer.borderWidth = 5.0;
        
        // *** Set masks bounds to NO to display shadow visible ***
        cell.layer.masksToBounds = NO;
        // *** Set light gray color as shown in sample ***
        cell.layer.shadowColor = [UIColor whiteColor].CGColor; //[UIColor blackColor].CGColor;
        // *** *** Use following to add Shadow top, left ***
        //cell.layer.shadowOffset = CGSizeMake(-5.0f, -5.0f);
        
        // *** Use following to add Shadow bottom, right ***
        //self.avatarImageView.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
        
        // *** Use following to add Shadow top, left, bottom, right ***
        cell.layer.shadowOffset = CGSizeZero;
        cell.layer.shadowRadius = 5.0f;
        
        // *** Set shadowOpacity to full (1) ***
        cell.layer.shadowOpacity = 0.6f;
        
    }
    
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self stopAudio];
    
    
    NSString *catid =[[catArr objectAtIndex:indexPath.row]valueForKey:@"id"];
    
    [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidFromHomeCat"];
    
    NSString *catName =[[catArr objectAtIndex:indexPath.row]valueForKey:@"name"];
    
    if(!([catid caseInsensitiveCompare:@"64"]==NSOrderedSame))
    [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"catName"];
    
    NSString *subExist = [[catArr objectAtIndex:indexPath.row]valueForKey:@"sub_exists"];
    
    NSString *descrptn = [[catArr objectAtIndex:indexPath.row]valueForKey:@"description"];
    
     [[NSUserDefaults standardUserDefaults]setObject:descrptn forKey:@"catDescription"];
    
    if([catid caseInsensitiveCompare:@"64"]==NSOrderedSame)
    {
        NSString *subCatName =[[catArr objectAtIndex:indexPath.row]valueForKey:@"name"];
        
        [[NSUserDefaults standardUserDefaults]setObject:subCatName forKey:@"subCatName"];
        
        galleryListingViewController *glVC = [[galleryListingViewController alloc]initWithNibName:@"galleryListingViewController" bundle:nil];
        [self.navigationController pushViewController:glVC animated:YES];
    }
    else if([catid caseInsensitiveCompare:@"63"]==NSOrderedSame)
    {
        if([subExist caseInsensitiveCompare:@"yes"]==NSOrderedSame)
        {
        homeCategoryViewController *hcVC = [[homeCategoryViewController alloc]initWithNibName:@"homeCategoryViewController" bundle:nil];
        [self.navigationController pushViewController:hcVC animated:YES];
        }
    }
    else
    {
    if([subExist caseInsensitiveCompare:@"yes"]==NSOrderedSame)
    {
        if([catid caseInsensitiveCompare:@"1"]==NSOrderedSame)
        {
            communityViewController *cVC = [[communityViewController alloc]initWithNibName:@"communityViewController" bundle:nil];
            if(isFrmCommunityNMore)
            {
               // hcVC.isFrmCommunity = YES;
            }
            else
            {
               // hcVC.isFrmCommunity = NO;
            }
            cVC.isFrmHomeC = YES;
            [self.navigationController pushViewController:cVC animated:YES];
        }
        
        else
        {
        homeCategoryViewController *hcVC = [[homeCategoryViewController alloc]initWithNibName:@"homeCategoryViewController" bundle:nil];
        if(isFrmCommunityNMore)
        {
            hcVC.isFrmCommunity = YES;
        }
        else
        {
            hcVC.isFrmCommunity = NO;
        }
        [self.navigationController pushViewController:hcVC animated:YES];
      }
    }
    else
    {
        if(isFrmCommunity)
        {
            
            [[NSUserDefaults standardUserDefaults]setInteger:[catid integerValue] forKey:@"listid"];
            
            [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"MOcatName"];
            
            NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCatName"];
            
            [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"headerTopMO"];
            
            articleListViewController *atqVC = [[articleListViewController alloc]initWithNibName:@"articleListViewController" bundle:nil];
            [self.navigationController pushViewController:atqVC animated:YES];
            
            return;
            
            
            [[NSUserDefaults standardUserDefaults] setInteger:[catid integerValue] forKey:@"articlelistid"];
            
            [[NSUserDefaults standardUserDefaults] setObject:catName forKey:@"articlelistHeader"];
            
            [[NSUserDefaults standardUserDefaults]setObject:_subHeaderLbl.text forKey:@"articlelistMainHeader"];
            
            articleListViewController *atVC = [[articleListViewController alloc]initWithNibName:@"articleListViewController" bundle:nil];
            [self.navigationController pushViewController:atVC animated:YES];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setInteger:[catid integerValue] forKey:@"listid"];
            
            [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"MOcatName"];
            
           NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCatName"];
            
            [[NSUserDefaults standardUserDefaults]setObject:str forKey:@"headerTopMO"];
            
            
            
         moListViewController *liVC = [[moListViewController alloc]initWithNibName:@"moListViewController" bundle:nil];
            liVC.isFrmSubCat = YES;
         [self.navigationController pushViewController:liVC animated:YES];
            
           // articleListViewController *atVC = [[articleListViewController alloc]initWithNibName:@"articleListViewController" bundle:nil];
           // [self.navigationController pushViewController:atVC animated:YES];
        }
    }
    }
    return;
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    
   // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              
              
             
                  
    if([urlData length] == 0)
    {
        NSLog(@"internet is not connected");
        
       
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
       NSLog(@"internet is connected");
        
       
        
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable )
        {
            //connection unavailable
            
            [self showMessage:errorMessage withTitle:nil];
        }
        else
        {
            
            NSInteger ctag;
          //  ctag = [[[homeArray objectAtIndex:indexPath.row]valueForKey:@"tag"]integerValue];
            
           // [[NSUserDefaults standardUserDefaults]setInteger:ctag forKey:@"catTag"];
            
           // [[NSUserDefaults standardUserDefaults]setObject:[[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"] forKey:@"HomeClickedCellName"];
            
            NSString *subexist;
        
            switch (ctag) {
                    
                case 2:
                    //experience
                {
                    subexist = @"experience_sub";
                    
                    break;
                }
                case 3:
                    //quick learn
                {
                    subexist = @"quicklearn_sub";
                    
                    break;
                }
                case 4:
                    //news
                {
                    subexist = @"news_sub_exists";
                    
                    break;
                }
                case 5:
                    //health
                {
                    subexist = @"health_sub_exists";
                    
                    break;
                }
                default:
                    break;
            }
            
            NSLog(@"%@",subCatCheckArr);
            //subCatCheckArr
            
            @try {
                
                
                if([[subCatCheckArr valueForKey:subexist] isEqualToString:@"yes"])
                {
                    categoryViewController *catVC = [[categoryViewController alloc]initWithNibName:@"categoryViewController" bundle:nil];
                    [self.navigationController pushViewController:catVC animated:YES];
                }
                else
                {
                   // NSString *name;
                    
                  //  name = [[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"];
                    
                   // [[NSUserDefaults standardUserDefaults]setInteger:ctag forKey:@"listid"];
                    
                   // [[NSUserDefaults standardUserDefaults]setObject:name forKey:@"listname"];
                    
                    
                    listViewController *lVC = [[listViewController alloc]initWithNibName:@"listViewController" bundle:nil];
                    [self.navigationController pushViewController:lVC animated:YES];
                }
                
            }
            @catch (NSException *exception)
            {
                [self showMessage:errorMessage withTitle:nil];
            }
            
            
        }
    }
    
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
          
                         
                         });
          
          
      }] resume];
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
     if(IS_IPAD)
     {
        return 40.0;
     }
    else
    return 15.0; //0.01;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 40.0;
    }
    else
        return 0.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 2.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    
    CGSize size ;
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
//    if(pathToFirstRow==indexPath)
//    {
//       float cellWidth2 = screenWidth / 1;
//
//
//        if(IS_IPAD)
//        {
//            size = CGSizeMake(555, 250);//cellWidth-84
//        }
//        else
//            size = CGSizeMake(cellWidth2-35, cellWidth-25); //162
//    }
//    else
    {
        if(IS_IPAD)
        {
            size = CGSizeMake(250, 250); //(258, 250);//cellWidth-84
        }
        else
        {
            if(screenWidth  == 320 )
            {
                //iphone 5s
                size = CGSizeMake(cellWidth-15, cellWidth-15);
            }
            else
          size = CGSizeMake(cellWidth-50, cellWidth-20); //CGSizeMake(cellWidth-25, cellWidth+15); //162
        }
    }
    
    
    return size;
}


- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    if(IS_IPAD)
    {
        return UIEdgeInsetsMake(0, 105, 0, 105);
    }
    else
    {
        if(screenWidth  == 320 )
            return UIEdgeInsetsMake(0, 5, 0, 5);
        else
            return UIEdgeInsetsMake(0, 35, 0, 35); // top, left, bottom, right
    }
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

# pragma mark check for internet Ping

-(BOOL)iConnected
{
   __block BOOL connectin;
    NSString *wUrl=[NSString stringWithFormat:@"%@static_pages.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self staticPagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
    
         if(json == nil)
         {
             connectin = NO;
         }
        else
        {
            connectin = YES;
        }
    }];
    
    [dataTask resume];
    
    return connectin;
}

# pragma mark textfield delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
