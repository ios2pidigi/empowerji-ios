//
//  cTableViewCell.h
//  ssc
//
//  Created by swaroop on 20/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLRadioButton.h"

@interface lngTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lnLbl;
@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet DLRadioButton *radioBtn;
@property (strong, nonatomic) IBOutlet UIButton *cellRadioBtn;

@end
