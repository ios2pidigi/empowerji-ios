//
//  popUpVideoViewController.h
//  ssc
//
//  Created by sarath sb on 10/29/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface popUpVideoViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet UIWebView *webVw;

@end

NS_ASSUME_NONNULL_END
