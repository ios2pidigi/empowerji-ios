//
//  popUpVideoViewController.m
//  ssc
//
//  Created by sarath sb on 10/29/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "videoViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface videoViewController ()

@end

@implementation videoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *urlTrial ;
    
    urlTrial= [[NSUserDefaults standardUserDefaults]valueForKey:@"urlTrial"];
    
     NSString *newString1 = [self extractYoutubeIdFromLink:urlTrial];
    
  //  NSString *newString1 = [urlTrial stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
    
    
   // newString1 = [urlTrial stringByReplacingOccurrencesOfString:@"&feature=youtu.be" withString:@""];
    
   
    
    NSDictionary *playerVars = @{
                                 @"origin" :@"http://www.youtube.com",
                                 @"playsinline" : @1,
                                 @"autoplay" : @1,
                                 @"showinfo" : @0,
                                 @"rel" : @0,
                                 @"fs" : @1,
                                 @"modestbranding" : @1,
                                 };
    
    
    
    [_playerVw loadWithVideoId:newString1 playerVars:playerVars];
    
    
    self.webVw.hidden = YES;
    
   
}

- (NSString *)extractYoutubeIdFromLink:(NSString *)link
{
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
        return [link substringWithRange:result.range];
    }
    return nil;
}


- (IBAction)closeAction:(id)sender
{
    //addChildViewController
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end

