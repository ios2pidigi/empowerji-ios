//
//  moListViewController.h
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface moListViewController : UIViewController
//@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UITableView *mTblVw;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UILabel *catNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *headerTop;
@property (strong, nonatomic) IBOutlet UIButton *helpBtn;
@property (strong, nonatomic) IBOutlet UIImageView *helpImg;
@property (strong, nonatomic) IBOutlet UILabel *helpLbl;

@property(nonatomic) BOOL isFrmBkmd;
@property(nonatomic) BOOL isFrmHome;
@property(nonatomic) BOOL isFrmSubCat;

@end
