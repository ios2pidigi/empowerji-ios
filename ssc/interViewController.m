//
//  interViewController.m
//  ssc
//
//  Created by sarath sb on 3/26/19.
//  Copyright © 2019 Pi Digi-Logical Solutions. All rights reserved.
//

#import "interViewController.h"
#import "Constants.h"
#import "JMImageCache.h"
#import "UIImageView+WebCache.h"

@interface interViewController ()
{
    uint32_t rnd2;
    uint32_t rnd;
}
@end

@implementation interViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [interBtn addTarget:self action:@selector(clickedInter) forControlEvents:UIControlEventTouchUpInside];
    
    [closebtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    // "custom_banner_arr"
    
    //prefetch advw images and interstitial
    
    NSMutableArray *custom_banner_arr = [[NSMutableArray alloc]init];
    
    
    NSArray *custom_banner_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_banner_arr"];
    
    for(NSString *arr in custom_banner_arr2)
    {
        NSString *url = [arr valueForKey:@"custom_banner"];
        [custom_banner_arr addObject:url];
    }
    
     rnd = arc4random_uniform([custom_banner_arr count]);
    
    NSString *randomObject = [custom_banner_arr objectAtIndex:rnd];
    
    NSLog(@"rand:%u, rnd:%@",rnd,randomObject);
    
    NSLog(@"custom_banner url :%@",randomObject);
    
    [[NSUserDefaults standardUserDefaults]setObject:randomObject forKey:@"custom_banner_url"];
    
    
    
    /////
    NSMutableArray *custom_inter_arr = [[NSMutableArray alloc]init];
    
    NSArray *custom_inter_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_inter_arr"];
    
    for(NSString *arr in custom_inter_arr2)
    {
        NSString *url = [arr valueForKey:@"custom_inter"];
        [custom_inter_arr addObject:url];
    }
    
     rnd2 = arc4random_uniform([custom_inter_arr count]);
    
    NSString *randomObject2 = [custom_inter_arr objectAtIndex:rnd2];
    
    NSLog(@"custom_inter_arr url :%@",randomObject2);
    
    [[NSUserDefaults standardUserDefaults]setObject:randomObject2 forKey:@"custom_inter"];
    
    
    /////
    
  NSString *custom_inter =  [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_inter"];
    
    [interImgVw sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",custom_inter]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
}

-(void)clicksWS :(NSString *)type :(NSString *)cid
{
    
    //app.empowerji.com/webservice4/clicks.php?id=5&type=offer
    
    //e
    /*
     type=banner
     type=inter
     type=offer
     */
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    int r = arc4random_uniform(10000);
    
    NSString *wUrl=[NSString stringWithFormat:@"%@clicks.php?id=%@&type=%@",baseUrl,cid,type];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
        });
    }];
    
    
    [dataTask resume];
}


-(void)clickedInter
{
    NSString *theUrl, *custom_inter_id;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
     NSArray *custom_inter_arr = [[arrSet objectAtIndex:0]valueForKey:@"custom_inter_arr"];
    
    if(arrSet.count >0)
        custom_inter_id = [[custom_inter_arr objectAtIndex:rnd2]valueForKey:@"custom_inter_id"];
    
    [self clicksWS:@"inter":custom_inter_id];
    
    
    
   // if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_inter_url"])
       // theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_inter_url"];
    
    theUrl = [[custom_inter_arr objectAtIndex:rnd2]valueForKey:@"custom_inter_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
}

-(void)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
