//
//  moListViewController.m
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "moListViewController.h"
#import "detailViewController.h"
#import "Constants.h"
#import "moTableViewCell.h"
#import "UIImageView+JMImageCache.h"
#import "searchViewController.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"

#import "JMImageCache.h"
#import "UIImageView+WebCache.h"
#import "headerArticleListTableViewCell.h"

#import "popUpVideoViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "UIButton+WebCache.h"

#import "homeViewController.h"
#import "inAppViewController.h"

@interface moListViewController ()
{
    NSArray *mArray;
    
    int index;
    
    UIButton *customAdBanner;
}
@end

@implementation moListViewController
@synthesize isFrmBkmd,isFrmHome,isFrmSubCat;

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
     adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        
        [self.adVw loadRequest:[GADRequest request]];
    }
    
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"MOcatName"])
    {
        NSString *catName = [[NSUserDefaults standardUserDefaults]objectForKey:@"MOcatName"];
        _catNameLbl.text = catName;
        
        
      /*  NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        style.alignment = NSTextAlignmentJustified;
        style.firstLineHeadIndent = 10.0f;
        style.headIndent = 10.0f;
        style.tailIndent = -10.0f;
        
        NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:catName attributes:@{ NSParagraphStyleAttributeName : style}];
        
        _catNameLbl.numberOfLines = 0;
        _catNameLbl.attributedText = attrText;
       */
    }
    
    mArray = [[NSArray alloc]init];
    
    
    [self listMWebservice];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"headerTopMO"])
    _headerTop.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"headerTopMO"];
    
     [_helpBtn addTarget:self action:@selector(helpAction) forControlEvents:UIControlEventTouchUpInside];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"MOList Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if(isFrmHome)
    {
       //_catNameLbl.frame = CGRectMake(0, _catNameLbl.frame.origin.y, 0, 0);
       // _headerLbl.frame = CGRectMake(0, _headerTop.frame.origin.y + _headerTop.frame.size.height, 0, 0);
        
        _headerTop.hidden = YES;
        
        NSString *str;
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"headerTopMO"])
        str = [[NSUserDefaults standardUserDefaults]objectForKey:@"headerTopMO"];
        else
            str= @"";
        
        _catNameLbl.text = str;
        
        
        //////
        NSString *descptn;
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
            descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
        
        descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        
        UIFont *font = [UIFont systemFontOfSize:16.0];
        
        CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(self.view.frame.size.width-80, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        
        CGSize size = textRect.size;
        
        ///////
    }
    
    customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
    
}

-(void)viewWillAppear:(BOOL)animated
{
  /*if(_isFrmBkmd)
  {
    mArray = [[NSArray alloc]init];
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        mArray = [plistDict objectForKey:@"rateArray"];
    }
    
    [self.mTblVw reloadData];
      
   }
   */
    
    
    ////////
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count > 0)
    {
        // "custom_banner_arr"
        
        //prefetch advw images and interstitial
        
        NSMutableArray *custom_banner_arr = [[NSMutableArray alloc]init];
        
        
        NSArray *custom_banner_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_banner_arr"];
        
        for(NSString *arr in custom_banner_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_banner"];
            [custom_banner_arr addObject:url];
        }
        
        // uint32_t rnd = arc4random_uniform([custom_banner_arr count]);
        
        index = arc4random_uniform(custom_banner_arr2.count);
        
        NSString *randomObject = [custom_banner_arr objectAtIndex:index];
        
        NSLog(@"rand:%u, rnd:%@",index,randomObject);
        
        NSLog(@"custom_banner url :%@",randomObject);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject forKey:@"custom_banner_url"];
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:randomObject] forState:UIControlStateNormal];
        
        /////
        NSMutableArray *custom_inter_arr = [[NSMutableArray alloc]init];
        
        NSArray *custom_inter_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_inter_arr"];
        
        for(NSString *arr in custom_inter_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_inter"];
            [custom_inter_arr addObject:url];
        }
        
        // uint32_t rnd2 = arc4random_uniform([custom_inter_arr count]);
        
        // NSUInteger randomIndex = arc4random() % custom_inter_arr.count;
        
        //  index = arc4random_uniform(custom_inter_arr2.count);
        
        
        NSString *randomObject2 = [custom_inter_arr objectAtIndex:index];
        
        NSLog(@"custom_inter_arr url :%@",randomObject2);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject2 forKey:@"custom_inter"];
        
        
        /////
    }
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    NSString *descptn;
    NSInteger height;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
        descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
    
    descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    
    UIFont *font =  _headerLbl.font; //[UIFont systemFontOfSize:16.0];
    
    CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(_headerLbl.frame.size.width  , CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    
    CGSize size = textRect.size;
    
    height = size.height;
    
    if([descptn caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        height = 0;
    }
    else
    {
        if(height<=20)
        {
            height = height + 10;
        }
    }
    
    //_headerLbl.text = descptn;
    ////
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentJustified;
    style.firstLineHeadIndent = 10.0f;
    style.headIndent = 10.0f;
    style.tailIndent = -10.0f;
    
    if(descptn == nil)
    {
        descptn = @" ";
    }
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:descptn attributes:@{ NSParagraphStyleAttributeName : style}];
    
    _headerLbl.numberOfLines = 10;
   // _headerLbl.attributedText = attrText;
    
    _headerLbl.text = descptn;
    
    ////
    
    CGRect lblFrame = _headerLbl.frame;
    lblFrame.size.height = height+10;
    _headerLbl.frame = lblFrame;
    
    CGRect frame = _mTblVw.frame;
    frame.origin.y = _headerLbl.frame.origin.y + _headerLbl.frame.size.height;
    
    //if(isFrmHome)
     //   frame.origin.y = _headerLbl.frame.origin.y + _headerLbl.frame.size.height;

    frame.size.height = [UIScreen mainScreen].bounds.size.height - (frame.origin.y + _helpBtn.frame.size.height+10);
    _mTblVw.frame = frame;
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
        
        CGRect frameC = _mTblVw.frame;
        frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
        _mTblVw.frame = frameC;
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
       // UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
    //    [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
        
    }
    
}

# pragma mark WebService Calls

-(void)listMWebservice
{
    // article_full_list.php
    //main_cat_id=3
    NSInteger listid;
    listid = [[NSUserDefaults standardUserDefaults]integerForKey:@"listid"];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *mainCatId;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
        mainCatId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
    
    
    NSArray *arrUser;
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid = @"";
    }
    else
    {
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    }
    
  
    NSString *wUrl=[NSString stringWithFormat:@"%@cat_articles_group.php?cat_id=%ld&lang_id=%ld&main_cat_id=%@&user_id=%@",baseUrl,(long)listid,(long)langid,mainCatId,userid];
    
    //if(isFrmSubCat)
    {
        //wUrl=[NSString stringWithFormat:@"%@com_article_list.php?cat_id=%ld&lang_id=%ld",baseUrl,(long)listid,(long)langid];
    }
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self listMWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            mArray = [json objectForKey:@"result"]; 
            
            if([mArray count]>0)
            {
                if([[mArray objectAtIndex:0]objectForKey:@"error"])
                {
                    [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"error"]
                                duration:1.0
                                position:CSToastPositionBottom];
                    
                    //[_mTblVw reloadData];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
                
                
                
                 ////////
                 NSMutableArray *arrRate = [[NSMutableArray alloc]init];
                 NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                 NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                 NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                 NSFileManager * fileManager = [NSFileManager defaultManager];
                 //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                 if([fileManager fileExistsAtPath:finalPath])
                 {
                 NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                 arrRate = [plistDict objectForKey:@"moreArray"];
                 
                 if ([arrRate count] == 0)
                 {
                 arrRate = [NSMutableArray new];
                 
                 for(NSDictionary *dict in mArray)
                 {
                 NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                 
                 NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                 [dictToSave removeObjectsForKeys:keysForNullValues];
                 
                 [arrRate addObject:dictToSave];
                 }
                 NSLog(@"arrRate :%@",arrRate);

                 [plistDict setValue:arrRate forKey:@"moreArray"];
                 [plistDict writeToFile:finalPath atomically:YES];
                 
                 }
                 else
                 {
                 arrRate = [NSMutableArray new];
                 
                 for(NSDictionary *dict in mArray)
                 {
                 NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                 
                 NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                 [dictToSave removeObjectsForKeys:keysForNullValues];
                 
                 [arrRate addObject:dictToSave];
                 }
                 NSLog(@"arrRate :%@",arrRate);
                 
                 [plistDict setValue:arrRate forKey:@"moreArray"];
                 [plistDict writeToFile:finalPath atomically:YES];
                 }
                 }
                 ///////
            }
            
            
            [_mTblVw reloadData];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return mArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    NSString *lId = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"id"]];
    
    if([lId caseInsensitiveCompare:@"0"]==NSOrderedSame)
    {
        if(IS_IPAD)
            return 10;
        else
        return 5;
    }
    else
    {
        if(IS_IPAD)
        {
            return 130;
        }
        else
            return 110;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if(section == 0)
    {
        if(isFrmHome)
            return 0;
        else
        return 20;
    }
    else
        return 20;
    
    
    NSString *descptn;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
    descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
    
    descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    
    UIFont *font = [UIFont systemFontOfSize:16.0];
    
    CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    
    CGSize size = textRect.size;
    
    if(section == 0)
    return size.height;
    else
    return 20;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
    
    if(section == 0)
    {
        NSString *descptn;
        NSInteger height;
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"])
            descptn = [[NSUserDefaults standardUserDefaults]objectForKey:@"catDescription"];
        
        descptn = [descptn stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        
        UIFont *font = [UIFont systemFontOfSize:16.0];
        
        CGRect textRect = [descptn boundingRectWithSize:CGSizeMake(200, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        
        CGSize size = textRect.size;
        
            height = size.height;
        
        UILabel *headrLbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, _mTblVw.frame.size.width-10, height)];
        headrLbl.text = descptn;
        headrLbl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
        headrLbl.textAlignment = NSTextAlignmentLeft;
        //headrLbl.backgroundColor = pinkLightColorBG;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        view.backgroundColor = [UIColor whiteColor];
        [view addSubview:headrLbl];
       
        return view ;
    }
    else
    {
      UIView *v = [UIView new];
      [v setBackgroundColor:[UIColor clearColor]];
      return v;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    NSString *lId = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"id"]];
    
    if([lId caseInsensitiveCompare:@"0"]==NSOrderedSame)
    {
        static NSString *CellIdentifier = @"header";
        headerArticleListTableViewCell *cellH = (headerArticleListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cellH.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cellH == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"headerArticleListTableViewCell" owner:self options:nil];
            cellH = [nib objectAtIndex:0];
        }
        
        //NSInteger section = indexPath.section;
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *otherLng = [languageBundle localizedStringForKey:@"otherLanguage" value:@"" table:@"strings-english"];
        
       
        cellH.cellTitle.text = otherLng;
        cellH.cellTitle.textAlignment = NSTextAlignmentLeft;
        
        cellH.clipsToBounds = NO;
        cellH.layer.masksToBounds = NO;
        
        return cellH;
        
    }
    else
    {
    
    static NSString *CellIdentifier =@"more";
    
    moTableViewCell *cell = (moTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"moTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.cellTitle.text = [[mArray objectAtIndex:section]valueForKey:@"title"];
    
   [cell.cellImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    //[cell.cellImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"image"]]] placeholder:[UIImage imageNamed:@"logo"]];
    
        NSString *lang =[[mArray objectAtIndex:section]valueForKey:@"language"];
        
        cell.langLbl.text = lang;
        
        cell.langLbl.clipsToBounds = YES;
        cell.langLbl.layer.cornerRadius = 5.0;
        
        CGRect frame = cell.langLbl.frame;
        frame.size.width = cell.langLbl.intrinsicContentSize.width;
        //cell.langLbl.frame = frame;
        
        NSString *langG = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:langG ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *parts = [languageBundle localizedStringForKey:@"parts" value:@"" table:@"strings-english"];
        
        NSString *isgrp = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section] valueForKey:@"is_group"]];
        
        if(([isgrp caseInsensitiveCompare:@"yes"]==NSOrderedSame) && (isgrp != nil))
        {
            NSString *video_count = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"video_count"]];
            cell.lblParts.text = [NSString stringWithFormat:@"(%@ %@)",video_count,parts];
        }
        else
        {
            cell.lblParts.text = @"";
        }
        
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *idval = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"id"]];
    
    if([idval isEqualToString:@"0"])
    {
        return;
    }
    
     NSString *paid = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"paid"]];
    
    if([paid caseInsensitiveCompare:@"paid"]==NSOrderedSame)
    {
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *ok = [languageBundle localizedStringForKey:@"ok" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
         NSString *message = [languageBundle localizedStringForKey:@"alertPurchase" value:@"" table:@"strings-english"];
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                inAppViewController *inAPVC = [[inAppViewController alloc]init];
                [self.navigationController pushViewController:inAPVC animated:YES];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
         [alert addAction:cancel];
        
        [alert addAction:okAction];
       
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
        
        
            
       
    }
    else
    {
    
    NSString *isgrp = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"is_group"]];
    
    [[NSUserDefaults standardUserDefaults]setInteger:indexPath.section forKey:@"moreArrayId"];
    
    detailViewController *dVC = [[detailViewController alloc]init];
    
    if([isgrp caseInsensitiveCompare:@"yes"]==NSOrderedSame)
    {
        dVC.isGroup=YES;
        
        NSString *video_ids = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"video_ids"]];
        [[NSUserDefaults standardUserDefaults]setObject:video_ids forKey:@"grpIDs"];
        
    }
    else
        dVC.isGroup = NO;
    
    [dVC getDictionaryDetailid:idval];
    if(isFrmBkmd)
    {
        dVC.isfrmBkMore = YES;
    }
    else
    dVC.isFrmMore=YES;
    
    
    
    [self.navigationController pushViewController:dVC animated:YES];
        
    }
}

# pragma mark Button Actions

- (IBAction)btnSearchActn:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)homeAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)helpAction
{
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
  /*  NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
   */
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
    
}

@end
