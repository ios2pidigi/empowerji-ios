//
//  forgotPassViewController.h
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface forgotPassViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *fpTF;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@property (nonatomic) BOOL isFrmHom;

@end
