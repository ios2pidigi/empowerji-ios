//
//  moListViewController.h
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@import GoogleMobileAds;

@interface inAppViewController : UIViewController<SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    SKProductsRequest *productsRequest;
    NSArray *validProducts;
    UIActivityIndicatorView *activityIndicatorView;
    IBOutlet UILabel *productTitleLabel;
    IBOutlet UILabel *productDescriptionLabel;
    IBOutlet UILabel *productPriceLabel;
    IBOutlet UIButton *purchaseButton;
}

- (void)fetchAvailableProducts;
- (BOOL)canMakePurchases;
- (void)purchaseMyProduct:(SKProduct*)product;
- (IBAction)purchase:(id)sender;

@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnContactUs;
@property (strong, nonatomic) IBOutlet UILabel *headerLblT;
@property (strong, nonatomic) IBOutlet UILabel *lblExpTop;
@property (strong, nonatomic) IBOutlet UILabel *lblStaticBottom;

@property (strong, nonatomic) IBOutlet UIView *basicVwBg;
@property (strong, nonatomic) IBOutlet UIView *premiumVwBg;
@property (strong, nonatomic) IBOutlet UIButton *btnBasic;
@property (strong, nonatomic) IBOutlet UIButton *btnPre;
@property (strong, nonatomic) IBOutlet UIView *basicDetailVw;
@property (strong, nonatomic) IBOutlet UIView *premDetailVw;
@property (strong, nonatomic) IBOutlet UITableView *tblVw;
@property (strong, nonatomic) IBOutlet UIView *plusVw;
@property (strong, nonatomic) IBOutlet UIButton *btnPurchasePrem;
@property (strong, nonatomic) IBOutlet UIButton *btnPurchasePlus;
@property (strong, nonatomic) IBOutlet UIButton *btnPurchaseBasic;

@property (strong, nonatomic) IBOutlet UIButton *askQnBtn;
@property(nonatomic) BOOL isFrmBkmd;

@property (strong, nonatomic) IBOutlet UIButton *btnAQBD;
@property (strong, nonatomic) IBOutlet UIButton *btnAQPD;
@property (strong, nonatomic) IBOutlet UIButton *btnAQPMD;

@property (strong, nonatomic) IBOutlet UILabel *lblBasicH1;
@property (strong, nonatomic) IBOutlet UILabel *lblBasicH2;
@property (strong, nonatomic) IBOutlet UILabel *lblBasicD1;
@property (strong, nonatomic) IBOutlet UILabel *lblBasicD2;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVwBasic;
@property (strong, nonatomic) IBOutlet UIButton *feature1;
@property (strong, nonatomic) IBOutlet UIButton *feature2;
@property (strong, nonatomic) IBOutlet UIButton *feature3;

@property (strong, nonatomic) IBOutlet UIButton *feature4;

@property (strong, nonatomic) IBOutlet UIButton *feature5;
@property (strong, nonatomic) IBOutlet UIButton *btnBasicPlanVideo;
@property (strong, nonatomic) IBOutlet UILabel *bgGrayBasic;
@property (strong, nonatomic) IBOutlet UIButton *btnDummyBasic;


#pragma mark plus view

@property (strong, nonatomic) IBOutlet UIScrollView *scrollVwPlus;
@property (strong, nonatomic) IBOutlet UIView *whatYouGetView;
@property (strong, nonatomic) IBOutlet UIView *youNeedView;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePlus1;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePlus2;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePlus3;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePlus4;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePlus5;
@property (strong, nonatomic) IBOutlet UIButton *btnPlusPlanVideo;
@property (strong, nonatomic) IBOutlet UIButton *btnTrial;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader1;
@property (strong, nonatomic) IBOutlet UILabel *lblDes1;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader2;
@property (strong, nonatomic) IBOutlet UILabel *lbldes2;
@property (strong, nonatomic) IBOutlet UIImageView *imgP2;
@property (strong, nonatomic) IBOutlet UILabel *bgGrayPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnDummyPlus;


#pragma mark premium view

@property (strong, nonatomic) IBOutlet UIScrollView *scrollVwPrem;
@property (strong, nonatomic) IBOutlet UIView *whatYouGetPremVw;
@property (strong, nonatomic) IBOutlet UIView *youNeedPremVw;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePr1;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePr2;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePr3;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePr4;
@property (strong, nonatomic) IBOutlet UIButton *btnFeaturePr5;
@property (strong, nonatomic) IBOutlet UILabel *lblHeaderPr1;
@property (strong, nonatomic) IBOutlet UILabel *lblDesPr1;
@property (strong, nonatomic) IBOutlet UILabel *lblHeaderPr2;
@property (strong, nonatomic) IBOutlet UILabel *lblDesPr2;
@property (strong, nonatomic) IBOutlet UIImageView *imgPr2;
@property (strong, nonatomic) IBOutlet UIButton *btnPremPlanVideo;
@property (strong, nonatomic) IBOutlet UIButton *btnPremTrial;
@property (strong, nonatomic) IBOutlet UILabel *bgGreyPrem;
@property (strong, nonatomic) IBOutlet UIImageView *imgPr1;
@property (strong, nonatomic) IBOutlet UIButton *btnDummyPremium;
@property (strong, nonatomic) IBOutlet UILabel *proPlusLbl;


@property (strong, nonatomic) IBOutlet UILabel *proPremLbl;

@property (strong, nonatomic) IBOutlet UILabel *proBasicLbl;

@property (strong, nonatomic) IBOutlet UILabel *proBg1;
@property (strong, nonatomic) IBOutlet UILabel *proBg2;
@property (strong, nonatomic) IBOutlet UILabel *proBg3;

@property (strong, nonatomic) IBOutlet UILabel *lblPriceBasic;
@property (strong, nonatomic) IBOutlet UILabel *lblPricePlus;
@property (strong, nonatomic) IBOutlet UILabel *lblPricePrem;

@end
