//
//  forgotPassViewController.m
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "forgotPassViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "connectivity.h"
#import "SVProgressHUD.h"
#import "sideMenuViewController.h"

@interface forgotPassViewController ()

@end

@implementation forgotPassViewController
@synthesize isFrmHom;

- (void)viewDidLoad {
    [super viewDidLoad];
    
     _fpTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _fpTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _fpTF.layer.borderWidth= 1.0f;
    
    _fpTF.layer.cornerRadius = 5.0;
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
    UITouch *touch = [touches anyObject];
    if(![touch.view isKindOfClass:[UITextField class]])
    {
        [self.view endEditing:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark WebService Calls

-(void)fpWebservice
{
    //http://listlinkz.com/ssc/webservice/forgot_password.php?email=test@mail.com
    
    NSString *nameTf;
    
    nameTf = [_fpTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
  //  NSInteger langid = 5;
   // if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
      //  langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *langIdLogIn;
    
    NSString *lang = langGlobal;
    
    NSString *theString = lang;   // The one we want to switch on
    NSArray *items = @[@"other", @"no", @"hi",  @"mr-IN", @"gu-IN", @"en" ];
    NSInteger item = [items indexOfObject:theString];
    switch (item) {
        case 0:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 1:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 2:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 3:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 4:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 5:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        default:
            break;
    }
    
    NSString *wUrl=[NSString stringWithFormat:@"%@forgot_password.php?email=%@&lang_id=%@",baseUrl,nameTf,langIdLogIn];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self fpWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.view endEditing:YES];
            
            NSString *str = [[[json objectForKey:@"response"]objectAtIndex:0]valueForKey:@"message"];
            
            NSString *status = [[[json objectForKey:@"response"]objectAtIndex:0]valueForKey:@"status"];
            
            if([status caseInsensitiveCompare:@"success"] == NSOrderedSame )
            {
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:str
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    //push to login view and clear all the textfieldValues
                    
                    _fpTF.text = @"";
                    [self.view endEditing:YES];
                    [self onBack:nil];
                    
                }];
                [alert addAction:okAction];
                UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
                //[vc presentViewController:alert animated:YES completion:nil];
                
                [self presentViewController: alert animated: NO completion:nil];
                
                // [self showMessage:[[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"signup_status"] withTitle:@"Success"];
            }
            else
            {
                [self.view makeToast:str
                            duration:3.0
                            position:CSToastPositionBottom];
            }
            
            
        });
    }];
    
    [dataTask resume];
    
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
   // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
   // [vc presentViewController:alert animated:YES completion:nil];
    
    UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
    
    if([pvc isKindOfClass:[sideMenuViewController class]])
    {
        [self presentViewController: alert animated: NO completion:nil];
    }
    else
        [self presentViewController: alert animated: NO completion:nil];
}



#pragma mark button action

- (IBAction)btnSubmit:(id)sender
{
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");

                  
    if (!([_fpTF.text length] == 0))
    {
        
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            //connection unavailable
            
            [self showMessage:errorMessage withTitle:nil];
        }
        else
        {
            [self fpWebservice];
        }
    }
    else
    {
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *message = [languageBundle localizedStringForKey:@"emailEmptyLogin" value:@"" table:@"strings-english"];
         NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
        
        [self showMessage:message withTitle:error];
    }
    
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
    
}
- (IBAction)onBack:(id)sender
{
    if(isFrmHom)
    {
     [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Keyboard return

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


@end
