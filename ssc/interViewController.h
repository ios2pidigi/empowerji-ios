//
//  interViewController.h
//  ssc
//
//  Created by sarath sb on 3/26/19.
//  Copyright © 2019 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface interViewController : UIViewController
{
    
    IBOutlet UIButton *closebtn;
    IBOutlet UIImageView *interImgVw;
    IBOutlet UIButton *interBtn;
}
@end

NS_ASSUME_NONNULL_END
