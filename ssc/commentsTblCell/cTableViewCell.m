//
//  cTableViewCell.m
//  ssc
//
//  Created by swaroop on 20/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "cTableViewCell.h"

@implementation cTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
