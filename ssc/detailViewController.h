//
//  detailViewController.h
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"
@import GoogleMobileAds;

@interface detailViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (nonatomic, strong) IBOutlet YTPlayerView *playerVw;
@property (strong, nonatomic) IBOutlet UILabel *scrollArrowLbl;
@property (strong, nonatomic) IBOutlet UIButton *btnPrev;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UITextView *txtVw;
@property (strong, nonatomic) IBOutlet UIButton *btnBookMk;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIButton *readMoreBtn;
@property (strong, nonatomic) IBOutlet UIView *readMoreVw;
@property (strong, nonatomic) IBOutlet UIWebView *webVw;
@property (strong, nonatomic) IBOutlet UILabel *rmHeader;
@property (strong, nonatomic) IBOutlet UILabel *rmBackgroungLbl;
@property (strong, nonatomic) IBOutlet UIButton *btnComments;
@property (strong, nonatomic) IBOutlet UIView *commentDetailVw;
@property (strong, nonatomic) IBOutlet UITextView *commentTxtVw;
@property (strong, nonatomic) IBOutlet UILabel *commentDetailHeader;
@property (strong, nonatomic) IBOutlet UITableView *commentsTblVw;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionVw;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (strong, nonatomic) IBOutlet UIButton *helpBtn;
@property (strong, nonatomic) IBOutlet UIImageView *helpImg;
@property (strong, nonatomic) IBOutlet UILabel *helpLbl;
@property (strong, nonatomic) IBOutlet UILabel *shareLbl;

@property(nonatomic, strong) GADInterstitial *interstitial;

@property(nonatomic) BOOL isFrmMore;
@property(nonatomic) BOOL isFrmSearch;
@property(nonatomic) BOOL isfrmBkMore;

@property(nonatomic) BOOL isGroup;

-(void)getDictionaryDetailid:(NSString *) idHere;

@end
