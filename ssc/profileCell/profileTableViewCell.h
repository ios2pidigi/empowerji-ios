//
//  profileTableViewCell.h
//  ssc
//
//  Created by swaroop on 17/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface profileTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl;
@property (strong, nonatomic) IBOutlet UIButton *btnChk;
@property (strong, nonatomic) IBOutlet UIImageView *chkImg;

@end
