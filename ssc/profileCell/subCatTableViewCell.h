//
//  subCatTableViewCell.h
//  ssc
//
//  Created by swaroop on 17/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface subCatTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *subLbl;
@property (strong, nonatomic) IBOutlet UIImageView *subchkImg;
@property (strong, nonatomic) IBOutlet UIButton *subChkBtn;

@end
