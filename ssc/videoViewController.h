//
//  popUpVideoViewController.h
//  ssc
//
//  Created by sarath sb on 10/29/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface videoViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet YTPlayerView *playerVw;
@property (strong, nonatomic) IBOutlet UIWebView *webVw;

@end

NS_ASSUME_NONNULL_END
