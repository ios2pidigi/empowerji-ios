//
//  galleryListTableViewCell.h
//  ssc
//
//  Created by sarath sb on 10/15/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface offerListTableViewCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *descptnLbl;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIButton *viewDetailsBtn;
@property (strong, nonatomic) IBOutlet UIImageView *brandImg;
@property (strong, nonatomic) IBOutlet UIButton *availOffersBtn;
@property (strong, nonatomic) IBOutlet UIImageView *imageThumb;
@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet UIButton *btn;

@end

NS_ASSUME_NONNULL_END
