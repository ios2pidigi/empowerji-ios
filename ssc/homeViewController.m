//
//  homeViewController.m
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "homeViewController.h"
#import "homeCollectionViewCell.h"
#import "sideMenuViewController.h"
#import "Constants.h"
#import "categoryViewController.h"
#import "SVProgressHUD.h"
#import "searchViewController.h"
#import "moListViewController.h"
#import "myFeedViewController.h"
#import "sendMessageViewController.h"
#import "supportViewController.h"
#import "connectivity.h"
#import "UIView+Toast.h"
#import "homeCategoryViewController.h"
#import "cSearchViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#import "JMImageCache.h"
#import "UIImageView+WebCache.h"
#import "communityViewController.h"

#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "notificationsViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "loginViewController.h"
#import "UIButton+WebCache.h"

#import "offersListingViewController.h"
#import "interViewController.h"

#import "SDWebImagePrefetcher.h"
#include <stdlib.h>
#import "inAppViewController.h"

#import "videoViewController.h"

@import GoogleMobileAds;

@interface homeViewController ()<AVAudioPlayerDelegate>
{
    NSMutableArray *homeArray;
    NSArray *subCatCheckArr;
    
    UIView* TransprentView;
    //UILabel *hLbl;
    UIButton *hLbl;
    UIButton *btn;
    UIImageView *arrw;
    
    BOOL playedOnce;
    
    BOOL once;
    
    int index;
    
    UIButton *customAdBanner;
    
    NSString *activePlan;
    
   // AVAudioPlayer *_audioPlayer;
   // AVPlayer *player;
}
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@end

typedef void(^myCompletion)(BOOL);

@implementation homeViewController
@synthesize isfrmPush,isFrmUpdate,isFrmLogin,isFrmLangScreen,isFrmSignOut,isFrmInAPP;

- (void)viewDidLoad {
    [super viewDidLoad];
    //230, 176, 80a
    
    subCatCheckArr = [[NSArray alloc]init];
    
    [SVProgressHUD setDefaultStyle:(SVProgressHUDStyleDark)];
    [SVProgressHUD setDefaultMaskType:(SVProgressHUDMaskTypeCustom)];
    
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    
    [SVProgressHUD setMaximumDismissTimeInterval:10.0];
    [SVProgressHUD setMinimumDismissTimeInterval:3];
    
    
    _searchTF.layer.borderColor = [UIColor whiteColor].CGColor;
    _searchTF.layer.borderWidth = 1.0;
    
   // _header1.layer.borderColor =  btnBorderColor;//[UIColor whiteColor].CGColor;
    //_header1.layer.borderWidth = 1.0;
    
    [_searchBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if(isFrmLogin)
    {
        NSInteger langid = 5;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"langIDtoCat"])
            langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"langIDtoCat"];
        
        [[NSUserDefaults standardUserDefaults]setInteger:langid forKey:@"languageIDMain"];
    }
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        NSInteger langid = 5;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"langidFromLanguages"])
            langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"langidFromLanguages"];
        
        [[NSUserDefaults standardUserDefaults]setInteger:langid forKey:@"languageIDMain"];
        
        if(isFrmSignOut)
        {
            //isFrmSignOut =NO;
            
            NSString *langIdLogIn;
            
            NSString *lang = langGlobal;
            
            NSString *theString = lang;   // The one we want to switch on
            NSArray *items = @[@"other", @"no", @"hi",  @"mr-IN", @"gu-IN", @"en" ];
            NSInteger item = [items indexOfObject:theString];
            switch (item) {
                case 0:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 1:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 2:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 3:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 4:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 5:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                default:
                    break;
            }
            
            langid = [langIdLogIn integerValue];
            
            NSString *langidGuest;
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"])
                langidGuest = [[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"];
            else
                langidGuest = @"5";
            
           [[NSUserDefaults standardUserDefaults]setInteger:[langidGuest integerValue] forKey:@"languageIDMain"];
            
        }
    }
    
    
    [self settingsWebservice];
    
    [self subCategoryCheckWebService];
    
     //[self writeAudioFilesToLocal];
    
    homeArray = [[NSMutableArray alloc]init];
  //  homeArray = @[ @{@"name": @"MOBILE APPS", @"image": @"home-mobile-apps.png", @"tag": @"3"},@{@"name": @"SITES AND MORE", @"image": @"home-sites-more.png", @"tag": @"4"},@{@"name": @"FEATURES", @"image": @"home-features.png", @"tag": @"5"},@{@"name": @"COMMUNITY", @"image": @"home-community.png", @"tag": @"6"} ];
    
    //,@{@"name": @"EXPERIENCES", @"image": @"experiences.png", @"tag": @"2"}
   
    
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
   // [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
  /*  [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {*/
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
            /*  if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
             */
              {
                  NSLog(@"internet is connected");
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
    }
    else
    {
        [self staticPagesWS];
    }
        
              }
              
             // dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
               //   [SVProgressHUD dismiss];
             // });
              
              
          });
          
          
    /*  }] resume];*/

//    https://app.empowerji.com/webservice/static_pages.php
    
    [_playBtn addTarget:self action:@selector(playAudio) forControlEvents:UIControlEventTouchUpInside];
    
    [_helpBtn addTarget:self action:@selector(playVideo) forControlEvents:UIControlEventTouchUpInside];
    
     //_audioPlayer.delegate = self;
    
    //[_closeVideoBtn addTarget:self action:@selector(closeVideo) forControlEvents:UIControlEventTouchUpInside];
    
    ///dummy video preload
    NSString *url=@"";//https://www.youtube.com/embed/8NT-E-nrJ9M
    [self.webVw loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    self.webVw.scrollView.bounces = NO;
    [self.webVw setMediaPlaybackRequiresUserAction:NO];
    ////
    
    
    [_btnMenu addTarget:self action:@selector(menuBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnContactUs addTarget:self action:@selector(homeActn:) forControlEvents:UIControlEventTouchUpInside];
    
    [_closeBtn addTarget:self action:@selector(closeVideo) forControlEvents:UIControlEventTouchUpInside];
    
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
    
    
   
    if(!isFrmUpdate)
    [self homeCategotyWS];
    
   // [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"firstInstall"];
    
    //comment this line to show bubble on audio
    //[[NSUserDefaults standardUserDefaults]setObject:@"firstInstall" forKey:@"firstInstall"];
    
    //show help view
    //uncomment this line to hide bubble
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"firstInstall"])
    {
       // [[NSUserDefaults standardUserDefaults]setObject:@"firstInstall" forKey:@"firstInstall"];
        
        // get window screen size for frame
        CGRect screensize = [[UIScreen mainScreen] bounds];
        //create a new UIview with the same size for same view
        TransprentView = [[UIView alloc] initWithFrame:screensize];
        
        // change the background color to black and the opacity to 0.6 for Transparent
       // TransprentView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        
        // here add your Label, Image, etc, what ever you want
        /*hLbl = [[UILabel alloc]initWithFrame:CGRectMake(((screensize.size.width/2)-(screensize.size.width/2)), ((screensize.size.height/2)-(50/2)), screensize.size.width, 50)];
        hLbl.text = @"⇧ Click Here for Help ";//↪ ⤻ ⇧
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *helpText = [languageBundle localizedStringForKey:@"helpText" value:@"" table:@"strings-english"];
        hLbl.text = helpText;
        
        hLbl.font = [UIFont systemFontOfSize:18.0 weight:UIFontWeightBold];
        hLbl.textColor = [UIColor whiteColor];
        hLbl.backgroundColor = [UIColor clearColor];
        hLbl.textAlignment = NSTextAlignmentCenter;
        hLbl.numberOfLines = 0;
         */
        //hLbl.center = _playBtn.center;
        
        hLbl = [[UIButton alloc]initWithFrame:CGRectMake(((screensize.size.width/2)-(screensize.size.width/2)), ((screensize.size.height/2)-(50/2) -25), screensize.size.width, 50)];
        [hLbl setTitle:@"Click Here for Help" forState:UIControlStateNormal]; //↪ ⤻ ⇧
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *helpText = [languageBundle localizedStringForKey:@"helpText" value:@"" table:@"strings-english"];
        [hLbl setTitle:helpText forState:UIControlStateNormal];
        
        
        hLbl.titleLabel.textColor = [UIColor whiteColor];
        hLbl.backgroundColor = [UIColor clearColor];
        hLbl.titleLabel.textAlignment = NSTextAlignmentCenter;
        hLbl.titleLabel.numberOfLines = 0;
        
        if(IS_IPAD)
        {
           /* [hLbl setBackgroundImage:[UIImage imageNamed:@"bubble-2"] forState:UIControlStateNormal];
            hLbl.titleLabel.font = [UIFont systemFontOfSize:18.0 weight:UIFontWeightBold];
            */
            
            hLbl.backgroundColor = blueColorBtn;
            hLbl.titleLabel.font = [UIFont systemFontOfSize:14.0 weight:UIFontWeightBold];
            hLbl.layer.cornerRadius = 5.0;
            hLbl.clipsToBounds = YES;
        }
        else
        {
       // [hLbl setBackgroundImage:[UIImage imageNamed:@"bubble-2"] forState:UIControlStateNormal];
            
            hLbl.backgroundColor = blueColorBtn;
            hLbl.titleLabel.font = [UIFont systemFontOfSize:10.0 weight:UIFontWeightBold];
            hLbl.layer.cornerRadius = 5.0;
            hLbl.clipsToBounds = YES;
        }
        
        [hLbl addTarget:self action:@selector(bubbleClicked) forControlEvents:UIControlEventTouchUpInside];
        
        hLbl.titleLabel.alpha = 1;
        [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
            hLbl.titleLabel.alpha = 0;
        } completion:nil];
        
        [self.view addSubview:hLbl];
        
        
        //add a left arrow to btn
        arrw = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        arrw.image = [UIImage imageNamed:@"arrow-left"];
        arrw.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:arrw];
        
        
        //add a button above hlbl to play audio
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor = [UIColor clearColor];
        btn.frame = _playBtn.frame;
        [btn addTarget:self action:@selector(bubbleClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        
        
        
        // add this TransprentView to your main view
       // [self.view addSubview:TransprentView];
    }
    
    if(isfrmPush)
    {
        notificationsViewController *ntVC = [[notificationsViewController alloc]initWithNibName:@"notificationsViewController" bundle:nil];
        self.navigationController.navigationBarHidden=YES;
        ntVC.isfrmHome = YES;
        [self.navigationController pushViewController:ntVC animated:YES];
    }
    
    
    [FIRAnalytics logEventWithName:@"Pages"
                        parameters:@{
                                     @"name": @"home"
                                     
                                     }];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Home Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationIsActive:)name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnteredForeground:)name:UIApplicationWillEnterForegroundNotification object:nil];
    
    
   // [_btnCommunityNMore addTarget:self action:@selector(btnCommunityNMoreAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnCommunityNMore addTarget:self action:@selector(offersAction) forControlEvents:UIControlEventTouchUpInside];
    
    _collectionVw.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    
    _counterLbl.textAlignment = NSTextAlignmentCenter;
    
   
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cacheUpdated:) name:@"MyCacheUpdatedNotification" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
    
    
    [_btnClosePopImg addTarget:self action:@selector(closePopImg) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect ff;
    if(IS_IPAD)
    {
      ff = [self frameForImage:_logoImg.image inImageViewAspectFit:_logoImg];
        NSLog(@"ff :%@",NSStringFromCGRect(ff));
    }
    
   if(isFrmInAPP)
   {
       inAppViewController *inapp = [[inAppViewController alloc] init];
       UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:inapp];
       navController.navigationBar.hidden = YES;
       [self presentViewController:navController animated:YES completion:nil];
   }
    
    
}

-(CGRect)frameForImage:(UIImage*)image inImageViewAspectFit:(UIImageView*)imageView
{
    float imageRatio = image.size.width / image.size.height;
    float viewRatio = imageView.frame.size.width / imageView.frame.size.height;
    if(imageRatio < viewRatio)
    {
        float scale = imageView.frame.size.height / image.size.height;
        float width = scale * image.size.width;
        float topLeftX = (imageView.frame.size.width - width) * 0.5;
        return CGRectMake(topLeftX, 0, width, imageView.frame.size.height);
    }
    else
    {
        float scale = imageView.frame.size.width / image.size.width;
        float height = scale * image.size.height;
        float topLeftY = (imageView.frame.size.height - height) * 0.5;
        
        return CGRectMake(0, topLeftY, imageView.frame.size.width, height);
    }
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    NSString *url;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"popup_url"])
    {
        url = [[NSUserDefaults standardUserDefaults]objectForKey:@"popup_url"];
    }
    
   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)cacheUpdated:(NSNotification *)notification
{
    [self resetCounter];
}

-(void)updateCat
{
  [self homeCategotyWS];
}

- (void)applicationIsActive:(NSNotification *)notification
{
    NSLog(@"Application Did Become Active");
    
    hLbl.titleLabel.alpha = 1;
}

- (void)applicationEnteredForeground:(NSNotification *)notification
{
    NSLog(@"Application Entered Foreground");
    
    //if(![[NSUserDefaults standardUserDefaults]objectForKey:@"showPopUpImageView"])
    
    if (self.isViewLoaded && self.view.window)
    {
        // viewController is visible
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"showPopUpImageView"];

        
        [self settingsWebservice];
    }
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //_header1.layer.cornerRadius = _header1.frame.size.height / 2;
    
    CGRect frameHeader1 = _header1.frame;
   // frameHeader1.size.width = _header1.intrinsicContentSize.width ;
    frameHeader1.origin.x =  ([UIScreen mainScreen].bounds.size.width/2) - (frameHeader1.size.width/2);
    _header1.frame = frameHeader1;
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
   // gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor blackColor].CGColor];
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    _supportBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    //Rgb2UIColor(0, 93, 155);
    _supportBtn.layer.borderWidth= 2.0f;
    
    _keepGoingBtn.layer.borderColor=[UIColor whiteColor].CGColor;//Rgb2UIColor(0, 93, 155);
    _keepGoingBtn.layer.borderWidth= 2.0f;
    
    _btnMenu.layer.cornerRadius = _btnMenu.frame.size.height/2;
    _btnContactUs.layer.cornerRadius = _btnContactUs.frame.size.height/2;
    
    //make menu btn background gradient
    CAGradientLayer *gradientbtn = [CAGradientLayer layer];
    gradientbtn.frame = _btnMenu.bounds;
    gradientbtn.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnMenu.layer insertSublayer:gradientbtn atIndex:0];
    
    //make contactUs btn background gradient
    CAGradientLayer *gradientbtn2 = [CAGradientLayer layer];
    gradientbtn2.frame = _btnContactUs.bounds;
    gradientbtn2.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnContactUs.layer insertSublayer:gradientbtn2 atIndex:0];
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
        
        if(!once)
        {
            once = YES;
            CGRect frameC = _collectionVw.frame;
            frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
            _collectionVw.frame = frameC;
        }
    }
    else
    {
        if(!once)
        {
            once = YES;
            
        CGRect frameC = _collectionVw.frame;
        frameC.size.height = ([UIScreen mainScreen].bounds.size.height - frameC.origin.y) ; /*frameC.size.height - _helpBtn.frame.size.height;*/
        _collectionVw.frame = frameC;
        }
    }
   /* else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
       NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];

        NSString *custom_banner_url = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner_url"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:custom_banner_url forKey:@"custom_banner_url"];
        
        NSString *custom_inter_url = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_inter_url"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:custom_inter_url forKey:@"custom_inter_url"];
        
        NSString *custom_inter = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_inter"]];
        
         [[NSUserDefaults standardUserDefaults]setObject:custom_inter forKey:@"custom_inter"];
        
        UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];

        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        

        [self.view addSubview:customAdBanner];
        
        if(!once)
        {
            once = YES;
            CGRect frameC = _collectionVw.frame;
            frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
            _collectionVw.frame = frameC;
        }
    }
    */
    
    CGRect f1 = _speakerImg.frame; CGRect f2 = _playBtn.frame;
    if(IS_IPAD)
    {
        f1.origin.x = _header1.frame.origin.x + _header1.frame.size.width -5;
        _speakerImg.frame = f1;
        
        f2.origin.x = _header1.frame.origin.x + _header1.frame.size.width;
        f2.size.width = _speakerImg.frame.size.width+5;
        _playBtn.frame = f2;
        
    }
    
    
    CGRect frameHelpLbl = hLbl.frame;
    if(IS_IPAD)
    frameHelpLbl.origin.x =   _playBtn.frame.origin.x + _playBtn.frame.size.width;//_speakerImg.frame.origin.x - ((([UIScreen mainScreen].bounds.size.width - _playBtn.frame.origin.x)/2)+20);
    else
        frameHelpLbl.origin.x = _playBtn.frame.origin.x + _playBtn.frame.size.width; //(([UIScreen mainScreen].bounds.size.width - _speakerImg.frame.origin.x)+40);
    
    frameHelpLbl.origin.y = _playBtn.frame.origin.y;// + (_playBtn.frame.size.height);
    
    if(IS_IPAD)
    frameHelpLbl.size.width = [UIScreen mainScreen].bounds.size.width - frameHelpLbl.origin.x;
    else
    {
        frameHelpLbl.size.width = [UIScreen mainScreen].bounds.size.width - (frameHelpLbl.origin.x);
    }
    
    if(IS_IPAD)
    {
        frameHelpLbl.size.height = 50;
        frameHelpLbl.size.width = 80;
    }
    else
    {
        frameHelpLbl.size.height = 40;
        frameHelpLbl.size.width = 70;
    }
    
    hLbl.frame = frameHelpLbl;
    //[hLbl sizeToFit];
    
    btn.frame = _playBtn.frame;
    
    hLbl.titleEdgeInsets = UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f);
    //top left bottom right
    
    hLbl.titleLabel.alpha = 1;
    [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
        hLbl.titleLabel.alpha = 0;
    } completion:nil];
    
    [_helpLbl sizeToFit];
    //_helpLbl.minimumFontSize = 8;
   // _helpLbl.adjustsFontSizeToFitWidth = YES;
    
    CGRect arrFrm = arrw.frame;
    arrFrm.origin.x = hLbl.frame.origin.x - 10;
    if(IS_IPAD)
    {
       arrFrm.size.width = 10;
        arrFrm.size.height = 20;
        
        arrFrm.origin.y = hLbl.frame.origin.y+5;

    }
    else
    {
        arrFrm.size.width = 10;
        arrFrm.size.height = 10;
        
        arrFrm.origin.y = hLbl.frame.origin.y+10;

    }
    
    arrw.frame = arrFrm;
    
    _counterLbl.layer.cornerRadius = _counterLbl.frame.size.height/2;
    
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_audioPlayer pause];
     [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
    
    [self removeTransparentView];
}

static NSInteger countG;

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   //[self setStatusBarBackgroundColor:[UIColor colorWithRed:(230/255.0) green:(176/255.0) blue:(80/255.0) alpha:1.0]];
    if ((_audioPlayer.rate != 0) /*&& (_audioPlayer.error == nil)*/)
    {
       // [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
        // player is playing
    }
    else
    {
        // [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
       //ico-speaker.png
    }
    
   // [self refreshSettingsWS];
    
    if(countG > 5)
    {
    [[NSUserDefaults standardUserDefaults]setObject:@"firstInstall" forKey:@"firstInstall"];
        
         [hLbl removeFromSuperview];
        [arrw removeFromSuperview];
    }
    else
    {
        [self.view addSubview:hLbl];
    }
    
    countG ++;
    
    
    if(isFrmUpdate)
    {
       // [self refreshSettingsWS];
       // isFrmUpdate = NO;
    }
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        NSInteger langid = 5;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"langidFromLanguages"])
            langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"langidFromLanguages"];
        
        [[NSUserDefaults standardUserDefaults]setInteger:langid forKey:@"languageIDMain"];
        
        
        if(isFrmSignOut)
        {
            //isFrmSignOut =NO;
            
            NSString *langIdLogIn;
            
            NSString *lang = langGlobal;
            
            NSString *theString = lang;   // The one we want to switch on
            NSArray *items = @[@"other", @"no", @"hi",  @"mr-IN", @"gu-IN", @"en" ];
            NSInteger item = [items indexOfObject:theString];
            switch (item) {
                case 0:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 1:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 2:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 3:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 4:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                case 5:
                    langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
                    break;
                default:
                    break;
            }
            
            langid = [langIdLogIn integerValue];
            
           // [[NSUserDefaults standardUserDefaults]setInteger:langid forKey:@"languageIDMain"];
            
            NSString *langidGuest;
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"])
                langidGuest = [[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"];
            else
                langidGuest = @"5";
            
            [[NSUserDefaults standardUserDefaults]setInteger:[langidGuest integerValue] forKey:@"languageIDMain"];
            
        }
    }
    
    
    _searchTF.text = @"";
    
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger count = 0;
    
    if([userDefaults integerForKey:@"notificationCount"])
    {
        count = [userDefaults integerForKey:@"notificationCount"];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0; //count;
    
    
   // NSInteger badgeNum;
    //badgeNum = [UIApplication sharedApplication].applicationIconBadgeNumber;
   // _counterLbl.text = [NSString stringWithFormat:@"%ld",(long)badgeNum];
    
    _counterLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
    
    if(([_counterLbl.text caseInsensitiveCompare:@"0"]==NSOrderedSame))
    {
        _counterLbl.hidden = YES;
    }
    else
    {
        _counterLbl.hidden = NO;
    }
    
    
    ////////
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count > 0)
    {
    // "custom_banner_arr"
    
    //prefetch advw images and interstitial
    
    NSMutableArray *custom_banner_arr = [[NSMutableArray alloc]init];
    
    NSArray *custom_banner_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_banner_arr"];
    
        [[NSUserDefaults standardUserDefaults]setObject:custom_banner_arr2 forKey:@"custom_banner_arr2"];
        
        
    for(NSString *arr in custom_banner_arr2)
    {
        NSString *url = [arr valueForKey:@"custom_banner"];
        [custom_banner_arr addObject:url];
    }
    
   // uint32_t rnd = arc4random_uniform([custom_banner_arr count]);
    
        index = arc4random_uniform(custom_banner_arr2.count);
        
        @try {
          
    NSString *randomObject = [custom_banner_arr objectAtIndex:index];
    
    NSLog(@"rand:%u, rnd:%@",index,randomObject);
    
    NSLog(@"custom_banner url :%@",randomObject);
    
    [[NSUserDefaults standardUserDefaults]setObject:randomObject forKey:@"custom_banner_url"];
    
    [customAdBanner sd_setImageWithURL:[NSURL URLWithString:randomObject] forState:UIControlStateNormal];
    
    /////
    NSMutableArray *custom_inter_arr = [[NSMutableArray alloc]init];
    
    NSArray *custom_inter_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_inter_arr"];
    
    for(NSString *arr in custom_inter_arr2)
    {
        NSString *url = [arr valueForKey:@"custom_inter"];
        [custom_inter_arr addObject:url];
    }
    
   // uint32_t rnd2 = arc4random_uniform([custom_inter_arr count]);
    
    // NSUInteger randomIndex = arc4random() % custom_inter_arr.count;
        
       //  index = arc4random_uniform(custom_inter_arr2.count);
        
        
    NSString *randomObject2 = [custom_inter_arr objectAtIndex:index];
        
    NSLog(@"custom_inter_arr url :%@",randomObject2);
    
    [[NSUserDefaults standardUserDefaults]setObject:randomObject2 forKey:@"custom_inter"];
    
            
        }
        @catch (NSException *exception)
        {
            [self settingsWebservice];
        }
    
    /////
    }
}

-(void)resetCounter
{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger count = 0;
    
    if([userDefaults integerForKey:@"notificationCount"])
    {
        count = [userDefaults integerForKey:@"notificationCount"];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;//count;
    
    
    // NSInteger badgeNum;
    //badgeNum = [UIApplication sharedApplication].applicationIconBadgeNumber;
    // _counterLbl.text = [NSString stringWithFormat:@"%ld",(long)badgeNum];
    
    _counterLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
    
    if(([_counterLbl.text caseInsensitiveCompare:@"0"]==NSOrderedSame))
    {
        _counterLbl.hidden = YES;
    }
    else
    {
        _counterLbl.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark WebService Calls

-(void)settingsWebservice
{
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
    langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    int r = arc4random_uniform(10000);
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid =@"";
    }
    else
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self settingsWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
                [def synchronize];
            
            NSString *admobId; NSString *adStatus;
            admobId = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"admob_id"];
            
             adStatus = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"admob_status"];
            
            NSString *help_video = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"help_video"]];
            
            
            if(![[NSUserDefaults standardUserDefaults]objectForKey:@"firstLaunchShowHelp"])
            {
                [[NSUserDefaults standardUserDefaults]setObject:help_video forKey:@"urlTrial"];
                // help_video = [[NSUserDefaults standardUserDefaults]valueForKey:@"urlTrial"];
                
                    [[NSUserDefaults standardUserDefaults]setObject:@"launched" forKey:@"firstLaunchShowHelp"];
                    
                    videoViewController *popVC = [[videoViewController alloc]initWithNibName:@"videoViewController" bundle:nil];
                    //[self.navigationController pushViewController:homeVC animated:YES];
                    
                    self.definesPresentationContext = YES; //self is presenting view controller
                    popVC.view.backgroundColor = [UIColor clearColor];
                    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    
                    [self presentViewController:popVC animated:NO completion:nil];
                    
            }
            
            
            //[GADMobileAds configureWithApplicationID:admobId];
            
            _adVw.adSize = kGADAdSizeSmartBannerPortrait;
            self.adVw.adUnitID = admobId;//@"ca-app-pub-8214158784518238/4464482105";
            self.adVw.rootViewController = self;
            
            
            // "custom_banner_arr"
            
            //prefetch advw images and interstitial
            
            NSMutableArray *custom_banner_arr = [[NSMutableArray alloc]init];
            
            
            NSArray *custom_banner_arr2 = [[[json objectForKey:@"settings"] objectAtIndex:0]valueForKey:@"custom_banner_arr"];
            
             NSString *current_package_name = [[[json objectForKey:@"settings"] objectAtIndex:0]valueForKey:@"current_package_name"];
            
            if([current_package_name caseInsensitiveCompare:@"premium"]==NSOrderedSame)
            {
                _btnContactUs.hidden = NO;
            }
            if([current_package_name caseInsensitiveCompare:@"plus"]==NSOrderedSame)
            {
                _btnContactUs.hidden = NO;
            }
            
            for(NSString *arr in custom_banner_arr2)
            {
                NSString *url = [arr valueForKey:@"custom_banner"];
                [custom_banner_arr addObject:url];
            }
            
           uint32_t rnd = arc4random_uniform([custom_banner_arr count]);
            
            NSString *randomObject = [custom_banner_arr objectAtIndex:rnd];
            
            NSLog(@"rand:%u, rnd:%@",rnd,randomObject);
            
            NSLog(@"custom_banner url :%@",randomObject);
            
            [[NSUserDefaults standardUserDefaults]setObject:randomObject forKey:@"custom_banner_url"];
            
            
            [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:custom_banner_arr];
            
            /////
            NSMutableArray *custom_inter_arr = [[NSMutableArray alloc]init];
            
            NSArray *custom_inter_arr2 = [[[json objectForKey:@"settings"] objectAtIndex:0]valueForKey:@"custom_inter_arr"];
            
            for(NSString *arr in custom_inter_arr2)
            {
                NSString *url = [arr valueForKey:@"custom_inter"];
                [custom_inter_arr addObject:url];
            }
            
            uint32_t rnd2 = arc4random_uniform([custom_inter_arr count]);
            
            NSString *randomObject2 = [custom_inter_arr objectAtIndex:rnd2];
            
            NSLog(@"custom_inter_arr url :%@",randomObject2);
            
            [[NSUserDefaults standardUserDefaults]setObject:randomObject2 forKey:@"custom_inter"];
            
            [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:custom_inter_arr];
            
            
            /////
            
            if([adStatus isEqualToString:@"Yes"])
            {
                CGRect frameH = _helpImg.frame;
                frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
                _helpImg.frame = frameH;
                
                CGRect frameB = _helpBtn.frame;
                frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
                _helpBtn.frame = frameB;
                
                [self.helpLbl setCenter:_helpBtn.center];
                
                
             [self.adVw loadRequest:[GADRequest request]];
            }
            else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
            {
                CGRect frameH = _helpImg.frame;
                frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
                _helpImg.frame = frameH;
                
                CGRect frameB = _helpBtn.frame;
                frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
                _helpBtn.frame = frameB;
                
                [self.helpLbl setCenter:_helpBtn.center];
                
                //[self.adVw loadRequest:[GADRequest request]];
                
                NSString *custom_banner = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"] objectAtIndex:0]valueForKey:@"custom_banner"]];
                
               custom_banner = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
                
                NSString *custom_banner_url = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"] objectAtIndex:0]valueForKey:@"custom_banner_url"]];
                
               // [[NSUserDefaults standardUserDefaults]setObject:custom_banner_url forKey:@"custom_banner_url"];
                
                NSString *custom_inter_url = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"] objectAtIndex:0]valueForKey:@"custom_inter_url"]];
                
                [[NSUserDefaults standardUserDefaults]setObject:custom_inter_url forKey:@"custom_inter_url"];
                
                NSString *custom_inter = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"] objectAtIndex:0]valueForKey:@"custom_inter"]];
                
               // [[NSUserDefaults standardUserDefaults]setObject:custom_inter forKey:@"custom_inter"];
                
                customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
                
                [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
                
                [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
                
                
                [self.view addSubview:customAdBanner];
                
                if(!once)
                {
                    once = YES;
                    CGRect frameC = _collectionVw.frame;
                    frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
                    _collectionVw.frame = frameC;
                }
            }
            
           
            
            NSString *helpUrl ;
            helpUrl = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"help_video"];
            
           // NSString *url=@"https://www.youtube.com/embed/8NT-E-nrJ9M";
            [self.webVw loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:helpUrl]]];
            self.webVw.scrollView.bounces = NO;
            [self.webVw setMediaPlaybackRequiresUserAction:NO];
            
            
            //download audio
            NSArray *audioUrlArr = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"audio"];
            
            if([audioUrlArr isKindOfClass:[NSString class]])
            {
               
            }
            else
            {
                if(audioUrlArr.count >0)
                {
                    NSString *url;
                    
                    if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                        url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                    
                   // [self writeAudioFilesToLocal];
                    
                  /*  for(NSString *page in audioUrlArr)
                    {
                        if([[page valueForKey:@"page"]caseInsensitiveCompare:@"home"]==NSOrderedSame)
                        {
                            url = [page valueForKey:@"file"];
                            
                            NSURL *urlT = [[NSURL alloc] initWithString:url];
                            NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                            NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *filePath = [documents stringByAppendingPathComponent:@"home.mp3"];
                            [soundData writeToFile:filePath atomically:YES];
                        }
                        
                   
                        
                    }
                    */
                    
                }
                
            }
            
            NSString *popup_image;
            
            popup_image = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"popup_image"]];
            
            NSString *popup_url;
            
            popup_url = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"popup_url"]];
            
            [[NSUserDefaults standardUserDefaults]setObject:popup_url forKey:@"popup_url"];
            
            NSString *popup_status;
            
            popup_status = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"popup_status"]];
            
            //show popup view if popup_image exists
            //if popup_image=NA don't show popup else show.
            
            if([popup_image caseInsensitiveCompare:@"NA"]==NSOrderedSame)
            {
                
            }
            else
            {
                UIImageView *popImg = [[UIImageView alloc]initWithFrame:_webVw.frame];
                
               // [popImg sd_setImageWithURL:[NSURL URLWithString:popup_image] placeholderImage:nil];
                
                
                [popImg sd_setImageWithURL:[NSURL URLWithString:popup_image] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                {
                    
                    [_popUpImgVw addSubview:popImg];
                    
                    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"showPopUpImageView"])
                    {
                        if([popup_status caseInsensitiveCompare:@"Disabled"]==NSOrderedSame)
                        {
                            
                        }
                        else
                        {
                            if (error != nil)
                            {
                                //Failure code here
                            }
                            else
                            _popUpImgVw.hidden = NO;
                        }
                    }
                    
                     [[NSUserDefaults standardUserDefaults]setObject:@"showedOnce" forKey:@"showPopUpImageView"];
                }];
                
                
                
                //The setup code (in viewDidLoad in your view controller)
                UITapGestureRecognizer *singleFingerTap =
                [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
                [popImg addGestureRecognizer:singleFingerTap];
                
                popImg.userInteractionEnabled = YES;
                
               
                
            }
            
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)subCategoryCheckWebService
{
  //http://listlinkz.com/ssc/webservice/check_sub_exists.php
    
    NSString *wUrl=[NSString stringWithFormat:@"%@check_sub_exists.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self subCategoryCheckWebService];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            subCatCheckArr = [[json objectForKey:@"result"]objectAtIndex:0];
            
           //result
            
        });
    }];
    
    [dataTask resume];
    
    
}

-(void)staticPagesWS
{
    NSString *wUrl=[NSString stringWithFormat:@"%@static_pages.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self staticPagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
          //  NSArray *staticArr = [[json objectForKey:@"result"]objectAtIndex:0];
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"result"]] forKey:@"staticArrContents"];
            [def synchronize];
            //result
            
            //settings
            /*
             NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
             NSData *data = [def objectForKey:@"settings"];
             NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
             */
            
          /*  NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            NSData *data = [def objectForKey:@"staticArrContents"];
            NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
           */
            
        });
    }];
    
    [dataTask resume];
}

-(void)homeCategotyWS
{
    //app.empowerji.com/webservice3/home_category.php
    
   /* NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"langidEdit"])
    {
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"langidEdit"];
    }
    else if([[NSUserDefaults standardUserDefaults]integerForKey:@"langIDtoCat"])
    {
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"langIDtoCat"];
    }
    */
    if(!isFrmUpdate)
    [SVProgressHUD show];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    int r = arc4random_uniform(10000);
    
    if(isFrmLogin)
    {
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"langIDtoCat"])
            langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"langIDtoCat"];
        
        [[NSUserDefaults standardUserDefaults]setInteger:langid forKey:@"languageIDMain"];
    }
    
    NSString *wUrl=[NSString stringWithFormat:@"%@home_category.php?lang_id=%ld&counts=%d",baseUrl,(long)langid,r];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self homeCategotyWS];
            
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *no_internet = [languageBundle localizedStringForKey:@"no_internet" value:@"" table:@"strings-english"];
            
            NSString *ok = [languageBundle localizedStringForKey:@"ok" value:@"" table:@"strings-english"];
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:no_internet
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [self homeCategotyWS];
                
            }];
            [alert addAction:okAction];
            UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            [vc presentViewController:alert animated:YES completion:nil];
            
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            NSData *data = [def objectForKey:@"homeCategory"];
            NSMutableArray *homeCategory2 = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            homeArray = [homeCategory2 mutableCopy];
            
         /*   for (int i =0 ; i < homeCategory2.count ; i++ )
            {
                NSString *idV = [[homeCategory2 objectAtIndex:i]valueForKey:@"id"];
                
                if([idV caseInsensitiveCompare:@"4"] == NSOrderedSame)
                {
                    [homeArray replaceObjectAtIndex:0 withObject:[homeCategory2 objectAtIndex:i]];
                }
                if([idV caseInsensitiveCompare:@"5"] == NSOrderedSame)
                {
                    [homeArray replaceObjectAtIndex:1 withObject:[homeCategory2 objectAtIndex:i]];
                }
                if([idV caseInsensitiveCompare:@"3"] == NSOrderedSame)
                {
                    [homeArray replaceObjectAtIndex:2 withObject:[homeCategory2 objectAtIndex:i]];
                }
                if([idV caseInsensitiveCompare:@"1"] == NSOrderedSame)
                {
                    [homeArray replaceObjectAtIndex:3 withObject:[homeCategory2 objectAtIndex:i]];
                }
                
            }
            
            */
            
            NSArray *homeCategory = [[NSArray alloc]init];
            
            homeCategory = [json valueForKey:@"categories"];
            
            homeArray = [homeCategory mutableCopy];
            
            NSLog(@"homeArray :%@",homeArray);
            
         /*   for (int i =0 ; i < homeCategory.count ; i++ )
            {
                NSString *idV = [[homeCategory objectAtIndex:i]valueForKey:@"id"];
                
                if([idV caseInsensitiveCompare:@"4"] == NSOrderedSame)
                {
                    [homeArray replaceObjectAtIndex:0 withObject:[homeCategory objectAtIndex:i]];
                }
                if([idV caseInsensitiveCompare:@"5"] == NSOrderedSame)
                {
                    [homeArray replaceObjectAtIndex:1 withObject:[homeCategory objectAtIndex:i]];
                }
                if([idV caseInsensitiveCompare:@"3"] == NSOrderedSame)
                {
                    [homeArray replaceObjectAtIndex:2 withObject:[homeCategory objectAtIndex:i]];
                }
                if([idV caseInsensitiveCompare:@"1"] == NSOrderedSame)
                {
                    [homeArray replaceObjectAtIndex:3 withObject:[homeCategory objectAtIndex:i]];
                }
                
            }
            */
            
            [_collectionVw reloadData];
            
            
            [SVProgressHUD dismiss];
        });
    }];
    
    [dataTask resume];
    
}

-(void)refreshSettingsWS
{
   // [SVProgressHUD show];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    int r = arc4random_uniform(10000);
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid =@"";
    }
    else
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self playAudio];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
            [def synchronize];
           
            
            //download audio
            NSArray *audioUrlArr = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"audio"];
            
            if([audioUrlArr isKindOfClass:[NSString class]])
            {
                
            }
            else
            {
                if(audioUrlArr.count >0)
                {
                    NSString *url;
                    
                    if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                        url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                    
                    for(NSString *page in audioUrlArr)
                    {
                        if([[page valueForKey:@"page"]caseInsensitiveCompare:@"home"]==NSOrderedSame)
                        {
                            url = [page valueForKey:@"file"];
                            
                            NSURL *urlT = [[NSURL alloc] initWithString:url];
                            NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                            NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *filePath = [documents stringByAppendingPathComponent:@"home.mp3"];
                          BOOL success = [soundData writeToFile:filePath atomically:YES];
                            
                            if(success)
                            {
                                
                            }
                            break;
                        }
                        
                        
                    }
                    
                   // [self writeAudioFilesToLocal];
                }
                
            }
                                              
        });
    }];
            
    
    [dataTask resume];
    
}


-(void)clicksWS :(NSString *)type :(NSString *)cid
{
    
    //app.empowerji.com/webservice4/clicks.php?id=5&type=offer

    //e
  /*
   type=banner
   type=inter
   type=offer
   */
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    int r = arc4random_uniform(10000);
    
    NSString *wUrl=[NSString stringWithFormat:@"%@clicks.php?id=%@&type=%@",baseUrl,cid,type];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
        });
    }];
    
    
    [dataTask resume];
}

-(void)packagesWS:(myCompletion) compblock
{
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *mainCatId;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
        mainCatId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
    
    NSArray *arrUser;
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid = @"";
    }
    else
    {
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    }
    
    int r = arc4random_uniform(10000);
    
    NSString *wUrl=[NSString stringWithFormat:@"%@packages.php?lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
           // [self packagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
          NSArray *mArray = [json objectForKey:@"result"];
            
            NSArray *active_package = [[NSArray alloc]init];
            active_package = [json objectForKey:@"active_package"];
            
            NSString *package_name = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"package_name"]];
            
            NSLog(@"packgage name :%@",package_name);
            
            if([package_name caseInsensitiveCompare:@"(null)"]==NSOrderedSame)
            {
                package_name = @"";
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:package_name forKey:@"package_name"];
            
            
            NSString *validity = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"validity"]];
            
            NSLog(@"validity :%@",validity);
            
            if([validity caseInsensitiveCompare:@"(null)"]==NSOrderedSame)
            {
                validity = @"";
            }
            
            if([validity caseInsensitiveCompare:@"active"]==NSOrderedSame )
            {
                
            }
            
            
            activePlan = [[NSUserDefaults standardUserDefaults]objectForKey:@"package_name"];
            
            NSString *end_date = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"end_date"]];
            
           
            compblock(YES);
            
            
        });
    }];
    
    [dataTask resume];
    
    
}

# pragma mark Button Actions

- (IBAction)menuBtnAction:(id)sender
{
    [self stopAudio];
    
    sideMenuViewController *svc=[[sideMenuViewController alloc]init];
    svc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    svc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:svc animated:YES completion:nil];
}

- (IBAction)searchAction:(id)sender
{
    //_searchBar.hidden=NO;
    //[_searchBar becomeFirstResponder];
    
    [self stopAudio];
    
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}

-(void)searchAction
{
    [self stopAudio];
    
    [self.view endEditing:YES];
    
    NSString *searchStr; NSString *catid;
    
    searchStr = _searchTF.text;
    
    if(([searchStr isEqualToString:@""]) || (searchStr.length == 0) )
    {
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *message = [languageBundle localizedStringForKey:@"searchEmpty" value:@"" table:@"strings-english"];
        
        [self.view makeToast:message
                    duration:1.5
                    position:CSToastPositionBottom];
    }
    else
    {
        catid = [[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"];
        
        cSearchViewController *seVC = [[cSearchViewController alloc]initWithNibName:@"cSearchViewController" bundle:nil];
        seVC.isfrmHome = YES;
        [seVC searchWS :catid :searchStr];
        [self.navigationController pushViewController:seVC animated:YES];
    }
}
- (IBAction)btnSelectionAction:(id)sender
{
    [self stopAudio];
    
    myFeedViewController *mfVC = [[myFeedViewController alloc]initWithNibName:@"myFeedViewController" bundle:nil];
    mfVC.isfrmHome = YES;
    [self.navigationController pushViewController:mfVC animated:YES];
}
- (IBAction)homeActn:(id)sender
{
    [self stopAudio];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
      // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"signinAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                [rvc presentViewController: lgVC animated: NO completion:nil];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        //[vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [rvc presentViewController: alert animated: NO completion:nil];
    }
    else
    {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        [self packagesWS :^(BOOL finished)
         {
             [self.view hideToastActivity];
             
             NSLog(@"activePlan :%@",activePlan);
             
             if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
             {
                 
                 
                 ///
                 NSString *lang = langGlobal;
                 NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                 NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                 
                 NSString *message = [languageBundle localizedStringForKey:@"loginAlertToPurchase" value:@"" table:@"strings-english"];
                 
                 NSString *ok = [languageBundle localizedStringForKey:@"ok" value:@"" table:@"strings-english"];
                 
                 NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
                 
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:message
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         inAppViewController *inapp = [[inAppViewController alloc] init];
                         UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:inapp];
                         navController.navigationBar.hidden = YES;
                         [self presentViewController:navController animated:YES completion:nil];
                         
                     });
                 }];
                 
                 UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                          {
                                              
                                          }];
                 
                 [alert addAction:okAction];
                 [alert addAction:cancel];
                 
                 // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
                 //[vc presentViewController:alert animated:YES completion:nil];
                 
                 UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                 UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                 [rvc presentViewController: alert animated: NO completion:nil];
                 
             }
             else
             {
             
             sendMessageViewController *smVC = [[sendMessageViewController alloc]initWithNibName:@"sendMessageViewController" bundle:nil];
             smVC.isfrmHelp = YES;
             [self.navigationController pushViewController:smVC animated:YES];
             }
             
         }];
        
     
    }
}

- (IBAction)supportBtnAction:(id)sender
{
    [self stopAudio];
    
    supportViewController *smVC = [[supportViewController alloc]initWithNibName:@"supportViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}
- (IBAction)keepGoingbtnAction:(id)sender
{
    [self stopAudio];
    
    supportViewController *smVC = [[supportViewController alloc]initWithNibName:@"supportViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}

-(void)playAudio
{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    if(_audioPlayer.isPlaying)
    {
      [_audioPlayer pause];
      [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
        return;
    }
    
    if(isFrmLogin || isFrmUpdate)
    {
        playedOnce = YES;
        
        isFrmUpdate = NO;
        
         isFrmLogin = NO;
        
    }
    
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        [self stopAudio];
        
        if(isFrmLangScreen)
        {
            playedOnce = YES;
            
            isFrmLangScreen = NO;
        }
    }
    
    if(playedOnce)
    {
    _speakerImg.hidden = YES;
    
       playedOnce = NO;
   // CGFloat halfButtonHeight = _playBtn.bounds.size.height ;
   // CGFloat buttonWidth = _playBtn.bounds.size.width;
    indicator.center = _playBtn.center;
    [self.view addSubview:indicator];
    [indicator startAnimating];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    int r = arc4random_uniform(10000);
    
        NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
        NSData *datauser = [defuser objectForKey:@"userData"];
        NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
        
        NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
        
        NSString *userid;
        
        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
        {
            userid =@"";
        }
        else
            userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
        
    NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
     {
         NSDictionary *json;
         
         if(data ==nil)
         {
             [self playAudio];
             return ;
         }
         
         json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
         NSLog(@"json :%@", json);
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             
             NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
             [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
             [def synchronize];
             
             
             //download audio
             NSArray *audioUrlArr = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"audio"];
             
             if([audioUrlArr isKindOfClass:[NSString class]])
             {
                 
             }
             else
             {
                 if(audioUrlArr.count >0)
                 {
                     NSString *url;
                     
                     if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                         url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                     
                     for(NSString *page in audioUrlArr)
                     {
                         if([[page valueForKey:@"page"]caseInsensitiveCompare:@"home"]==NSOrderedSame)
                         {
                             url = [page valueForKey:@"file"];
                             
                             NSURL *urlT = [[NSURL alloc] initWithString:url];
                             NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                             NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                             NSString *filePath = [documents stringByAppendingPathComponent:@"home.mp3"];
                             BOOL success = [soundData writeToFile:filePath atomically:YES];
                             
                             if(success)
                             {
                                 _speakerImg.hidden = NO;
                                 
                                 [indicator removeFromSuperview];
                                 [indicator stopAnimating];
                                 
                                 NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                                 NSData *data = [def objectForKey:@"settings"];
                                 NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                                 
                                 NSArray *audioUrlArr = [[retrievedDictionary valueForKey:@"audio"]objectAtIndex:0];
                                 NSString *path;
                                 
                                 NSString *url;
                                 
                                 if([audioUrlArr isKindOfClass:[NSString class]])
                                 {
                                     return;
                                 }
                                 else
                                 {
                                    if(audioUrlArr.count >0)
                                    {
                                         
                                         if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                                             url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                                         
                                         for(NSString *page in audioUrlArr)
                                         {
                                             if([[page valueForKey:@"page"]caseInsensitiveCompare:@"home"]==NSOrderedSame)
                                             {
                                                 url = [page valueForKey:@"file"];
                                             }
                                         }
                                         
                                         // [self downloadAudio:url];
                                         
                                         path = [NSString stringWithFormat:@"%@", url];
                                     }
                                     
                                     else
                                         path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
                                     
                                 }
                                 
                                 path = [NSString stringWithFormat:@"%@/home.mp3", [[NSBundle mainBundle] resourcePath]];
                                 
                                 
                                 NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                                 NSString *soundFilePath = [documentDir stringByAppendingPathComponent:@"home.mp3"];
                                 
                                 
                                 NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
                                 NSError *error = nil;
                                 
                                 _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
                                 _audioPlayer.delegate=self;
                                 
                                 
                                 if(!(_audioPlayer) || (_audioPlayer == nil))
                                 {
                                     
                                     [_audioPlayer play];
                                     
                                     [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                                     
                                 }
                                 
                                 else
                                 {
                                     
                                     {
                                         if((_speakerImg.image !=nil) && [_speakerImg.image isEqual:[UIImage imageNamed:@"ico-speaker-close.png"]])
                                         {
                                             NSLog(@"player is playing");
                                             
                                             [_audioPlayer pause];
                                             [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
                                         }
                                         else
                                         {
                                             NSLog(@"player not playing");
                                             
                                             [_audioPlayer play];
                                             
                                             [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                                         }
                                         
                                         
                                     }
                                 }
                             }
                             break;
                         }
                         
                         
                     }
                     
                     // [self writeAudioFilesToLocal];
                 }
                 
             }
             
         });
     }];
    
    
    [dataTask resume];
    
    
    }
    else
    {
        _speakerImg.hidden = NO;
        
        [indicator removeFromSuperview];
        [indicator stopAnimating];
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:@"settings"];
        NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        NSArray *audioUrlArr = [[retrievedDictionary valueForKey:@"audio"]objectAtIndex:0];
        NSString *path;
        
        NSString *url;
        
        if([audioUrlArr isKindOfClass:[NSString class]])
        {
            return;
        }
        else
        {
            if(audioUrlArr.count >0)
            {
                
                if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                    url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                
                for(NSString *page in audioUrlArr)
                {
                    if([[page valueForKey:@"page"]caseInsensitiveCompare:@"home"]==NSOrderedSame)
                    {
                        url = [page valueForKey:@"file"];
                    }
                }
                
                // [self downloadAudio:url];
                
                path = [NSString stringWithFormat:@"%@", url];
            }
            
            else
                path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
            
        }
        
        path = [NSString stringWithFormat:@"%@/home.mp3", [[NSBundle mainBundle] resourcePath]];
        
        
        NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *soundFilePath = [documentDir stringByAppendingPathComponent:@"home.mp3"];
        
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:soundFilePath];
        
        if(fileExists)
        {
            NSLog(@"fileExists");
            
            NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
            NSError *error = nil;
            
            _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
            _audioPlayer.delegate=self;
            
            
            if(!(_audioPlayer) || (_audioPlayer == nil))
            {
                
                [_audioPlayer play];
                
                [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                
            }
            
            else
            {
                
                {
                    if((_speakerImg.image !=nil) && [_speakerImg.image isEqual:[UIImage imageNamed:@"ico-speaker-close.png"]])
                    {
                        NSLog(@"player is playing");
                        
                        [_audioPlayer pause];
                        [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
                    }
                    else
                    {
                        NSLog(@"player not playing");
                        
                        [_audioPlayer play];
                        
                        [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                    }
                    
                    
                }
            }
        }
        else
        {
            NSLog(@"no file");
            
            [self downloadAudioWhenNoFile];
        }
        
        
        
        
    }
    
}

-(void)downloadAudioWhenNoFile
{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    {
        _speakerImg.hidden = YES;
        
        playedOnce = NO;
        // CGFloat halfButtonHeight = _playBtn.bounds.size.height ;
        // CGFloat buttonWidth = _playBtn.bounds.size.width;
        indicator.center = _playBtn.center;
        [self.view addSubview:indicator];
        [indicator startAnimating];
        
        NSInteger langid = 5;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
            langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
        
        int r = arc4random_uniform(10000);
        
        NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
        NSData *datauser = [defuser objectForKey:@"userData"];
        NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
        
        NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
        
        NSString *userid;
        
        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
        {
            userid =@"";
        }
        else
            userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
        
        NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
        
        NSLog(@"URL :%@", wUrl);
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = 100.0;
        sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            NSDictionary *json;
            
            if(data ==nil)
            {
                [self playAudio];
                return ;
            }
            
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"json :%@", json);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"settings"]] forKey:@"settings"];
                [def synchronize];
                
                
                //download audio
                NSArray *audioUrlArr = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"audio"];
                
                if([audioUrlArr isKindOfClass:[NSString class]])
                {
                    
                }
                else
                {
                    if(audioUrlArr.count >0)
                    {
                        NSString *url;
                        
                        if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                            url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                        
                        for(NSString *page in audioUrlArr)
                        {
                            if([[page valueForKey:@"page"]caseInsensitiveCompare:@"home"]==NSOrderedSame)
                            {
                                url = [page valueForKey:@"file"];
                                
                                NSURL *urlT = [[NSURL alloc] initWithString:url];
                                NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                                NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                NSString *filePath = [documents stringByAppendingPathComponent:@"home.mp3"];
                                BOOL success = [soundData writeToFile:filePath atomically:YES];
                                
                                if(success)
                                {
                                    _speakerImg.hidden = NO;
                                    
                                    [indicator removeFromSuperview];
                                    [indicator stopAnimating];
                                    
                                    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                                    NSData *data = [def objectForKey:@"settings"];
                                    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                                    
                                    NSArray *audioUrlArr = [[retrievedDictionary valueForKey:@"audio"]objectAtIndex:0];
                                    NSString *path;
                                    
                                    NSString *url;
                                    
                                    if([audioUrlArr isKindOfClass:[NSString class]])
                                    {
                                        return;
                                    }
                                    else
                                    {
                                        if(audioUrlArr.count >0)
                                        {
                                            
                                            if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                                                url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
                                            
                                            for(NSString *page in audioUrlArr)
                                            {
                                                if([[page valueForKey:@"page"]caseInsensitiveCompare:@"home"]==NSOrderedSame)
                                                {
                                                    url = [page valueForKey:@"file"];
                                                }
                                            }
                                            
                                            // [self downloadAudio:url];
                                            
                                            path = [NSString stringWithFormat:@"%@", url];
                                        }
                                        
                                        else
                                            path = [NSString stringWithFormat:@"%@/etest.mp3", [[NSBundle mainBundle] resourcePath]];
                                        
                                    }
                                    
                                    path = [NSString stringWithFormat:@"%@/home.mp3", [[NSBundle mainBundle] resourcePath]];
                                    
                                    
                                    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
                                    NSString *soundFilePath = [documentDir stringByAppendingPathComponent:@"home.mp3"];
                                    
                                    
                                    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
                                    NSError *error = nil;
                                    
                                    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&error];
                                    _audioPlayer.delegate=self;
                                    
                                    
                                    if(!(_audioPlayer) || (_audioPlayer == nil))
                                    {
                                        
                                        [_audioPlayer play];
                                        
                                        [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                                        
                                    }
                                    
                                    else
                                    {
                                        
                                        {
                                            if((_speakerImg.image !=nil) && [_speakerImg.image isEqual:[UIImage imageNamed:@"ico-speaker-close.png"]])
                                            {
                                                NSLog(@"player is playing");
                                                
                                                [_audioPlayer pause];
                                                [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
                                            }
                                            else
                                            {
                                                NSLog(@"player not playing");
                                                
                                                [_audioPlayer play];
                                                
                                                [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker-close.png"]];
                                            }
                                            
                                            
                                        }
                                    }
                                }
                                break;
                            }
                            
                            
                        }
                        
                        // [self writeAudioFilesToLocal];
                    }
                    
                }
                                                  
                                              
          });
        }];
        
        [dataTask resume];
        
        
    }
}

-(void)downloadAudio:(NSString *)urlString
{
    // Download
   // NSString *urlString = @"https://oi.net/songs/5-dope-ski-128k.mp3";
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSData *soundData = [NSData dataWithContentsOfURL:url];
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documents stringByAppendingPathComponent:@"home.mp3"];
    [soundData writeToFile:filePath atomically:YES];
    
    // Play
  /*  NSString *filename = @"Ola.mp3";
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    NSString *documentsDirectory = [pathArray objectAtIndex:0];
    NSString *yourSoundPath = [documentsDirectory stringByAppendingPathComponent:filename];
    
    NSURL *soundUrl;
    if ([[NSFileManager defaultManager] fileExistsAtPath:yourSoundPath])
    {
        soundUrl = [NSURL fileURLWithPath:yourSoundPath isDirectory:NO];
    }
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    [_audioPlayer play];
   */
}

-(void)playVideo
{
    [self stopAudio];
    
    _videoVw.hidden = NO;
    
   // NSString *url=@""; //https://www.youtube.com/embed/8NT-E-nrJ9M
    
    
  //  [self.webVw loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    self.webVw.scrollView.bounces = NO;
    [self.webVw setMediaPlaybackRequiresUserAction:NO];
    
    //[self.view addSubview:_videoVw];
}
-(void)closeVideo
{
    _videoVw.hidden = YES;
    //[_videoVw removeFromSuperview];
    
    NSString *script = @"var videos = document.querySelectorAll(\"video\"); for (var i = videos.length - 1; i >= 0; i--) { videos[i].pause(); };";
    [_webVw stringByEvaluatingJavaScriptFromString:script];
}

-(void)stopAudio
{
    [_audioPlayer pause];
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
}

-(void)writeHomeAudioToLocal
{
    
}

-(void)writeAudioFilesToLocal
{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *data = [def objectForKey:@"settings"];
        NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
    //download audio
    NSArray *audioUrlArr = [[retrievedDictionary objectAtIndex:0]valueForKey:@"audio"];
    
    if([audioUrlArr isKindOfClass:[NSString class]])
    {
        
    }
    else
    {
        if(audioUrlArr.count >0)
        {
            NSString *url;
            
            if([[audioUrlArr objectAtIndex:0]valueForKey:@"file"])
                url = [[audioUrlArr objectAtIndex:0]valueForKey:@"file"];
            
            for(NSString *page in audioUrlArr)
            {
                if([[page valueForKey:@"page"]caseInsensitiveCompare:@"home"]==NSOrderedSame)
                {
                    url = [page valueForKey:@"file"];
                    
                    NSURL *urlT = [[NSURL alloc] initWithString:url];
                    NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *filePath = [documents stringByAppendingPathComponent:@"home.mp3"];
                    [soundData writeToFile:filePath atomically:YES];
                }
                
                if([[page valueForKey:@"page"]caseInsensitiveCompare:@"1"]==NSOrderedSame)
                {
                    //Community
                    
                    url = [page valueForKey:@"file"];
                    
                    NSURL *urlT = [[NSURL alloc] initWithString:url];
                    NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *filePath = [documents stringByAppendingPathComponent:@"1.mp3"];
                    [soundData writeToFile:filePath atomically:YES];
                }
                
                if([[page valueForKey:@"page"]caseInsensitiveCompare:@"3"]==NSOrderedSame)
                {
                    //Features
                    
                    url = [page valueForKey:@"file"];
                    
                    NSURL *urlT = [[NSURL alloc] initWithString:url];
                    NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *filePath = [documents stringByAppendingPathComponent:@"3.mp3"];
                    [soundData writeToFile:filePath atomically:YES];
                }
                
                if([[page valueForKey:@"page"]caseInsensitiveCompare:@"4"]==NSOrderedSame)
                {
                    //Apps
                    
                    url = [page valueForKey:@"file"];
                    
                    NSURL *urlT = [[NSURL alloc] initWithString:url];
                    NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *filePath = [documents stringByAppendingPathComponent:@"4.mp3"];
                    [soundData writeToFile:filePath atomically:YES];
                }
                
                if([[page valueForKey:@"page"]caseInsensitiveCompare:@"5"]==NSOrderedSame)
                {
                    //Sites
                    
                    url = [page valueForKey:@"file"];
                    
                    NSURL *urlT = [[NSURL alloc] initWithString:url];
                    NSData *soundData = [NSData dataWithContentsOfURL:urlT];
                    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *filePath = [documents stringByAppendingPathComponent:@"5.mp3"];
                    [soundData writeToFile:filePath atomically:YES];
                }
                
                
            }
            
            
        }
        
        if(audioUrlArr == nil)
        {
            double delayInSeconds = 10.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
               
                //[self writeAudioFilesToLocal];
            });
        }
    }
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
        
    });

}

-(void)removeTransparentView
{
   [TransprentView removeFromSuperview];
    
}
-(void)bubbleClicked
{
    [self playAudio];
    [TransprentView removeFromSuperview];
}

-(void)btnCommunityNMoreAction
{
    [self stopAudio];
    
    [[NSUserDefaults standardUserDefaults]setObject:_header1.titleLabel.text forKey:@"mainCatName"];
    
    [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:@"catidFromHomeCat"];
    
    homeCategoryViewController *hcVC = [[homeCategoryViewController alloc]initWithNibName:@"homeCategoryViewController" bundle:nil];
    hcVC.isFrmHome = YES;
    hcVC.isFrmCommunityNMore = YES;
    [self.navigationController pushViewController:hcVC animated:YES];
}

-(void)offersAction
{
    [self stopAudio];
    
    offersListingViewController *loginViewController = [[offersListingViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    navController.navigationBar.hidden = YES;
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)closePopImg
{
    _popUpImgVw.hidden = YES;
}

-(void)customAdAction
{
    NSString *theUrl, *custom_banner_id;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSArray *custom_banner_arr = [[arrSet objectAtIndex:0]valueForKey:@"custom_banner_arr"];
    
    if(arrSet.count >0)
        custom_banner_id = [[custom_banner_arr objectAtIndex:index]valueForKey:@"custom_banner_id"];
        
    [self clicksWS:@"banner":custom_banner_id];
    
   // if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
   // theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
     theUrl = [[custom_banner_arr objectAtIndex:index]valueForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];

}

-(void)showInter
{
    interViewController*loginViewController = [[interViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    navController.navigationBar.hidden = YES;
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma mark avaudio delegate
-(void)itemDidFinishPlaying:(NSNotification *) notification
{
    // Will be called when AVPlayer finishes playing playerItem
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
}

#pragma mark audio delegates
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
}

#pragma mark SearchBar Cancel Action

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [_searchBar resignFirstResponder];
    _searchBar.hidden=YES;
}
#pragma mark status bar color

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}


#pragma mark UICollectionView methods

- (CGSize)itemSize
{
    NSInteger numberOfColumns = 2;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionVw.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   
    return [homeArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"home";
    
    UINib *nib = [UINib nibWithNibName:@"homeCollectionViewCell" bundle: nil];
    
    [collectionView registerNib:nib forCellWithReuseIdentifier:simpleTableIdentifier];
    
    NSInteger section = indexPath.row;
    
   homeCollectionViewCell *cell = (homeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    cell.homeLbl.text = [[homeArray objectAtIndex:section]valueForKey:@"name"];
  
   // cell.homeImg1.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[[homeArray objectAtIndex:section]valueForKey:@"image"]]];
    
    [cell.homeImg1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[homeArray objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    cell.tag = [[[homeArray objectAtIndex:section]valueForKey:@"tag"]integerValue];
    [cell.homeLbl setText:[cell.homeLbl.text uppercaseString]];
    
    cell.paddingLbl.layer.cornerRadius = cell.homeImg.layer.cornerRadius;
    
    [cell layoutSubviews];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    
    cell.homeImg.layer.cornerRadius = 10;
    
   // cell.homeImg1.layer.cornerRadius = 10;
    
//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.homeImg1.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(10, 10) ];
//
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//
//    maskLayer.frame = cell.homeImg1.bounds;
//    maskLayer.path = maskPath.CGPath;
//    cell.homeImg1.layer.mask = maskLayer;
    
    
   // [cell.homeImg.layer setBorderColor:[UIColor whiteColor].CGColor];
   // [cell.homeImg.layer setBorderWidth:2.0f];
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
//    if(pathToFirstRow==indexPath)
//    {
//        CGRect frame;
//        frame = cell.homeImg1.frame;
//        if(IS_IPAD)
//        frame.origin.x = cell.homeImg.frame.origin.x+3;
//        else
//           frame.origin.x = cell.homeImg.frame.origin.x+2;
//
//        cell.homeImg1.frame = frame;
//
//        cell.homeImg1.contentMode = UIViewContentModeScaleAspectFill;
//    }
//    else
    {
        CGRect frame;
        frame = cell.homeImg1.frame;
        frame.size.width = frame.size.width ;//- 2;
       // frame.size.height = frame.size.width;
        
        cell.homeImg1.frame = frame;
        
       // cell.homeImg.hidden  = YES;
        
        
        
       
        ///corner radius to imageview top left and top right
        UIBezierPath *maskPath = [UIBezierPath
                                  bezierPathWithRoundedRect:cell.homeImg1.bounds
                                  byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                  cornerRadii:CGSizeMake(10, 10)
                                  ];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        
        maskLayer.frame = self.view.bounds;
        maskLayer.path = maskPath.CGPath;
        
        cell.homeImg1.layer.mask = maskLayer;
        /////
       // [[cell.homeImg1 layer] setBorderWidth:2.0f];
       // [[cell.homeImg1 layer] setBorderColor:[UIColor whiteColor].CGColor];
        
        
        /////corner radius to label bottom left and bottom right
        UIBezierPath *maskPath2 = [UIBezierPath
                                  bezierPathWithRoundedRect:cell.paddingLbl.bounds
                                  byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                  cornerRadii:CGSizeMake(10, 10)
                                  ];
        CAShapeLayer *maskLayer2 = [CAShapeLayer layer];
        maskLayer2.frame = self.view.bounds;
        maskLayer2.path = maskPath2.CGPath;
        cell.paddingLbl.layer.mask = maskLayer2;
        ///
        
        
        CGRect padFrame = cell.paddingLbl.frame;
        padFrame.size.height = 40;
       // cell.paddingLbl.frame = padFrame;
        
        //cell.paddingLbl.hidden = NO;
        
       /* cell.layer.masksToBounds = NO;
        cell.layer.shadowOffset = CGSizeMake(-15, 20);
        cell.layer.shadowRadius = 5;
        cell.layer.shadowOpacity = 0.5;
        */
        
        
        cell.homeImg.layer.borderColor = [UIColor whiteColor].CGColor; //cellBorderColor;
        cell.homeImg.layer.borderWidth = 5.0;
        
        // *** Set masks bounds to NO to display shadow visible ***
        cell.layer.masksToBounds = NO;
        // *** Set light gray color as shown in sample ***
        cell.layer.shadowColor = [UIColor whiteColor].CGColor; //[UIColor blackColor].CGColor;
        // *** *** Use following to add Shadow top, left ***
        //cell.layer.shadowOffset = CGSizeMake(-5.0f, -5.0f);
        
        // *** Use following to add Shadow bottom, right ***
        //self.avatarImageView.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
        
        // *** Use following to add Shadow top, left, bottom, right ***
         cell.layer.shadowOffset = CGSizeZero;
         cell.layer.shadowRadius = 5.0f;
        
        // *** Set shadowOpacity to full (1) ***
        cell.layer.shadowOpacity = 0.6f;
        
    }
    
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self stopAudio];
    
    NSString *catid =[[homeArray objectAtIndex:indexPath.row]valueForKey:@"id"];
    
    [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"catidFromHomeCat"];
    
    NSString *catName =[[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"];
    
    
    NSString *sub_exists =[[homeArray objectAtIndex:indexPath.row]valueForKey:@"sub_exists"];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"catName"];
    
    [[NSUserDefaults standardUserDefaults]setObject:catid forKey:@"mainCategoryId"];
    
     [[NSUserDefaults standardUserDefaults]setObject:catName forKey:@"mainCatName"];
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    
   // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              
              
             
                  
    if([urlData length] == 0)
    {
        NSLog(@"internet is not connected");
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *no_internet = [languageBundle localizedStringForKey:@"no_internet" value:@"" table:@"strings-english"];
        
        [self showMessage:no_internet withTitle:nil];
    }
    else
    {
       NSLog(@"internet is connected");
        
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable )
        {
            //connection unavailable
            
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *no_internet = [languageBundle localizedStringForKey:@"no_internet" value:@"" table:@"strings-english"];
            
            [self showMessage:no_internet withTitle:nil];
        }
        else
        {
            
            NSInteger ctag;
            ctag = [[[homeArray objectAtIndex:indexPath.row]valueForKey:@"tag"]integerValue];
            
            [[NSUserDefaults standardUserDefaults]setInteger:ctag forKey:@"catTag"];
            
            [[NSUserDefaults standardUserDefaults]setObject:[[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"] forKey:@"HomeClickedCellName"];
            
           // NSString *subexist;
            
          /*  switch (ctag) {
                    
                case 2:
                    //experience
                {
                    subexist = @"experience_sub";
                    
                    break;
                }
                case 3:
                    //quick learn
                {
                    subexist = @"quicklearn_sub";
                    
                    break;
                }
                case 4:
                    //news
                {
                    subexist = @"news_sub_exists";
                    
                    break;
                }
                case 5:
                    //health
                {
                    subexist = @"health_sub_exists";
                    
                    break;
                }
                default:
                    break;
            }
            */
            NSLog(@"%@",subCatCheckArr);
            //subCatCheckArr
            
            @try {
                
                
                if([sub_exists caseInsensitiveCompare:@"yes"]==NSOrderedSame)
                {
                  //  categoryViewController *catVC = [[categoryViewController alloc]initWithNibName:@"categoryViewController" bundle:nil];
                  //  [self.navigationController pushViewController:catVC animated:YES];\
                    
                   
                    
                    homeCategoryViewController *hcVC = [[homeCategoryViewController alloc]initWithNibName:@"homeCategoryViewController" bundle:nil];
                    hcVC.isFrmHome = YES;
                    [self.navigationController pushViewController:hcVC animated:YES];
                }
                else
                {
                    NSString *name;
                    
                    name = [[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"];
                    
                    [[NSUserDefaults standardUserDefaults]setInteger:[catid integerValue] forKey:@"listid"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:name forKey:@"listname"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:name forKey:@"headerTopMO"];
                    
                    NSString *descrptn = [[homeArray objectAtIndex:indexPath.row]valueForKey:@"description"];
                    
                  //  NSString *name = [[homeArray objectAtIndex:indexPath.row]valueForKey:@"name"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:name forKey:@"headerTopMO"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:descrptn forKey:@"catDescription"];
                    
                    moListViewController *lVC = [[moListViewController alloc]initWithNibName:@"moListViewController" bundle:nil];
                    lVC.isFrmHome = YES;
                    [self.navigationController pushViewController:lVC animated:YES];
                    
                    
                    return;
                    homeCategoryViewController *hcVC = [[homeCategoryViewController alloc]initWithNibName:@"homeCategoryViewController" bundle:nil];
                    
                    communityViewController *cVC = [[communityViewController alloc]initWithNibName:@"communityViewController" bundle:nil];
                    
                    if([catid caseInsensitiveCompare:@"1"]==NSOrderedSame)
                    {
                        cVC.isFrmHomeC = YES;
                        [self.navigationController pushViewController:cVC animated:YES];
                    }
                    else
                    {
                        hcVC.isFrmHome = YES;
                     [self.navigationController pushViewController:hcVC animated:YES];
                    }
                        
                    
                }
                
            }
            @catch (NSException *exception)
            {
                NSString *lang = langGlobal;
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                NSString *no_internet = [languageBundle localizedStringForKey:@"no_internet" value:@"" table:@"strings-english"];
                
                [self showMessage:no_internet withTitle:nil];
            }
            
            
        }
    }
    
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
          
                         
                         });
          
          
      }] resume];
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
     if(IS_IPAD)
     {
        return 40.0;
     }
    else
    return 15.0; //0.01;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 40.0;
    }
    else
        return 0.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 2.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    
    CGSize size ;
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
//    if(pathToFirstRow==indexPath)
//    {
//       float cellWidth2 = screenWidth / 1;
//
//
//        if(IS_IPAD)
//        {
//            size = CGSizeMake(555, 250);//cellWidth-84
//        }
//        else
//            size = CGSizeMake(cellWidth2-35, cellWidth-25); //162
//    }
//    else
    {
        if(IS_IPAD)
        {
            size = CGSizeMake(250, 250);//cellWidth-84
        }
        else
        {
            if(screenWidth  == 320 )
            {
              //iphone 5s
                size = CGSizeMake(cellWidth-15, cellWidth-15);
            }
            else
            size = CGSizeMake(cellWidth-50, cellWidth-20); //162
        }
    }
    
    
    return size;
}


- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    if(IS_IPAD)
    {
        return UIEdgeInsetsMake(0, 105, 0, 105);
    }
    else
    {
      if(screenWidth  == 320 )
        return UIEdgeInsetsMake(0, 5, 0, 5);
      else
       return UIEdgeInsetsMake(0, 35, 0, 35); // top, left, bottom, right
    }
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    NSString *lang = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    NSString *ok = [languageBundle localizedStringForKey:@"ok" value:@"" table:@"strings-english"];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

# pragma mark check for internet Ping

-(BOOL)iConnected
{
   __block BOOL connectin;
    NSString *wUrl=[NSString stringWithFormat:@"%@static_pages.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self staticPagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
    
         if(json == nil)
         {
             connectin = NO;
         }
        else
        {
            connectin = YES;
        }
    }];
    
    [dataTask resume];
    
    return connectin;
}

# pragma mark textfield delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
