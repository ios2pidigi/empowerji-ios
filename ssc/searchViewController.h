//
//  searchViewController.h
//  ssc
//
//  Created by sarath sb on 4/16/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface searchViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *seTblVw;
@property (strong, nonatomic) IBOutlet UISearchBar *searhBar;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@end
