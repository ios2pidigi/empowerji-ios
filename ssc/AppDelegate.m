//
//  AppDelegate.m
//  ssc
//
//  Created by swaroop on 02/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "loginViewController.h"
#import "homeViewController.h"
#import "Constants.h"
#import "splashViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "UIImageView+WebCache.h"

#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

#import "GAI.h"
#import "GAIDictionaryBuilder.h"

#import "languagesViewController.h"
#import <OneSignal/OneSignal.h>
#import "notificationsViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <AppsFlyerLib/AppsFlyerTracker.h>

@interface AppDelegate () <AppsFlyerTrackerDelegate>
{
    BOOL isFrmPush;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FIRApp configure];
    
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"UfdjizscVNbbW79AbFEVhC";
    [AppsFlyerTracker sharedTracker].appleAppID = @"1397344988";
    [AppsFlyerTracker sharedTracker].delegate = self;
#ifdef DEBUG //[AppsFlyerTracker sharedTracker].isDebug = true;
#endif

    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"showPopUpImageView"];
    
    if (@available(iOS 10.0, *)) {
        [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
            
            NSLog(@"getDeliveredNotificationsWithCompletionHandler count %lu", (unsigned long)[notifications count]);
            
            if(notifications.count)
            {
                NSLog(@"notif count : %lu",(unsigned long)notifications.count);
                
                NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                
                NSInteger count = 0;
                
                if([userDefaults integerForKey:@"notificationCount"])
                {
                    count = [userDefaults integerForKey:@"notificationCount"];
                }
                
                count = notifications.count;
                
                [userDefaults setInteger:count forKey:@"notificationCount"];
                
                [userDefaults synchronize];
                
                //[[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
            }
            
        }];
    } else {
        // Fallback on earlier versions
    }
    
    
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) ) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        //if( option != nil )
        //{
        //    NSLog( @"registerForPushWithOptions:" );
        //}
    } else {
        if (@available(iOS 10.0, *)) {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if( !error ) {
                    // required to get the app to do anything at all about push notifications
                    
                dispatch_async(dispatch_get_main_queue(),
                    ^{
                    
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                      });
                    
                    
                    NSLog( @"Push registration success." );
                } else {
                    NSLog( @"Push registration FAILED" );
                    NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                    NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
                }
            }];
            
        }
        else
        {
            // Fallback on earlier versions
        }
        
    }
    /////
    
    
   // id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    //[tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          //action:@"button_press"
                                                          // label:@"play"
                                                          // value:nil] build]];
    
    
    //analytics
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
   // [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    //[GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
   // [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:analyticsID];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"pages"
                                                          action:@"home"
                                                           label:@"Test"
                                                           value:nil] build]];
    
    //crashlytics
        [Fabric with:@[[Crashlytics class]]];
    
    
    //Onesignal
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:oneSignalID
            handleNotificationAction:nil
                            settings:@{kOSSettingsKeyAutoPrompt: @false}];
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
    
    // Recommend moving the below line to prompt for push after informing the user about
    //   how your app will use them.
    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted)
     {
         NSLog(@"User accepted notifications: %d", accepted);
     }];
    
    
    //fbsdk
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];//fbsdk
    
    
     self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:finalPath])
    {
        NSError *error;
        NSString * sourcePath = [[NSBundle mainBundle] pathForResource:@"LocalData" ofType:@"plist"];
        [fileManager copyItemAtPath:sourcePath toPath:finalPath error:&error];
    }
    
   /* loginViewController* loginVC = [[loginViewController alloc] init];
    
    homeViewController* homeVC = [[homeViewController alloc] init];
   
   // if([[NSUserDefaults standardUserDefaults]objectForKey:@"keepmeloggedin"])
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        navigationController = [[UINavigationController alloc]initWithRootViewController:homeVC];
    }
    else
    navigationController = [[UINavigationController alloc]initWithRootViewController:loginVC];
    
    [self.window setRootViewController:navigationController];
    
    navigationController.navigationBarHidden=YES;
    */
    
    
    
    splashViewController *spVC = [[splashViewController alloc]initWithNibName:@"splashViewController" bundle:nil];
    
    [self.window setRootViewController:spVC];
    
    [self homeCategotyWS];
    
    [self.window makeKeyAndVisible];
    
    if (launchOptions != nil)
    {
        //opened from a push notification when the app is closed
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (userInfo != nil)
        {
            
            isFrmPush = YES;
            
        }
    }
    
    //check if user has the latest version
  /* BOOL updateNeeded = [self needsUpdate];
    
    if(updateNeeded)
    {
        
        
    }
   */
    
    //[self settingsWebservice];
    
   // [self localNotif];
    
    
    return YES;
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
API_AVAILABLE(ios(10.0))
{
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
    
    
    NSLog(@"userinfo from msg :%@",notification.request.content.userInfo);
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    
    NSString *lang_id = [[notification.request.content.userInfo valueForKey:@"aps"] valueForKey:@"lang_id"];
    
    NSLog(@"lang_id :%@",lang_id);
    
    
    
    // If your app is running
    if (state == UIApplicationStateActive)
    {
        /*
         aps =     {
         alert = "test ios - sdf";
         badge = 1;
         "lang_id" = 5;
         sound = default;
         };
         */
        if((!lang_id)||(lang_id == nil))
        {
            
        }
        else
        {
            NSString *langidMain;
            if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
                langidMain = [NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"]];
            else
            {
                langidMain = @"";
            }
            
            if([lang_id caseInsensitiveCompare:langidMain]==NSOrderedSame)
            {
                completionHandler(UNNotificationPresentationOptionAlert);
                
                NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                
                NSInteger count = 0;
                
                if([userDefaults integerForKey:@"notificationCount"])
                {
                    count = [userDefaults integerForKey:@"notificationCount"];
                }
                
                [userDefaults setInteger:count+1 forKey:@"notificationCount"];
                
                [userDefaults synchronize];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification2" object:self];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadT" object:self];
            }
        }
        
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler
API_AVAILABLE(ios(10.0))
{
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
    
    @try
    {
        NSString *userid = [response.notification.request.content.userInfo valueForKey:@"user_id"];
        
        NSString *taskid = [response.notification.request.content.userInfo valueForKey:@"task_id"];
        
        NSLog(@"userid from push : %@",userid);
        
        NSLog(@"taskid from push : %@",taskid);
        
        [[NSUserDefaults standardUserDefaults]setObject:userid forKey:@"useridFrmDetail"];
        
        [[NSUserDefaults standardUserDefaults]setObject:taskid forKey:@"taskidFrmDetail"];
        
        
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        
        NSString *open_page = [response.notification.request.content.userInfo valueForKey:@"open_page"];
        
        NSLog(@"open_page :%@",open_page);
        
        // if (state == UIApplicationStateActive)
        /*
         "open_page" = task;
         "task_id" = 582;
         "tech_id" = 3;
         "tech_name" = "Swaroop Attingal";
         "user_id" = 20;
         "user_name" = "swaroop S";
         */
        
        if((!open_page)||(open_page == nil))
        {
            
        }
        else
        {
            
        }
        
        notificationsViewController *nVC = [[notificationsViewController alloc]initWithNibName:@"notificationsViewController" bundle:nil];
        nVC.isfrmAppdelegate = YES;
       
       // navigationController.definesPresentationContext = YES;
        //[navigationController presentViewController:nVC animated:NO completion:nil];
        
        homeViewController* homeVC = [[homeViewController alloc] init];
        homeVC.isfrmPush = YES;
        
        navigationController = [[UINavigationController alloc]initWithRootViewController:homeVC];
        
        
        if (state == UIApplicationStateActive)
        {
            [self.window setRootViewController:navigationController];
            
            navigationController.navigationBarHidden=YES;
        }
        else if(state == UIApplicationStateBackground || state == UIApplicationStateInactive)
        {
            if(isFrmPush)
            {
                
            }
            else
            {
                [self.window setRootViewController:navigationController];
                
                navigationController.navigationBarHidden=YES;
            }
            
        }
        else
        {
            
        }
        
        
        
        
        //[navigationController pushViewController:nVC animated:YES];
        
        
       // self.definesPresentationContext = YES; //self is presenting view controller
        // mfVC.view.backgroundColor = [UIColor clearColor];
        // mfVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
      //  [self presentViewController:nVC animated:NO completion:nil];
        
    }
    @catch (NSException *exception)
    {
        
    }
    
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    application.applicationIconBadgeNumber = 0;
    NSLog(@"userInfo %@",userInfo);
    
    for (id key in userInfo) {
        NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    
    [application setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]];
    
    NSLog(@"Badge %d",[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]);
    
}


-(void)settingsWebservice
{
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    int r = arc4random_uniform(10000);
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
   NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid =@"";
    }
    else
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSString *wUrl=[NSString stringWithFormat:@"%@app_settings.php?device_type=ios&lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
           // [self settingsWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(),
        ^{
            
            NSString *ios_version;
            NSString *ios_force_update;
            
            ios_version = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"ios_version"];
            
            ios_force_update = [[[json objectForKey:@"settings"]objectAtIndex:0]valueForKey:@"ios_force_update"];
            
           
            
            
            if([ios_force_update caseInsensitiveCompare:@"yes"]==NSOrderedSame)
            {
                NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
                
                NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
                
                float cVersion = [currentVersion floatValue];
                
                float iVersion = [ios_version floatValue];
                
                if(fabsf(cVersion)<fabsf(iVersion))
                {
                    //show alert
                    NSString *lang = langGlobal;
                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    NSString *forceUpddateAlert = [languageBundle localizedStringForKey:@"forceUpddateAlert" value:@"" table:@"strings-english"];
                    
                    NSString *forceUpdate = [languageBundle localizedStringForKey:@"forceUpdate" value:@"" table:@"strings-english"];
                    
                    NSString *exit = [languageBundle localizedStringForKey:@"exit" value:@"" table:@"strings-english"];
                    
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:forceUpddateAlert delegate:self cancelButtonTitle:nil otherButtonTitles:forceUpdate,exit, nil];
                    
                    alert.tag = 007;
                      [alert show];
                }

                
               // abort();
            }
            
          });
        }];
    
    [dataTask resume];
    
}


//check force update of app from iTunes
-(BOOL) needsUpdate
{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    if ([lookup[@"resultCount"] integerValue] == 1){
        NSString* appStoreVersion = lookup[@"results"][0][@"version"];
        NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
        if (![appStoreVersion isEqualToString:currentVersion]){
            NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
            return YES;
        }
    }
    return NO;
}


- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the OK/Cancel buttons
    
    if(actionSheet.tag == 007)
    {
    if (buttonIndex == 1)
    {
        abort();

    }
    else
    {
        NSString *iTunesLink = @"https://itunes.apple.com/us/app/empowerji/id1397344988?ls=1&mt=8";
        //@"itms-apps://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftwareUpdate?id=<appid>&mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        
    }
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    isFrmPush = NO;

}
-(void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler
{
    NSLog(@"did receive remote notifications");
    
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    
    NSString *lang_id = [[userInfo valueForKey:@"aps"] valueForKey:@"lang_id"];
    
    NSLog(@"state background : %ld langid : %@",(long)state,lang_id);
    
    
    if (state == UIApplicationStateActive)
    {
        
    }
    else
    {
       
        if((!lang_id)||(lang_id == nil))
        {
            
        }
        else
        {
            NSString *langidMain;
            if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
                langidMain = [NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"]];
            else
            {
                langidMain = @"";
            }
            
            if([lang_id caseInsensitiveCompare:langidMain]==NSOrderedSame)
            {
                NSLog(@"same");
                
                NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                
                NSInteger count = 0;
                
                if([userDefaults integerForKey:@"notificationCount"])
                {
                    count = [userDefaults integerForKey:@"notificationCount"];
                }
                
                [userDefaults setInteger:count+1 forKey:@"notificationCount"];
                
                [userDefaults synchronize];
               
            }
            else
            {
                
                NSLog(@"different");
            }
        }
    }
        
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification2" object:self];
    
    
}

- (void)application:(UIApplication *)application
handleEventsForBackgroundURLSession:(NSString *)identifier
  completionHandler:(void (^)(void))completionHandler
{
   NSLog(@"background notification for push");
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"msg applicationWillEnterForeground");
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    if (@available(iOS 10.0, *)) {
        [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
            NSLog(@"msg getDeliveredNotificationsWithCompletionHandler count %lu", (unsigned long)[notifications count]);
            
            for (UNNotification* notification in notifications) {
                // do something with object
                NSLog(@"msg noti %@", notification.request);
            }
            
        }];
    } else {
        // Fallback on earlier versions
    }
#endif
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
   
        [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    
    if (@available(iOS 10.0, *)) {
        [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
            
            NSLog(@"getDeliveredNotificationsWithCompletionHandler count %lu", (unsigned long)[notifications count]);
            
            if(notifications.count)
            {
                NSLog(@"notif count : %lu",(unsigned long)notifications.count);
                
                NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                
                NSInteger count = 0;
                
                if([userDefaults integerForKey:@"notificationCount"])
                {
                    count = [userDefaults integerForKey:@"notificationCount"];
                }
                
                count = notifications.count;
                
                [userDefaults setInteger:count forKey:@"notificationCount"];
                
                [userDefaults synchronize];
                
                
                
                //[[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
            }
            
        }];
    } else {
        // Fallback on earlier versions
    }
    
    
     [self settingsWebservice];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadT" object:self];
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

}

-(void)localNotif
{
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert)
                              completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if (!error) {
                 NSLog(@"request authorization succeeded!");
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                 
                 UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
                 
                 objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"Alert" arguments:nil];
                 
                 objNotificationContent.body = [NSString localizedUserNotificationStringForKey:@"Test" arguments:nil];
                 
                 objNotificationContent.sound = [UNNotificationSound defaultSound];
                 
                  // dispatch_async(dispatch_get_main_queue(), ^{
                 
                 // 4. update application icon badge number
                // objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
                 
                   //  });
                 
                 // Deliver the notification in five seconds.
                 UNTimeIntervalNotificationTrigger *trigger =  [UNTimeIntervalNotificationTrigger                                             triggerWithTimeInterval:60.f repeats:YES];
                 
                 UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"ten"                                                                            content:objNotificationContent trigger:trigger];
                 
                 // 3. schedule localNotification
                 UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                 
                 [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                     if (!error) {
                         NSLog(@"Local Notification succeeded");
                         
                        // [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
                         

                         
                         
                     } else {
                         NSLog(@"Local Notification failed");
                     }
                     
                     
                 }];
                 
                 });
                 
                 // MPRemoteCommandCenter.sharedCommandCenter;
                 
                 
                 
             }
         }];
        
    } else {
        // Fallback on earlier versions
    }
    

}

-(void)homeCategotyWS
{
    //app.empowerji.com/webservice3/home_category.php
   
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@home_category.php?lang_id=%ld",baseUrl,(long)langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self homeCategotyWS];
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:@"You need an active internet connection"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                [self homeCategotyWS];
                
            }];
            [alert addAction:okAction];
            UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            [vc presentViewController:alert animated:YES completion:nil];
            
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"categories"]] forKey:@"homeCategory"];
            [def synchronize];
            
           
            loginViewController* loginVC = [[loginViewController alloc] init];
            
            homeViewController* homeVC = [[homeViewController alloc] init];
            
            languagesViewController* langVC = [[languagesViewController alloc] init];
            
            // if([[NSUserDefaults standardUserDefaults]objectForKey:@"keepmeloggedin"])
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
            {
                UIImageView *testHomeImage = [[UIImageView alloc]init];
                
                NSString *imgUrl =[[[json objectForKey:@"categories"]objectAtIndex:0]valueForKey:@"image"];
                
                
                [testHomeImage sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL)
                 {
                     NSLog(@"success");
                     
                     dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
                     dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                         
                         
                     });
                     
                     
                 }];
                
                     navigationController = [[UINavigationController alloc]initWithRootViewController:homeVC];
                
                if(isFrmPush)
                homeVC.isfrmPush = YES;
                
                     [self.window setRootViewController:navigationController];
                     
                     navigationController.navigationBarHidden=YES;
                
            }
            else
            {
                if(![[NSUserDefaults standardUserDefaults]objectForKey:@"firstInstallLANG"])
                {
                    navigationController = [[UINavigationController alloc]initWithRootViewController:langVC];
                    langVC.isFromAppdelegate = YES;
                    
                    [self.window setRootViewController:navigationController];
                    
                    navigationController.navigationBarHidden=YES;
                }
                else
                {
                    UIImageView *testHomeImage = [[UIImageView alloc]init];
                    
                    NSString *imgUrl =[[[json objectForKey:@"categories"]objectAtIndex:0]valueForKey:@"image"];
                    
                    [testHomeImage sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL)
                     {
                         NSLog(@"success");
                         
                         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
                         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                             
                             
                         });
                         
                         
                     }];
                    
                    navigationController = [[UINavigationController alloc]initWithRootViewController:homeVC];
                    
                    if(isFrmPush)
                        homeVC.isfrmPush = YES;
                    
                    [self.window setRootViewController:navigationController];
                    
                    navigationController.navigationBarHidden=YES;
                    
                }
                
                
            }
            
        });
    }];
    
    [dataTask resume];
    
}


//push
#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}


- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)(void))completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"deviceToken :%@",deviceToken);
    
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    //token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"apnsToken"]; //save token to resend it if request fails
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"apnsTokenSentSuccessfully"]; // set flag for request status
    
    
    [[AppsFlyerTracker sharedTracker] registerUninstall:deviceToken];
    
    // [DataUpdater sendUserToken]; //send token
    
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"deviceToken-error :%@",error);
}


#pragma mark Facebook Helpers
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
    // Add any custom logic here.
    return handled;
}

-(void)trackEvents
{
    [[AppsFlyerTracker sharedTracker] trackEvent:AFEventPurchase
                                  withValues: @{
                                                AFEventParamRevenue: @200,
                                                AFEventParamCurrency: @"USD",
                                                AFEventParamQuantity: @2,
                                                AFEventParamContentId: @"092",
                                                AFEventParamReceiptId: @"9277"
                                                }];
}

/*
///Local notification
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
API_AVAILABLE(ios(10.0))
{
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
    
    
    NSLog(@"userinfo from msg :%@",notification.request.content.userInfo);
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    
    // If your app is running
    if (state == UIApplicationStateActive)
    {
        
        //You need to customize your alert by yourself for this situation. For ex,
        //  NSString *cancelTitle = @"Close";
        // NSString *showTitle = @"Get Photos";
        // NSString *message = [[notification.request.content.userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        
        
        if(![navigationController.topViewController isKindOfClass:[chatViewController class]])
        {
            completionHandler(UNNotificationPresentationOptionAlert);
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessagesUponPush" object:self];
        }
        
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler
API_AVAILABLE(ios(10.0))
{
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"Notification response :%@", response.notification.request.content.userInfo);
    
 
    
    @try{
        
        
        NSString *open_page = [response.notification.request.content.userInfo valueForKey:@"open_page"];
        
        NSLog(@"open_page :%@",open_page);
        
        NSString *userid = [response.notification.request.content.userInfo valueForKey:@"user_id"];
        
        NSString *taskid = [response.notification.request.content.userInfo valueForKey:@"task_id"];
        
        NSLog(@"userid from push : %@",userid);
        
        NSLog(@"taskid from push : %@",taskid);
        
        [[NSUserDefaults standardUserDefaults]setObject:userid forKey:@"useridFrmDetail"];
        
        [[NSUserDefaults standardUserDefaults]setObject:taskid forKey:@"taskidFrmDetail"];
        
        if((!open_page)||(open_page == nil))
        {
            if(![navigationController.topViewController isKindOfClass:[chatViewController class]])
            {
                [[NSUserDefaults standardUserDefaults]setObject:taskid forKey:@"chatVCSecondHeaderID"];
                
                chatViewController *chatVC = [[chatViewController alloc]initWithNibName:@"chatViewController" bundle:nil];
                [navigationController pushViewController:chatVC animated:YES];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessagesUponPush" object:self];
            }
            
        }
        else
        {
            if([open_page caseInsensitiveCompare:@"task"]==NSOrderedSame)
            {
                newTaskViewController *ntVC = [[newTaskViewController alloc]initWithNibName:@"newTaskViewController" bundle:nil];
                [navigationController pushViewController:ntVC animated:YES];
            }
            else
            {
                if(![navigationController.topViewController isKindOfClass:[chatViewController class]])
                {
                    chatViewController *chatVC = [[chatViewController alloc]initWithNibName:@"chatViewController" bundle:nil];
                    // [navigationController pushViewController:chatVC animated:YES];
                }
                else
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshChatMessagesUponPush" object:self];
                }
                
            }
        }
    }
    @catch (NSException *exception)
    {
        
    }
}


//Your app receives push notification.

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"Remote Notification userInfo is %@", userInfo);
    
    NSNumber *contentID = userInfo[@"content-id"];
    // Do something with the content ID
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo

{
    
    [self application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result)
     {
         NSLog(@"userinfo from msg 2:%@",userInfo);
     }];
    
    
    NSLog(@"userinfo from msg :%@",userInfo);
    
    UIApplicationState state = [application applicationState];
    
    // If your app is running
    if (state == UIApplicationStateActive)
    {
        
        //You need to customize your alert by yourself for this situation. For ex,
        NSString *cancelTitle = @"Close";
        // NSString *showTitle = @"Get Photos";
        NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        
 
        
    }
    // If your app was in in active state
    else if (state == UIApplicationStateInactive)
    {
    }
}

*/
@end

//prints the font names used in application
/*for (NSString* family in [UIFont familyNames])
{
    NSLog(@"%@", family);
    
    for (NSString* name in [UIFont fontNamesForFamilyName: family])
    {
        NSLog(@"  %@", name);
    }
}*/

