//
//  homeViewController.h
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface homeViewController : UIViewController

@property (strong, nonatomic) IBOutlet UICollectionView *collectionVw;
//@property (strong, nonatomic) IBOutlet UIView *adVw;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIButton *supportBtn;
@property (strong, nonatomic) IBOutlet UIButton *keepGoingBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@property (strong, nonatomic) IBOutlet UIButton *helpBtn;
@property (strong, nonatomic) IBOutlet UIView *videoVw;
@property (strong, nonatomic) IBOutlet UIButton *closeVideoBtn;
@property (strong, nonatomic) IBOutlet UIWebView *webVw;
@property (strong, nonatomic) IBOutlet UIButton *btnMenu;
@property (strong, nonatomic) IBOutlet UIButton *btnContactUs;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;
@property (strong, nonatomic) IBOutlet UIImageView *speakerImg;
@property (strong, nonatomic) IBOutlet UIImageView *helpImg;
@property (strong, nonatomic) IBOutlet UILabel *helpLbl;

//@property (strong, nonatomic) IBOutlet UILabel *header1;
@property (strong, nonatomic) IBOutlet UIButton *header1;
@property (strong, nonatomic) IBOutlet UILabel *header2;
@property (strong, nonatomic) IBOutlet UITextField *searchTF;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) IBOutlet UIButton *btnCommunityNMore;

@property (strong, nonatomic) IBOutlet UILabel *counterLbl;
@property (strong, nonatomic) IBOutlet UIView *popUpImgVw;
@property (strong, nonatomic) IBOutlet UIButton *btnClosePopImg;

@property (strong, nonatomic) IBOutlet UIImageView *logoImg;


@property(nonatomic) BOOL isfrmPush;
@property(nonatomic) BOOL isFrmUpdate;
@property(nonatomic) BOOL isFrmLogin;
@property(nonatomic) BOOL isFrmLangScreen;
@property(nonatomic) BOOL isFrmSignOut;
@property(nonatomic) BOOL isFrmInAPP;


-(void)resetCounter;

-(void)updateCat;

-(void)customAdAction;

@end
