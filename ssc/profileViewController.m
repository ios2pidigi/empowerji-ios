//
//  profileViewController.m
//  ssc
//
//  Created by swaroop on 17/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "profileViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "profileTableViewCell.h"
#import "subCatTableViewCell.h"
#import "connectivity.h"
#import "homeViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface profileViewController ()
{
    NSArray *pArray;
    NSArray *arrUser;
    NSMutableArray *totalcheckmarkArray;
    
    NSArray *ageArr;
    NSMutableArray *langArr;
    
    BOOL changed;
}
@end

@implementation profileViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    changed = NO;
    
    _myProfileVw.frame = [UIScreen mainScreen].bounds;
    _changePassVw.frame = [UIScreen mainScreen].bounds;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    [self languagesWS];
    
    NSString *nameTxt;
    nameTxt = [[arrUser objectAtIndex:0]valueForKey:@"name"];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userNameL"])
    {
        
     [[NSUserDefaults standardUserDefaults]setObject:nameTxt forKey:@"userNameL"];
    
        self.nameTF.text = nameTxt;
    }
    else
    {
        self.nameTF.text =  nameTxt;//[[NSUserDefaults standardUserDefaults]objectForKey:@"userNameL"];
    }
    
    NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    NSString *hello = [languageBundle localizedStringForKey:@"hello" value:@"" table:@"strings-english"];
    
    NSString *youremail = [languageBundle localizedStringForKey:@"enter_email_txt" value:@"" table:@"strings-english"];
    
    NSString *yourPhone = [languageBundle localizedStringForKey:@"yourPhone" value:@"" table:@"strings-english"];
    
    nameTxt = [nameTxt stringByReplacingOccurrencesOfString:@"%20"
                                               withString:@" "];
    
    _welcomeLbl.text = [NSString stringWithFormat:@" %@, %@",hello,nameTxt];
    
    NSString *str = [[arrUser objectAtIndex:0]valueForKey:@"email"];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail2"])
    _emailLbl.text = [NSString stringWithFormat:@" %@: %@",youremail,[[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail2"]];
    else
        _emailLbl.text = [NSString stringWithFormat:@" %@:  ",youremail];
    
    if(str)
    {
      _emailLbl.text = [NSString stringWithFormat:@" %@: %@",youremail,str];
    }
    
    NSString *mobNum;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userMobileNum"])
    {
        mobNum = [[NSUserDefaults standardUserDefaults]objectForKey:@"userMobileNum"];
    }
    else
    mobNum = [[arrUser objectAtIndex:0]valueForKey:@"mobile_number"];
    
    mobNum = [[arrUser objectAtIndex:0]valueForKey:@"mobile_number"];
    
    _phoneLbl.text = [NSString stringWithFormat:@" %@: %@",yourPhone,mobNum];
    
    _nameTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _emailTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _numTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    
    _cityTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _ageTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _languageTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _otherTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _nameTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _nameTF.layer.borderWidth= 1.0f;
    
    _emailTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _emailTF.layer.borderWidth= 1.0f;
    
    _numTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _numTF.layer.borderWidth= 1.0f;
    
    _passwordTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _passwordTF.layer.borderWidth= 1.0f;
    
    
    _npTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _npTF.layer.borderWidth= 1.0f;
    
    _cpTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _cpTF.layer.borderWidth= 1.0f;
    
    
    _cityTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _cityTF.layer.borderWidth= 1.0f;
    
    _ageTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _ageTF.layer.borderWidth= 1.0f;
    
    _languageTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _languageTF.layer.borderWidth= 1.0f;
    
    _otherTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _otherTF.layer.borderWidth= 1.0f;
    
    NSString *city = [[arrUser objectAtIndex:0]valueForKey:@"city"];
    
    _cityTF.text = [NSString stringWithFormat:@"%@",city];
    
    NSString *age_group = [[arrUser objectAtIndex:0]valueForKey:@"age_group"];
    
    _ageTF.text = [NSString stringWithFormat:@"%@",age_group];
    
    NSString *lang_id = [NSString stringWithFormat:@"%@",[[arrUser objectAtIndex:0]valueForKey:@"lang_id"]];
    
    
    [_btnSelAge addTarget:self action:@selector(ageSelAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSelLang addTarget:self action:@selector(langSelAction) forControlEvents:UIControlEventTouchUpInside];
    
    if([lang_id caseInsensitiveCompare:@"0"]==NSOrderedSame)
    {
        _otherTF.hidden = NO;
        
        NSString *language = [[arrUser objectAtIndex:0]valueForKey:@"language"];
        
        _otherTF.text = [NSString stringWithFormat:@"%@",language];
        
        _languageTF.text = [NSString stringWithFormat:@"Other"];
        
    }
    else
    {
        _otherTF.hidden = YES;
        
        NSString *language = [[arrUser objectAtIndex:0]valueForKey:@"language"];
        
        _languageTF.text = [NSString stringWithFormat:@"%@",language];
    }
    
    
    
    ageArr = [[NSArray alloc]initWithObjects:@"< 50 ",@"50 - 65",@"65 - 75",@"above 75", nil];
    
    [_ageTblVw reloadData];
    
   // [self subCatWebservice];
    
    if(([_nameTF.text caseInsensitiveCompare:@"(null)"]==NSOrderedSame) || ([_nameTF.text caseInsensitiveCompare:@"null"]==NSOrderedSame) || ([_nameTF.text caseInsensitiveCompare:@"<null>"]==NSOrderedSame))
    {
        _nameTF.text = @"";
    }
    if(([_numTF.text caseInsensitiveCompare:@"(null)"]==NSOrderedSame) || ([_numTF.text caseInsensitiveCompare:@"null"]==NSOrderedSame) || ([_numTF.text caseInsensitiveCompare:@"<null>"]==NSOrderedSame))
    {
        _numTF.text = @"";
    }
    if(([_cityTF.text caseInsensitiveCompare:@"(null)"]==NSOrderedSame) || ([_cityTF.text caseInsensitiveCompare:@"null"]==NSOrderedSame) || ([_cityTF.text caseInsensitiveCompare:@"<null>"]==NSOrderedSame))
    {
        _cityTF.text = @"";
    }
    if(([_ageTF.text caseInsensitiveCompare:@"(null)"]==NSOrderedSame) || ([_ageTF.text caseInsensitiveCompare:@"null"]==NSOrderedSame) || ([_ageTF.text caseInsensitiveCompare:@"<null>"]==NSOrderedSame))
    {
        _ageTF.text = @"";
    }
    if(([_languageTF.text caseInsensitiveCompare:@"(null)"]==NSOrderedSame) || ([_languageTF.text caseInsensitiveCompare:@"null"]==NSOrderedSame) || ([_languageTF.text caseInsensitiveCompare:@"<null>"]==NSOrderedSame))
    {
        _languageTF.text = @"";
    }
    
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Profile Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    //make menu btn background gradient
    CAGradientLayer *gradientbtn = [CAGradientLayer layer];
    gradientbtn.frame = _updateBtn.bounds;
    gradientbtn.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_updateBtn.layer insertSublayer:gradientbtn atIndex:0];
    
    
    CAGradientLayer *gradientbtn2 = [CAGradientLayer layer];
    gradientbtn2.frame = _editBtn.bounds;
    gradientbtn2.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_editBtn.layer insertSublayer:gradientbtn2 atIndex:0];
    
    CAGradientLayer *gradientbtn3 = [CAGradientLayer layer];
    gradientbtn3.frame = _changePassBtn.bounds;
    gradientbtn3.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_changePassBtn.layer insertSublayer:gradientbtn3 atIndex:0];
    
    CAGradientLayer *gradientbtn4 = [CAGradientLayer layer];
    gradientbtn4.frame = _updateCPBtn.bounds;
    gradientbtn4.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_updateCPBtn.layer insertSublayer:gradientbtn4 atIndex:0];
    
    _editBtn.layer.cornerRadius = _editBtn.frame.size.height/2;
    _changePassBtn.layer.cornerRadius = _changePassBtn.frame.size.height/2;
    _updateBtn.layer.cornerRadius = _updateBtn.frame.size.height/2;
    _updateCPBtn.layer.cornerRadius = _updateCPBtn.frame.size.height/2;
    
    int heightt = 0;
    if(IS_IPAD)
    {
        heightt = 40;
    }
    else
        heightt = 25;
    
    CGRect frame = _ageTblVw.frame;
    frame.size.height = heightt * ageArr.count;//_ageTblVw.contentSize.height;
    frame.origin.y = _ageTF.frame.origin.y - (heightt * ageArr.count);
    _ageTblVw.frame = frame;
    
    CGRect frame2 = _langTblVw.frame;
    frame2.size.height = _langTblVw.contentSize.height;
    frame2.origin.y = _languageTF.frame.origin.y - _langTblVw.contentSize.height;
   // _langTblVw.frame = frame2;
}

# pragma mark WebService Calls

-(void)updateWebservice
{
    //listlinkz.com/ssc/webservice/update_profile.php?user_id=2&name=tester1&mobile_number=2222222222
    
    
    
    [SVProgressHUD show];
    
    NSString *nameTxt; NSString *emailTxt; NSString *mobTxt; //NSString *passTxt;
    
    nameTxt = self.nameTF.text ;
    nameTxt = [nameTxt stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    emailTxt = self.emailTF.text ;
    emailTxt = [emailTxt stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    mobTxt = self.numTF.text ;
    mobTxt = [mobTxt stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    //passTxt = self.passwordTF.text ;
    //passTxt = [passTxt stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSString *cityTf,*ageTf,*langTf,*otherTf;
    
    cityTf = [_cityTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    ageTf = [_ageTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    langTf = [_languageTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    otherTf = [_otherTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSString *langid = [[NSUserDefaults standardUserDefaults]objectForKey:@"langidEdit"];
    
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSString *wUrl=[NSString stringWithFormat:@"%@update_profile.php?user_id=%@&name=%@&email=%@&mobile_number=%@&lang_id=%@&city=%@&age_group=%@&language_other=%@",baseUrl,userid,nameTxt,emailTxt,mobTxt,langid,cityTf,ageTf,otherTf];
    
    NSLog(@"URL :%@", wUrl);
    
    if(mobTxt.length>0 && nameTxt.length>0 && langTf.length>0 )
    {
       if([langid caseInsensitiveCompare:@"0"]==NSOrderedSame )
       {
         if(otherTf.length>0)
         {
             
         }
           else
           {
               
               NSString *lang = langGlobal;
               NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
               NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
               NSString *fieldBlank = [languageBundle localizedStringForKey:@"fieldBlank" value:@"" table:@"strings-english"];
               
               [self.view makeToast:fieldBlank
                           duration:3.0
                           position:CSToastPositionBottom];
               
               // [_setTblVw reloadData];
               
               [SVProgressHUD dismiss];
               return ;
           }
       }
        
    }
    else
    {
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *fieldBlank = [languageBundle localizedStringForKey:@"fieldBlank" value:@"" table:@"strings-english"];
        
        [self.view makeToast:fieldBlank
                    duration:3.0
                    position:CSToastPositionBottom];
        
       // [_setTblVw reloadData];
        
        [SVProgressHUD dismiss];
        return ;
    }
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self updateWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *nameTx;
            
           NSArray *mArray = [json objectForKey:@"response"];
            
            NSString *status = [[[json objectForKey:@"response"]objectAtIndex:0]valueForKey:@"status"];
            
            if([status caseInsensitiveCompare:@"success"] == NSOrderedSame )
            {
                [[NSUserDefaults standardUserDefaults]setObject:mobTxt forKey:@"userMobileNum"];
                
                [[NSUserDefaults standardUserDefaults]setObject:mobTxt forKey:@"userEmail"];
                
                [[NSUserDefaults standardUserDefaults]setObject:nameTxt forKey:@"userNameL"];
                
                [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"message"]
                            duration:3.0
                            position:CSToastPositionBottom];
                
                
                
               // self.nameTF.text =  [[NSUserDefaults standardUserDefaults]objectForKey:@"userNameL"];
                
             self.numTF.text =  [[NSUserDefaults standardUserDefaults]objectForKey:@"userMobileNum"];
                
                
                NSString *lang = langGlobal;
                
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                
                NSString *hello = [languageBundle localizedStringForKey:@"hello" value:@"" table:@"strings-english"];
                
                NSString *youremail = [languageBundle localizedStringForKey:@"enter_email_txt" value:@"" table:@"strings-english"];
                
                NSString *yourPhone = [languageBundle localizedStringForKey:@"yourPhone" value:@"" table:@"strings-english"];
                
               // nameTxt
                
                nameTx = nameTxt;
                
                nameTx = [nameTx stringByReplacingOccurrencesOfString:@"%20"
                                                     withString:@" "];
                
                _welcomeLbl.text = [NSString stringWithFormat:@" %@, %@",hello,nameTx];
                
                if([[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail2"])
                _emailLbl.text = [NSString stringWithFormat:@" %@: %@",youremail,[[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail2"]];
                else
                _emailLbl.text = [NSString stringWithFormat:@" %@:  ",youremail];
                
                _phoneLbl.text = [NSString stringWithFormat:@" %@: %@",yourPhone,mobTxt];
                
                
                changed = YES;
          /*
            login_status: "success",
            user_id: "191",
            name: "prem",
            email: "prem@pi-digi.com",
            mobile_number: "9895994746",
            cat_ids: "9",
            support: "",
            keep_going: "",
            age_group: "< 50",
            city: "Trivandrum",
            lang_id: "5",
            language: "English"
                */
                
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                NSData *data = [def objectForKey:@"userData"];
                NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                
               NSMutableArray  *mutArr = [[NSMutableArray alloc] initWithArray:retrievedDictionary];
                
              mutArr = [mutArr mutableCopy];
                //[[[mutArr replaceObjectAtIndex:indexe]objectForKey:@"likes"] withObject:result];
                NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                NSDictionary *oldDict = (NSDictionary *)[mutArr objectAtIndex:0];
                [newDict addEntriesFromDictionary:oldDict];
                [newDict setObject:@"" forKey:@"test"];
                [mutArr replaceObjectAtIndex:0 withObject:newDict];
                
                NSLog(@"changed mutArr :%@",mutArr);
                
                [self userDetailsWS];
                
                [SVProgressHUD dismiss];
                
               
                
               // return ;
            }
            else
            {
               NSString *message = [[[json objectForKey:@"response"]objectAtIndex:0]valueForKey:@"message"];
                
                [self.view makeToast:message
                            duration:3.0
                            position:CSToastPositionBottom];
            }
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)subCatWebservice
{
    //http://listlinkz.com/ssc/webservice/sub_categories.php
    
    [SVProgressHUD show];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@sub_categories.php",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self subCatWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
            pArray = [json objectForKey:@"category"];
            
            if([pArray count]>0)
            {
                if([[pArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[pArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
            }
            [_setTblVw reloadData];
            
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)languagesWS
{
    //app.empowerji.com/webservice2/languages.php
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl = [NSString stringWithFormat:@"%@languages.php?lang_id=%ld",baseUrl,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
   {
       NSDictionary *json;
       
       if(data ==nil)
       {
           //[self feedbackSubmitWS];
           return ;
       }
       
       json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
       
       NSLog(@"json :%@", json);
       
       
       dispatch_async(dispatch_get_main_queue(), ^{
           
           langArr = [json objectForKey:@"result"];
           
           langArr = [langArr mutableCopy];
           
           
           NSDictionary *dict;
           NSString *lang;
           NSString *langid;
           NSString *otherLanguage;
           NSString *language_preference;
           
           for ( int i = 0; i < langArr.count ; i++ )
           {
               langid  = [NSString stringWithFormat:@"%@",[[langArr objectAtIndex:i] valueForKey:@"id"]];
               
               
               if([langid caseInsensitiveCompare:@"0"]==NSOrderedSame)
               {
                   NSString *lang = langGlobal;
                   NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                   NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                   language_preference = [languageBundle localizedStringForKey:@"language_preference" value:@"" table:@"strings-english"];
                   
                   otherLanguage = [languageBundle localizedStringForKey:@"other" value:@"" table:@"strings-english"];
                   
                   if(i == 0)
                       lang = [NSString stringWithFormat:@"%@",language_preference];
                   else
                       lang = [NSString stringWithFormat:@"%@",otherLanguage];
                   
                   dict = @{ @"id" : langid, @"language" : lang};
               }
               else
               {
                   lang = [NSString stringWithFormat:@"%@ (%@)",[[langArr objectAtIndex:i] valueForKey:@"reg_language"],[[langArr objectAtIndex:i] valueForKey:@"language"]];
                   
                   dict = @{ @"id" : langid, @"language" : lang};
               }
               
               
               [langArr replaceObjectAtIndex:i withObject:dict];
           }
           
           [_langTblVw reloadData];
           
           
         /*  CGRect frame = _langTblVw.frame;
           frame.size.height = _langTblVw.contentSize.height;
           frame.origin.y = _languageTF.frame.origin.y - _langTblVw.contentSize.height;
           _langTblVw.frame = frame;
          */
           
           CGFloat heightt;
           if(IS_IPAD)
           {
               heightt  = 40;
               
               CGRect frame = _langTblVw.frame;
               frame.size.height = _langTblVw.contentSize.height;
               frame.origin.y = _languageTF.frame.origin.y - _langTblVw.contentSize.height;
               _langTblVw.frame = frame;
           }
           else
           {
               heightt  = 25;
               
               CGRect frame = _langTblVw.frame;
               frame.size.height = heightt *langArr.count;
               frame.origin.y = _languageTF.frame.origin.y -(heightt *langArr.count);
               _langTblVw.frame = frame;
           }
           
           
       });
   }];
    
    [dataTask resume];
}

-(void)userDetailsWS
{
  //app.empowerji.com/webservice2/sign_in.php?username=sdfsd@sdf.sdf&password=sdfsdfsd
    
    NSString *username;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    username = [[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"];
    else
    username = [[NSUserDefaults standardUserDefaults]objectForKey:@"userMobileNum"];
    
    NSString *pass = [[NSUserDefaults standardUserDefaults]objectForKey:@"passwordText"];
    
    
    NSString *wUrl = [NSString stringWithFormat:@"%@sign_in.php?username=%@&password=%@",baseUrl,username,pass];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self userDetailsWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *str = [[[json objectForKey:@"result"]objectAtIndex:0] valueForKey:@"login_status"];
            
            if([str caseInsensitiveCompare:@"success"] == NSOrderedSame )
            {
             NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
             [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"result"]] forKey:@"userData"];
             [def synchronize];
                
                
                NSString *name = [[[json objectForKey:@"result"] objectAtIndex:0]valueForKey:@"name"];
                
                self.nameTF.text = [NSString stringWithFormat:@"%@",name];
                
                NSString *mobile_number = [[[json objectForKey:@"result"] objectAtIndex:0]valueForKey:@"mobile_number"];
                
                self.numTF.text = [NSString stringWithFormat:@"%@",mobile_number];
                
                
                NSString *lang = langGlobal;
                
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                
                NSString *hello = [languageBundle localizedStringForKey:@"hello" value:@"" table:@"strings-english"];
                
              //  NSString *youremail = [languageBundle localizedStringForKey:@"enter_email_txt" value:@"" table:@"strings-english"];
                
                NSString *yourPhone = [languageBundle localizedStringForKey:@"yourPhone" value:@"" table:@"strings-english"];
                
                name = [name stringByReplacingOccurrencesOfString:@"%20"
                                                           withString:@" "];
                
                _welcomeLbl.text = [NSString stringWithFormat:@" %@, %@",hello,name];
                
               _phoneLbl.text = [NSString stringWithFormat:@" %@: %@",yourPhone,mobile_number];
                
                NSString *city = [[[json objectForKey:@"result"] objectAtIndex:0]valueForKey:@"city"];
                
                _cityTF.text = [NSString stringWithFormat:@"%@",city];
                
                NSString *age_group = [[[json objectForKey:@"result"] objectAtIndex:0]valueForKey:@"age_group"];
                
                _ageTF.text = [NSString stringWithFormat:@"%@",age_group];
                
                NSString *lang_id = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"result"] objectAtIndex:0]valueForKey:@"lang_id"]];
                
                
                [[NSUserDefaults standardUserDefaults]setObject:lang_id forKey:@"langidEdit"];
                
                [[NSUserDefaults standardUserDefaults]setInteger:[lang_id integerValue] forKey:@"languageIDMain"];
                
                if([lang_id caseInsensitiveCompare:@"0"]==NSOrderedSame)
                {
                    _otherTF.hidden = NO;
                    
                    NSString *language = [[[json objectForKey:@"result"] objectAtIndex:0]valueForKey:@"language"];
                    
                    _otherTF.text = [NSString stringWithFormat:@"%@",language];
                    
                    _languageTF.text = [NSString stringWithFormat:@"Other"];
                    
                }
                else
                {
                    _otherTF.hidden = YES;
                    
                    NSString *language = [[[json objectForKey:@"result"] objectAtIndex:0]valueForKey:@"language"];
                    
                    _languageTF.text = [NSString stringWithFormat:@"%@",language];
                }
                
                
                //save language selected locally
                NSString *selLang;
                
                NSInteger i = [lang_id integerValue];
                switch (i) {
                    case 0:
                        selLang = @"en";
                        break;
                    case 1:
                        selLang = @"en";
                        break;
                    case 2:
                        selLang = @"hi";
                        break;
                    case 3:
                        selLang = @"mr-IN";
                        break;
                    case 4:
                        selLang = @"gu-IN";
                        break;
                    case 5:
                        selLang = @"en";
                        break;
                    default:
                        break;
                }
                if(selLang)
                [[NSUserDefaults standardUserDefaults] setObject:selLang forKey:@"language"];
                
                
              if(changed)
              {
                  homeViewController* homeVC = [[homeViewController alloc] init];
                  
                  UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:homeVC];
                  
                  homeVC.isFrmUpdate = YES;
                  
                  [homeVC updateCat];
                  
                  [[[UIApplication sharedApplication] keyWindow] setRootViewController:navigationController];
                  
                  navigationController.navigationBarHidden=YES;
                  
                  changed = NO;
              }
            }
            
            
        });
    }];
    
    [dataTask resume];
}

# pragma mark button action

- (IBAction)back:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)updateActn:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
      if((!([_nameTF.text length] == 0)))
       {
           if (([_emailTF.text length] == 0))
           {
               
           if (!([_numTF.text length] == 0))
           {
               BOOL valid;
               NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
               NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:_numTF.text];
               valid = [alphaNums isSupersetOfSet:inStringSet];
               
               if(([_numTF.text length] == 10)&& valid)
               {
                   
                if(!([_cityTF.text length] == 0))
                {
                    if(!([_ageTF.text length] == 0))
                    {
                        
                        if(!([_languageTF.text length] == 0))
                        {
                            if([_languageTF.text caseInsensitiveCompare:@"other"]!=NSOrderedSame)
                            {
                                if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                                {
                                    //connection unavailable
                                    
                                    [self showMessage:errorMessage withTitle:nil];
                                }
                                else
                                {
                                    [self updateWebservice];
                                }
                            }
                            else
                            {
                                if([_languageTF.text caseInsensitiveCompare:@"other"]==NSOrderedSame)
                                {
                                    if(_otherTF.text.length > 0)
                                    {
                                        {
                                            if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                                            {
                                                //connection unavailable
                                                
                                                [self showMessage:errorMessage withTitle:nil];
                                            }
                                            else
                                            {
                                                [self updateWebservice];
                                            }
                                            
                                        }
                                    }
                                    else
                                    {
                                        NSString *lang = langGlobal;
                                        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                                        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                                        NSString *message = [languageBundle localizedStringForKey:@"otherLangEmpty" value:@"" table:@"strings-english"];
                                        NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                                        
                                        [self showMessage:message withTitle:error];
                                        //[self showMessage:@"Please enter Other language" withTitle:@"Error"];
                                    }
                                }
                            }
                        }
                        else
                        {
                            NSString *lang = langGlobal;
                            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                            NSString *message = [languageBundle localizedStringForKey:@"selLangEmpty" value:@"" table:@"strings-english"];
                            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                            
                            [self showMessage:message withTitle:error];
                            
                           // [self showMessage:@"Please select language" withTitle:@"Error"];
                        }
                    }
                    else
                    {
                        NSString *lang = langGlobal;
                        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                        NSString *message = [languageBundle localizedStringForKey:@"ageEmpty" value:@"" table:@"strings-english"];
                        NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                        
                        [self showMessage:message withTitle:error];
                        
                        //[self showMessage:@"Please select your age" withTitle:@"Error"];
                    }
                }
               
                else
                {
                    NSString *lang = langGlobal;
                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    NSString *message = [languageBundle localizedStringForKey:@"cityEmpty" value:@"" table:@"strings-english"];
                    NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                    
                    [self showMessage:message withTitle:error];
                    
                    //[self showMessage:@"Please enter your city" withTitle:@"Error"];
                }
               
               }
               else
               {
                   //numberValid
                   
                   NSString *lang = langGlobal;
                   NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                   NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                   NSString *message = [languageBundle localizedStringForKey:@"numberValid" value:@"" table:@"strings-english"];
                   NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                   
                   [self showMessage:message withTitle:error];
               }
           }
               
           else
           {
               
               NSString *lang = langGlobal;
               NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
               NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
               NSString *message = [languageBundle localizedStringForKey:@"mobEmpty" value:@"" table:@"strings-english"];
               NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
               
               [self showMessage:message withTitle:error];
               
               //[self showMessage:@"Please Enter your mobile number" withTitle:@"Error"];
           }
    }
    else if(([_emailTF.text length] != 0))
   {
       BOOL stricterFilter = YES;
       NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
       NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
       NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
       NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
       BOOL myStringMatchesRegEx = [emailTest evaluateWithObject:_emailTF.text];
       
       //myStringMatchesRegEx=YES;
       
       if (myStringMatchesRegEx)
       {
           
           if (!([_numTF.text length] == 0))
           {
               BOOL valid;
               NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
               NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:_numTF.text];
               valid = [alphaNums isSupersetOfSet:inStringSet];
               
               if(([_numTF.text length] == 10)&& valid)
               {
                   
                   if(!([_cityTF.text length] == 0))
                   {
                       if(!([_ageTF.text length] == 0))
                       {
                           
                           if(!([_languageTF.text length] == 0))
                           {
                               if([_languageTF.text caseInsensitiveCompare:@"other"]!=NSOrderedSame)
                               {
                                   if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                                   {
                                       //connection unavailable
                                       
                                       [self showMessage:errorMessage withTitle:nil];
                                   }
                                   else
                                   {
                                       [self updateWebservice];
                                   }
                               }
                               else
                               {
                                   if([_languageTF.text caseInsensitiveCompare:@"other"]==NSOrderedSame)
                                   {
                                       if(_otherTF.text.length > 0)
                                       {
                                           {
                                               if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                                               {
                                                   //connection unavailable
                                                   
                                                   [self showMessage:errorMessage withTitle:nil];
                                               }
                                               else
                                               {
                                                   [self updateWebservice];
                                               }
                                               
                                           }
                                       }
                                       else
                                       {
                                           NSString *lang = langGlobal;
                                           NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                                           NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                                           NSString *message = [languageBundle localizedStringForKey:@"otherLangEmpty" value:@"" table:@"strings-english"];
                                           NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                                           
                                           [self showMessage:message withTitle:error];
                                           //[self showMessage:@"Please enter Other language" withTitle:@"Error"];
                                       }
                                   }
                               }
                           }
                           else
                           {
                               NSString *lang = langGlobal;
                               NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                               NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                               NSString *message = [languageBundle localizedStringForKey:@"selLangEmpty" value:@"" table:@"strings-english"];
                               NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                               
                               [self showMessage:message withTitle:error];
                               
                               // [self showMessage:@"Please select language" withTitle:@"Error"];
                           }
                       }
                       else
                       {
                           NSString *lang = langGlobal;
                           NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                           NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                           NSString *message = [languageBundle localizedStringForKey:@"ageEmpty" value:@"" table:@"strings-english"];
                           NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                           
                           [self showMessage:message withTitle:error];
                           
                           //[self showMessage:@"Please select your age" withTitle:@"Error"];
                       }
                   }
                   
                   else
                   {
                       NSString *lang = langGlobal;
                       NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                       NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                       NSString *message = [languageBundle localizedStringForKey:@"cityEmpty" value:@"" table:@"strings-english"];
                       NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                       
                       [self showMessage:message withTitle:error];
                       
                       //[self showMessage:@"Please enter your city" withTitle:@"Error"];
                   }
                   
               }
               else
               {
                   //numberValid
                   
                   NSString *lang = langGlobal;
                   NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                   NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                   NSString *message = [languageBundle localizedStringForKey:@"numberValid" value:@"" table:@"strings-english"];
                   NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                   
                   [self showMessage:message withTitle:error];
               }
           }
           
           else
           {
               
               NSString *lang = langGlobal;
               NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
               NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
               NSString *message = [languageBundle localizedStringForKey:@"mobEmpty" value:@"" table:@"strings-english"];
               NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
               
               [self showMessage:message withTitle:error];
               
               //[self showMessage:@"Please Enter your mobile number" withTitle:@"Error"];
           }
       }
       else
       {
           NSString *lang = langGlobal;
           NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
           NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
           NSString *message = [languageBundle localizedStringForKey:@"emailValid" value:@"" table:@"strings-english"];
           NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
           
           [self showMessage:message withTitle:error];
           
           //[self showMessage:@"Please Enter a valid email id" withTitle:@"Error"];
       }
    }
   }
      else
        {
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *message = [languageBundle localizedStringForKey:@"nameEmpty" value:@"" table:@"strings-english"];
            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
            
            [self showMessage:message withTitle:error];
            
            //[self showMessage:@"Please Enter your name" withTitle:@"Error"];
        }
        
       
    }
                  
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
          });
          
          
      }] resume];

}


- (IBAction)showMyProfileVw:(id)sender
{
    [self userDetailsWS];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail2"])
    _emailTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail2"];
    else
        _emailTF.text = @"";
    
    _emailTF.text = [[arrUser objectAtIndex:0]valueForKey:@"email"];
    
    NSString *mobNum;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userMobileNum"])
    {
        mobNum = [[NSUserDefaults standardUserDefaults]objectForKey:@"userMobileNum"];
    }
    else
        mobNum = [[arrUser objectAtIndex:0]valueForKey:@"mobile_number"];
    
    mobNum = [[arrUser objectAtIndex:0]valueForKey:@"mobile_number"];
    
    _numTF.text = mobNum;
    
    totalcheckmarkArray =[[NSMutableArray alloc]init];
    
    for (int i=0; i<[pArray count]; i++) // Number of Rows count
    {
        [totalcheckmarkArray addObject:[[pArray objectAtIndex:i]valueForKey:@"sub"]];
    }
    
    
    [self.view addSubview:_myProfileVw];
}
- (IBAction)backProfileVw:(id)sender
{
    _ageTblVw.hidden = YES;
    _langTblVw.hidden = YES;
    
    [self.view endEditing:YES];
    
   [_myProfileVw removeFromSuperview];
}
- (IBAction)changePassVwShow:(id)sender
{
    _cpTF.text = @"";//[[NSUserDefaults standardUserDefaults]objectForKey:@"passwordText"];
    
    _cpTF.secureTextEntry = YES;
    _npTF.secureTextEntry = YES;
    
    [self.view addSubview:_changePassVw];
}

- (IBAction)homeAction:(id)sender
{
   // [self dismissViewControllerAnimated:YES completion:nil];
   // [self.navigationController popToRootViewControllerAnimated:YES];
    
    UINavigationController *nav = (UINavigationController *)self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        [nav popViewControllerAnimated:YES];
    }];
}

-(void)ageSelAction
{
    _ageTblVw.hidden = NO;
    _langTblVw.hidden = YES;
}

-(void)langSelAction
{
    _ageTblVw.hidden = YES;
    _langTblVw.hidden = NO;
}

# pragma mark Change Password View

- (IBAction)backCP:(id)sender
{
    [_changePassVw removeFromSuperview];
}
- (IBAction)changePassBtnAction:(id)sender
{
    [self.view endEditing:YES];
    
    if(_npTF.text.length>0)
    {
        if(_npTF.text.length>=6)
        {
            NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
            
            [SVProgressHUD show];
            
            // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
            
            [[[NSURLSession sharedSession]
              dataTaskWithURL:[NSURL URLWithString:wUrl]
              completionHandler:^(NSData *urlData,
                                  NSURLResponse *response,
                                  NSError *error)
              {
                  // handle response
                  dispatch_async(dispatch_get_main_queue(), ^{
                      
                      if([urlData length] == 0)
                      {
                          NSLog(@"internet is not connected");
                          
                          [self showMessage:errorMessage withTitle:nil];
                      }
                      else
                      {
                          NSLog(@"internet is connected");
                          
            if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
            {
                //connection unavailable
                
                [self showMessage:errorMessage withTitle:nil];
            }
            else
            {
                [self cpWebservice];
            }
                          
                      }
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //[self.view makeToastActivity:CSToastPositionCenter];
                          [SVProgressHUD dismiss];
                      });
                      
                      
                  });
                  
                  
              }] resume];

        }
        else
        {
            NSString *lang = langGlobal;
            
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            
            NSString *message = [languageBundle localizedStringForKey:@"passwordMin" value:@"" table:@"strings-english"];
            
            [self.view makeToast:message
                        duration:1.0
                        position:CSToastPositionBottom];
        }
    }
    else
    {
        NSString *lang = langGlobal;
        
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"passwordBlank" value:@"" table:@"strings-english"];
        
        
        [self.view makeToast:message
                    duration:1.0
                    position:CSToastPositionBottom];
        
    }
    
    
}
-(void)cpWebservice
{
    //http://listlinkz.com/ssc/webservice/change_pass.php?user_id=1&cur_pass=12345&new_pass=123456
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *curTxt; NSString *newTxt;
    
    curTxt = self.cpTF.text ;
    curTxt = [curTxt stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    newTxt = self.npTF.text ;
    newTxt = [newTxt stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@change_pass.php?user_id=%@&cur_pass=%@&new_pass=%@&lang_id=%ld",baseUrl,userid,curTxt,newTxt,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self cpWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           if([[json objectForKey:@"status"] isEqualToString:@"Success"])
           {
            [[NSUserDefaults standardUserDefaults]setObject:_npTF.text forKey:@"passwordText"];
               
               [self.view makeToast:[json objectForKey:@"message"]
                           duration:3.0
                           position:CSToastPositionBottom];
           }
            else
            {
                [self.view makeToast:[json objectForKey:@"message"]
                            duration:3.0
                            position:CSToastPositionBottom];
            }
            
        });
    }];
    
    [dataTask resume];
    
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == 1)
        return langArr.count;
    else if(tableView.tag == 2)
        return ageArr.count;
    else
        return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 40;
    }
    else
        return 25;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    
    
    NSInteger section = indexPath.section;
    
    if(tableView.tag == 1)
    {
        NSString * language = [[langArr objectAtIndex:section]valueForKey:@"language"];
        
        if(([language caseInsensitiveCompare:@"language preference"] == NSOrderedSame) ||([language caseInsensitiveCompare:@"langauge preference"] == NSOrderedSame))
        {
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *language_preference = [languageBundle localizedStringForKey:@"language_preference" value:@"" table:@"strings-english"];
            
            language = language_preference;
        }
        
        cell.textLabel.text = language;
    }
    else if(tableView.tag == 2)
    {
        cell.textLabel.text = [ageArr objectAtIndex:section];
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1)
    {
        //lang_tblvw
        NSString *langid = [NSString stringWithFormat:@"%@",[[langArr objectAtIndex:indexPath.section]valueForKey:@"id"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"langidEdit"];
        
        NSString * language = [[langArr objectAtIndex:indexPath.section]valueForKey:@"language"];
        
        if(([language caseInsensitiveCompare:@"language preference"] == NSOrderedSame) ||([language caseInsensitiveCompare:@"langauge preference"] == NSOrderedSame))
        {
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *language_preference = [languageBundle localizedStringForKey:@"language_preference" value:@"" table:@"strings-english"];
            
            language = language_preference;
        }
        
       
        
        _languageTF.text = language;
        _langTblVw.hidden = YES;
        
        _ageTblVw.hidden = YES;
        
        if(([langid caseInsensitiveCompare:@"0"]==NSOrderedSame) && (indexPath.section !=0))
        {
            _otherTF.hidden = NO;
            
            [_otherTF becomeFirstResponder];
            
            UIFont *font = _staticLbl.font;
            
            CGRect textRect = [_staticLbl.text boundingRectWithSize:CGSizeMake(_staticLbl.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
            
            CGSize size = textRect.size;
            
            CGRect frameS = _staticLbl.frame;
            frameS.size.height = size.height;
            _staticLbl.frame = frameS;
            
            CGRect btnFrame = _updateBtn.frame;
            btnFrame.origin.y = frameS.origin.y + frameS.size.height + 10;
            _updateBtn.frame = btnFrame;
            
        }
        else
        {
            [_otherTF endEditing:YES];
            _otherTF.hidden = YES;
            
            CGRect frameS = _staticLbl.frame;
            frameS.size.height = 0;
            _staticLbl.frame = frameS;
            
            CGRect btnFrame = _updateBtn.frame;
            btnFrame.origin.y = frameS.origin.y + frameS.size.height;
            _updateBtn.frame = btnFrame;
        }
    }
    else if(tableView.tag == 2)
    {
        //age_tblvw
        
        _ageTF.text = [ageArr objectAtIndex:indexPath.section];
        _ageTblVw.hidden = YES;
        
        _langTblVw.hidden = YES;
    }
    
}

#pragma mark - TextField Delegates

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag == 0)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag == 0)
    {
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.3];
        [UIView setAnimationBeginsFromCurrentState:TRUE];
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
        
        [UIView commitAnimations];
    }
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
    UITouch *touch = [touches anyObject];
    if(![touch.view isKindOfClass:[UITextField class]])
    {
        [self.view endEditing:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
