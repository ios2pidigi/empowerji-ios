//
//  popUpVideoViewController.m
//  ssc
//
//  Created by sarath sb on 10/29/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "popUpVideoViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface popUpVideoViewController ()

@end

@implementation popUpVideoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *helpUrl ;
    helpUrl = [[retrievedDictionary objectAtIndex:0]valueForKey:@"help_video"];
    
    // NSString *url=@"https://www.youtube.com/embed/8NT-E-nrJ9M";
    [self.webVw loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:helpUrl]]];
    self.webVw.scrollView.bounces = NO;
    [self.webVw setMediaPlaybackRequiresUserAction:NO];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Help Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (IBAction)closeAction:(id)sender
{
    //addChildViewController
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end

