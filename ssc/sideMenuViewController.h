//
//  sideMenuViewController.h
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sideMenuViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UITextView *feedTxtVw;
@property (strong, nonatomic) IBOutlet UIButton *feedSubmitBtn;
@property (strong, nonatomic) IBOutlet UITextView *aboutTxtVw;
@property (strong, nonatomic) IBOutlet UIView *aboutVw;
@property (strong, nonatomic) IBOutlet UIView *termsVw;
@property (strong, nonatomic) IBOutlet UITextView *termsTxtVw;

@property (strong, nonatomic) IBOutlet UIView *feedBackVw;
@property (strong, nonatomic) IBOutlet UITableView *sideMenuTblVw;
@property (strong, nonatomic) IBOutlet UILabel *titleAbout;
@property (strong, nonatomic) IBOutlet UIImageView *bgAbout;
@property (strong, nonatomic) IBOutlet UIImageView *bgfeedBack;

@property (strong, nonatomic) IBOutlet UILabel *titleTerms;
@property (strong, nonatomic) IBOutlet UIImageView *bgTerms;

@property (strong, nonatomic) IBOutlet UIImageView *twitterImg;
@property (strong, nonatomic) IBOutlet UIImageView *faceBookImg;
@property (strong, nonatomic) IBOutlet UIImageView *googImg;
@property (strong, nonatomic) IBOutlet UILabel *lblFollowus;

@property (strong, nonatomic) IBOutlet UIView *shareVw;
@property (strong, nonatomic) IBOutlet UIView *popVw;
@property (strong, nonatomic) IBOutlet UIButton *instaBtn;
@property (strong, nonatomic) IBOutlet UIButton *linkinBtn;
@property (strong, nonatomic) IBOutlet UIImageView *instImg;
@property (strong, nonatomic) IBOutlet UIImageView *linkImg;



@property (nonatomic) BOOL isfrmSupport;
@end
