//
//  loginViewController.h
//  ssc
//
//  Created by swaroop on 05/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loginViewController : UIViewController
{
    NSBundle* languageBundle;
}
@property (strong, nonatomic) IBOutlet UITextField *emailTF;
@property (strong, nonatomic) IBOutlet UITextField *passTF;
@property (strong, nonatomic) IBOutlet UIButton *showPassBtn;
@property (strong, nonatomic) IBOutlet UIButton *keepMeSignedInBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIButton *btnSignIn;
@property (strong, nonatomic) IBOutlet UIImageView *showPassImg;
@property (strong, nonatomic) IBOutlet UIButton *btnLang;
@property (strong, nonatomic) IBOutlet UIImageView *backImg;
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUP;

@property (nonatomic) BOOL isfrmHome;

@property (nonatomic) BOOL isfrmInapp;

@end
