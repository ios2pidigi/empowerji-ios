//
//  listViewController.m
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "listViewController.h"
#import "listTableViewCell.h"
#import "listCollectionViewCell.h"
#import "Constants.h"
#import "UIImageView+JMImageCache.h"
#import "detailViewController.h"
#import "SVProgressHUD.h"
#import "UIView+Toast.h"
#import "moListViewController.h"
#import "searchViewController.h"
#import "bmCollectionViewCell.h"
#import "connectivity.h"

#import "homeViewController.h"

@interface listViewController () <UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSMutableDictionary *dictDetails;
    NSArray *arr;
    
    NSDictionary *json;
    UIPageControl *pageControl;
    
    NSMutableArray *arrCV;
    NSMutableArray *arrSec;
    NSMutableArray *arrBkmk;
    NSTimer *timer;
}
@property (nonatomic, strong) NSArray *listArray;
@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;
@end

@implementation listViewController

-(void)loadView
{
    [super loadView];
    
    arrCV = [[NSMutableArray alloc]init];
    arrSec = [[NSMutableArray alloc]init];
    
    const NSInteger numberOfTableViewRows = 20;
    const NSInteger numberOfCollectionViewCells = 15;
    
    NSMutableArray *mutableArray = [NSMutableArray arrayWithCapacity:numberOfTableViewRows];
    
    for (NSInteger tableViewRow = 0; tableViewRow < numberOfTableViewRows; tableViewRow++)
    {
        NSMutableArray *colorArray = [NSMutableArray arrayWithCapacity:numberOfCollectionViewCells];
        
        for (NSInteger collectionViewItem = 0; collectionViewItem < numberOfCollectionViewCells; collectionViewItem++)
        {
            
            CGFloat red = arc4random() % 255;
            CGFloat green = arc4random() % 255;
            CGFloat blue = arc4random() % 255;
            UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1.0f];
            
            [colorArray addObject:color];
        }
        
        [mutableArray addObject:colorArray];
    }
    
   // self.listArray = [NSArray arrayWithArray:mutableArray];
    
    self.contentOffsetDictionary = [NSMutableDictionary dictionary];
    
    _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"listname"];
    //[[NSUserDefaults standardUserDefaults]objectForKey:@"HomeClickedCellName"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
     admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
     adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    [self.adVw loadRequest:[GADRequest request]];
    
    
    arr = [[NSArray alloc]init];
    
    [self listWebservice];
    
}

-(void)viewWillAppear:(BOOL)animated
{
   // arrBkmk = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
       // arrBkmk = [plistDict objectForKey:@"rateArray"];
    }
    
   // [self.listTblVw reloadData];
}
-(void)viewDidDisappear:(BOOL)animated
{
    if ([timer isValid]) {
        [timer invalidate];
    }
    timer = nil;
}
-(void)getDictionary:(NSMutableDictionary *) dictHere
{
    dictDetails = dictHere;
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}

#pragma mark - Webservice List
-(void)listWebservice
{
    //article_brief.php?cat_id=
    [SVProgressHUD show];
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSInteger listid;
    listid = [[NSUserDefaults standardUserDefaults]integerForKey:@"listid"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@article_brief.php?cat_id=%ld&lang_id=%ld",baseUrl,(long)listid,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data ==nil)
        {
            [self listWebservice];
            return ;
        }
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            arr =[json allKeys];
            if([arr containsObject:@"all_article_ids"])
            {
                NSMutableArray *mtta = [[NSMutableArray alloc]initWithArray:arr];
                [mtta removeObjectAtIndex:0];
                arr = [mtta mutableCopy];
            }
            if([arr containsObject:@"mostviewd_articles"])
            {
                NSMutableArray *mtta = [[NSMutableArray alloc]initWithArray:arr];
                [mtta removeObjectAtIndex:0];
                arr = [mtta mutableCopy];
            }
            
            [SVProgressHUD dismiss];
            
            _listArray = [json objectForKey:@"first_row_articles"];
            
            arrSec = [json objectForKey:@"second_row_articles"];
            
            ///to change bookmarks to mostviewd
            arrBkmk = [[NSMutableArray alloc]init];
            
            arrBkmk = [json objectForKey:@"mostviewd_articles"];
            ////
            
            if([arrSec count]>0)
            {
                if([[arrSec objectAtIndex:0]objectForKey:@"message"])
                {
                    arrSec = [[NSMutableArray alloc]init];
                }
            }
            
            if([_listArray count]>0)
            {
                if([[_listArray objectAtIndex:0]objectForKey:@"message"])
                {
                    
           // [self.view makeToast:[[_listArray objectAtIndex:0]objectForKey:@"message"]duration:3.0 position:CSToastPositionBottom];
                    
                    [SVProgressHUD dismiss];
                    
                    
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:nil
                                                  message:@"No articles found"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                    {
                        [self onBack:nil];
                        
                    }];
                    
                    [alert addAction:okAction];
                    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
                    [vc presentViewController:alert animated:YES completion:nil];
                    
                    return ;
                }
            }
            
            
            ////////
            NSArray *allarticleids = [json valueForKey:@"all_article_ids"];
            
            NSMutableArray *arrRate = [[NSMutableArray alloc]init];
            NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            NSFileManager * fileManager = [NSFileManager defaultManager];
            //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                arrRate = [plistDict objectForKey:@"detailListArray"];
                
                if ([arrRate count] == 0)
                {
                    arrRate = [NSMutableArray new];
                    
                    for(NSDictionary *dict in allarticleids)
                    {
                        NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                        
                        NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                        [dictToSave removeObjectsForKeys:keysForNullValues];
                        
                        [arrRate addObject:dictToSave];
                    }
                    NSLog(@"arrRate :%@",arrRate);
                    
                    [plistDict setValue:arrRate forKey:@"detailListArray"];
                    [plistDict writeToFile:finalPath atomically:YES];
                    
                }
                else
                {
                    arrRate = [NSMutableArray new];
                    
                    for(NSDictionary *dict in allarticleids)
                    {
                        NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                        
                        NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                        [dictToSave removeObjectsForKeys:keysForNullValues];
                        
                        [arrRate addObject:dictToSave];
                    }
                    NSLog(@"arrRate :%@",arrRate);
                    
                    [plistDict setValue:arrRate forKey:@"detailListArray"];
                    [plistDict writeToFile:finalPath atomically:YES];
                }
            }
            ///////
            
            
            int x=0;
            self.scrollVw.pagingEnabled=YES;
           // NSArray *image1=[[NSArray alloc]initWithObjects:@"1.png",@"2.png",@"3.png", nil];
            
            NSString *imageString;NSString *image;
            NSString *imgTitle;
            
            
            for (int i=0; i<_listArray.count; i++)
            {
                
                imageString = [[_listArray objectAtIndex:i]valueForKey:@"image"];
                
                imgTitle = [[_listArray objectAtIndex:i]valueForKey:@"title"];
                
                
                NSString *newString1 = [imageString stringByReplacingOccurrencesOfString:@"thumb/" withString:@""];
                
                image = newString1;
                
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
                singleTap.numberOfTapsRequired = 1;
                singleTap.cancelsTouchesInView = NO;
                
                
                
                UILabel *imgHdr = [[UILabel alloc]initWithFrame:CGRectMake(x, 0,[[UIScreen mainScreen] bounds].size.width, self.scrollVwHdrLbl.frame.size.height)];
                
                UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(x, imgHdr.frame.origin.y + imgHdr.frame.size.height,[[UIScreen mainScreen] bounds].size.width, self.scrollVw.frame.size.height)];
                
                
                imgHdr.text = imgTitle;
                imgHdr.textAlignment = NSTextAlignmentCenter;
                imgHdr.textColor = [UIColor whiteColor];
                imgHdr.font = [UIFont systemFontOfSize:20.0 weight:UIFontWeightSemibold];
                imgHdr.backgroundColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1.0];
                
                [img setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:[UIImage imageNamed:@"logo"]];
                //img.contentMode = UIViewContentModeScaleAspectFit;
                
                NSInteger myInt = [[[_listArray objectAtIndex:i]valueForKey:@"id"] intValue];
                img.tag = myInt;
                
                x=x+[[UIScreen mainScreen] bounds].size.width;
                
                
                [_scrollVw addSubview:img];
                [_scrollVw addSubview:imgHdr];
                
                [img setUserInteractionEnabled:YES];
                [img addGestureRecognizer:singleTap];
                
            }
            self.scrollVw.contentSize=CGSizeMake(x, self.scrollVw.frame.size.height);
            self.scrollVw.contentOffset=CGPointMake(0, 0);
            
          pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0, 25 + self.scrollVw.frame.size.height, self.scrollVw.frame.size.width, 25)];
            [pageControl addTarget:self action:@selector(changepage:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:pageControl];
            

            pageControl.numberOfPages = _listArray.count;
            pageControl.currentPage = 0;
            pageControl.backgroundColor = [UIColor clearColor];
            pageControl.pageIndicatorTintColor = [UIColor redColor];
            pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
             [self.listTblVw reloadData];
            
            if(pageControl.numberOfPages >1)
            _scrollRightArrowImg.hidden = NO;
            else
               _scrollRightArrowImg.hidden = YES;
            
            
         ///show a label(userinfolbl) to let user know "Click to View Item"
            _userInfoLbl.hidden = NO;
            _userInfoLbl.alpha = 1.0f;
            // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
            [UIView animateWithDuration:0.5 delay:2.0 options:0 animations:^{
                // Animate the alpha value of your imageView from 1.0 to 0.0 here
                _userInfoLbl.alpha = 0.0f;
            } completion:^(BOOL finished) {
                // Once the animation is completed and the alpha has gone to 0.0, hide the view for good
                _userInfoLbl.hidden = YES;
            }];
            ///
            if (!timer)
            {
                timer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
            }
            
        });
    }];
    
    [dataTask resume];
}

#pragma mark AutoScroll Images ScrollView
-(void)scrollingTimer
{
    pageControl.currentPage ++;
    _scrollRightArrowImg.hidden = NO;
    NSLog(@"current page :%ld",(long)pageControl.currentPage);
    
   // if(pageControl.currentPage == (_listArray.count-1))
    {
       // pageControl.currentPage = 0;
    }
    
    
    NSInteger page = pageControl.currentPage;
   /* if(page >=(pageControl.numberOfPages-1))
    {
        _scrollRightArrowImg.hidden = YES;
    }
    else
        _scrollRightArrowImg.hidden = NO;
    
    */
    
    CGRect frame = _scrollVw.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [_scrollVw scrollRectToVisible:frame animated:YES];
}

#pragma mark page control indicator action

- (IBAction)changepage:(id)sender {
    UIPageControl *pager=sender;
    NSInteger page = pager.currentPage;
    
    if(page == 0)
    {
       _scrollLeftArrowImg.hidden = YES;
    }
    else
         _scrollLeftArrowImg.hidden = NO;
    
        
    if((page >=(pageControl.numberOfPages-1)) && pageControl.numberOfPages >1)
    {
        _scrollRightArrowImg.hidden = YES;
    }
    else
        _scrollRightArrowImg.hidden = NO;
    
    CGRect frame = _scrollVw.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [_scrollVw scrollRectToVisible:frame animated:YES];
}

#pragma mark Tap Gesture

-(void)tapDetected:(UITapGestureRecognizer *)tapGesture
{
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
    NSString *idval = [NSString stringWithFormat:@"%ld",tapGesture.view.tag];
    
    NSLog(@"idval :%@",idval);
   
    detailViewController *dVC = [[detailViewController alloc]initWithNibName:@"detailViewController" bundle:nil];
    [dVC getDictionaryDetailid:idval];
    [self.navigationController pushViewController:dVC animated:YES];
    }
                  
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];

}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(arr.count == 0)
        return 0;
    else if(arr.count == 1)
    {
        return 1;
    }
    else if(arr.count == 2)
    {
       return 2;
    }
    else
        return arr.count ; //[arrSec count];//arr.count -1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10; //cell spacing
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    listTableViewCell *cell = (listTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSInteger section = indexPath.section;
    
    NSInteger totalSections = arr.count;
    
    
   // if (!cell)
    {
        if(section == (totalSections-1))
        
        {
            CellIdentifier = @"bm";
        }
        else
        {
            CellIdentifier = @"CellIdentifier";
        }
       
        cell = [[listTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
  
   
    
   
    
    cell.btnM = [UIButton buttonWithType:UIButtonTypeCustom];
    if(IS_IPAD)
    {
      cell.btnM.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-170, 2.5, 100, 30);
    }
    else
    cell.btnM.frame=CGRectMake([UIScreen mainScreen].bounds.size.width-120, 2.5, 100, 30);
   // [cell.btnM setBackgroundColor:[UIColor clearColor]];
    //[cell.btnM setTitle:@"Show All" forState:(UIControlStateNormal)];
    //[cell.btnM setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cell.btnM setImage:[UIImage imageNamed:@"show all"] forState:UIControlStateNormal];
    //cell.btnM.backgroundColor = [UIColor colorWithRed:(230/255.0) green:(176/255.0) blue:(80/255.0) alpha:1.0];
    cell.btnM.layer.masksToBounds = YES;
    cell.btnM.layer.cornerRadius = 17.0;
    cell.btnM.layer.borderWidth = 0.0f;
    cell.btnM.layer.borderColor = [UIColor colorWithRed:(109/255.0) green:(180/255.0) blue:(48/255.0) alpha:1.0].CGColor;
    cell.btnM.tag = section;
    [cell.btnM addTarget:self action:@selector(moreAction:) forControlEvents:UIControlEventTouchUpInside];
    
     NSLog(@"%@",NSStringFromCGRect(cell.btnM.frame));
    if(section == (totalSections-1))
    {
        if(arrBkmk.count>0)
        {
            //to hide showall btn in most viewed
            //[cell.contentView addSubview:cell.btnM];
        }
    }
    else
    [cell.contentView addSubview:cell.btnM];
    
    cell.titleLblC = [[UILabel alloc]init];
    cell.titleLblC.frame =CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-120, 40);

    if(section == (totalSections-1))
    {
      cell.titleLblC.text= @"  Most Viewed";
      cell.titleLblC.frame =CGRectMake(0, 0, 120, 30);
        
    }
    else
    cell.titleLblC.text= @"  More items in this category";
    
    [cell.contentView addSubview:cell.titleLblC];
    
    if(section == (totalSections-1))
    {
        if(arrBkmk.count>0)
        {
            cell.btnM.hidden = NO;
        }
        else
        {
            cell.btnM.hidden = YES;
        }
        return cell;
    }
    
   // cell.prevArrow = [UIButton buttonWithType:UIButtonTypeCustom];
   // cell.prevArrow.frame=CGRectMake(0, 80, 25, 40);
   // [cell.prevArrow setBackgroundColor:[UIColor clearColor]];
   // [cell.prevArrow setImage:[UIImage imageNamed:@"icon_leftarrow"] forState:UIControlStateNormal];
   // [cell.contentView addSubview:cell.prevArrow];
    
   
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(listTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setCollectionViewDataSourceDelegate:self indexPath:indexPath];
    NSInteger index = cell.collectionView.indexPath.section;
    
    CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
    [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - UITableViewDelegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"index Path :%ld",(long)indexPath.section);
    
    if(indexPath.section ==1)
    {
        return 200;
    }
    else
    return 250;
}

#pragma mark - UICollectionViewDataSource Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   // NSArray *collectionViewArray = _listArray[[(AFIndexedCollectionView *)collectionView indexPath].section];
    
    NSLog(@"section :%ld",[(AFIndexedCollectionView *)collectionView indexPath].section);
    
    
    NSString *wordNumber;
    
    int intvalue;
    intvalue = (int)([(AFIndexedCollectionView *)collectionView indexPath].section +1);
    
    switch (intvalue) {
        case 1:
           // wordNumber = @"first";
            wordNumber = @"second";
            break;
            
        case 2:
           // wordNumber = @"second";
            return arrBkmk.count;
            break;
            
        case 3:
            wordNumber = @"third";
            break;
            
        case 4:
            wordNumber = @"fourth";
            break;
            
        case 5:
            wordNumber = @"fifth";
            break;
            
        case 6:
            wordNumber = @"sixth";
            break;
            
        case 7:
            wordNumber = @"seventh";
            break;
            
        case 8:
            wordNumber = @"eighth";
            break;
            
        case 9:
            wordNumber = @"ninth";
            break;
            
        default:
            break;
    }
    
    
    NSLog(@"%lu",[[json objectForKey:[NSString stringWithFormat:@"%@_row_articles",wordNumber]] count]);
    
    arrCV = [json objectForKey:[NSString stringWithFormat:@"%@_row_articles",wordNumber]];
    
    if([[arrCV objectAtIndex:0]objectForKey:@"message"])
    {
        return 0;
    }
    else
    {
  //return [[json objectForKey:[NSString stringWithFormat:@"%@_row_articles",wordNumber]] count];
    return arrCV.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
   // NSArray *collectionViewArray = self.listArray[[(AFIndexedCollectionView *)collectionView indexPath].section];
    
    
    //cell.backgroundColor = collectionViewArray[indexPath.item];
    
   // NSLog(@"%@",[NSString stringWithFormat:@"%@",[collectionViewArray valueForKey:@"image"]]);
    
    cell.backgroundColor = [UIColor grayColor];
    
    // return cell;
    
   // cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[collectionViewArray valueForKey:@"image"]]]];
    
    static NSString *simpleTableIdentifier = @"CollectionViewCellIdentifier";
    
    UINib *nib = [UINib nibWithNibName:@"listCollectionViewCell" bundle: nil];
    
    [collectionView registerNib:nib forCellWithReuseIdentifier:simpleTableIdentifier];
    
    NSInteger section = indexPath.row;
    
    
    
    
   
    
    
   // NSInteger totalSections = indexPath.section;
    
    int intvalue;
    intvalue = (int)([(AFIndexedCollectionView *)collectionView indexPath].section +1);
    if(intvalue == 2)
    {
       
        
       /* listCollectionViewCell *lcell = (listCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
        
        
        [lcell.imgCollec setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrBkmk objectAtIndex:section] valueForKey:@"image"]]] placeholder:nil];
        
        lcell.titleLbl.text = [[arrBkmk objectAtIndex:section] valueForKey:@"title"];
        
        return lcell;
        */
        
        static NSString *bm = @"bm";
        
        UINib *nibbm = [UINib nibWithNibName:@"bmCollectionViewCell" bundle: nil];
        
        [collectionView registerNib:nibbm forCellWithReuseIdentifier:bm];
        
        bmCollectionViewCell *lcell = (bmCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:bm forIndexPath:indexPath];
        
        
        [lcell.imgbm setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrBkmk objectAtIndex:section] valueForKey:@"image"]]] placeholder:[UIImage imageNamed:@"logo"]];
        
        //lcell.titleLbl.text = [[arrBkmk objectAtIndex:section] valueForKey:@"title"];
        
        return lcell;
        
        
    }
    
    
    listCollectionViewCell *lcell = (listCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    
      [lcell.imgCollec setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrCV objectAtIndex:section] valueForKey:@"image"]]] placeholder:[UIImage imageNamed:@"logo"]];
   
    lcell.titleLbl.text = [[arrCV objectAtIndex:section] valueForKey:@"title"];
    
    return lcell;
    
   
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {

    NSString *idval;
    
    int intvalue;
    intvalue = (int)([(AFIndexedCollectionView *)collectionView indexPath].section +1);
    if(intvalue == 2)
    {
       idval = [NSString stringWithFormat:@"%@",[[arrBkmk objectAtIndex:indexPath.row] valueForKey:@"id"]];
    }
    else
    idval = [NSString stringWithFormat:@"%@",[[arrCV objectAtIndex:indexPath.row] valueForKey:@"id"]];
    
    detailViewController *dVC = [[detailViewController alloc]initWithNibName:@"detailViewController" bundle:nil];
    [dVC getDictionaryDetailid:idval];
    [self.navigationController pushViewController:dVC animated:YES];
        
    }
                  
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
}

/*
 
 */

#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([scrollView isKindOfClass:[UIScrollView class]])
    {
        CGFloat pageWidth = self.scrollVw.frame.size.width; // you need to have a **iVar** with getter for scrollView
        float fractionalPage = self.scrollVw.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        pageControl.currentPage = page; // y
        
        if(page == 0)
        {
            _scrollLeftArrowImg.hidden = YES;
        }
        else
            _scrollLeftArrowImg.hidden = NO;
        
        if(page >=(pageControl.numberOfPages-1))
        {
            _scrollRightArrowImg.hidden = YES;
        }
        else
            _scrollRightArrowImg.hidden = NO;
            
    }
    else
    {
    if (![scrollView isKindOfClass:[UICollectionView class]]) return;
    
    CGFloat horizontalOffset = scrollView.contentOffset.x;
    
    AFIndexedCollectionView *collectionView = (AFIndexedCollectionView *)scrollView;
    NSInteger index = collectionView.indexPath.row;
    self.contentOffsetDictionary[[@(index) stringValue]] = @(horizontalOffset);
    }
}


# pragma mark button action

-(IBAction)moreAction:(UIButton*)sender
{
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
        
    NSInteger section = sender.tag;
    NSInteger totalSections = arr.count;
    
    moListViewController *mVC = [[moListViewController alloc]initWithNibName:@"moListViewController" bundle:nil];
    
    if(section == (totalSections-1))
    {
        NSLog(@"%ld",(long)section);
        
        if(arrBkmk.count == 0)
        {
            [self.view makeToast:@"No bookmarks to show"
                        duration:3.0
                        position:CSToastPositionBottom];
            return;
        }
        else
        {
            mVC.isFrmBkmd = YES;
        }
        
    }
    
    
    [self.navigationController pushViewController:mVC animated:YES];
    }
                  
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
    
}
- (IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onSearchActn:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)homeAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
   /* NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    */
    
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
