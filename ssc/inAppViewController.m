//
//  moListViewController.m
//  ssc
//
//  Created by swaroop on 13/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "inAppViewController.h"
#import "detailViewController.h"
#import "Constants.h"
#import "moTableViewCell.h"
#import "UIImageView+JMImageCache.h"
#import "searchViewController.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "galleryListTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "galleryVC.h"

#import "popUpVideoViewController.h"
#import "sideMenuViewController.h"
#import "sendMessageViewController.h"

#import "offerListTableViewCell.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "loginViewController.h"
#import "offerdetailViewController.h"

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#import "UIButton+WebCache.h"

#import "homeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "basicpackagesTableViewCell.h"
#import "packagesTableViewCell.h"
#import <StoreKit/StoreKit.h>
#import "videoViewController.h"

#define basicPlan @"com.aparna.empowerji.basicPlanNR"
#define plusPlan @"com.aparna.empowerji.plusPlanNR"
#define premiumPlan @"com.aparna.empowerji.premiumPlusNR"

@interface inAppViewController ()<AVAudioPlayerDelegate,SKPaymentTransactionObserver,SKProductsRequestDelegate,SKStoreProductViewControllerDelegate>
{
    NSArray *mArray;
    
    BOOL playedOnce;
    
    BOOL once;
    
    UIButton *hLbl;
    
    UIButton *btn;
    
    NSArray *catArray;
    
    int index;
    
    UIButton *customAdBanner;
    
    CGRect frame3;
    
    BOOL onetime;
    
    NSInteger indexPathPckBtn;
    
    NSString *activePlan;
    
    UILabel *expLbl;
    
    NSString *purchase_type;
    
    UIButton *restoreBtn;
    
}
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@end

typedef void(^myCompletion)(BOOL);

@implementation inAppViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

     [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    
    [self packagesWS];
    
    once = NO;
    
    onetime = NO;
    
    NSString *admobId,*adStatus;
    
    NSString *headerLblText = [[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
    
   // _headerLbl.text = [NSString stringWithFormat:@"%@",headerLblText];
    
    NSString *subHeaderText = [[NSUserDefaults standardUserDefaults]objectForKey:@"subCatName"];
    
   // _subHeaderLbl.text = [NSString stringWithFormat:@"%@",subHeaderText];
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
     adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
      /*  CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        */
        
        [self.adVw loadRequest:[GADRequest request]];
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
//        CGRect frameH = _helpImg.frame;
//        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
//        _helpImg.frame = frameH;
//
//        CGRect frameB = _helpBtn.frame;
//        frameB.origin.y = frameH.origin.y;
//        _helpBtn.frame = frameB;
        
       // [self.helpLbl setCenter:_helpBtn.center];
        
        customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
        CGRect frame = _tblVw.frame;
        frame.size.height = [UIScreen mainScreen].bounds.size.height - (_tblVw.frame.origin.y + _askQnBtn.frame.size.height);
       // _tblVw.frame = frame;
    }
    
    _basicVwBg.layer.masksToBounds = NO;
    _basicVwBg.layer.shadowOffset = CGSizeMake(5.0f, 5.0f); //CGSizeMake(-15, 20);
    _basicVwBg.layer.shadowRadius = 2;
    _basicVwBg.layer.shadowOpacity = 0.5;
    _basicVwBg.layer.cornerRadius = 5.0;
    
    
    _premiumVwBg.layer.masksToBounds = NO;
    _premiumVwBg.layer.shadowOffset = CGSizeMake(5.0f, 5.0f); //CGSizeMake(-15, 20);
    _premiumVwBg.layer.shadowRadius = 2;
    _premiumVwBg.layer.shadowOpacity = 0.5;
    _premiumVwBg.layer.cornerRadius = 5.0;
    
    
    [_btnBasic addTarget:self action:@selector(basicView) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnPre addTarget:self action:@selector(premView) forControlEvents:UIControlEventTouchUpInside];
    
    
    _basicDetailVw.frame = [UIScreen mainScreen].bounds;
    _premDetailVw.frame = [UIScreen mainScreen].bounds;
    _plusVw.frame = [UIScreen mainScreen].bounds;
    
    // Adding activity indicator
    activityIndicatorView = [[UIActivityIndicatorView alloc]
                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorView.center = self.view.center;
    [activityIndicatorView hidesWhenStopped];
   // [self.view addSubview:activityIndicatorView];
  //  [activityIndicatorView startAnimating];
    
    //Hide purchase button initially
    purchaseButton.hidden = YES;
    [self fetchAvailableProducts];
    
    _askQnBtn.tag = 0; _btnAQBD.tag = 1; _btnAQPD.tag = 2; _btnAQPMD.tag = 3;
    
    [_askQnBtn addTarget:self action:@selector(askQN:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnAQBD addTarget:self action:@selector(askQN:) forControlEvents:UIControlEventTouchUpInside];
    [_btnAQPD addTarget:self action:@selector(askQN:) forControlEvents:UIControlEventTouchUpInside];
    [_btnAQPMD addTarget:self action:@selector(askQN:) forControlEvents:UIControlEventTouchUpInside];
    
    //purchase
    _btnPurchaseBasic.tag = 0;
    _btnPurchasePrem.tag = 2;
    _btnPurchasePlus.tag = 1;
    
    [_btnPurchaseBasic addTarget:self action:@selector(purchase:) forControlEvents:UIControlEventTouchUpInside];
    [_btnPurchasePrem addTarget:self action:@selector(purchase:) forControlEvents:UIControlEventTouchUpInside];
    [_btnPurchasePlus addTarget:self action:@selector(purchase:) forControlEvents:UIControlEventTouchUpInside];
    
    _scrollVwBasic.layer.cornerRadius = 10.0;
    _scrollVwPrem.layer.cornerRadius = 10.0;
    _scrollVwPlus.layer.cornerRadius = 10.0;
    
    [_btnBasicPlanVideo addTarget:self action:@selector(playvideo) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnPremPlanVideo addTarget:self action:@selector(playvideo) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnPlusPlanVideo addTarget:self action:@selector(playvideo) forControlEvents:UIControlEventTouchUpInside];
    
    restoreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    restoreBtn.backgroundColor = _askQnBtn.backgroundColor;
    restoreBtn.titleLabel.font = _askQnBtn.titleLabel.font;
    [restoreBtn setTitle:@"Restore" forState:UIControlStateNormal];
    restoreBtn.titleLabel.textColor = [UIColor whiteColor];
    restoreBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    restoreBtn.layer.borderWidth = 1.0;
    [restoreBtn addTarget:self action:@selector(restore) forControlEvents:UIControlEventTouchUpInside];
   // [self.view addSubview:restoreBtn];

   
    [_btnTrial addTarget:self action:@selector(trialWS:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnPremTrial addTarget:self action:@selector(trialWS:) forControlEvents:UIControlEventTouchUpInside];
    
    _proPlusLbl.layer.cornerRadius = _proPremLbl.layer.cornerRadius = _proBasicLbl.layer.cornerRadius = 5.0;
    
    _proPlusLbl.layer.borderColor = _proPremLbl.layer.borderColor = _proBasicLbl.layer.borderColor =
    [UIColor redColor].CGColor;
    
    _proPlusLbl.layer.borderWidth = _proPremLbl.layer.borderWidth = _proBasicLbl.layer.borderWidth =
    1.0;
    
    _proPlusLbl.adjustsFontSizeToFitWidth = _proPremLbl.adjustsFontSizeToFitWidth = _proBasicLbl.adjustsFontSizeToFitWidth = YES;
    
    
     // _proBg1.layer.borderColor = _proBg2.layer.borderColor = _proBg3.layer.borderColor =
    //  [UIColor redColor].CGColor;
    
   //   _proBg1.layer.borderWidth = _proBg2.layer.borderWidth = _proBg3.layer.borderWidth =
    //  1.0;
    
  //  _proBg1.layer.cornerRadius = _proBg2.layer.cornerRadius = _proBg3.layer.cornerRadius =
   // 5.0;
    
    
    _btnFeaturePr1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _btnFeaturePr2.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _btnFeaturePr3.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _btnFeaturePr4.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _btnFeaturePr5.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    _btnFeaturePlus1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _btnFeaturePlus2.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _btnFeaturePlus3.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _btnFeaturePlus4.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _btnFeaturePlus5.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    _feature1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _feature2.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _feature3.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _feature4.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _feature5.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    expLbl = [[UILabel alloc]init];
    
    
    _proPremLbl.text = [NSString stringWithFormat:@" Promotional Offer   "];
    _proPlusLbl.text = [NSString stringWithFormat:@" Promotional Offer   "];
    _proBasicLbl.text = [NSString stringWithFormat:@" Promotional Offer   "];
}

-(void)packagesWS
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    [expLbl removeFromSuperview];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *mainCatId;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
        mainCatId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
    
    NSArray *arrUser;
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid = @"";
    }
    else
    {
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    }
    
    int r = arc4random_uniform(10000);
    
    NSString *wUrl=[NSString stringWithFormat:@"%@packages.php?lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self packagesWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            mArray = [[NSArray alloc]init];
            mArray = [json objectForKey:@"result"];
            
            NSArray *active_package = [[NSArray alloc]init];
            active_package = [json objectForKey:@"active_package"];
            
            NSString *package_name = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"package_name"]];
            
            NSLog(@"packgage name :%@",package_name);
            
            if([package_name caseInsensitiveCompare:@"(null)"]==NSOrderedSame)
            {
                package_name = @"";
            }
            
            [[NSUserDefaults standardUserDefaults]setObject:package_name forKey:@"package_name"];
            
            
            NSString *validity = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"validity"]];
            
            NSLog(@"validity :%@",validity);
            
            if([validity caseInsensitiveCompare:@"(null)"]==NSOrderedSame)
            {
                validity = @"";
            }
            
            if([validity caseInsensitiveCompare:@"active"]==NSOrderedSame )
            {
                _askQnBtn.hidden = YES;
                _btnAQBD.hidden = YES;
                _btnAQPD.hidden = YES;
                _btnAQPMD.hidden = YES;
            }
            
            
            activePlan = [[NSUserDefaults standardUserDefaults]objectForKey:@"package_name"];
            
            NSString *end_date = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"end_date"]];
            
             [expLbl removeFromSuperview];
            
            if([end_date caseInsensitiveCompare:@"(null)"]==NSOrderedSame)
            {
                
            }
            else
            {
               // expLbl = [[UILabel alloc]init];
                expLbl.text = [NSString stringWithFormat:@"You have an active %@ plan till %@",package_name,end_date];
                expLbl.textColor = [UIColor blackColor];
                expLbl.font = [UIFont systemFontOfSize:16];
                // expLbl.frame = _btnPurchaseBasic.frame;
                expLbl.adjustsFontSizeToFitWidth = YES;
                expLbl.textAlignment = NSTextAlignmentCenter;
                expLbl.hidden = YES;
                
                _lblExpTop.text = [NSString stringWithFormat:@"You have an active %@ plan till %@",package_name,end_date];
                
                _lblExpTop.textColor = [UIColor whiteColor];
                _lblExpTop.font = [UIFont systemFontOfSize:16];
                _lblExpTop.adjustsFontSizeToFitWidth = YES;
                _lblExpTop.textAlignment = NSTextAlignmentCenter;
            }
            
            if([activePlan caseInsensitiveCompare:@"premium"]==NSOrderedSame)
            {
                _btnPurchasePrem.hidden = NO;
                [_btnPurchasePrem setTitle:@"RENEW" forState:UIControlStateNormal];
                
                /////
                _btnPurchasePlus.hidden = YES;
                _btnPurchaseBasic.hidden = YES;
                
                
                ////
                
                expLbl.hidden = NO;
                expLbl.frame = CGRectMake(_btnPurchasePrem.frame.origin.x, _btnPremPlanVideo.frame.origin.y + _btnPremPlanVideo.frame.size.height + 10, _btnPurchasePrem.frame.size.width, 20);
                
                //CGRectMake(_btnPurchasePrem.frame.origin.x, _btnPurchasePrem.frame.origin.y + _premDetailVw.frame.origin.y + 10, _btnPurchasePrem.frame.size.width, 20);
                
                [_scrollVwPrem addSubview:expLbl];
                
                CGRect btnFr = _btnPurchasePrem.frame;
                btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10;
                
                if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
                {
                    
                }
                else
                    _btnPurchasePrem.frame = btnFr;
                
                
                _scrollVwPrem.contentSize = CGSizeMake(_scrollVwPrem.frame.size.width, _btnPurchasePrem.frame.size.height + _btnPurchasePrem.frame.origin.y + 15);
                
                
            }
            else if([activePlan caseInsensitiveCompare:@"basic"]==NSOrderedSame)
            {
                _btnPurchaseBasic.hidden = NO;
                [_btnPurchaseBasic setTitle:@"RENEW" forState:UIControlStateNormal];
                
                /////
                _btnPurchasePlus.hidden = NO;
                _btnPurchasePrem.hidden = NO;
                
                
                //////
                expLbl.hidden = NO;
                expLbl.frame = CGRectMake(_btnPurchaseBasic.frame.origin.x, _btnDummyBasic.frame.origin.y + _basicDetailVw.frame.origin.y, _btnPurchaseBasic.frame.size.width, 20);
                
                [_scrollVwBasic addSubview:expLbl];
                
                CGRect btnFr = _btnPurchaseBasic.frame;
                btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10;
                
                if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
                {
                    
                }
                else
                    _btnPurchaseBasic.frame = btnFr;
                
                
                _scrollVwBasic.contentSize = CGSizeMake(_scrollVwBasic.frame.size.width, _btnPurchaseBasic.frame.size.height + _btnPurchaseBasic.frame.origin.y + 5);
                
                
                
            }
            else if([activePlan caseInsensitiveCompare:@"plus"]==NSOrderedSame)
            {
                _btnPurchasePlus.hidden = NO;
                [_btnPurchasePlus setTitle:@"RENEW" forState:UIControlStateNormal];
                
                /////
                _btnPurchaseBasic.hidden = YES;
                _btnPurchasePrem.hidden = NO;
                
                
                ////
                
                expLbl.hidden = NO;
                expLbl.frame = CGRectMake(_btnPurchasePlus.frame.origin.x, _btnPlusPlanVideo.frame.origin.y + _btnPlusPlanVideo.frame.size.height + 10, _btnPurchasePlus.frame.size.width, 20);
                
                //_btnPremPlanVideo.frame.origin.y + _btnPremPlanVideo.frame.size.height
                //_btnPlusPlanVideo.frame.origin.y + _btnPlusPlanVideo.frame.origin.y
                
                [_scrollVwPlus addSubview:expLbl];
                
                CGRect btnFr = _btnPurchasePlus.frame;
                btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10 ;
                
                if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
                {
                    
                }
                else
                    _btnPurchasePlus.frame = btnFr;
                
                
                _scrollVwPlus.contentSize = CGSizeMake(_scrollVwPlus.frame.size.width, _btnPurchasePlus.frame.size.height + _btnPurchasePlus.frame.origin.y + 15);
                
            }
            else
            {
                _btnPurchasePlus.hidden = NO;
                _btnPurchaseBasic.hidden = NO;
                _btnPurchasePrem.hidden = NO;
            }
            
            
            if([mArray count]>0)
            {
                if([[mArray objectAtIndex:0]objectForKey:@"error"])
                {
                    [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"error"]
                                duration:1.0
                                position:CSToastPositionBottom];
                    
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
                else
                {
                    [_tblVw reloadData];
                }
                
               
                 [self addDummy];
               
            }
            
            [self.view hideToastActivity];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
    
}

-(void)packagesWS:(myCompletion) compblock
    
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
        NSInteger langid = 5;
        if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
            langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
        
        NSString *mainCatId;
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
            mainCatId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
    
    NSArray *arrUser;
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid = @"";
    }
    else
    {
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    }
    
     int r = arc4random_uniform(10000);
    
    NSString *wUrl=[NSString stringWithFormat:@"%@packages.php?lang_id=%ld&user_id=%@&counts=%d",baseUrl,(long)langid,userid,r];
        
        NSLog(@"URL :%@", wUrl);
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = 100.0;
        sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary *json;
            
            if(data ==nil)
            {
               // [self packagesWS];
                return ;
            }
            
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"json :%@", json);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                mArray =[[NSArray alloc]init];
                
                mArray = [json objectForKey:@"result"];
                
                NSArray *active_package = [[NSArray alloc]init];
                active_package = [json objectForKey:@"active_package"];
                
                NSString *package_name = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"package_name"]];
                
                NSLog(@"packgage name :%@",package_name);
                
                if([package_name caseInsensitiveCompare:@"(null)"]==NSOrderedSame)
                {
                    package_name = @"";
                }
                
    [[NSUserDefaults standardUserDefaults]setObject:package_name forKey:@"package_name"];
                
                
                NSString *validity = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"validity"]];
                
                NSLog(@"validity :%@",validity);
                
                if([validity caseInsensitiveCompare:@"(null)"]==NSOrderedSame)
                {
                    validity = @"";
                }
                
                if([validity caseInsensitiveCompare:@"active"]==NSOrderedSame )
                {
                    _askQnBtn.hidden = YES;
                    _btnAQBD.hidden = YES;
                    _btnAQPD.hidden = YES;
                    _btnAQPMD.hidden = YES;
                }
                
                
   activePlan = [[NSUserDefaults standardUserDefaults]objectForKey:@"package_name"];
     
        NSString *end_date = [NSString stringWithFormat:@"%@",[[active_package objectAtIndex:0]valueForKey:@"end_date"]];
         
                if([end_date caseInsensitiveCompare:@"(null)"]==NSOrderedSame)
                {
                    
                }
                else
                {
                    [[NSUserDefaults standardUserDefaults]setObject:end_date forKey:@"end_date"];
                    
                
                expLbl.text = [NSString stringWithFormat:@"You have an active %@ plan till %@",package_name,end_date];
                expLbl.textColor = [UIColor blackColor];
                expLbl.font = [UIFont systemFontOfSize:16];
               // expLbl.frame = _btnPurchaseBasic.frame;
                expLbl.adjustsFontSizeToFitWidth = YES;
                    expLbl.textAlignment = NSTextAlignmentCenter;
                    expLbl.hidden = YES;
                }
                
        if([activePlan caseInsensitiveCompare:@"premium"]==NSOrderedSame)
        {
            _btnPurchasePrem.hidden = NO;
            [_btnPurchasePrem setTitle:@"RENEW" forState:UIControlStateNormal];
            
            /////
            _btnPurchasePlus.hidden = YES;
            _btnPurchaseBasic.hidden = YES;
            
            
            ////
            
            expLbl.hidden = NO;
            expLbl.frame = CGRectMake(_btnPurchasePrem.frame.origin.x, _btnPurchasePrem.frame.origin.y + _premDetailVw.frame.origin.y + 10, _btnPurchasePrem.frame.size.width, 20);
            
            [_scrollVwPrem addSubview:expLbl];
            
            CGRect btnFr = _btnPurchasePrem.frame;
            btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10;
            
            if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
            {
                
            }
            else
                _btnPurchasePrem.frame = btnFr;
            
            
            _scrollVwPrem.contentSize = CGSizeMake(_scrollVwPrem.frame.size.width, _btnPurchasePrem.frame.size.height + _btnPurchasePrem.frame.origin.y + 15);
            
            
        }
        else if([activePlan caseInsensitiveCompare:@"basic"]==NSOrderedSame)
        {
            _btnPurchaseBasic.hidden = NO;
            [_btnPurchaseBasic setTitle:@"RENEW" forState:UIControlStateNormal];
            
            /////
            _btnPurchasePlus.hidden = NO;
            _btnPurchasePrem.hidden = NO;
            
            
            //////
            expLbl.hidden = NO;
            expLbl.frame = CGRectMake(_btnPurchaseBasic.frame.origin.x, _btnDummyBasic.frame.origin.y + _basicDetailVw.frame.origin.y, _btnPurchaseBasic.frame.size.width, 20);
            
            [_scrollVwBasic addSubview:expLbl];
            
            CGRect btnFr = _btnPurchaseBasic.frame;
            btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10;
            
            if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
            {
                
            }
            else
                _btnPurchaseBasic.frame = btnFr;
            
            
            _scrollVwBasic.contentSize = CGSizeMake(_scrollVwBasic.frame.size.width, _btnPurchaseBasic.frame.size.height + _btnPurchaseBasic.frame.origin.y + 5);
            
            
            
        }
        else if([activePlan caseInsensitiveCompare:@"plus"]==NSOrderedSame)
        {
            _btnPurchasePlus.hidden = NO;
            [_btnPurchasePlus setTitle:@"RENEW" forState:UIControlStateNormal];
            
            /////
            _btnPurchaseBasic.hidden = YES;
            _btnPurchasePrem.hidden = NO;
            
            
            ////
            
            expLbl.hidden = NO;
            expLbl.frame = CGRectMake(_btnPurchasePlus.frame.origin.x, _btnPurchasePlus.frame.origin.y + _plusVw.frame.origin.y + 10, _btnPurchasePlus.frame.size.width, 20);
            
            [_scrollVwPlus addSubview:expLbl];
            
            CGRect btnFr = _btnPurchasePlus.frame;
            btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10 ;
            
            if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
            {
                
            }
            else
                _btnPurchasePlus.frame = btnFr;
            
            
            _scrollVwPlus.contentSize = CGSizeMake(_scrollVwPlus.frame.size.width, _btnPurchasePlus.frame.size.height + _btnPurchasePlus.frame.origin.y + 15);
            
        }
        else
        {
            _btnPurchasePlus.hidden = NO;
            _btnPurchaseBasic.hidden = NO;
            _btnPurchasePrem.hidden = NO;
        }
                
                
            if([mArray count]>0)
            {
                    if([[mArray objectAtIndex:0]objectForKey:@"error"])
                    {
                        [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"error"]
                                    duration:1.0
                                    position:CSToastPositionBottom];
                        
                        
                        [SVProgressHUD dismiss];
                        return ;
                    }
                    else
                    {
                        [_tblVw reloadData];
                    }
                    
                }
                    
               
                
                [SVProgressHUD dismiss];
                
                 compblock(YES);
                
            });
        }];
        
        [dataTask resume];
        
    
}

-(void)trialWS:(NSString *)package_id
{
   
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                lgVC.isfrmInapp = YES;
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                //   UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                // [rvc presentViewController: lgVC animated: NO completion:nil];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:lgVC animated:YES completion:nil];
                });
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        //  UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        // [rvc presentViewController: alert animated: NO completion:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
            
            [self.view hideToastActivity];
        });
        
    }
    else
    {
        
    NSString *trial_requested = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"trial_requested"]];
    
if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
{
    if([trial_requested caseInsensitiveCompare:@"yes"]==NSOrderedSame)
    {
       // You have already requested for a trial package
        //    You are already in a trial package
         //   You have a valid package already
        
     //   UIAlertView *tmp = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You have already requested for a trial. We will get back to you soon." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
       // [tmp show];
        
        
        [self.view makeToast:@"You have already requested for a trial. We will get back to you soon."
                    duration:2.0
                    position:CSToastPositionBottom];
    }
    else
    {
        
         [self.view makeToastActivity:CSToastPositionCenter];
        
            NSString *lId = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"id"]];
            package_id = lId;
            
            // return;
            NSInteger langid = 5;
            if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
                langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
            
            NSString *mainCatId;
            if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
                mainCatId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
            
            NSArray *arrUser;
            NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
            NSData *datauser = [defuser objectForKey:@"userData"];
            NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
            
            arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
            
            NSString *userid;
            
            
            if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
            {
                userid = @"";
            }
            else
            {
                userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
            }
            
            int r = arc4random_uniform(10000);
            
            NSString *wUrl=[NSString stringWithFormat:@"%@request_trial.php?user_id=%@&package_id=%@&period=%@&counts=%d",baseUrl,userid,package_id,@"7",r]; //(long)langid
            
            NSLog(@"URL :%@", wUrl);
            
            NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            sessionConfig.timeoutIntervalForRequest = 100.0;
            sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
            
            NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
            NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSDictionary *json;
                
                if(data ==nil)
                {
                    // [self trialWS];
                    return ;
                }
                
                json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                NSLog(@"json :%@", json);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    
                    NSArray *result  = [json objectForKey:@"result"];
                    
                    NSString *message = [[result valueForKey:@"message"]objectAtIndex:0];
                    
                    [self.view makeToast:message duration:2.0  position:CSToastPositionBottom];
                    
                   // [self showMessage:message withTitle:nil];
                    
                    [SVProgressHUD dismiss];
                    
                    [self packagesWS];
                    
                    [self.view hideToastActivity];
                    
                });
            }];
            
            [dataTask resume];
        
        
    }
   }
    else
    {
        
        if ([activePlan containsString:@"trial"])
        {
           [self.view makeToast:@"You are already in a trial package " duration:2.0  position:CSToastPositionBottom];
        }
        else
            [self.view makeToast:@"You have a valid package already" duration:2.0  position:CSToastPositionBottom];
        
        
        
        
      /*  if([trial_requested caseInsensitiveCompare:@"yes"]==NSOrderedSame)
        {
            // You have already requested for a trial package
            //    You are already in a trial package
            //   You have a valid package already
            
          //  UIAlertView *tmp = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"You have already requested for a trial. We will get back to you soon." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
          //  [tmp show];
            
            [self.view makeToast:@"You have already requested for a trial. We will get back to you soon." duration:2.0  position:CSToastPositionBottom];
        }
        else
        {
            
             [self.view makeToastActivity:CSToastPositionCenter];
            
    NSString *lId = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"id"]];
    package_id = lId;
    
   // return;
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *mainCatId;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
        mainCatId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
    
    NSArray *arrUser;
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid = @"";
    }
    else
    {
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    }
    
     int r = arc4random_uniform(10000);
        
        NSString *wUrl=[NSString stringWithFormat:@"%@request_trial.php?user_id=%@&package_id=%@&period=%@&counts=%d",baseUrl,userid,package_id,@"7",r]; //(long)langid
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
           // [self trialWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
          NSArray *result  = [json objectForKey:@"result"];
            
            NSString *message = [[result valueForKey:@"message"]objectAtIndex:0];
            
            [self.view makeToast:message duration:2.0  position:CSToastPositionBottom];
           // [self showMessage:message withTitle:nil];
            
            [SVProgressHUD dismiss];
            
            [self packagesWS];
            
             [self.view hideToastActivity];
            
        });
    }];
    
    [dataTask resume];
    
    }
    */
        // [self.view hideToastActivity];
        
    }
    }
    
    // [self.view hideToastActivity];
}


-(void)updatePackageWS:(NSString *)package_id
{
    /*
     app.empowerji.com/webservice4/update_package.php?user_id=924&package_id=3&payment_from=instamojo&lang_id=5&device=android&purchase_type=instamojo
     
    
     payment_from=payment gateway
     purchase_type=new / renew
     */
    
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *mainCatId;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"])
        mainCatId = [[NSUserDefaults standardUserDefaults]objectForKey:@"mainCategoryId"];
    
    NSArray *arrUser;
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid = @"";
    }
    else
    {
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    }
    
    NSString *payment_from = @"apple";
    
    NSString *device = @"ios";
    
   // NSString *purchase_type = @"new";
    
     int r = arc4random_uniform(10000);
    
    NSString *wUrl=[NSString stringWithFormat:@"%@update_package.php?user_id=%@&package_id=%@&payment_from=%@&lang_id=%ld&device=%@&purchase_type=%@&counts=%d",baseUrl,userid,package_id,payment_from,(long)langid,device,purchase_type,r];

    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
           // [self trialWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            /*  mArray = [json objectForKey:@"result"];
             
             if([mArray count]>0)
             {
             if([[mArray objectAtIndex:0]objectForKey:@"error"])
             {
             [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"error"]
             duration:1.0
             position:CSToastPositionBottom];
             
             
             [SVProgressHUD dismiss];
             return ;
             }
             else
             {
             
             }
             
             } */
            
            NSArray *result  = [json objectForKey:@"result"];
            
            NSString *message = [result valueForKey:@"message"];
            
            [self showMessage:message withTitle:nil];
            
            [self packagesWS];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
}

- (void)applicationIsActive:(NSNotification *)notification
{
    NSLog(@"Application Did Become Active");
    
   
}

- (void)applicationEnteredForeground:(NSNotification *)notification
{
    NSLog(@"Application Entered Foreground");
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_audioPlayer pause];
   // [_speakerImg setImage:[UIImage imageNamed:@"ico-speaker.png"]];
    
}

static NSInteger countG;

-(void)viewWillAppear:(BOOL)animated
{
  
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    _btnMenu.layer.cornerRadius = _btnMenu.frame.size.height/2;
    _btnContactUs.layer.cornerRadius = _btnContactUs.frame.size.height/2;
    
    //make menu btn background gradient
    CAGradientLayer *gradientbtn = [CAGradientLayer layer];
    gradientbtn.frame = _btnMenu.bounds;
    gradientbtn.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnMenu.layer insertSublayer:gradientbtn atIndex:0];
    
    //make contactUs btn background gradient
    CAGradientLayer *gradientbtn2 = [CAGradientLayer layer];
    gradientbtn2.frame = _btnContactUs.bounds;
    gradientbtn2.colors = @[(id)btngradientColor1, (id)btngradientColor2];
    [_btnContactUs.layer insertSublayer:gradientbtn2 atIndex:0];
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
 //   if([adStatus isEqualToString:@"Yes"])
 //   {
     /*   CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
        
        if(!once)
        {
            once = YES;
        CGRect frameC = _mTblVw.frame;
        frameC.size.height = frameC.size.height - _helpBtn.frame.size.height;
        _mTblVw.frame = frameC;
        }
      
      */
  //  }
  //  else
    {
        NSLog(@"screen : %f",[UIScreen mainScreen].bounds.size.height);
        
        CGRect frame = _tblVw.frame;
       // frame.size.height = [UIScreen mainScreen].bounds.size.height - (_tblVw.frame.origin.y /*+  _askQnBtn.frame.size.height */);
       // frame.size.height = _tblVw.contentSize.height;
        _tblVw.frame = frame;
        
        /////
        _lblStaticBottom.numberOfLines = 0;
        
        CGRect r = [_lblStaticBottom.text boundingRectWithSize:CGSizeMake(_lblStaticBottom.frame.size.width, 0)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName:_lblStaticBottom.font}
                                                    context:nil];
        
        
        
        
        CGRect frameSta = _lblStaticBottom.frame;
        frameSta.origin.y = _tblVw.frame.origin.y + _tblVw.frame.size.height + 5;
        frameSta.size.height = r.size.height;
        _lblStaticBottom.frame = frameSta;
        
        
        CGRect btnFrame = _askQnBtn.frame;
        btnFrame.origin.y =  [UIScreen mainScreen].bounds.size.height - (_askQnBtn.frame.size.height + 10);
        _askQnBtn.frame = btnFrame;
        
        
        ////basic vw
        CGRect askFrame = _btnAQBD.frame;
        askFrame.origin.y = [UIScreen mainScreen].bounds.size.height - ( askFrame.size.height + 5);
        _btnAQBD.frame = askFrame;
        
        CGRect bFrame = _scrollVwBasic.frame;
        bFrame.size.height = _btnAQBD.frame.origin.y - (bFrame.origin.y + 5);
        _scrollVwBasic.frame = bFrame;
        
        
     
        ////prem vw
        CGRect askFrame2 = _btnAQPMD.frame;
        askFrame2.origin.y = [UIScreen mainScreen].bounds.size.height - (askFrame2.size.height + 5);
        _btnAQPMD.frame = askFrame2;
        
        CGRect bFrame2 = _scrollVwPrem.frame;
        bFrame2.size.height = _btnAQPMD.frame.origin.y - (bFrame2.origin.y + 5);;
        _scrollVwPrem.frame = bFrame2;
        
        ////plus vw
        CGRect askFrame3 = _btnAQPD.frame;
        askFrame3.origin.y = [UIScreen mainScreen].bounds.size.height - (askFrame3.size.height + 5);
        _btnAQPD.frame = askFrame3;
        
        CGRect bFrame3 = _scrollVwPlus.frame;
        bFrame3.size.height = _btnAQPD.frame.origin.y - (bFrame3.origin.y + 5);;
        _scrollVwPlus.frame = bFrame3;
      
      
        
        
    }
    
    
    
    CGRect frameHelpLbl = hLbl.frame;
//    if(IS_IPAD)
//        frameHelpLbl.origin.x = _speakerImg.frame.origin.x - ((([UIScreen mainScreen].bounds.size.width - _playBtn.frame.origin.x)/2)+20);
//    else
//        frameHelpLbl.origin.x = (([UIScreen mainScreen].bounds.size.width - _speakerImg.frame.origin.x));
//
//    frameHelpLbl.origin.y = _playBtn.frame.origin.y + _playBtn.frame.size.height;
    
    if(IS_IPAD)
        frameHelpLbl.size.width = [UIScreen mainScreen].bounds.size.width - frameHelpLbl.origin.x;
    else
        frameHelpLbl.size.width = [UIScreen mainScreen].bounds.size.width - (frameHelpLbl.origin.x);
    
    frameHelpLbl.size.height = 500;
    hLbl.frame = frameHelpLbl;
    [hLbl sizeToFit];
    
    hLbl.titleEdgeInsets = UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f);
    
  //  btn.frame = _playBtn.frame;
   
    
    hLbl.titleLabel.alpha = 1;
    [UIView animateWithDuration:1.0 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
        hLbl.titleLabel.alpha = 0;
    } completion:nil];
    
    
    NSString *catid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"catidFromHomeCat"]];
    
    if(([catid caseInsensitiveCompare:@"1"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"3"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"4"]==NSOrderedSame) || ([catid caseInsensitiveCompare:@"5"]==NSOrderedSame))
    {
        
    }
    else
    {
       // _speakerImg.hidden = YES;
       // _playBtn.hidden = YES;
        btn.hidden = YES;
        [hLbl removeFromSuperview];
    }
    
   // [_helpLbl sizeToFit];
    
    
    _btnAQBD.layer.cornerRadius = _btnAQBD.frame.size.height/2;
    _btnAQPD.layer.cornerRadius = _btnAQPD.frame.size.height/2;
    _btnAQPMD.layer.cornerRadius = _btnAQPMD.frame.size.height/2;
    _askQnBtn.layer.cornerRadius = _askQnBtn.frame.size.height/2;
    
    _btnAQBD.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnAQPD.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnAQPMD.layer.borderColor = [UIColor whiteColor].CGColor;
    _askQnBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _btnAQBD.layer.borderWidth = 1.0;
    _btnAQPD.layer.borderWidth = 1.0;
    _btnAQPMD.layer.borderWidth = 1.0;
    _askQnBtn.layer.borderWidth = 1.0;
    
    
    CGRect restoreFr = _askQnBtn.frame;
    restoreFr.origin.y = _askQnBtn.frame.origin.y - (restoreFr.size.height + 5);
    restoreBtn.frame = restoreFr;
    
    restoreBtn.clipsToBounds = YES;
    restoreBtn.layer.cornerRadius = restoreBtn.frame.size.height/2;
    
    if(IS_IPAD)
       [_tblVw reloadData];
    
    _proBasicLbl.frame = CGRectMake(_lblPriceBasic.frame.origin.x -(_proBasicLbl.intrinsicContentSize.width +5) , _proBasicLbl.frame.origin.y, _proBasicLbl.intrinsicContentSize.width, _proBasicLbl.frame.size.height);
    
    _proPlusLbl.frame = CGRectMake(_lblPricePlus.frame.origin.x -(_proPlusLbl.intrinsicContentSize.width +5), _proPlusLbl.frame.origin.y, _proPlusLbl.intrinsicContentSize.width, _proPlusLbl.frame.size.height);
    
    _proPremLbl.frame = CGRectMake(_lblPricePrem.frame.origin.x -(_proPremLbl.intrinsicContentSize.width +5), _proPremLbl.frame.origin.y, _proPremLbl.intrinsicContentSize.width, _proPremLbl.frame.size.height);
    
    
    _btnFeaturePr5.titleLabel.adjustsFontSizeToFitWidth = YES;
    
}


# pragma mark Button Actions

- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)homeAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"backPop" object:self];
    }];
}

-(void)availOffers:(UIButton *)sender
{
    NSString *cid;
    cid = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:sender.tag]valueForKey:@"id"]];
    
    
     NSString *theUrl;
    theUrl = [[mArray objectAtIndex:sender.tag]valueForKey:@"offer_url"];
     
     if(theUrl)
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    
}


-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
}


- (IBAction)backBasicVw:(id)sender
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    [self packagesWS :^(BOOL finished)
     {
         [self.view hideToastActivity];
         
         [_basicDetailVw removeFromSuperview];
         
         _lblExpTop.text = expLbl.text;
     }];
    
    
}

- (IBAction)backPreVw:(id)sender
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    [self packagesWS :^(BOOL finished)
     {
         [self.view hideToastActivity];
         
      [_premDetailVw removeFromSuperview];
    
    _lblExpTop.text = expLbl.text;
         
          }];
}

- (IBAction)backPlus:(id)sender
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    [self packagesWS :^(BOOL finished)
     {
         [self.view hideToastActivity];
         
     [_plusVw removeFromSuperview];
    
    _lblExpTop.text = expLbl.text;
         
          }];
}

-(void)askQN:(UIButton *)sender
{
    // _askQnBtn.tag = 0; _btnAQBD.tag = 1; _btnAQPD.tag = 2; _btnAQPMD.tag = 3;
    
    NSString *planName = @"";
    
    switch (sender.tag)
    {
        case 0:
            planName = @"";
            break;
        case 1:
            planName = @"basic";
            break;
        case 2:
            planName = @"plus";
            break;
        case 3:
            planName = @"premium";
            break;
            
        default:
            break;
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:planName forKey:@"planName"];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                lgVC.isfrmInapp = YES;
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
             //   UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
               // [rvc presentViewController: lgVC animated: NO completion:nil];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:lgVC animated:YES completion:nil];
                });
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
      //  UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
       // [rvc presentViewController: alert animated: NO completion:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    else
    {
    sendMessageViewController *smVC = [[sendMessageViewController alloc]initWithNibName:@"sendMessageViewController" bundle:nil];
        smVC.isfrmHelp = NO;
    [self.navigationController pushViewController:smVC animated:YES];
    }
}

-(IBAction)purchase:(UIButton*)sender
{
   
    NSString *btntext = [NSString stringWithFormat:@"%@",sender.titleLabel.text];
    NSLog(@"btntext :%@",btntext);
    
    if([btntext caseInsensitiveCompare:@"renew"]==NSOrderedSame)
    {
        //renew ws
        
        purchase_type = @"renew";
        
      //  purchase_type=new / renew
    }
    else
    {
         purchase_type = @"new";
    }
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        
       // UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:message delegate:self cancelButtonTitle:@"ok" otherButtonTitles: @"Sign-In", nil];
        
       // alertView.tag = 10;
       // [alertView show];
        
        
         UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
               
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                lgVC.isfrmInapp = YES;
       
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:lgVC animated:YES completion:nil];
                });
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
      
        
    UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
       // [rvc presentViewController: alert animated: NO completion:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    else
    {
        
        NSLog(@"%ld",(long)sender.tag);
        
        NSString *currentPlan = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"name"]];
        
        switch (sender.tag) {
            case 0:
            {
                NSLog(@"basic");
                
                [self purchaseProceed:sender.tag];
                
                break;
            }
            case 1:
            {
                if(([purchase_type caseInsensitiveCompare:@"new"]==NSOrderedSame) && [activePlan localizedCaseInsensitiveContainsString:@"basic"])
                {
                    {
                       
                        NSString *message = [NSString stringWithFormat:@"You have a %@ plan active. To upgrade to %@ plan click 'OK'. Contact support@empowerji.com for refund inquiries. To stay with your current plan click ' Cancel'.",activePlan,currentPlan];
                        
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:nil
                                                      message:message
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                        {
                            [self purchaseProceed:sender.tag];
                        }];
                        
                        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                                 {
                                                     
                                                 }];
                        
                        [alert addAction:okAction];
                        [alert addAction:cancel];
                        
                        
                        
                        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                        // [rvc presentViewController: alert animated: NO completion:nil];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self presentViewController:alert animated:YES completion:nil];
                        });
                    }
                }
                else
                {
                  [self purchaseProceed:sender.tag];
                }
                NSLog(@"plus");
                
                break;
            }
            case 2:
            {
                if(([purchase_type caseInsensitiveCompare:@"new"]==NSOrderedSame) && [activePlan localizedCaseInsensitiveContainsString:@"plus"])
                {
                    {
                        
                        NSString *message = [NSString stringWithFormat:@"You have a %@ plan active. To upgrade to %@ plan click 'OK'. Contact support@empowerji.com for refund inquiries. To stay with your current plan click ' Cancel'.",activePlan,currentPlan];
                        
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:nil
                                                      message:message
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                   {
                                                       [self purchaseProceed:sender.tag];
                                                   }];
                        
                        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                                 {
                                                     
                                                 }];
                        
                        [alert addAction:okAction];
                        [alert addAction:cancel];
                        
                        
                        
                        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                        // [rvc presentViewController: alert animated: NO completion:nil];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self presentViewController:alert animated:YES completion:nil];
                        });
                    }
                }
                else if(([purchase_type caseInsensitiveCompare:@"new"]==NSOrderedSame) && [activePlan localizedCaseInsensitiveContainsString:@"basic"])
                {
                    {
                        
                        NSString *message = [NSString stringWithFormat:@"You have a %@ plan active. To upgrade to %@ plan click 'OK'. Contact support@empowerji.com for refund inquiries. To stay with your current plan click ' Cancel'.",activePlan,currentPlan];
                        
                        UIAlertController * alert=   [UIAlertController
                                                      alertControllerWithTitle:nil
                                                      message:message
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                   {
                                                       [self purchaseProceed:sender.tag];
                                                   }];
                        
                        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                                 {
                                                     
                                                 }];
                        
                        [alert addAction:okAction];
                        [alert addAction:cancel];
                        
                        
                        
                        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                        // [rvc presentViewController: alert animated: NO completion:nil];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self presentViewController:alert animated:YES completion:nil];
                        });
                    }
                }
                else
                {
                    [self purchaseProceed:sender.tag];
                }
                
                NSLog(@"premium");
                
                break;
            }
            default:
                break;
        }
        
        
    }
    
    
}

-(void)purchaseProceed:(NSInteger)sender
{
    NSLog(@"sender ;%ld",(long)sender);
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    [self purchaseMyProduct:[validProducts objectAtIndex:sender]];
    purchaseButton.enabled = NO;
}

-(void)playvideo
{
    videoViewController *popVC = [[videoViewController alloc]initWithNibName:@"videoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
}


-(IBAction)basicView:(UIButton*)sender
{
   // [expLbl removeFromSuperview];
    
    
    
    [self packagesWS :^(BOOL finished)
    {
        if(finished)
        {
            NSLog(@"success");
        }
        
        indexPathPckBtn = sender.tag;
        
        NSString *video_url = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:0]valueForKey:@"video_url"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:video_url forKey:@"urlTrial"];
        
        NSString *short_desc_title1 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:0]valueForKey:@"short_desc_title1"]];
        
        NSString *short_desc_title2 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:0]valueForKey:@"short_desc_title2"]];
        
        NSString *short_desc = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:0]valueForKey:@"short_desc"]];
        
        NSString *short_desc2 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:0]valueForKey:@"short_desc2"]];
        
        NSString *feature1 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:0]valueForKey:@"feature1"]];
        
        NSString *feature2 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:0]valueForKey:@"feature2"]];
        
        NSString *feature3 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:0]valueForKey:@"feature3"]];
        
        NSString *feature4 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:0]valueForKey:@"feature4"]];
        
        NSString *feature5 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:0]valueForKey:@"feature5"]];
        
        NSString *price = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:0]valueForKey:@"price"]];
        
        NSString *duration = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:0]valueForKey:@"duration"]];
        
        _lblPriceBasic.text = [NSString stringWithFormat:@"INR %@/%@",price,duration];
        
        if(IS_IPAD)
        {
            _proBasicLbl.frame = CGRectMake(_lblPriceBasic.frame.origin.x - (_proBasicLbl.intrinsicContentSize.width +10 ),_proBasicLbl.frame.origin.y, _proBasicLbl.intrinsicContentSize.width  , _proBasicLbl.frame.size.height);
            
            _proBg1.frame = CGRectMake(_lblPriceBasic.frame.origin.x - (_proBasicLbl.intrinsicContentSize.width  + 20 ) ,_proBasicLbl.frame.origin.y, _proBasicLbl.intrinsicContentSize.width + 15 , _proBasicLbl.frame.size.height);
        }
        
        
        [_feature1 setTitle:feature1 forState:UIControlStateNormal];
        
        [_feature2 setTitle:feature2 forState:UIControlStateNormal];
        
        [_feature3 setTitle:feature3 forState:UIControlStateNormal];
        
        [_feature4 setTitle:feature4 forState:UIControlStateNormal];
        
        [_feature5 setTitle:feature5 forState:UIControlStateNormal];
        
        
        NSLog(@"feature 3 : %@,feature 4 : %@,feature 5 : %@",feature3,feature4,feature5);
        
        if(([feature1 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature1 caseInsensitiveCompare:@" "] ==NSOrderedSame))
            _feature1.hidden = YES;
        
        if(([feature2 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature2 caseInsensitiveCompare:@" "] ==NSOrderedSame))
            _feature2.hidden = YES;
        
        if(([feature3 caseInsensitiveCompare:@""] ==NSOrderedSame) || ([feature3 caseInsensitiveCompare:@" "] ==NSOrderedSame))
            _feature3.hidden = YES;
        
        if(([feature4 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature4 caseInsensitiveCompare:@" "] ==NSOrderedSame))
            _feature4.hidden = YES;
        
        if(([feature5 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature5 caseInsensitiveCompare:@" "] ==NSOrderedSame))
            _feature5.hidden = YES;
        
        
        NSAttributedString * attrStr1 =
        [[NSAttributedString alloc] initWithData:[short_desc_title1 dataUsingEncoding:NSUnicodeStringEncoding]
                                         options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        if(IS_IPAD)
        {
            short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 22]];
        }
        else
        {
            short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;margin-left:-10px;} </style>", @"Roboto-Medium", 16]];
            
        }
        
        
        NSAttributedString * attrStr22 =
        [[NSAttributedString alloc] initWithData:[short_desc dataUsingEncoding:NSUnicodeStringEncoding]
                                         options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                   NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
        
     NSAttributedString * attrStr2 =  [[NSAttributedString alloc] initWithData:[short_desc dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                   NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                              documentAttributes:nil error:nil];
        
        
        NSAttributedString * attrStr3 =
        [[NSAttributedString alloc] initWithData:[short_desc_title2 dataUsingEncoding:NSUnicodeStringEncoding]
                                         options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        
        if(IS_IPAD)
        {
            short_desc2 = [short_desc2 stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 22]];
        }
        else
        {
           // short_desc2 = [short_desc2 stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 18]];
            
            short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;margin-left:-10px;} </style>", @"Roboto-Medium", 16]];
        }
        
        NSAttributedString * attrStr4 =
        [[NSAttributedString alloc] initWithData:[short_desc2 dataUsingEncoding:NSUnicodeStringEncoding]
                                         options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        
       
        
        
        //_lblBasicH1.text = short_desc_title1;
        
        //_lblBasicD1.text = short_desc;
        
        // _lblBasicH2.text = short_desc_title2;
        
        // _lblBasicD2.text = short_desc2;
        
        // _lblBasicH1.attributedText = attrStr1;
        _lblBasicD1.attributedText = attrStr2;
        // _lblBasicH2.attributedText = attrStr3;
        _lblBasicD2.attributedText = attrStr4;
        
        _lblBasicH1.text = short_desc_title1;
        
        _lblBasicH2.text = short_desc_title2;
        
        [_lblBasicD1 sizeToFit];
        
        if([short_desc_title2 caseInsensitiveCompare:@""]==NSOrderedSame)
        {
            _lblBasicH2.hidden = YES;
        }
        
        if([short_desc2 caseInsensitiveCompare:@""]==NSOrderedSame)
        {
            _lblBasicD2.hidden = YES;
        }
        
       // expLbl = [[UILabel alloc]init];
        
        NSString *enddate = @"";
        NSString *packname = @"";
        
        if([[NSUserDefaults standardUserDefaults]objectForKey:@"end_date"])
        {
            enddate = [[NSUserDefaults standardUserDefaults]objectForKey:@"end_date"];
        }
        
         if([[NSUserDefaults standardUserDefaults]objectForKey:@"package_name"])
         {
             packname = ([[NSUserDefaults standardUserDefaults]objectForKey:@"package_name"]);
         }
        
        
        expLbl.text = [NSString stringWithFormat:@"You have an active %@ plan till %@",packname,enddate];
        expLbl.textColor = [UIColor blackColor];
        expLbl.font = [UIFont systemFontOfSize:16];
        // expLbl.frame = _btnPurchaseBasic.frame;
        expLbl.adjustsFontSizeToFitWidth = YES;
        expLbl.textAlignment = NSTextAlignmentCenter;
        expLbl.hidden = NO;
        expLbl.frame = CGRectMake(_btnPurchaseBasic.frame.origin.x, _btnDummyBasic.frame.origin.y + _basicDetailVw.frame.origin.y, _btnPurchaseBasic.frame.size.width, 20);
        
       // [_scrollVwBasic addSubview:expLbl];
        
        CGRect btnFr = _btnPurchaseBasic.frame;
        btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10;
        
        if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
        {
            
        }
        else
        {
            _btnPurchaseBasic.frame = btnFr;
            
            [_scrollVwBasic addSubview:expLbl];
        }
        
        
        _scrollVwBasic.contentSize = CGSizeMake(_scrollVwBasic.frame.size.width, _btnPurchaseBasic.frame.size.height + _btnPurchaseBasic.frame.origin.y + 5);
        
        [self.view addSubview:_basicDetailVw];
        
        
        [_btnBasicPlanVideo.imageView setContentMode:UIViewContentModeScaleAspectFit];
        
        _btnBasicPlanVideo.contentEdgeInsets = UIEdgeInsetsMake(10, 0, 10, 0);
        
        CGRect btnImageVwFrame = _btnBasicPlanVideo.imageView.frame;
        btnImageVwFrame.size.height =_btnBasicPlanVideo.frame.size.height;
        btnImageVwFrame.size.width =44;
        _btnBasicPlanVideo.imageView.frame = btnImageVwFrame;
        
        
        [self.view hideToastActivity];
        
        _proBg1.hidden = YES;
    }];
    
    
}

-(IBAction)premView:(UIButton*)sender
{
  //  [expLbl removeFromSuperview];
    
    [self packagesWS :^(BOOL finished)
     {
         
    indexPathPckBtn = sender.tag;
    
    NSString *video_url = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"video_url"]];
    
    [[NSUserDefaults standardUserDefaults]setObject:video_url forKey:@"urlTrial"];
    
    NSString *short_desc_title1 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"short_desc_title1"]];
    
    NSString *short_desc_title2 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"short_desc_title2"]];
    
    NSString *short_desc = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"short_desc"]];
    
    NSString *short_desc2 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"short_desc2"]];
    
    NSString *feature1 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature1"]];
    
    NSString *feature2 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature2"]];
    
    NSString *feature3 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature3"]];
    
    NSString *feature4 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature4"]];
    
    NSString *feature5 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature5"]];
    
         NSString *price = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"price"]];
         
         NSString *duration = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"duration"]];
         
         _lblPricePrem.text = [NSString stringWithFormat:@"INR %@/%@",price,duration];
         
         if(IS_IPAD)
         {
             _proPremLbl.frame = CGRectMake(_lblPricePrem.frame.origin.x - (_proPremLbl.intrinsicContentSize.width +10 ),_proPremLbl.frame.origin.y, _proPremLbl.intrinsicContentSize.width  , _proPremLbl.frame.size.height);
             
             _proBg2.frame = CGRectMake(_lblPricePrem.frame.origin.x - (_proPremLbl.intrinsicContentSize.width  + 20 ) ,_proPremLbl.frame.origin.y, _proPremLbl.intrinsicContentSize.width + 15 , _proPremLbl.frame.size.height);
         }
         
    [_btnFeaturePr1 setTitle:feature1 forState:UIControlStateNormal];
    
    [_btnFeaturePr2 setTitle:feature2 forState:UIControlStateNormal];
    
    [_btnFeaturePr3 setTitle:feature3 forState:UIControlStateNormal];
    
    [_btnFeaturePr4 setTitle:feature4 forState:UIControlStateNormal];
    
    [_btnFeaturePr5 setTitle:feature5 forState:UIControlStateNormal];
    
    
    NSLog(@"feature 3 : %@,feature 4 : %@,feature 5 : %@",feature3,feature4,feature5);
    
    if(([feature1 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature1 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePr1.hidden = YES;
    
    if(([feature2 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature2 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePr2.hidden = YES;
    
    if(([feature3 caseInsensitiveCompare:@""] ==NSOrderedSame) || ([feature3 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePr3.hidden = YES;
    
    if(([feature4 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature4 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePr4.hidden = YES;
    
    if(([feature5 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature5 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePr5.hidden = YES;
    
    
    
    NSAttributedString * attrStr1 =
    [[NSAttributedString alloc] initWithData:[short_desc_title1 dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
    if(IS_IPAD)
    {
     short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 22]];
    }
    else
    {
      //  short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 18]];
        
        short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;margin-left:-10px;} </style>", @"Roboto-Medium", 16]];
    }
         
         
    NSAttributedString * attrStr2 =
    [[NSAttributedString alloc] initWithData:[short_desc dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
    
    NSAttributedString * attrStr3 =
    [[NSAttributedString alloc] initWithData:[short_desc_title2 dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
         if(IS_IPAD)
         {
             short_desc2 = [short_desc2 stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 22]];
         }
         else
         {
            // short_desc2 = [short_desc2 stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 18]];
             
             short_desc2 = [short_desc2 stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;margin-left:-10px;} </style>", @"Roboto-Medium", 16]];
         }
         
    NSAttributedString * attrStr4 =
    [[NSAttributedString alloc] initWithData:[short_desc2 dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    

    
   // _lblHeaderPr1.attributedText = attrStr1;
    _lblDesPr1.attributedText = attrStr2;
 //  _lblHeaderPr2.attributedText = attrStr3;
    _lblDesPr2.attributedText = attrStr4;
    
    _lblHeaderPr1.text = short_desc_title1;
    _lblHeaderPr2.text = short_desc_title2;
    
         
         [_lblDesPr1 sizeToFit];
         
         
   // _lblHeaderPr1.text = short_desc_title1;
    
  //  _lblDesPr1.text = short_desc;
    
    //_lblHeaderPr2.text = short_desc_title2;
    
  //  _lblDesPr2.text = short_desc2;
    
    if([short_desc_title1 caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        
    }
    else
    {
        _lblHeaderPr1.numberOfLines = 0;
        
        CGRect r = [_lblHeaderPr1.text boundingRectWithSize:CGSizeMake(_lblHeaderPr1.frame.size.width, 0)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:_lblHeaderPr1.font}
                                                 context:nil];
        
        
        CGRect frameHeader = _lblHeaderPr1.frame;
        frameHeader.size.height = r.size.height;
       // _lblHeaderPr1.frame = frameHeader;
    }
    
    if([short_desc caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        
    }
    else
    {
        _lblDesPr1.numberOfLines = 0;
        
        CGRect r = [_lblDesPr1.text boundingRectWithSize:CGSizeMake(_lblDesPr1.frame.size.width, 0)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName:_lblDesPr1.font}
                                                    context:nil];
        
        
        CGRect frameHeader = _lblDesPr1.frame;
        frameHeader.size.height = r.size.height;
        _lblDesPr1.frame = frameHeader;
        
    }
    
    
    
    if([short_desc_title2 caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _lblHeaderPr2.hidden = YES;
        
        _imgPr2.hidden = YES;
    }
    else
    {
        CGSize textSize = [_lblHeaderPr2.text sizeWithAttributes:@{NSFontAttributeName:[_lblHeaderPr2 font]}];
        
        CGRect frameHeader = _lblHeaderPr2.frame;
        frameHeader.origin.y = _lblDesPr1.frame.origin.y + _lblDesPr1.frame.size.height + 5;
        frameHeader.size.height = textSize.height;
        _lblHeaderPr2.frame = frameHeader;
        
        CGRect img2 = _imgPr2.frame;
        img2.origin.y = frameHeader.origin.y;
        _imgPr2.frame = img2;
        
        
    }
    
    if([short_desc2 caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _lblDesPr2.hidden = YES;
    }
    else
    {
       // CGSize textSize = [_lblDesPr2.text sizeWithAttributes:@{NSFontAttributeName:[_lblDesPr2 font]}];
        
       // CGSize textSize = [_lblDesPr2 intrinsicContentSize];
        
        _lblDesPr2.numberOfLines = 0;
        
        CGRect r = [_lblDesPr2.text boundingRectWithSize:CGSizeMake(_lblDesPr2.frame.size.width, 0)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{NSFontAttributeName:_lblDesPr2.font}
                                                 context:nil];
        
        
        CGRect frameHeader = _lblDesPr2.frame;
        frameHeader.origin.y = _lblHeaderPr2.frame.origin.y + _lblHeaderPr2.frame.size.height;
        frameHeader.size.height = r.size.height;
        _lblDesPr2.frame = frameHeader;
        
        CGRect frameViewN = _whatYouGetPremVw.frame;
        frameViewN.size.height = frameViewN.size.height + r.size.height;
       // _whatYouGetPremVw.frame = frameViewN;
        
        CGRect frameView = _youNeedPremVw.frame;
        frameView.origin.y = _whatYouGetPremVw.frame.origin.y + _whatYouGetPremVw.frame.size.height + _lblDesPr2.frame.size.height + _lblDesPr1.frame.size.height + 5;
        _youNeedPremVw.frame = frameView;
        
        
        
        
        CGRect btn1 = _btnPremPlanVideo.frame;
        btn1.origin.y = _youNeedPremVw.frame.origin.y + _youNeedPremVw.frame.size.height + 10;
        _btnPremPlanVideo.frame = btn1;
        
        CGRect btn2 = _btnPremTrial.frame;
        btn2.origin.y = _youNeedPremVw.frame.origin.y + _youNeedPremVw.frame.size.height + 10;
        _btnPremTrial.frame = btn2;
        
        CGRect btn3 = _btnPurchasePrem.frame;
        btn3.origin.y = _btnPremPlanVideo.frame.origin.y + _btnPremPlanVideo.frame.size.height + 15;
        _btnPurchasePrem.frame = btn3;
        
        CGRect bgF = _bgGreyPrem.frame;
        bgF.size.height = _btnPurchasePrem.frame.origin.y + _btnPurchasePrem.frame.size.height + 10;
        _bgGreyPrem.frame = bgF;
        
       
    }
    
        // expLbl = [[UILabel alloc]init];
         
         NSString *enddate = @"";
         NSString *packname = @"";
         
         if([[NSUserDefaults standardUserDefaults]objectForKey:@"end_date"])
         {
             enddate = [[NSUserDefaults standardUserDefaults]objectForKey:@"end_date"];
         }
         
         if([[NSUserDefaults standardUserDefaults]objectForKey:@"package_name"])
         {
             packname = ([[NSUserDefaults standardUserDefaults]objectForKey:@"package_name"]);
         }
         
         
         expLbl.text = [NSString stringWithFormat:@"You have an active %@ plan till %@",packname,enddate];
         expLbl.textColor = [UIColor blackColor];
         expLbl.font = [UIFont systemFontOfSize:16];
         // expLbl.frame = _btnPurchaseBasic.frame;
         expLbl.adjustsFontSizeToFitWidth = YES;
         expLbl.textAlignment = NSTextAlignmentCenter;
         expLbl.hidden = NO;
         
    expLbl.frame = CGRectMake(_btnPurchasePrem.frame.origin.x, _btnPurchasePrem.frame.origin.y + _premDetailVw.frame.origin.y + 10, _btnPurchasePrem.frame.size.width, 20);
    
   // [_scrollVwPrem addSubview:expLbl];
    
    CGRect btnFr = _btnPurchasePrem.frame;
    btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10;
    
    if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        
    }
    else
    {
        [_scrollVwPrem addSubview:expLbl];
        
       _btnPurchasePrem.frame = btnFr;
    }
    
    
     _scrollVwPrem.contentSize = CGSizeMake(_scrollVwPrem.frame.size.width, _btnPurchasePrem.frame.size.height + _btnPurchasePrem.frame.origin.y + 15);
    
    [self.view addSubview:_premDetailVw];
    
    
    [_btnPremPlanVideo.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    
    ///////
    // [[self.btnPremPlanVideo imageView] setContentMode: UIViewContentModeScaleAspectFit];
    //[self.btnPremPlanVideo setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
     /////////////
   // _btnPremPlanVideo.contentMode = UIViewContentModeScaleToFill;
   // _btnPremPlanVideo.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
   // _btnPremPlanVideo.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
  ///////
    
    _btnPremPlanVideo.contentEdgeInsets = UIEdgeInsetsMake(10, 0, 10, 0);

    CGRect btnImageVwFrame = _btnPremPlanVideo.imageView.frame;
    btnImageVwFrame.size.height =_btnPremPlanVideo.frame.size.height;
    btnImageVwFrame.size.width =44;
    _btnPremPlanVideo.imageView.frame = btnImageVwFrame;
         
         [self.view hideToastActivity];
     }];
}

-(IBAction)plusView:(UIButton*)sender
{
  //  [expLbl removeFromSuperview];
    
    [self packagesWS :^(BOOL finished)
     {
         
    indexPathPckBtn = sender.tag;
    
    NSString *video_url = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"video_url"]];
    
    [[NSUserDefaults standardUserDefaults]setObject:video_url forKey:@"urlTrial"];
    
    NSString *short_desc_title1 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"short_desc_title1"]];
    
    NSString *short_desc_title2 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"short_desc_title2"]];
    
    NSString *short_desc = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"short_desc"]];
    
    NSString *short_desc2 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"short_desc2"]];
    
    NSString *feature1 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature1"]];
    
    NSString *feature2 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature2"]];
    
    NSString *feature3 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature3"]];
    
    NSString *feature4 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature4"]];
    
    NSString *feature5 = [NSString stringWithFormat:@" %@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"feature5"]];
    
         NSString *price = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"price"]];
         
         NSString *duration = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"duration"]];
         
         _lblPricePlus.text = [NSString stringWithFormat:@"INR %@/%@",price,duration];
         
    
         if(IS_IPAD)
         {
             _proPlusLbl.frame = CGRectMake(_lblPricePlus.frame.origin.x - (_proPlusLbl.intrinsicContentSize.width +10 ),_proPlusLbl.frame.origin.y, _proPlusLbl.intrinsicContentSize.width  , _proPlusLbl.frame.size.height);
             
             _proBg3.frame = CGRectMake(_lblPricePlus.frame.origin.x - (_proPlusLbl.intrinsicContentSize.width  + 20 ) ,_proPlusLbl.frame.origin.y, _proPlusLbl.intrinsicContentSize.width + 15 , _proPlusLbl.frame.size.height);
         }
         
         
         
    [_btnFeaturePlus1 setTitle:feature1 forState:UIControlStateNormal];
    
    [_btnFeaturePlus2 setTitle:feature2 forState:UIControlStateNormal];
    
    [_btnFeaturePlus3 setTitle:feature3 forState:UIControlStateNormal];
    
    [_btnFeaturePlus4 setTitle:feature4 forState:UIControlStateNormal];
    
    [_btnFeaturePlus5 setTitle:feature5 forState:UIControlStateNormal];
    
    
    NSLog(@"feature 3 : %@,feature 4 : %@,feature 5 : %@",feature3,feature4,feature5);
    
    if(([feature1 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature1 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePlus1.hidden = YES;
    
    if(([feature2 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature2 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePlus2.hidden = YES;
    
    if(([feature3 caseInsensitiveCompare:@""] ==NSOrderedSame) || ([feature3 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePlus3.hidden = YES;
    
    if(([feature4 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature4 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePlus4.hidden = YES;
    
    if(([feature5 caseInsensitiveCompare:@""]==NSOrderedSame)|| ([feature5 caseInsensitiveCompare:@" "] ==NSOrderedSame))
        _btnFeaturePlus5.hidden = YES;
    
    
    NSAttributedString * attrStr1 =
    [[NSAttributedString alloc] initWithData:[short_desc_title1 dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
    
         if(IS_IPAD)
         {
             short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 22]];
         }
         else
         {
             
             short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;margin-left:-10px;} </style>", @"Roboto-Medium", 16]];
             
            // short_desc = [short_desc stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 18]];
         }
         
    NSAttributedString * attrStr2 =
    [[NSAttributedString alloc] initWithData:[short_desc dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
    
    NSAttributedString * attrStr3 =
    [[NSAttributedString alloc] initWithData:[short_desc_title2 dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
         if(IS_IPAD)
         {
             short_desc2 = [short_desc2 stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 22]];
         }
         else
         {
             
             short_desc2 = [short_desc2 stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;margin-left:-10px;} </style>", @"Roboto-Medium", 16]];
             
             //short_desc2 = [short_desc2 stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; font-size:%dpx;}</style>", _lblDesPr1.font, 18]];
         }
         
    NSAttributedString * attrStr4 =
    [[NSAttributedString alloc] initWithData:[short_desc2 dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
    
    
   // _lblHeader1.attributedText = attrStr1;
    _lblDes1.attributedText = attrStr2;
  //  _lblHeader2.attributedText = attrStr3;
    _lbldes2.attributedText = attrStr4;
    
    _lblHeader1.text = short_desc_title1;
    
    _lblHeader2.text = short_desc_title2;
    
   // _lblHeader1.text = short_desc_title1;
   // _lblDes1.text = short_desc;
   // _lblHeader2.text = short_desc_title2;
   // _lbldes2.text = short_desc2;
    
    
    
    if([short_desc_title2 caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _lblHeader2.hidden = YES;
    }
    
    if([short_desc2 caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _lbldes2.hidden = YES;
    }
    
    
    //////////////////
    if([short_desc_title1 caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        
    }
    else
    {
        _lblHeader1.numberOfLines = 0;
        
        CGRect r = [_lblHeader1.text boundingRectWithSize:CGSizeMake(_lblHeader1.frame.size.width, 0)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName:_lblHeader1.font}
                                                    context:nil];
        
        
        CGRect frameHeader = _lblHeader1.frame;
        frameHeader.size.height = r.size.height;
        // _lblHeaderPr1.frame = frameHeader;
    }
    
    if([short_desc caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        
    }
    else
    {
        _lblDes1.numberOfLines = 0;
        
        CGRect r = [_lblDes1.text boundingRectWithSize:CGSizeMake(_lblDes1.frame.size.width, 0)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:_lblDes1.font}
                                                 context:nil];
        
        
        CGRect frameHeader = _lblDes1.frame;
        frameHeader.size.height = r.size.height;
        _lblDes1.frame = frameHeader;
        
    }
    
    
    
    if([short_desc_title2 caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _lblHeader2.hidden = YES;
        
        _imgP2.hidden = YES;
    }
    else
    {
        CGSize textSize = [_lblHeader2.text sizeWithAttributes:@{NSFontAttributeName:[_lblHeader2 font]}];
        
        CGRect frameHeader = _lblHeader2.frame;
        frameHeader.origin.y = _lblDes1.frame.origin.y + _lblDes1.frame.size.height + 5;
        frameHeader.size.height = textSize.height;
        _lblHeader2.frame = frameHeader;
        
        CGRect img2 = _imgP2.frame;
        img2.origin.y = frameHeader.origin.y;
        _imgP2.frame = img2;
        
        
    }
    
    if([short_desc2 caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _lbldes2.hidden = YES;
    }
    else
    {
        // CGSize textSize = [_lblDesPr2.text sizeWithAttributes:@{NSFontAttributeName:[_lblDesPr2 font]}];
        
        // CGSize textSize = [_lblDesPr2 intrinsicContentSize];
        
        _lbldes2.numberOfLines = 0;
        
        CGRect r = [_lbldes2.text boundingRectWithSize:CGSizeMake(_lbldes2.frame.size.width, 0)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:_lbldes2.font}
                                                 context:nil];
        
        
        CGRect frameHeader = _lbldes2.frame;
        frameHeader.origin.y = _lblHeader2.frame.origin.y + _lblHeader2.frame.size.height;
        frameHeader.size.height = r.size.height;
        _lbldes2.frame = frameHeader;
        
        CGRect frameViewN = _whatYouGetView.frame;
        frameViewN.size.height = frameViewN.size.height + r.size.height;
        // _whatYouGetPremVw.frame = frameViewN;
        
        CGRect frameView = _youNeedView.frame;
        frameView.origin.y = _whatYouGetView.frame.origin.y + _whatYouGetView.frame.size.height + _lbldes2.frame.size.height + _lblDes1.frame.size.height + 5;
        _youNeedView.frame = frameView;
        
        
        
        
        CGRect btn1 = _btnPlusPlanVideo.frame;
        btn1.origin.y = _youNeedView.frame.origin.y + _youNeedView.frame.size.height + 5;
        _btnPlusPlanVideo.frame = btn1;
        
        CGRect btn2 = _btnTrial.frame;
        btn2.origin.y = _youNeedView.frame.origin.y + _youNeedView.frame.size.height + 5;
        _btnTrial.frame = btn2;
        
        CGRect btn3 = _btnPurchasePlus.frame;
        btn3.origin.y = _btnPlusPlanVideo.frame.origin.y + _btnPlusPlanVideo.frame.size.height + 15;
        _btnPurchasePlus.frame = btn3;
        
        CGRect bgF = _bgGrayPlus.frame;
        bgF.size.height = _btnPurchasePlus.frame.origin.y + _btnPurchasePlus.frame.size.height + 10;
        _bgGrayPlus.frame = bgF;
        
        
    }
    
    
        // expLbl.text = [NSString stringWithFormat:@"You have an active %@ plan till %@",packname,enddate];
         expLbl.textColor = [UIColor blackColor];
         expLbl.font = [UIFont systemFontOfSize:16];
         // expLbl.frame = _btnPurchaseBasic.frame;
         expLbl.adjustsFontSizeToFitWidth = YES;
         expLbl.textAlignment = NSTextAlignmentCenter;
         expLbl.hidden = NO;
         
    expLbl.frame = CGRectMake(_btnPurchasePlus.frame.origin.x, _btnPurchasePlus.frame.origin.y + _plusVw.frame.origin.y + 10, _btnPurchasePlus.frame.size.width, 20);
    
   // [_scrollVwPlus addSubview:expLbl];
    
    CGRect btnFr = _btnPurchasePlus.frame;
    btnFr.origin.y = expLbl.frame.origin.y + expLbl.frame.size.height +10 ;
    
    if([activePlan caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        
    }
    else
    {
        [_scrollVwPlus addSubview:expLbl];
    _btnPurchasePlus.frame = btnFr;
    }
    
    
    _scrollVwPlus.contentSize = CGSizeMake(_scrollVwPlus.frame.size.width, _btnPurchasePlus.frame.size.height + _btnPurchasePlus.frame.origin.y + 15);
    ///////////////////
    
    [self.view addSubview:_plusVw];
    
    [_btnPlusPlanVideo.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    _btnPlusPlanVideo.contentEdgeInsets = UIEdgeInsetsMake(10, 0, 10, 0);
    
    CGRect btnImageVwFrame = _btnPlusPlanVideo.imageView.frame;
    btnImageVwFrame.size.height =_btnPlusPlanVideo.frame.size.height;
    btnImageVwFrame.size.width =44;
    _btnPlusPlanVideo.imageView.frame = btnImageVwFrame;
    
    [self.view hideToastActivity];
}];

}
-(void)addDummy
{
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
   lbl.text = @"";
   [self.view addSubview:lbl];
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return mArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    NSString *lId = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"id"]];
    
    if([lId caseInsensitiveCompare:@"0"]==NSOrderedSame)
    {
        if(IS_IPAD)
            return 35;
        else
            return 20;
    }
    else
    {
        NSString *name = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"name"]];
        
        if([name caseInsensitiveCompare:@"basic"]==NSOrderedSame)
        {
            if(IS_IPAD)
            {
                return 160;
            }
            else
                return 120;
        }
        else
        {
            if(IS_IPAD)
            {
                return 200;
            }
            else
                return 140;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 6;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    NSString *name = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"name"]];
    
    NSString *price = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"price"]];
    
    NSString *duration = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"duration"]];
    
    NSString *payment_duration = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"payment_duration"]];
    
    NSString *short_desc_title1 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"short_desc_title1"]];
    
    NSString *short_desc_title2 = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"short_desc_title2"]];
    
     NSString *video_url = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"video_url"]];
    
     NSString *idP = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"id"]];
    
   // [[NSUserDefaults standardUserDefaults]setObject:video_url forKey:@"urlTrial"];
    
   
    
   // if([name caseInsensitiveCompare:@"basic"]==NSOrderedSame)
    if([idP isEqualToString:@"3"])
    {
        static NSString *CellIdentifier = @"basic";
        basicpackagesTableViewCell *cell = (basicpackagesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"basicpackagesTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        //NSInteger section = indexPath.section;
        cell.img.image = [UIImage imageNamed:@"icon-Basic"];
        cell.headerLbl.text =  [NSString stringWithFormat:@"%@",name];
        cell.priceLbl.text = [NSString stringWithFormat:@"INR %@/%@",price,duration];
        [cell.priceLbl setText:[cell.priceLbl.text uppercaseString]];
        
        if([payment_duration caseInsensitiveCompare:@"yearly"]==NSOrderedSame)
        cell.planLbl.text =  [NSString stringWithFormat:@"Yearly Plan"];
        else
        cell.planLbl.text =  [NSString stringWithFormat:@"Quarterly Plan"];
        
        cell.unlimLbl.text = [NSString stringWithFormat:@" %@",short_desc_title1];
        
        cell.clipsToBounds = YES;
        cell.layer.cornerRadius = 5.0;
        
        
            
        cell.proLbl.text = @"";//[NSString stringWithFormat:@" Promotional Offer   "];
        cell.proLbl.clipsToBounds = YES;
        cell.proLbl.layer.cornerRadius = 5.0;
        cell.proLbl.layer.borderWidth = 1.0;
        cell.proLbl.layer.borderColor = [UIColor redColor].CGColor;
        cell.proLbl.adjustsFontSizeToFitWidth = YES;
        
        cell.proLbl.frame = CGRectMake(cell.proLbl.frame.origin.x, cell.proLbl.frame.origin.y, cell.proLbl.intrinsicContentSize.width, cell.proLbl.frame.size. height);
        
        if(IS_IPAD)
        {
            cell.proLbl.frame = CGRectMake(cell.priceLbl.frame.origin.x, cell.proLbl.frame.origin.y, cell.proLbl.intrinsicContentSize.width, cell.proLbl.frame.size. height);
            
            cell.priceLbl.textAlignment = NSTextAlignmentLeft;
        }
        
       
        
       // cell.layer.masksToBounds = NO;
        
        cell.btnClick.tag = section;
        
        [cell.btnClick addTarget:self action:@selector(basicView:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    else
    {
        static NSString *CellIdentifier = @"premium";
        packagesTableViewCell *cell = (packagesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"packagesTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        //NSInteger section = indexPath.section;
        cell.img.image = [UIImage imageNamed:@"icon-Basic"];
        cell.headerLbl.text =  [NSString stringWithFormat:@"%@",name];
        cell.priceLbl.text = [NSString stringWithFormat:@"INR %@/%@",price,duration];
        [cell.priceLbl setText:[cell.priceLbl.text uppercaseString]];
        
        if([payment_duration caseInsensitiveCompare:@"yearly"]==NSOrderedSame)
            cell.planLbl.text =  [NSString stringWithFormat:@"Yearly Plan"];
        else
            cell.planLbl.text =  [NSString stringWithFormat:@"Quarterly Plan"];
        
        cell.unlimVidLbl.text = [NSString stringWithFormat:@" %@",short_desc_title1];
        
        cell.unlimTechLbl.text = [NSString stringWithFormat:@"%@",short_desc_title2];
        
        cell.clipsToBounds = YES;
        cell.layer.cornerRadius = 5.0;
        // cell.layer.masksToBounds = NO;
        
        cell.proLbl.text = [NSString stringWithFormat:@" Promotional Offer   "];
        cell.proLbl.clipsToBounds = YES;
        cell.proLbl.layer.cornerRadius = 5.0;
        cell.proLbl.layer.borderWidth = 1.0;
        cell.proLbl.layer.borderColor = [UIColor redColor].CGColor;
        cell.proLbl.adjustsFontSizeToFitWidth = YES;
        
        cell.proLbl.frame = CGRectMake(cell.proLbl.frame.origin.x, cell.proLbl.frame.origin.y, cell.proLbl.intrinsicContentSize.width, cell.proLbl.frame.size. height);
        
        if(IS_IPAD)
        {
            cell.proLbl.frame = CGRectMake(cell.priceLbl.frame.origin.x, cell.proLbl.frame.origin.y, cell.proLbl.intrinsicContentSize.width, cell.proLbl.frame.size. height);
            
            cell.priceLbl.textAlignment = NSTextAlignmentLeft;
        }
        
        if([name caseInsensitiveCompare:@"premium"]==NSOrderedSame)
        {
            cell.btnClick.tag = section;
            
            [cell.btnClick addTarget:self action:@selector(premView:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.img.image = [UIImage imageNamed:@"icon-premium"];
            
            cell.imgTec.image = [UIImage imageNamed:@"icon-call"];
            
        }
       else if([name caseInsensitiveCompare:@"Plus"]==NSOrderedSame)
       {
           cell.btnClick.tag = section;
           
           [cell.btnClick addTarget:self action:@selector(plusView:) forControlEvents:UIControlEventTouchUpInside];
           
           cell.img.image = [UIImage imageNamed:@"icon-Pluss"];
       }
        else
        {
            cell.btnClick.tag = section;
            
            [cell.btnClick addTarget:self action:@selector(basicView:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.img.image = [UIImage imageNamed:@"icon-Basic"];
        }
        
        return cell;
        
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *idval = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"id"]];
    
    if([idval isEqualToString:@"0"])
    {
        return;
    }
    
    
}


-(void) tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row)
    {
        //end of loading
        //for example [activityIndicator stopAnimating];
        
        [self addDummy];
        
        NSLog(@"loaded fully");
    }
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    @try
    {
        
        if([message isKindOfClass:[NSArray class]])
        {
            return;
        }
        
        if([message isKindOfClass:[NSNull class]])
        {
            return;
        }
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:title
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            
            
        }];
        [alert addAction:okAction];
        // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        // [vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        
        if([pvc isKindOfClass:[sideMenuViewController class]])
        {
            [self presentViewController: alert animated: NO completion:nil];
        }
        else
            [pvc presentViewController: alert animated: NO completion:nil];
    }
    @catch (NSException *exception)
    {
        
    }
    
    
}


-(void)fetchAvailableProducts {
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects:basicPlan,plusPlan,premiumPlan,nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (BOOL)canMakePurchases {
    return [SKPaymentQueue canMakePayments];
}

- (void)purchaseMyProduct:(SKProduct*)product {
    if ([self canMakePurchases])
    {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
       
        @try
        {
          [[SKPaymentQueue defaultQueue] addPayment:payment];
        }
        @catch (NSException *exception)
        {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                      @"Please check your internet connection" message:nil delegate:
                                      self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alertView show];
        }
        
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"Purchases are disabled in your device" message:nil delegate:
                                  self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
}

#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
                
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier
                     isEqualToString:basicPlan])
                {
                    NSLog(@"Purchased basicPlan");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    
                  NSString *packageid=  [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"id"]];
                    
                    [self updatePackageWS:packageid];
                    
                   // [alertView show];
                }
                else if ([transaction.payment.productIdentifier
                          isEqualToString:plusPlan])
                {
                    NSLog(@"Purchased plusPlan");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    
                    NSString *packageid=  [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"id"]];
                    
                    [self updatePackageWS:packageid];
                    
                    
                   // [alertView show];
                }
                else if ([transaction.payment.productIdentifier
                          isEqualToString:premiumPlan])
                {
                    NSLog(@"Purchased premiumPlan");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    
                    NSString *packageid=  [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"id"]];
                    
                    [self updatePackageWS:packageid];
                    
                   // [alertView show];
                }
                [self.view hideToastActivity];
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateRestored:
                if ([transaction.payment.productIdentifier
                     isEqualToString:basicPlan])
                {
                    NSLog(@"Purchased basicPlan");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    
                    NSString *packageid=  [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"id"]];
                    
                    [self updatePackageWS:packageid];
                    
                  //  [alertView show];
                }
                else if ([transaction.payment.productIdentifier
                          isEqualToString:plusPlan])
                {
                    NSLog(@"Purchased plusPlan");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    
                    NSString *packageid=  [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"id"]];
                    
                    [self updatePackageWS:packageid];
                    
                    
                  //  [alertView show];
                }
                else if ([transaction.payment.productIdentifier
                          isEqualToString:premiumPlan])
                {
                    NSLog(@"Purchased premiumPlan");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Purchase is completed succesfully" message:nil delegate:
                                              self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                    
                    NSString *packageid=  [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPathPckBtn]valueForKey:@"id"]];
                    
                    [self updatePackageWS:packageid];
                    
                  //  [alertView show];
                }
                [self.view hideToastActivity];
                NSLog(@"Restored ");
                [self packagesWS];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
                
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
               // [self packagesWS];
                [self.view hideToastActivity];
                break;
            default:
                break;
        }
    }
    
    [activityIndicatorView stopAnimating];
}

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
   
    if (SKPaymentTransactionStateRestored)
    {
        NSLog(@"restored");
        // Successfully restored
    }else{
        // No transaction to restore
    }
}
-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    
    if (count>0) {
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        
        if ([validProduct.productIdentifier
             isEqualToString:basicPlan])
        {
            [productTitleLabel setText:[NSString stringWithFormat:
                                        @"Product Title: %@",validProduct.localizedTitle]];
            [productDescriptionLabel setText:[NSString stringWithFormat:
                                              @"Product Desc: %@",validProduct.localizedDescription]];
            [productPriceLabel setText:[NSString stringWithFormat:
                                        @"Product Price: %@",validProduct.price]];
        }
        else if ([validProduct.productIdentifier
                  isEqualToString:plusPlan])
        {
            [productTitleLabel setText:[NSString stringWithFormat:
                                        @"Product Title: %@",validProduct.localizedTitle]];
            [productDescriptionLabel setText:[NSString stringWithFormat:
                                              @"Product Desc: %@",validProduct.localizedDescription]];
            [productPriceLabel setText:[NSString stringWithFormat:
                                        @"Product Price: %@",validProduct.price]];
        }
        else if ([validProduct.productIdentifier
                  isEqualToString:premiumPlan])
        {
            [productTitleLabel setText:[NSString stringWithFormat:
                                        @"Product Title: %@",validProduct.localizedTitle]];
            [productDescriptionLabel setText:[NSString stringWithFormat:
                                              @"Product Desc: %@",validProduct.localizedDescription]];
            [productPriceLabel setText:[NSString stringWithFormat:
                                        @"Product Price: %@",validProduct.price]];
        }
    }
    else
    {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
    }
    
    [activityIndicatorView stopAnimating];
    purchaseButton.hidden = NO;
}

- (IBAction) restore
{
    //this is called when the user restores purchases, you should hook this up to a button
   // [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
   // [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}


/*- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10)
    {
      
    }
    else
    {
        //Do something else
    }
}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
    
}

@end
