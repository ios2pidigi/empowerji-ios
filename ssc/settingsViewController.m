//
//  settingsViewController.m
//  ssc
//
//  Created by swaroop on 18/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "settingsViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "profileTableViewCell.h"
#import "subCatTableViewCell.h"

@interface settingsViewController ()
{
    NSArray *pArray;
    NSArray *arrUser;
    NSMutableArray *totalcheckmarkArray;
    
    NSMutableArray *checkArrTblvw;
    NSArray *chkArr;
}
@end

@implementation settingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
   [self subCatWebservice];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}
#pragma mark webservice Calls

-(void)subCatWebservice
{
    //http://listlinkz.com/ssc/webservice/sub_categories.php?user_id=63
    
    [SVProgressHUD show];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@sub_categories.php?user_id=%@",baseUrl,[[arrUser objectAtIndex:0]valueForKey:@"user_id"]];
   // NSString *wUrl=[NSString stringWithFormat:@"%@sub_categories.php?user_id=65",baseUrl];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 20.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self subCatWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
            pArray = [json objectForKey:@"category"];
            
            if([pArray count]>0)
            {
                if([[pArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[pArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
            }
            
            totalcheckmarkArray =[[NSMutableArray alloc]init];
            
            for (int i=0; i<[pArray count]; i++) // Number of Rows count
            {
                [totalcheckmarkArray addObject:[[pArray objectAtIndex:i]valueForKey:@"sub"]];
            }
            
            chkArr = [[NSArray alloc]init];
            NSArray *arrstr = [[json objectForKey:@"user_catgories"]valueForKey:@"cat_ids"];
            
            NSString * str = [arrstr componentsJoinedByString:@""];
            
            chkArr = [str componentsSeparatedByString:@","];
            
           
            
          /*  if(chkArr.count == 0 || (chkArr.count!=totalcheckmarkArray.count))
            {
                checkArrTblvw =[[NSMutableArray alloc]init];
                
                for (int i=0; i<[pArray count]; i++) // Number of Rows count
                {
                    [checkArrTblvw addObject:@"NO"];
                }
            }
            else
            {
                checkArrTblvw =[[NSMutableArray alloc]init];
                checkArrTblvw = [chkArr mutableCopy];
            }
            */
            
            checkArrTblvw =[[NSMutableArray alloc]init];
            
            
            
            for (int i=0; i<[pArray count]; i++) // Number of Rows count
            {
                [checkArrTblvw addObject:@"NO"];
                
                for(int j=0; j<[chkArr count]; j++)
                {
                if([ [[pArray objectAtIndex:i]valueForKey:@"id"] isEqualToString:[chkArr objectAtIndex:j]])
                    {
                        [checkArrTblvw replaceObjectAtIndex:i withObject:[[pArray objectAtIndex:i]valueForKey:@"id"]];
                    }
                }
            }
            
            NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            NSFileManager * fileManager = [NSFileManager defaultManager];
            //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
               
                [plistDict setValue:checkArrTblvw forKey:@"checkArray"];
                [plistDict writeToFile:finalPath atomically:YES];
            }
            
            
            [_setTblVw reloadData];
            
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)submitWebservice
{
    //http://listlinkz.com/ssc/webservice/checked_category.php?user_id=19&cat_id=17
    
    [SVProgressHUD show];
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSString *catid;
    catid = [checkArrTblvw componentsJoinedByString:@","];
    
   // catid = [catid stringByReplacingOccurrencesOfString:@"NO" withString:@""];
    catid = [catid stringByReplacingOccurrencesOfString:@",NO" withString:@""];
    catid = [catid stringByReplacingOccurrencesOfString:@"NO," withString:@""];
    
    if([catid isEqualToString:@"NO"])
    {
        [self.view makeToast:@"Please select any categories to submit"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        [SVProgressHUD dismiss];
        return;
    }
    
    NSString *wUrl=[NSString stringWithFormat:@"%@checked_category.php?user_id=%@&cat_id=%@",baseUrl,userid,catid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self submitWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSArray *arr = [json objectForKey:@"result"];
            
            if([arr count]>0)
            {
                if([[arr objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[arr objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    [SVProgressHUD dismiss];
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                    return ;
                }
            }
            
            [SVProgressHUD dismiss];
            
           
            
        });
    }];
    
    [dataTask resume];
    
}


#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return pArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 50;
    }
    else
        return 40;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier =@"profile"; static NSString *CellIdentifierSub =@"sub";
    
    subCatTableViewCell *scell;
    
    profileTableViewCell *cell = (profileTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"profileTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    NSString *sub;
    sub = [[pArray objectAtIndex:section]valueForKey:@"sub"];
    
    NSString *subexist;
    subexist = [[pArray objectAtIndex:section]valueForKey:@"sub_exists"];
    
    if([sub isEqualToString:@"no"])
    {
        cell.lbl.text = [[pArray objectAtIndex:section]valueForKey:@"name"];
        
        
       // [cell.btnChk setImage:[UIImage imageNamed:@"checkbox.png"] forState:(UIControlStateNormal)];
        
        if(![[checkArrTblvw objectAtIndex:section] isEqualToString:@"NO"])
        {
            [cell.btnChk setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.btnChk setImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
        }
        
        cell.btnChk.tag = section;
        
        [cell.btnChk addTarget:self action:@selector(chkAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if([subexist isEqualToString:@"yes"])
        {
            cell.btnChk.hidden = YES;
        }
    }
    else
    {
        scell = (subCatTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierSub];
        scell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (scell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"subCatTableViewCell" owner:self options:nil];
            scell = [nib objectAtIndex:0];
        }
        
        scell.subLbl.text = [[pArray objectAtIndex:section]valueForKey:@"name"];
        
       
        //[scell.subChkBtn setImage:[UIImage imageNamed:@"checkbox.png"] forState:(UIControlStateNormal)];
        
        if(![[checkArrTblvw objectAtIndex:section] isEqualToString:@"NO"])
        {
            [scell.subChkBtn setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            [scell.subChkBtn setImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
        }
        
        scell.subChkBtn.tag = section;
        
        [scell.subChkBtn addTarget:self action:@selector(chkAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if([subexist isEqualToString:@"yes"])
        {
            scell.subChkBtn.hidden = YES;
        }
        
        scell.clipsToBounds = NO;
        scell.layer.masksToBounds = NO;
        
        return scell;
        
    }
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    
}

#pragma mark button actions

- (IBAction)onBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)btnSubmit:(id)sender
{
    [self submitWebservice];
}
- (void)chkAction:(UIButton*)sender
{
    
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"checkArray"];
    }
    NSMutableDictionary *plistDict;
    if([fileManager fileExistsAtPath:finalPath])
    {
        plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
        arrRate = [plistDict valueForKey:@"checkArray"];
        if ([arrRate count] == 0)
        {
            arrRate = [NSMutableArray new];
            
        }
    }
    
    
    if([sender.currentImage isEqual:[UIImage imageNamed:@"checkbox.png"]])
    {
        [sender setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:(UIControlStateNormal)];
        
        NSString *sub;
        sub = [[pArray objectAtIndex:sender.tag]valueForKey:@"sub"];
        
        if([sub isEqualToString:@"no"])
        {
            for (UITableViewCell *view in self.setTblVw.visibleCells)
            {
                
                
                for(UIButton *btn in view.contentView.subviews)
                {
                    if ([btn isKindOfClass:[UIButton class]])
                    {
                        
                        
                        NSLog(@"%ld",sender.tag+1); NSLog(@"%ld",(long)btn.tag);
                        
                        //if (btn.tag==sender.tag)//+1||btn.tag==sender.tag+2)
                        {
                            for (int j =1; j<(totalcheckmarkArray.count); j++)// - sender.tag
                            {
                                if([[totalcheckmarkArray objectAtIndex:sender.tag] isEqualToString:@"yes"])//sender.tag+j
                                {
                                    if (btn.tag==sender.tag+j)
                                    {
                                        [btn setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:(UIControlStateNormal)];
                                        
                                        [checkArrTblvw replaceObjectAtIndex:btn.tag withObject:[[pArray objectAtIndex:btn.tag]valueForKey:@"id"]];
                                        
                                        //[checkArrTblvw replaceObjectAtIndex:sender.tag withObject:@"NO"];
                                    }
                                    else if(btn.tag==sender.tag)
                                    {
                                        [checkArrTblvw replaceObjectAtIndex:sender.tag withObject:[[pArray objectAtIndex:sender.tag]valueForKey:@"id"]];
                                    }
                                }
                                else
                                {
                                    [checkArrTblvw replaceObjectAtIndex:sender.tag withObject:[[pArray objectAtIndex:sender.tag]valueForKey:@"id"]];
                                    
                                    break;
                                }
                                
                            }
                            
                            
                            
                        }
                        
                        
                    }
                }
                
            }
        }
        
        else
            [checkArrTblvw replaceObjectAtIndex:sender.tag withObject:[[pArray objectAtIndex:sender.tag]valueForKey:@"id"]];
        
    }
    else
    {
        [sender setImage:[UIImage imageNamed:@"checkbox.png"] forState:(UIControlStateNormal)];
        
        [checkArrTblvw replaceObjectAtIndex:sender.tag withObject:@"NO"];
    }
    
    [plistDict setValue:checkArrTblvw forKey:@"checkArray"];
    [plistDict writeToFile:finalPath atomically:YES];
}

- (IBAction)homeActn:(id)sender
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    UINavigationController *nav = (UINavigationController *)self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        [nav popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
