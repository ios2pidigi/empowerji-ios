//
//  signupViewController.h
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface signupViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *nameTF;
@property (strong, nonatomic) IBOutlet UITextField *addTF;
@property (strong, nonatomic) IBOutlet UITextField *mobTF;
@property (strong, nonatomic) IBOutlet UITextField *passwrdTF;
@property (strong, nonatomic) IBOutlet UIButton *termsBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@property (strong, nonatomic) IBOutlet UITextField *cityTF;
@property (strong, nonatomic) IBOutlet UITextField *ageTF;
@property (strong, nonatomic) IBOutlet UITextField *langTF;
@property (strong, nonatomic) IBOutlet UITextField *otherTF;
@property (strong, nonatomic) IBOutlet UITableView *langTblVw;
@property (strong, nonatomic) IBOutlet UITableView *ageTblVw;
@property (strong, nonatomic) IBOutlet UIButton *btnSelAge;
@property (strong, nonatomic) IBOutlet UIButton *btnSelLang;

@property (strong, nonatomic) IBOutlet UIButton *showPassBtn;
@property (strong, nonatomic) IBOutlet UIImageView *showPassImg;
@property (strong, nonatomic) IBOutlet UIButton *showTerms;

@property (nonatomic) BOOL isFrmHomeLogin;
@property (nonatomic) BOOL isfrmInapp;

@end
