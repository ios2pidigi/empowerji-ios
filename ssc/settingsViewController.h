//
//  settingsViewController.h
//  ssc
//
//  Created by swaroop on 18/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface settingsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *setTblVw;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@end
