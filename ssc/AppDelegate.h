//
//  AppDelegate.h
//  ssc
//
//  Created by swaroop on 02/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>
{
    UINavigationController *navigationController;
}
@property (strong, nonatomic) UIWindow *window;


@end

