//
//  homeCollectionViewCell.h
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface homeCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *homeImg;
@property (strong, nonatomic) IBOutlet UILabel *homeLbl;
@property (strong, nonatomic) IBOutlet UILabel *paddingLbl;
@property (strong, nonatomic) IBOutlet UIImageView *homeImg1;
@end
