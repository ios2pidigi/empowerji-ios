//
//  sideMenuTableViewCell.h
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface sideMenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *sideMenuImg;
@property (strong, nonatomic) IBOutlet UILabel *sideMenuLbl;
@property (strong, nonatomic) IBOutlet UIImageView *cellBg;

@end
