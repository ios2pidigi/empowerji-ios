//
//  searchViewController.m
//  ssc
//
//  Created by sarath sb on 4/16/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "searchViewController.h"
#import "moTableViewCell.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "UIImageView+JMImageCache.h"
#import "detailViewController.h"
#import "SVProgressHUD.h"
#import "connectivity.h"

@interface searchViewController ()
{
    NSArray *mArray;
}
@end

@implementation searchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark WebService Calls

-(void)searchWebservice
{
    //http://listlinkz.com/ssc/webservice/article_search_list.php?search_txt=17
    [SVProgressHUD show];
    mArray = [[NSArray alloc]init];
    
    NSString *searchTxt;
    
    searchTxt = self.searhBar.text ;
    searchTxt = [searchTxt stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSInteger listid;
    listid = [[NSUserDefaults standardUserDefaults]integerForKey:@"listid"];
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@article_search_list.php?search_txt=%@&lang_id=%ld",baseUrl,searchTxt,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    if(searchTxt.length>0)
    {
        
    }
    else
    {
        [self.view makeToast:@"Search field should not be blank"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        [_seTblVw reloadData];
        
        [SVProgressHUD dismiss];
        return ;
    }
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self searchWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           
            mArray = [json objectForKey:@"result"];
            
            if([mArray count]>0)
            {
                if([[mArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[mArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    [_seTblVw reloadData];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
                
                ////////
                NSMutableArray *arrRate = [[NSMutableArray alloc]init];
                NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                NSFileManager * fileManager = [NSFileManager defaultManager];
                //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                if([fileManager fileExistsAtPath:finalPath])
                {
                    NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                    arrRate = [plistDict objectForKey:@"searchArray"];
                    
                    if ([arrRate count] == 0)
                    {
                        arrRate = [NSMutableArray new];
                        
                        for(NSDictionary *dict in mArray)
                        {
                            NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                            
                            NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                            [dictToSave removeObjectsForKeys:keysForNullValues];
                            
                            [arrRate addObject:dictToSave];
                        }
                        NSLog(@"arrRate :%@",arrRate);
                        
                        [plistDict setValue:arrRate forKey:@"searchArray"];
                        [plistDict writeToFile:finalPath atomically:YES];
                        
                    }
                    else
                    {
                        arrRate = [NSMutableArray new];
                        
                        for(NSDictionary *dict in mArray)
                        {
                            NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                            
                            NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                            [dictToSave removeObjectsForKeys:keysForNullValues];
                            
                            [arrRate addObject:dictToSave];
                        }
                        NSLog(@"arrRate :%@",arrRate);
                        
                        [plistDict setValue:arrRate forKey:@"searchArray"];
                        [plistDict writeToFile:finalPath atomically:YES];
                    }
                }
                
                
                ///////
            }
            [_seTblVw reloadData];
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
}


#pragma mark buttonActions

- (IBAction)backActn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
     [self searchWebservice];
    }
                  
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
}
- (IBAction)homeActn:(id)sender
{
   [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return mArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 120;
    }
    else
        return 90;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier =@"more";
    
    moTableViewCell *cell = (moTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"moTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.cellTitle.text = [[mArray objectAtIndex:section]valueForKey:@"title"];
    
    //cell.cellImg = [[mArray objectAtIndex:section]valueForKey:@"image"];
    [cell.cellImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[mArray objectAtIndex:section]valueForKey:@"image"]]] placeholder:nil];
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    [[NSUserDefaults standardUserDefaults]setInteger:indexPath.section forKey:@"searchIndexPsth"];
    
    NSString *idval = [NSString stringWithFormat:@"%@",[[mArray objectAtIndex:indexPath.section] valueForKey:@"id"]];
    
    detailViewController *dVC = [[detailViewController alloc]init];
    [dVC getDictionaryDetailid:idval];
    dVC.isFrmSearch=YES;
    [self.navigationController pushViewController:dVC animated:YES];
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

@end
