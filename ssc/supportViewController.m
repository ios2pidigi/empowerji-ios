//
//  supportViewController.m
//  ssc
//
//  Created by sarath sb on 6/4/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "supportViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "sendMessageViewController.h"
#import "sideMenuViewController.h"
#import "connectivity.h"


@interface supportViewController ()
{
    NSArray *arrUser;
    
    NSArray *sArray;
    NSArray *kArray;
    
    NSArray *staticPages;
}
@end

@implementation supportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _borderVw.layer.borderColor=[[UIColor whiteColor]CGColor];
    _borderVw.layer.borderWidth= 1.0f;
    
   if([[NSUserDefaults standardUserDefaults]objectForKey:@"supportClickedHereButton"])
   {
       _supportclickehereBtn.hidden = YES;
       _suppclickHereImage.hidden = YES;
    //hide button here
   }
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"keepgoingClickedHereButton"])
    {
        _kgclickhereBtn.hidden = YES;
        _kgclickHereImage.hidden = YES;
      //hide button here
    }
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                 // [self showMessage:errorMessage withTitle:nil];
                  
                  NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                  NSData *data = [def objectForKey:@"staticArrContents"];
                  staticPages = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                  [self showStaticPagesFromLocal];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            NSData *data = [def objectForKey:@"staticArrContents"];
        staticPages = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [self showStaticPagesFromLocal];
        
    }
    else
    {
      [self supportWebService];
    }
    
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];

    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
   
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
   gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}

-(void)supportWebService
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    //http://listlinkz.com/ssc/webservice/support_keep.php?user_id=19
    
    NSString *userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@support_keep.php?user_id=%@",baseUrl,userid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self supportWebService];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            sArray = [json objectForKey:@"support"];
            
            kArray = [json objectForKey:@"keep_going"];
            
            
            if([sArray count]>0)
            {
                UIFont *font = [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
                
                UIColor *color = [UIColor whiteColor];
                
                
                
                NSDictionary *attrDict = @{
                                           NSFontAttributeName : font,
                                           NSForegroundColorAttributeName : color
                                           };
                
                NSString *htmlString = [[sArray objectAtIndex:0] valueForKey:@"page_content"];
                
                htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
                
                htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"5\" align=\"left\">%@<font>",htmlString];
                
                NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                        initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                        options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                        documentAttributes: nil
                                                        error: nil ];
                
                
                
                NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
                NSRange range = (NSRange){0,[newString length]};
                [newString enumerateAttribute:NSFontAttributeName inRange:range options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
                    UIFont *replacementFont =  [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
                    UIColor *color = [UIColor whiteColor];
                    [newString addAttribute:NSForegroundColorAttributeName value:color range:range];
                   // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
                }];
                
            
                _supportTxtVw.attributedText = newString;//attrStr;
                
            }
            
            UIFont *systemFont = [UIFont systemFontOfSize:12];
            NSLog(@"what is it? %@ %@", systemFont.familyName, systemFont.fontName);
            
            if([kArray count]>0)
            {
                NSString *htmlString = [[kArray objectAtIndex:0] valueForKey:@"page_content"];
                
                
                htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
                
                htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"5\" align=\"left\">%@<font>",htmlString];
                
                NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                        initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                        options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                        documentAttributes: nil
                                                        error: nil ];
                
                
                
                NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
                NSRange range = (NSRange){0,[newString length]};
                [newString enumerateAttribute:NSFontAttributeName inRange:range options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
                    UIFont *replacementFont =  [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
                    UIColor *color = [UIColor whiteColor];
                    [newString addAttribute:NSForegroundColorAttributeName value:color range:range];
                    
                   // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
                    
                    // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
                }];
                
                
                _keepGngTxtVw.attributedText = newString;//attrStr;
                
              /*  htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
                //  htmlString =[NSString stringWithFormat:@"<font face=\"%@\" size=\"4\" align=\"left\">%@<font>",htmlString];
                
                htmlString =[htmlString stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
                htmlString =[htmlString stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
                
                htmlString=[self convertHTML:htmlString];
                
                UIFont *font = [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
                
                UIColor *color = [UIColor whiteColor];
                
                
                
                NSDictionary *attrDict = @{
                                           NSFontAttributeName : font,
                                           NSForegroundColorAttributeName : color
                                           };
                
                // NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSFontAttributeName : font,NSForegroundColorAttributeName : color , NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                
                NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:htmlString attributes: attrDict];
                
                 _keepGngTxtVw.attributedText = attrStr;
                
               // _keepGngTxtVw.text = [[kArray objectAtIndex:0] valueForKey:@"page_content"];
               
               */
            }
            
            
           // [_mTblVw reloadData];
            
            [SVProgressHUD dismiss];
            
            dispatch_async(dispatch_get_main_queue(),
                           ^{
                               
                               [self.view hideToastActivity];
                               
                           });
            
        });
    }];
    
    [dataTask resume];
}

-(void)requestWebService:(NSString *)type
{
      //http://listlinkz.com/ssc/webservice/request.php?user_id=19&req_type=support
    
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSString *userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
        
        NSString *wUrl=[NSString stringWithFormat:@"%@request.php?user_id=%@&req_type=%@",baseUrl,userid,type];
        
        NSLog(@"URL :%@", wUrl);
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = 100.0;
        sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary *json;
            
            if(data ==nil)
            {
               // [self supportWebService];
                return ;
            }
            
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"json :%@", json);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([type caseInsensitiveCompare:@"keep_going"]==NSOrderedSame)
                {
                    _kgclickhereBtn.hidden = YES;
                    _kgclickHereImage.hidden = YES;
                    
                  [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"keepgoingClickedHereButton"];
                }
                else
                {
                    _supportclickehereBtn.hidden = YES;
                    _suppclickHereImage.hidden = YES;
                    
                  [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"supportClickedHereButton"];
                }
                
                [SVProgressHUD dismiss];
                
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   
                                   [self.view hideToastActivity];
                                   
                               });
                
            });
        }];
        
        [dataTask resume];
}

-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}

- (IBAction)supportClickHereAction:(id)sender
{
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
        
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:@"Your interest in Support-ji has been recorded.We will keep you updated about the launch on this service."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    //Are you sure you are interested in Support-ji ? Please click OK to proceed. Else click cancel.
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
     {
                                   
       //Ok clicked
         
         [self requestWebService:@"support"];
                                   
     }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        
        // cancel clicked
        
    }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    
    
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
        
    }
                  
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
}
- (IBAction)keepGngClickHereAction:(id)sender
{
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
        
        
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:@"Your interest in Going-ji has been recorded.We will keep you updated about the launch on this service."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        
        //Ok clicked
        
        [self requestWebService:@"keep_going"];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        // cancel clicked
    }];
    
    [alert addAction:okAction];
    [alert addAction:cancelAction];
    
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
        
    }
                  
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
}

- (IBAction)sendMessageAction:(id)sender
{
    sendMessageViewController *smVC = [[sendMessageViewController alloc]initWithNibName:@"sendMessageViewController" bundle:nil];
    
    [self.navigationController pushViewController:smVC animated:YES];
}
- (IBAction)menuAction:(id)sender
{
    sideMenuViewController *svc=[[sideMenuViewController alloc]init];
    svc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    svc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:svc animated:YES completion:nil];
}

-(void)showStaticPagesFromLocal
{
    sArray = [staticPages objectAtIndex:2] ;
    
    kArray = [staticPages objectAtIndex:3] ;
    
    
    if([sArray count]>0)
    {
        UIFont *font = [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
        
        UIColor *color = [UIColor whiteColor];
        
        
        
        NSDictionary *attrDict = @{
                                   NSFontAttributeName : font,
                                   NSForegroundColorAttributeName : color
                                   };
        
        NSString *htmlString = [sArray valueForKey:@"page_content"];
        
        htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
        
        htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"5\" align=\"left\">%@<font>",htmlString];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil ];
        
        
        
        NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
        NSRange range = (NSRange){0,[newString length]};
        [newString enumerateAttribute:NSFontAttributeName inRange:range options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
            UIFont *replacementFont =  [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
            UIColor *color = [UIColor whiteColor];
            [newString addAttribute:NSForegroundColorAttributeName value:color range:range];
            // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
        }];
        
        
        _supportTxtVw.attributedText = newString;//attrStr;
        
    }
    
    UIFont *systemFont = [UIFont systemFontOfSize:12];
    NSLog(@"what is it? %@ %@", systemFont.familyName, systemFont.fontName);
    
    if([kArray count]>0)
    {
        NSString *htmlString = [kArray valueForKey:@"page_content"];
        
        
        htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
        
        htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"5\" align=\"left\">%@<font>",htmlString];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil ];
        
        
        
        NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
        NSRange range = (NSRange){0,[newString length]};
        [newString enumerateAttribute:NSFontAttributeName inRange:range options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
            UIFont *replacementFont =  [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
            UIColor *color = [UIColor whiteColor];
            [newString addAttribute:NSForegroundColorAttributeName value:color range:range];
            
            // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
            
            // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
        }];
        
        
        _keepGngTxtVw.attributedText = newString;
    }
}

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 
 Are you sure you are interested in 'category name' ?
 Please click OK to proceed. Else click cancel.
 
 Replace category name with Support-ji / Keep going-ji
 
 */



@end
