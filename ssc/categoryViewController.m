//
//  categoryViewController.m
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "categoryViewController.h"
#import "newsTableViewCell.h"
#import "Constants.h"
#import "listViewController.h"
#import "SVProgressHUD.h"
#import "UIView+Toast.h"
#import "searchViewController.h"
#import "connectivity.h"
#import <QuartzCore/QuartzCore.h>

#import "homeViewController.h"

@interface categoryViewController ()
{
    NSMutableArray *catArray;
    NSMutableDictionary *catDict;
}
@end

@implementation categoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_lblHead.layer setCornerRadius:10.0f];
    [_lblHead.layer setMasksToBounds:YES];
    
    NSString *admobId, *adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
    adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    [self.adVw loadRequest:[GADRequest request]];
    
    
    catArray = [[NSMutableArray alloc]init];
    
    [self catWebservice];
}

-(void)getCatDictionary:(NSMutableDictionary *) dictHere
{
    catDict = dictHere;
    
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}
# pragma mark WebService Calls

-(void)catWebservice
{
    [SVProgressHUD show];
    
    NSInteger catid;
    catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"catTag"];
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@categories.php?cat_id=%ld&lang_id=%ld",baseUrl,(long)catid,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
             [self catWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
           
            catArray = [json objectForKey:@"categories"];
            
            if([catArray count]>0)
            {
                if([[catArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[catArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
            }
            [_categoryTblVw reloadData];
            
        });
    }];
    
    [dataTask resume];
    
}


#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [catArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier =@"category";
    
    newsTableViewCell *cell = (newsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"newsTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.newsHdr.text = [[catArray objectAtIndex:section]valueForKey:@"name"];
    cell.newsPrevw.text = [[catArray objectAtIndex:section]valueForKey:@"description"];
    
    [cell.layer setCornerRadius:10.0f];
    [cell.layer setMasksToBounds:YES];
    [cell.cellBgImg.layer setCornerRadius:10.0f];
    [cell.cellBgImg.layer setMasksToBounds:YES];
    
    NSString *preText;
    preText = [[catArray objectAtIndex:section]valueForKey:@"description"];
    if([preText isEqualToString:@""])
    {
        cell.newsHdr.frame = CGRectMake(cell.newsHdr.frame.origin.x, (cell.contentView.frame.size.height/2)-(cell.newsHdr.frame.size.height)/2, cell.newsHdr.frame.size.width, cell.newsHdr.frame.size.height);
        
    }
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {

    NSInteger section = indexPath.section;
    
    NSInteger listid; NSString *name;
    listid = [[[catArray objectAtIndex:section]valueForKey:@"id"]integerValue];
    name = [[catArray objectAtIndex:section]valueForKey:@"name"];
    
    [[NSUserDefaults standardUserDefaults]setInteger:listid forKey:@"listid"];
    
    [[NSUserDefaults standardUserDefaults]setObject:name forKey:@"listname"];
    
    
    NSString *nameString = [catArray objectAtIndex:section];
    NSLog(@"headr:%@",nameString);
    
    listViewController *lVC = [[listViewController alloc]initWithNibName:@"listViewController" bundle:nil];
    [self.navigationController pushViewController:lVC animated:YES];
    
    }
                  
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
   
}
- (void)setCellColor:(UIColor *)color ForCell:(UITableViewCell *)cell
{
    cell.contentView.backgroundColor = color;
    cell.backgroundColor = color;
}

# pragma mark button action

- (IBAction)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)searchActn:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}

- (IBAction)homeAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
   /* NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    */
    
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
