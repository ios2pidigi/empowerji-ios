//
//  detailViewController.m
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "detailViewController.h"
#import "Constants.h"
#import "SVProgressHUD.h"
#import "UIImageView+JMImageCache.h"
#import <QuartzCore/QuartzCore.h>
#import "YTPlayerView.h"
#import "UIView+Toast.h"
#import "searchViewController.h"
#import "cTableViewCell.h"
#import "listCollectionViewCell.h"

#import "UIImageView+WebCache.h"
#import "popUpVideoViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "UIButton+WebCache.h"

#import "homeViewController.h"
#import "interViewController.h"

#import "inAppViewController.h"

@interface detailViewController ()<UIWebViewDelegate>
{
    NSMutableDictionary *dictDetails;
    NSString *strID;
     UIImageView *imageVw;
    
    BOOL isbkmd;
    NSArray *arrUser;
    
    NSInteger currentSearchIndex;
    
    NSInteger currentMoreIndex;
    
    NSArray *cArray;
    
    NSArray *newArray;
    
    UIButton *fullScreenBtn;
    
    BOOL isfs;
    
    CGRect yPFrame;
    
    int index;
    
    UIButton *customAdBanner;
    
}
@end

@implementation detailViewController
@synthesize isFrmMore,isFrmSearch,isfrmBkMore,isGroup;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid =@"";
    }
    else
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
     self.commentsTblVw.tableFooterView = [UIView new];
    
    currentSearchIndex = 0; currentMoreIndex = 0;
    
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"searchIndexPsth"])
    currentSearchIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"searchIndexPsth"];
    
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"moreArrayId"])
        currentMoreIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"moreArrayId"];
    
    
    
     _readMoreVw.frame = [UIScreen mainScreen].bounds;
    
    _commentDetailVw.frame = [UIScreen mainScreen].bounds;
    
    NSString *admobId; NSString *interstitial; NSString *adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
    interstitial = [[arrSet objectAtIndex:0]valueForKey:@"admob_inter_id"];
    
    adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        
        [self.adVw loadRequest:[GADRequest request]];
    }
    
    
    //interstitial
    self.interstitial = [[GADInterstitial alloc]
                         initWithAdUnitID:interstitial];
    GADRequest *request = [GADRequest request];
    [self.interstitial loadRequest:request];
    
    
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [rightSwipe setDirection:(UISwipeGestureRecognizerDirectionRight)];
    //[self.view addGestureRecognizer:rightSwipe];
    
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerLeft:)];
    [leftSwipe setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    //[self.view addGestureRecognizer:leftSwipe];
    
    
    if(isFrmMore)
    {
         _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"listname"];
    }
    else
     _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"listname"];
    //[[NSUserDefaults standardUserDefaults]objectForKey:@"HomeClickedCellName"];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"MOcatName"])
    {
        NSString *catName = [[NSUserDefaults standardUserDefaults]objectForKey:@"MOcatName"];
        _headerLbl.text = catName;
        
       /* NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        style.alignment = NSTextAlignmentJustified;
        style.firstLineHeadIndent = 10.0f;
        style.headIndent = 10.0f;
        style.tailIndent = -10.0f;
        
        NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:catName attributes:@{ NSParagraphStyleAttributeName : style}];
        
        _headerLbl.numberOfLines = 0;
        _headerLbl.attributedText = attrText;
        */
    }
    
     [_helpBtn addTarget:self action:@selector(helpAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Detail Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    yPFrame = _playerVw.frame;
    
    fullScreenBtn.frame = CGRectMake(_playerVw.frame.size.width - 45, (_playerVw.frame.origin.y + _playerVw.frame.size.height) - 45, 45, 45);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    ////////
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count > 0)
    {
        // "custom_banner_arr"
        
        //prefetch advw images and interstitial
        
        NSMutableArray *custom_banner_arr = [[NSMutableArray alloc]init];
        
        
        NSArray *custom_banner_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_banner_arr"];
        
        for(NSString *arr in custom_banner_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_banner"];
            [custom_banner_arr addObject:url];
        }
        
        // uint32_t rnd = arc4random_uniform([custom_banner_arr count]);
        
        index = arc4random_uniform(custom_banner_arr2.count);
        
        NSString *randomObject = [custom_banner_arr objectAtIndex:index];
        
        NSLog(@"rand:%u, rnd:%@",index,randomObject);
        
        NSLog(@"custom_banner url :%@",randomObject);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject forKey:@"custom_banner_url"];
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:randomObject] forState:UIControlStateNormal];
        
        /////
        NSMutableArray *custom_inter_arr = [[NSMutableArray alloc]init];
        
        NSArray *custom_inter_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_inter_arr"];
        
        for(NSString *arr in custom_inter_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_inter"];
            [custom_inter_arr addObject:url];
        }
        
        // uint32_t rnd2 = arc4random_uniform([custom_inter_arr count]);
        
        // NSUInteger randomIndex = arc4random() % custom_inter_arr.count;
        
        //  index = arc4random_uniform(custom_inter_arr2.count);
        
        
        NSString *randomObject2 = [custom_inter_arr objectAtIndex:index];
        
        NSLog(@"custom_inter_arr url :%@",randomObject2);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject2 forKey:@"custom_inter"];
        
        
        /////
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
     _readMoreBtn.layer.cornerRadius = _readMoreBtn.frame.size.height/2;
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        
        [self.adVw loadRequest:[GADRequest request]];
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
       // UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        customAdBanner.frame = _adVw.frame;
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
       // [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
        
    }
    
    [_shareLbl sizeToFit];
}

-(void)getDictionaryDetailid:(NSString *) idHere
{
    strID = idHere;
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    [self detailWebservice];
}

# pragma mark WebService Calls
static int count = 1;

-(void)detailWebservice
{
    [SVProgressHUD show];
    
    NSString *userid;
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid =@"";
    }
    else
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *strCount = [[arrSet objectAtIndex:0]valueForKey:@"popup_interval"];
    
    NSString *admobinter = [[arrSet objectAtIndex:0]valueForKey:@"admob_inter"];
    
    NSLog(@"%d",count % [strCount intValue]);
    
    if(count != 0 && count % [strCount intValue] == 0)
    {
        if([admobinter isEqualToString:@"Yes"])
        {
            
            if (self.interstitial.isReady) {
                [self.interstitial presentFromRootViewController:self];
            }
        }
        else if([admobinter caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
        {
            [self showInter];
        }
    }
    count++;
    
   // NSString *wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&user_id=%@",baseUrl,strID,userid];
    
   NSInteger catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"listid"];
    
  //  NSString *action = @"";
    NSString *grpid;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"grpIDs"])
    {
        grpid = [[NSUserDefaults standardUserDefaults]objectForKey:@"grpIDs"];
    }
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&cat_id=%ld&action=&user_id=%@&lang_id=%ld",baseUrl,strID,(long)catid,userid,langidd];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&cat_id=%ld&action=&lang_id=%ld",baseUrl,strID,(long)catid,langidd];
    }
    
        
    if(isGroup)
        wUrl=[NSString stringWithFormat:@"%@article_search_detail.php?article_id=%@&cat_id=%ld&action=&user_id=%@&search_ids=%@&lang_id=%ld",baseUrl,strID,(long)catid,userid,grpid,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
             [self detailWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
             dictDetails = [[json objectForKey:@"article"]objectAtIndex:0];
            
            
            newArray = [json objectForKey:@"more_articles"];
            
            [_collectionVw reloadData];
            
            _scrollArrowLbl.text = [dictDetails objectForKey:@"title"];
            
            _dateLbl.text = [dictDetails objectForKey:@"date"];
            
            NSString * htmlString = [dictDetails objectForKey:@"description"];
            if(htmlString.length>0)
            {
                
            htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
            
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            
            _txtVw.attributedText = attrStr;
                
                UIFont *font = _txtVw.font;
                
                CGRect textRect = [htmlString boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
                
                CGSize size = textRect.size;
                
                CGRect frame = _txtVw.frame;
                frame.size.height = size.height;
                _txtVw.frame = frame;
                
                CGRect fa2 = _readMoreBtn.frame;
                fa2.origin.y = frame.origin.y + frame.size.height;
                _readMoreBtn.frame = fa2;
                
                CGRect fa3 = _rmBackgroungLbl.frame;
                fa3.origin.y = frame.origin.y + frame.size.height;
                _rmBackgroungLbl.frame = fa3;
                
                CGRect cFrame = _collectionVw.frame;
                cFrame.origin.y = frame.origin.y + frame.size.height + fa3.size.height;
                _collectionVw.frame = cFrame;
                
                _scrollVw.contentSize = CGSizeMake(_scrollVw.frame.size.width, frame.size.height  + cFrame.size.height +fa2.size.height);
                
        }
            
            ////
            //////
            [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
            
          /*  NSMutableArray *arrRate = [[NSMutableArray alloc]init];
            NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            NSFileManager * fileManager = [NSFileManager defaultManager];
            //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                arrRate = [plistDict objectForKey:@"rateArray"];
            }
            
            if ([arrRate count] == 0) {
                isbkmd = NO;
            }
            
            for (NSDictionary *dic in arrRate) {
                
                @try
                {
                    
                    NSLog(@"%@  == %@",[dictDetails objectForKey:@"id"],[dic objectForKey:@"id"]);
                    
                    //crashes here
                    if ([[dictDetails objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]] && ([[dic objectForKey:@"userid"] isEqualToString:[[arrUser objectAtIndex:0]valueForKey:@"user_id"]]))
                    {
                        
                        isbkmd = YES;
                        
                        break;
                    }
                    else {
                        isbkmd = NO;
                    }
                    
                } @catch (NSException *exception)
                {
                    
                }
            }
           
           */
            
             if ([[dictDetails objectForKey:@"bookmark"] isEqualToString:@"yes"])
             {
                
                isbkmd = YES;
                
                [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateNormal];
                //change bookmark button image here
                
                
            }
            else {
                
                isbkmd = NO;
                [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
                //change bookmark image here
                
            }
            
            //////
            [imageVw removeFromSuperview];
            
           NSString *videoid;NSString *ylink;NSString *image; NSString *extrnLink; NSString *comments;
            
            comments = [dictDetails objectForKey:@"comments"];
            
            if([comments isEqualToString:@"yes"])
            {
               // [self textResizeForButton];
                //_btnComments.hidden = NO;
                
                
               NSRange range = [self visibleRangeOfTextView:_txtVw];
                
                NSLog(@"range :%@",NSStringFromRange(range));
                
               // _txtVw.contentInset = UIEdgeInsetsMake(0, 0, _btnComments.frame.size.height, 0);// top, left, bottom, right
                
                 _txtVw.textContainerInset = UIEdgeInsetsMake(0, 0, _btnComments.frame.size.height+50, 0);
                
                _btnComments.hidden = NO;
            }
            else
            {
                _btnComments.hidden = YES;
            }
            
            
           ylink = [dictDetails objectForKey:@"youtube_link"];
            image = [dictDetails objectForKey:@"image"];
            
            //external_link
            extrnLink = [dictDetails objectForKey:@"external_link"];
            
            if([extrnLink isEqualToString:@""])
            {
                _readMoreBtn.hidden=YES;
                _rmBackgroungLbl.hidden=YES;
            }
            NSURL *url = [NSURL URLWithString:extrnLink];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [self.webVw loadRequest:request];
            
            
            if([ylink isEqualToString:@""])
            {
                [SVProgressHUD dismiss];
                
                imageVw = [[UIImageView alloc]init];
                imageVw.frame = _playerVw.frame;
                @try {
                    
                [imageVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:nil];
                
                } @catch (NSException *exception)
                {
                  [imageVw sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
                }
                
                [self.view addSubview:imageVw];
                return ;
            }
            
          //  NSString *newString1 = [ylink stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
            
             NSString *newString1 = [self extractYoutubeIdFromLink:ylink];
            
            videoid = newString1;
            
            if(IS_IPAD)
            {
                isfs = NO;
                
                NSDictionary *playvarsDic = @{ @"controls" : @1, @"playsinline" : @1, @"autohide" : @1, @"showinfo" : @1, @"autoplay": @1, @"modestbranding" : @1 };
                
                 [_playerVw loadWithVideoId:videoid playerVars:playvarsDic];
                
                fullScreenBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                fullScreenBtn.frame = CGRectMake(_playerVw.frame.size.width - 45, (_playerVw.frame.origin.y + _playerVw.frame.size.height) - 45, 45, 45);
                fullScreenBtn.backgroundColor = [UIColor clearColor];
                [fullScreenBtn addTarget:self action:@selector(showFullScreeniPad) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:fullScreenBtn];
                
            }
            else
            {
                NSDictionary *playerVars = @{
                                             @"origin" :@"http://www.youtube.com",
                                             @"playsinline" : @1,
                                             @"autoplay" : @1,
                                             @"showinfo" : @0,
                                             @"rel" : @0,
                                             @"fs" : @1,
                                             @"modestbranding" : @1,
                                             };
                
                
                
                [_playerVw loadWithVideoId:videoid playerVars:playerVars];
                
                NSLog(@"playerView frame :%@",NSStringFromCGRect(self.playerVw.frame));
                
            if(IS_IPAD)
                _playerVw.webView.allowsInlineMediaPlayback = false;
            
            }
            
            [SVProgressHUD dismiss];
            
          
            
        });
    }];
    
    [dataTask resume];
    
}


-(void)nextPredetailWebservice :(NSInteger)catid :(NSString*)action
{
    //http://listlinkz.com/ssc/webservice/article_detail.php?article_id=1720&cat_id=12&action=next
    
    [SVProgressHUD show];
    //count ++;
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *strCount = [[arrSet objectAtIndex:0]valueForKey:@"popup_interval"];
    
    NSString *admobinter = [[arrSet objectAtIndex:0]valueForKey:@"admob_inter"];
    
    NSLog(@"%d",count % [strCount intValue]);
    
    if(count != 0 && count % [strCount intValue] == 0)
    {
        if([admobinter isEqualToString:@"Yes"])
        {
            
            if (self.interstitial.isReady) {
                [self.interstitial presentFromRootViewController:self];
            }
        }
        else if([admobinter caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
        {
            [self showInter];
        }
    }
    count++;
    
    //NSString *action; NSString *catid;
    
    catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"listid"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&user_id=%@&cat_id=%ld&action=%@",baseUrl,strID,userid,(long)catid,action];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&cat_id=%ld&action=%@",baseUrl,strID,(long)catid,action];
    }
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            // [self detailWebservice:nil];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           // if([_listArray count]>0)
            {
                if([[[json objectForKey:@"article"]objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[[json objectForKey:@"article"]objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
            }
            
            dictDetails = [[json objectForKey:@"article"]objectAtIndex:0];
            
            
            strID = [dictDetails objectForKey:@"id"];
            
            _scrollArrowLbl.text = [dictDetails objectForKey:@"title"];
            
            NSString * htmlString = [dictDetails objectForKey:@"description"];
            htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
            
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            
            _txtVw.attributedText = attrStr;
            
            ////
            
            //////
            [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
            
         /*   NSMutableArray *arrRate = [[NSMutableArray alloc]init];
            NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            NSFileManager * fileManager = [NSFileManager defaultManager];
            //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                arrRate = [plistDict objectForKey:@"rateArray"];
            }
            
            if ([arrRate count] == 0) {
                isbkmd = NO;
            }
            
            for (NSDictionary *dic in arrRate) {
                
                @try
                {
                    
                    NSLog(@"%@  == %@",[dictDetails objectForKey:@"id"],[dic objectForKey:@"id"]);
                    
                    //crashes here
                    if ([[dictDetails objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]] && ([[dic objectForKey:@"userid"] isEqualToString:[[arrUser objectAtIndex:0]valueForKey:@"user_id"]]))
                    {
                        
                        isbkmd = YES;
                        
                        break;
                    }
                    else {
                        isbkmd = NO;
                    }
                    
                } @catch (NSException *exception)
                {
                    
                }
            }
          
          */
            
        if ([[dictDetails objectForKey:@"bookmark"] isEqualToString:@"yes"])
            {
                
                isbkmd = YES;
                
                [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateNormal];
                //change bookmark button image here
                
                
            }
            else {
                
                isbkmd = NO;
                [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
                //change bookmark image here
                
            }
            
            //////
            [imageVw removeFromSuperview];
            
            NSString *videoid;NSString *ylink;NSString *image;NSString *extrnLink;
            
            ylink = [dictDetails objectForKey:@"youtube_link"];
            image = [dictDetails objectForKey:@"image"];
            
            
            extrnLink = [dictDetails objectForKey:@"external_link"];
            
            if([extrnLink isEqualToString:@""])
            {
                _readMoreBtn.hidden=YES;
                _rmBackgroungLbl.hidden=YES;
            }
            else
            {
                _readMoreBtn.hidden=NO;
                _rmBackgroungLbl.hidden=NO;
            }
            NSURL *url = [NSURL URLWithString:extrnLink];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [self.webVw loadRequest:request];
            
            
            
            if([ylink isEqualToString:@""])
            {
                [SVProgressHUD dismiss];
                
                imageVw = [[UIImageView alloc]init];
                imageVw.frame = _playerVw.frame;
                [imageVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:nil];
                
                [self.view addSubview:imageVw];
                return ;
            }
            
           // NSString *newString1 = [ylink stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
            
             NSString *newString1 = [self extractYoutubeIdFromLink:ylink];
            
            videoid = newString1;
            
            NSDictionary *playerVars = @{
                                         @"origin" :@"http://www.youtube.com",
                                         @"playsinline" : @1,
                                         @"autoplay" : @1,
                                         @"showinfo" : @0,
                                         @"rel" : @0,
                                         @"fs" : @1,
                                         @"modestbranding" : @1,
                                         };
            
            
            
            [_playerVw loadWithVideoId:videoid playerVars:playerVars];
            
            NSLog(@"playerView frame :%@",NSStringFromCGRect(self.playerVw.frame));
            
            if(IS_IPAD)
                _playerVw.webView.allowsInlineMediaPlayback = false;
            
            [SVProgressHUD dismiss];
        });
    }];
    
    [dataTask resume];
    
}

-(void)updatebookmarkWS:(NSString *)articleid
{
    //bookmarks are now from server rather than local
    
    //http://listlinkz.com/ssc/webservice/update_bookmark.php?user_id=3&article_id=1
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSString *wUrl=[NSString stringWithFormat:@"%@update_bookmark.php?user_id=%@&article_id=%@",baseUrl,userid,articleid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
     {
         NSDictionary *json;
         
         if(data ==nil)
         {
             // [self updatebookmarkWS];
             return ;
         }
         
         json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
         NSLog(@"json :%@", json);
         
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             
             
             
         });
     }];
    
    [dataTask resume];
    
}


-(void)showFullScreeniPad
{
    if(!isfs)
    {
        isfs = YES;
        
        [self.view bringSubviewToFront: _playerVw];
       
        
       // pFrame.size.width = 1024+128;
       // pFrame.size.height = 768+128;
         _playerVw.frame = self.view.frame;
        CGRect pFrame = _playerVw.frame;
        pFrame.origin.x = 0;
        pFrame.origin.y = 0;
        _playerVw.frame = pFrame;
        
        
        // _playerVw.frame = pFrame;
        
        fullScreenBtn.frame = CGRectMake(_playerVw.frame.size.width - 45, (_playerVw.frame.origin.y + _playerVw.frame.size.height) - 45, 45, 45);
        
        [self.view bringSubviewToFront: fullScreenBtn];
        
     /*   fullScreenBtn.backgroundColor = [UIColor redColor];
        
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
        
        float   angle = M_PI/2;  //rotate 180°, or 1 π radians
        _playerVw.layer.transform = CATransform3DMakeRotation(angle, 0, 0.0, 1.0);
        
        NSLog(@"player frame :%@",NSStringFromCGRect(_playerVw.frame));
        */
    }
    else
    {
        isfs = NO;
        
       /* [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
        
        float   angle = M_PI/2;  //rotate 180°, or 1 π radians
        _playerVw.layer.transform = CATransform3DMakeRotation(0, 0, 0.0, 1.0);
       */
        
       // [self.view bringSubviewToFront: _playerVw];
        _playerVw.frame = yPFrame;
        
        fullScreenBtn.frame = CGRectMake(_playerVw.frame.size.width - 45, (_playerVw.frame.origin.y + _playerVw.frame.size.height) - 45, 45, 45);
        
        [self.view bringSubviewToFront: fullScreenBtn];
    }
    
    
}


# pragma mark Swipe Gesture Delegate

-(void)swipeHandlerRight:(id)sender
{
    [self btnPrevActn:nil];
}

-(void)swipeHandlerLeft:(id)sender
{
    [self btnNxtActn:nil];
}


# pragma mark YTPlayer Delegates


-(void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Playback started" object:self];
    [self.playerVw playVideo];
}
- (IBAction)playVideo:(id)sender {
    [self.playerVw playVideo];
}

- (IBAction)stopVideo:(id)sender {
    [self.playerVw stopVideo];
}



# pragma mark button actions

- (IBAction)onBack:(id)sender
{
    [self.playerVw stopVideo];
    
    [imageVw removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onSearch:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnPrevActn:(id)sender
{
    NSString *action;
    action = @"previous";
    
    if(isFrmSearch)
    {
       // currentSearchIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"searchIndexPsth"];
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"searchArray"];
            
        }
        
       
        
        if(currentSearchIndex <= 0)
        {
            //currentSearchIndex = 0;
            
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
        }
        
         currentSearchIndex = currentSearchIndex-1;
        
        [self getDictionaryDetailid:[[arrRate objectAtIndex:currentSearchIndex]valueForKey:@"id"]];
    }
    else if(isFrmMore)
    {
        
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"moreArray"];
            
        }
        
        
        
        if(currentMoreIndex <= 0)
        {
            //currentMoreIndex = 0;
            
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
        }
        currentMoreIndex = currentMoreIndex-1;
        
        [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
    }
    else if(isfrmBkMore)
    {
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"rateArray"];
            
        }
        
        
        
        if(currentMoreIndex <= 0)
        {
            //currentMoreIndex = 0;
            
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
        }
        currentMoreIndex = currentMoreIndex-1;
        
        [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
    }
    else
    {
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"detailListArray"];
            
        }
        
        
        
        if(currentMoreIndex <= 0)
        {
            //currentMoreIndex = 0;
            
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
        }
        currentMoreIndex = currentMoreIndex-1;
        
        [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
        
      //NSInteger i=1;
      //[self nextPredetailWebservice:i:action];
    }
}
- (IBAction)btnNxtActn:(id)sender
{
    NSString *action;
    action = @"next";
    
    if(isFrmSearch)
    {
       
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"searchArray"];
            
        }
        
        
        
        
        if(currentSearchIndex >= ([arrRate count]-1))
        {
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
            //currentSearchIndex = 0;
        }
        currentSearchIndex = currentSearchIndex+1;
        
        [self getDictionaryDetailid:[[arrRate objectAtIndex:currentSearchIndex]valueForKey:@"id"]];
        //[self playVideoD:currentSearchIndex];
        
        
    }
    else if(isFrmMore)
    {
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"moreArray"];
            
        }
        
        
        
        if(currentMoreIndex >= ([arrRate count]-1))
        {
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
            //currentMoreIndex = 0;
        }
        
        currentMoreIndex = currentMoreIndex+1;
        
        [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
    }
    else if(isfrmBkMore)
    {
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"rateArray"];
            
        }
        
        
        
        if(currentMoreIndex >= ([arrRate count]-1))
        {
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
            //currentMoreIndex = 0;
        }
        
        currentMoreIndex = currentMoreIndex+1;
        
        [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
    }
    else
    {
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"detailListArray"];
            
        }
        
        
        
        if(currentMoreIndex >= ([arrRate count]-1))
        {
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
            //currentMoreIndex = 0;
        }
        
        currentMoreIndex = currentMoreIndex+1;
        
        [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
        
      //NSInteger i=0;
      //[self nextPredetailWebservice:i:action];
    }
}
- (IBAction)btnActionBookMark:(id)sender
{
    if([_btnBookMk.currentImage isEqual:[UIImage imageNamed:@"bookmark"]])
    {
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:(UIControlStateNormal)];
    }
    else
    {
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:(UIControlStateNormal)];
    }
    
     [self updatebookmarkWS:strID];
     
   /* NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
         arrRate = [plistDict objectForKey:@"rateArray"];
    }
    
    
   
    
    NSString *dict = [dictDetails objectForKey:@"id"];
    for (NSDictionary *dic in arrRate) {
        
        NSLog(@"%@",[dic objectForKey:@"id"]);
        NSLog(@"%@",dict);
        
        if ([dict isEqualToString:[dic objectForKey:@"id"]]&& ([[[arrUser objectAtIndex:0]valueForKey:@"user_id"] isEqualToString:[dic valueForKey:@"userid"]]))
        {
            isbkmd = YES;
            break;
        }
        else
        {
            isbkmd = NO;
        }
        
    }
    
    
    
    if (!isbkmd) {
        
        
        isbkmd = YES;
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
              arrRate = [plistDict valueForKey:@"rateArray"];
            if ([arrRate count] == 0)
            {
                arrRate = [NSMutableArray new];
                
                
         //       NSArray *keys = [dictDetails allKeys];
        //        NSArray * values = [dictDetails allValues];
//                NSArray * homeArray;
//
//                for(int i =0 ; i<[keys count] ; i++)
//                {
//                    homeArray = @[ @{[keys objectAtIndex:i]: [values objectAtIndex:i]} ];
//                }
                
                NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDetails];
                
                NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                [dictToSave removeObjectsForKeys:keysForNullValues];
                
                
                [dictToSave setObject:[[arrUser objectAtIndex:0]valueForKey:@"user_id"] forKey:@"userid"];
                
                NSInteger catid;
                catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"catTag"];
                
                [dictToSave setObject:[NSString stringWithFormat:@"%ld",(long)catid] forKey:@"catid"];
                
                [arrRate addObject:dictToSave];
                
               
                
            }
            else
            {
                NSMutableArray*arraySmpl;
                
                NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDetails];
                if ([arrRate isKindOfClass:[NSDictionary class]])
                {
                    NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
                    dict2= [dictToSave mutableCopy];
                    
                    [dictToSave setObject:dict2 forKey: @"test"];
                    
                    
                    NSMutableArray *ar = [[NSMutableArray alloc]init];
                    ar=[dictToSave objectForKey:@"test"];
                    
                    arraySmpl=[[NSMutableArray alloc]init];
                    [arraySmpl insertObject:ar atIndex:0];
                    arrRate = [arraySmpl mutableCopy];
                }
                else
                {
                   
                    
                   // [dictToSave setObject:[dictDetails objectForKey:@"id"] forKey:@"id"];
                    [dictToSave setObject:[[arrUser objectAtIndex:0]valueForKey:@"user_id"] forKey:@"userid"];
                    
                    NSInteger catid;
                    catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"catTag"];
                    
                    [dictToSave setObject:[NSString stringWithFormat:@"%ld",(long)catid] forKey:@"catid"];
                    
                    NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                    [dictToSave removeObjectsForKeys:keysForNullValues];
                    
                    [arrRate addObject:dictToSave];
                    
                   
                    
                }
                
                // [arrRate insertObject:dictToSave atIndex:0];
            }
            
            ///to remove <null> values from dict
            
            NSMutableDictionary *dict = [arrRate mutableCopy];
            if ([arrRate isKindOfClass:[NSDictionary class]])
            {
                if (!([arrRate count] == 0))
                {
                    @try {
                        NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                        [dict removeObjectsForKeys:keysForNullValues];
                        arrRate = dict;
                        
                    } @catch (NSException *exception) {
                        NSMutableDictionary *dict = [[arrRate objectAtIndex:0] mutableCopy];
                        
                        NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                        [dict removeObjectsForKeys:keysForNullValues];
                        
                        NSLog(@"dict :%@",dict);
                        
                        arrRate = dict;
                    }
                    
                }
            }
            ////
            
            [plistDict setValue:arrRate forKey:@"rateArray"];
            [plistDict writeToFile:finalPath atomically:YES];
            
        }
       
    }
    else
    {
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            
            int k =0;
            for (NSDictionary *dic in arrRate) {
                if ([dict isEqualToString:[dic objectForKey:@"id"]]&& ([[[arrUser objectAtIndex:0]valueForKey:@"user_id"] isEqualToString:[dic valueForKey:@"userid"]])) {
                    
                    [arrRate removeObjectAtIndex:k];
                    break;
                }
                k++;
            }
            [plistDict setValue:arrRate forKey:@"rateArray"];
            [plistDict writeToFile:finalPath atomically:YES];
        }
        
        
    }
    */
}
- (IBAction)btnActionShare:(id)sender
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    
    NSString *texttoshare =  [NSString stringWithFormat:@"%@  %@ %@",[dictDetails objectForKey:@"title"],[[arrSet objectAtIndex:0] valueForKey:@"article_share_text"],[[arrSet objectAtIndex:0] valueForKey:@"app_share_url"]];
    
    NSArray *activityItems = @[texttoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    if ([activityVC respondsToSelector:@selector(popoverPresentationController)])
    {
        // iOS 8+
        UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
        
        presentationController.sourceView = self.view; // if button or change to self.view.
    }
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

- (IBAction)readMoreActn:(id)sender
{
   [self.view addSubview:_readMoreVw];
}
- (IBAction)backRM:(id)sender
{
    [_readMoreVw removeFromSuperview];
}
- (IBAction)homeAction:(id)sender
{
   [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)playVideoD:(NSInteger)section
{
    NSMutableArray *arrSear = [[NSMutableArray alloc]init];
    NSArray * pathss = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePaths = ([pathss count] > 0) ? [pathss objectAtIndex:0] : nil;
    NSString * finalPaths = [basePaths stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManagers = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManagers fileExistsAtPath:finalPaths])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPaths];
        arrSear = [plistDict objectForKey:@"searchArray"];
        
    }
    
    dictDetails = [arrSear objectAtIndex:section];
    
    
    _scrollArrowLbl.text = [dictDetails objectForKey:@"title"];
    
    
    NSString * htmlString = [dictDetails objectForKey:@"description"];
    htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    
    _txtVw.attributedText = attrStr;
    
    ////
    
    //////
    [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
    
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"rateArray"];
    }
    
    if ([arrRate count] == 0) {
        isbkmd = NO;
    }
    
    for (NSDictionary *dic in arrRate) {
        
        @try
        {
            
            NSLog(@"%@  == %@",[dictDetails objectForKey:@"id"],[dic objectForKey:@"id"]);
            
            //crashes here
            if ([[dictDetails objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]] && ([[dic objectForKey:@"userid"] isEqualToString:[[arrUser objectAtIndex:0]valueForKey:@"user_id"]]))
            {
                
                isbkmd = YES;
                
                break;
            }
            else {
                isbkmd = NO;
            }
            
        } @catch (NSException *exception)
        {
            
        }
    }
    
    if (isbkmd) {
        
        isbkmd = YES;
        
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateNormal];
        //change bookmark button image here
        
        
    }
    else {
        
        isbkmd = NO;
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
        //change bookmark image here
        
    }
    
    [imageVw removeFromSuperview];
    
    NSString *videoid;NSString *ylink;NSString *image;
    
    ylink = [dictDetails objectForKey:@"youtube_link"];
    image = [dictDetails objectForKey:@"image"];
    
    if([ylink isEqualToString:@""])
    {
        
        
        imageVw = [[UIImageView alloc]init];
        imageVw.frame = _playerVw.frame;
        [imageVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:nil];
        
        [self.view addSubview:imageVw];
        return ;
    }
    
  //  NSString *newString1 = [ylink stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
    
    NSString *newString1 = [self extractYoutubeIdFromLink:ylink];
    
    videoid = newString1;
    
    NSDictionary *playerVars = @{
                                 @"origin" :@"http://www.youtube.com",
                                 @"playsinline" : @1,
                                 @"autoplay" : @1,
                                 @"showinfo" : @0,
                                 @"rel" : @0,
                                 @"fs" : @1,
                                 @"modestbranding" : @1,
                                 };
    
    
    
    [_playerVw loadWithVideoId:videoid playerVars:playerVars];
    
    NSLog(@"playerView frame :%@",NSStringFromCGRect(self.playerVw.frame));
    
    if(IS_IPAD)
    _playerVw.webView.allowsInlineMediaPlayback = false;
    
}

-(void)helpAction
{
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
   /* NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    */
    
}

-(void)showInter
{
    interViewController*loginViewController = [[interViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    navController.navigationBar.hidden = YES;
    [self presentViewController:navController animated:YES completion:nil];
}



- (NSString *)extractYoutubeIdFromLink:(NSString *)link
{
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
        return [link substringWithRange:result.range];
    }
    return nil;
}

#pragma mark change textview frame to fit a button
-(void)textResizeForButton{
    
    CGRect buttonFrame = self.btnComments.frame;
    UIBezierPath *exclusivePath = [UIBezierPath bezierPathWithRect:buttonFrame];
    self.txtVw.textContainer.exclusionPaths = @[exclusivePath];
    
}

#pragma mark scroll view delegate

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = scrollView.contentOffset.y;
    
    NSString *comments;
    
    comments = [dictDetails objectForKey:@"comments"];
    
    
    
    
    if (scrollOffset == 0)
    {
        // then we are at the top
    }
    else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight)
    {
        if([comments isEqualToString:@"yes"])
        {
           // [self textResizeForButton];
            _btnComments.hidden = NO;
            
            
        }
        else
        {
            _btnComments.hidden = YES;
        }
    }
    else
    {
        _btnComments.hidden = YES;
    }
}

- (IBAction)btnCommentsAction:(id)sender
{
    _commentDetailHeader.text = [dictDetails objectForKey:@"title"];
    
    [self commentsListWS];
    
    [self.view addSubview:_commentDetailVw];
    
}

- (IBAction)backCommentsDetail:(id)sender
{
    [_commentDetailVw removeFromSuperview];
}

- (IBAction)commentSubmit:(id)sender
{
    [self.view endEditing:YES];
    [self commentWs];
}

-(void)commentsListWS
{
    //http://listlinkz.com/ssc/webservice/read_comments.php?article_id=2
    
    NSString *wUrl=[NSString stringWithFormat:@"%@read_comments.php?article_id=%@",baseUrl,[dictDetails objectForKey:@"id"]];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self commentsListWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cArray = [[NSArray alloc]init];
            cArray = [json objectForKey:@"comments"];
            
            if([cArray count]>0)
            {
                if([[cArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[cArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    cArray = [[NSArray alloc]init];
                    
                     [_commentsTblVw reloadData];
                    [SVProgressHUD dismiss];
                    return ;
                }
            }
            
            [_commentsTblVw reloadData];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
}

-(void)commentWs
{
    //http://listlinkz.com/ssc/webservice/write_comment.php?article_id=1&user_id=3&comment=test-comment
    [SVProgressHUD show];
    
    NSString *commentTf;
    
    commentTf = [_commentTxtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    if(commentTf.length > 0)
    {
        
    }
    else
    {
        [self.view makeToast:@"Enter Your Comment"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        
        [SVProgressHUD dismiss];
        return ;
    }
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
        NSString *wUrl=[NSString stringWithFormat:@"%@write_comment.php?article_id=%@&user_id=%@&comment=%@",baseUrl,[dictDetails objectForKey:@"id"],userid,commentTf];
        
        NSLog(@"URL :%@", wUrl);
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = 100.0;
        sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary *json;
            
            if(data ==nil)
            {
                [self commentWs];
                return ;
            }
            
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"json :%@", json);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
               NSArray *mArray = [json objectForKey:@"comment"];
                
                if([mArray count]>0)
                {
                    if([[mArray objectAtIndex:0]objectForKey:@"success"])
                    {
                        [self.view makeToast:@"Your comment is submitted and pending approval. It will be displayed here once approved."
                                    duration:3.0
                                    position:CSToastPositionBottom];
                        
                        _commentTxtVw.text = @"";
                        [SVProgressHUD dismiss];
                        return ;
                    }
                    
                }
                
                [SVProgressHUD dismiss];
                
            });
        }];
        
        [dataTask resume];
        
    
}

#pragma mark UICollectionView methods

- (CGSize)itemSize
{
    NSInteger numberOfColumns = 1;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionVw.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return newArray.count;
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CollectionViewCellIdentifier";
    
    UINib *nib = [UINib nibWithNibName:@"listCollectionViewCell" bundle: nil];
    
    [collectionView registerNib:nib forCellWithReuseIdentifier:simpleTableIdentifier];
    
    NSInteger section = indexPath.row;
    
    listCollectionViewCell *cell = (listCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    NSString *title;
    
    if([[newArray objectAtIndex:section]valueForKey:@"name"])
        title = [[newArray objectAtIndex:section]valueForKey:@"name"];
    else if([[newArray objectAtIndex:section]valueForKey:@"title"])
        title = [[newArray objectAtIndex:section]valueForKey:@"title"];
    
    cell.titleLbl.text = title;
    
    cell.titleLbl.textColor = [UIColor whiteColor];
    
    [cell.imgCollec sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[newArray objectAtIndex:section]valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    
   
    
    [cell layoutSubviews];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    
//    cell.homeImg.layer.cornerRadius = 10;
//    cell.homeImg1.layer.cornerRadius = 10;
//
//    [cell.homeImg.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [cell.homeImg.layer setBorderWidth:2.0f];
    
    
    
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *articleId = [NSString stringWithFormat:@"%@",[[newArray objectAtIndex:indexPath.row]valueForKey:@"id"]];
    
    
    NSString *paid = [NSString stringWithFormat:@"%@",[[newArray objectAtIndex:indexPath.row] valueForKey:@"paid"]];
    
    if([paid caseInsensitiveCompare:@"paid"]==NSOrderedSame)
    {
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *ok = [languageBundle localizedStringForKey:@"ok" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        NSString *message = [languageBundle localizedStringForKey:@"alertPurchase" value:@"" table:@"strings-english"];
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                inAppViewController *inAPVC = [[inAppViewController alloc]init];
                [self.navigationController pushViewController:inAPVC animated:YES];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:cancel];
        
        [alert addAction:okAction];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
        
        
        
        
    }
    else
    {
        
    strID = articleId;
    
    [self detailWebservice];
        
    }
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 40.0;
    }
    else
        return 15.0; //0.01;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return 40.0;
    }
    else
        return 15.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 2.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    
    CGSize size ;
    
    
    NSIndexPath *pathToFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    
   
    {
        if(IS_IPAD)
        {
            size = CGSizeMake(258, 250);//cellWidth-84
        }
        else
            size = CGSizeMake(cellWidth-25, cellWidth-25); //162
    }
    
    
    return size;
}


- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if(IS_IPAD)
    {
        return UIEdgeInsetsMake(0, 105, 0, 105);
    }
    else
        return UIEdgeInsetsMake(0, 15, 0, 15); // top, left, bottom, right
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    
    return CGSizeMake(0, 0);
    
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    UINib *headerNib = [UINib nibWithNibName:@"searchCollectionHeaderView" bundle:nil];
    
    [collectionView registerNib:headerNib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"searchHead"];
    
    return nil;
   
}



#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return cArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 120;
    }
    else
        return 100;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier =@"c";
    
    cTableViewCell *cell = (cTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.userLbl.text = [[cArray objectAtIndex:section]valueForKey:@"user_name"];
    cell.dateLbl.text = [[cArray objectAtIndex:section]valueForKey:@"date"];
    cell.commentLbl.text = [[cArray objectAtIndex:section]valueForKey:@"comment"];
    
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSRange)visibleRangeOfTextView:(UITextView *)textView {
    CGRect bounds = textView.bounds;
    UITextPosition *start = [textView characterRangeAtPoint:bounds.origin].start;
    UITextPosition *end = [textView characterRangeAtPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds))].end;
    return NSMakeRange([textView offsetFromPosition:textView.beginningOfDocument toPosition:start],
                       [textView offsetFromPosition:start toPosition:end]);
}

@end
