//
//  supportViewController.h
//  ssc
//
//  Created by sarath sb on 6/4/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface supportViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *supportTxtVw;
@property (strong, nonatomic) IBOutlet UITextView *keepGngTxtVw;
@property (strong, nonatomic) IBOutlet UIImageView *suppclickHereImage;
@property (strong, nonatomic) IBOutlet UIImageView *kgclickHereImage;
@property (strong, nonatomic) IBOutlet UIButton *supportclickehereBtn;
@property (strong, nonatomic) IBOutlet UIButton *kgclickhereBtn;
@property (strong, nonatomic) IBOutlet UIView *borderVw;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@end
