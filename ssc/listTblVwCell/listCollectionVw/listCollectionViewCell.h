//
//  listCollectionViewCell.h
//  ssc
//
//  Created by swaroop on 10/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface listCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgCollec;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;

@end
