//
//  bmCollectionViewCell.h
//  ssc
//
//  Created by swaroop on 19/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface bmCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgbm;

@end
