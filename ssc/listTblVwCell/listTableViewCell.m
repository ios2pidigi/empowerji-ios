//
//  listTableViewCell.m
//  ssc
//
//  Created by swaroop on 10/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "listTableViewCell.h"

@implementation AFIndexedCollectionView

@end

@implementation listTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 9, 10);
    
    if([reuseIdentifier isEqualToString:@"bm"])
    {
        layout.itemSize = CGSizeMake(130, 130);
    }
    else
    layout.itemSize = CGSizeMake(230, 200);
    
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView = [[AFIndexedCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CollectionViewCellIdentifier];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.contentView addSubview:_collectionView];
    
    
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    NSLog(@"frame : :%@",NSStringFromCGRect(self.contentView.bounds));
    
    CGRect frame;
    frame = self.contentView.bounds;
    frame.origin.x = self.contentView.bounds.origin.x+5;
    frame.origin.y = self.contentView.bounds.origin.y+25;
    frame.size.width = self.contentView.bounds.size.width-15;
    frame.size.height = self.contentView.bounds.size.height-25;
    
    //self.collectionView.frame = self.contentView.bounds;
    
    self.collectionView.frame = frame; //self.contentView.bounds;
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath
{
     NSLog(@"index Path :%ld",(long)indexPath.section);
    
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.indexPath = indexPath;
    
    [self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
    
    [self.collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
   // [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
