//
//  listTableViewCell.h
//  ssc
//
//  Created by swaroop on 10/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFIndexedCollectionView : UICollectionView

@property (nonatomic, strong) NSIndexPath *indexPath;

@end

static NSString *CollectionViewCellIdentifier = @"CollectionViewCellIdentifier";

@interface listTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnM;
@property (strong, nonatomic) IBOutlet UIButton *prevArrow;
@property (strong, nonatomic) IBOutlet UILabel *titleLblC;

@property (nonatomic, strong) AFIndexedCollectionView *collectionView;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end
