//
//  categoryViewController.h
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface categoryViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UITableView *categoryTblVw;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UILabel *lblHead;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

-(void)getCatDictionary:(NSMutableDictionary *) dictHere;

@end
