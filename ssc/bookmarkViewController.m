//
//  bookmarkViewController.m
//  ssc
//
//  Created by swaroop on 17/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "bookmarkViewController.h"
#import "detailViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "moTableViewCell.h"
#import "UIImageView+JMImageCache.h"
#import "searchViewController.h"
#import "SVProgressHUD.h"
#import "cTableViewCell.h"


@interface bookmarkViewController ()
{
    NSArray *bArray;
    
    NSMutableDictionary *dictDetails;
    UIImageView *imageVw;
    BOOL isbkmd;
    NSArray *arrUser;
    
    NSInteger currentIndex;
    
    NSArray *cArray;
}

@end

@implementation bookmarkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentIndex =0;
    
    _detailVw.frame = [UIScreen mainScreen].bounds;
    
    _readMoreVw.frame = [UIScreen mainScreen].bounds;
    
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
       // bArray = [plistDict objectForKey:@"rateArray"];
    }
    
//[self.bkTblVw reloadData];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    //bookmarks are now from server rather than local
    [self bookmarkListWS];
    
    self.bkTblVw.tableFooterView = [UIView new];
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}
-(void)viewWillAppear:(BOOL)animated
{
    //bookmarks are now from server rather than local
    
   /* bArray = [[NSArray alloc]init];
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        bArray = [plistDict objectForKey:@"rateArray"];
    }
    
    [self.bkTblVw reloadData];
    */
}

-(void)bookmarkListWS
{
    //bookmarks are now from server rather than local
    [SVProgressHUD show];
    [_playerVw stopVideo];
    [imageVw removeFromSuperview];
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@bookmark_list.php?user_id=%@",baseUrl,userid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          NSDictionary *json;
          
          if(data ==nil)
          {
              [self bookmarkListWS];
              return ;
          }
          
          json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
          
          NSLog(@"json :%@", json);
          
          
          
          dispatch_async(dispatch_get_main_queue(), ^{
              
              
              bArray = [json objectForKey:@"result"];
              
              if([bArray count]>0)
              {
                  if([[bArray objectAtIndex:0]objectForKey:@"message"])
                  {
                      
                      [self.view makeToast:[[bArray objectAtIndex:0]objectForKey:@"message"]
                                  duration:3.0
                                  position:CSToastPositionBottom];
                      
                      bArray = [[NSArray alloc]init];
                      
                      [_bkTblVw reloadData];
                      
                      [SVProgressHUD dismiss];
                      return ;
                  }
                  
                  ////////
                  NSMutableArray *arrRate = [[NSMutableArray alloc]init];
                  NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                  NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                  NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                  NSFileManager * fileManager = [NSFileManager defaultManager];
                  //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                  if([fileManager fileExistsAtPath:finalPath])
                  {
                      NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                      arrRate = [plistDict objectForKey:@"bkArray"];
                      
                      if ([arrRate count] == 0)
                      {
                          arrRate = [NSMutableArray new];
                          
                          for(NSDictionary *dict in bArray)
                          {
                              NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                              
                              NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                              [dictToSave removeObjectsForKeys:keysForNullValues];
                              
                              [arrRate addObject:dictToSave];
                          }
                          NSLog(@"arrRate :%@",arrRate);
                          
                          [plistDict setValue:arrRate forKey:@"bkArray"];
                          [plistDict writeToFile:finalPath atomically:YES];
                          
                      }
                      else
                      {
                          arrRate = [NSMutableArray new];
                          
                          for(NSDictionary *dict in bArray)
                          {
                              NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dict];
                              
                              NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                              [dictToSave removeObjectsForKeys:keysForNullValues];
                              
                              [arrRate addObject:dictToSave];
                          }
                          NSLog(@"arrRate :%@",arrRate);
                          
                          [plistDict setValue:arrRate forKey:@"bkArray"];
                          [plistDict writeToFile:finalPath atomically:YES];
                      }
                  }
                  
                  
                  ///////
                  [_bkTblVw reloadData];
                  [SVProgressHUD dismiss];
              }
             
              
              [self.bkTblVw reloadData];
              
              [SVProgressHUD dismiss];
              
              });
          }];
    
    [dataTask resume];
    
}

-(void)detailWebservice:(NSString*)strID
{
    [SVProgressHUD show];
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&user_id=%@",baseUrl,strID,userid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          NSDictionary *json;
          
          if(data ==nil)
          {
              // [self detailWebservice:nil];
              return ;
          }
          
          json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
          
          NSLog(@"json :%@", json);
          
          
          
          dispatch_async(dispatch_get_main_queue(), ^{
              
              
              
              dictDetails = [[json objectForKey:@"article"]objectAtIndex:0];
              
              
              [self.view addSubview:_detailVw];
              
              _scrollArrowLbl.text = [dictDetails objectForKey:@"title"];
              
              _dateLbl.text = [dictDetails objectForKey:@"date"];
              
              
              NSString * htmlString = [dictDetails objectForKey:@"description"];
              htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
              htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
              
              NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
              
              
              _txtVw.attributedText = attrStr;
              
              ////
              //////
              [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
              
//              NSMutableArray *arrRate = [[NSMutableArray alloc]init];
//              NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//              NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
//              NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
//              NSFileManager * fileManager = [NSFileManager defaultManager];
//              //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
//              if([fileManager fileExistsAtPath:finalPath])
//              {
//                  NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
//                  arrRate = [plistDict objectForKey:@"rateArray"];
//              }
//
//              if ([arrRate count] == 0) {
//                  isbkmd = NO;
//              }
//
//              for (NSDictionary *dic in arrRate) {
//
//                  @try
//                  {
//
//                      NSLog(@"%@  == %@",[dictDetails objectForKey:@"id"],[dic objectForKey:@"id"]);
//
//                      //crashes here
//                      if ([[dictDetails objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]] && ([[dic objectForKey:@"userid"] isEqualToString:[[arrUser objectAtIndex:0]valueForKey:@"user_id"]]))
//                      {
//
//                          isbkmd = YES;
//
//                          break;
//                      }
//                      else {
//                          isbkmd = NO;
//                      }
//
//                  } @catch (NSException *exception)
//                  {
//
//                  }
//              }
              
              if ([[dictDetails objectForKey:@"bookmark"] isEqualToString:@"yes"]) {
                  
                  isbkmd = YES;
                  
                  [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateNormal];
                  //change bookmark button image here
                  
                  
              }
              else {
                  
                  isbkmd = NO;
                  [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
                  //change bookmark image here
                  
              }
              
              //////
              [imageVw removeFromSuperview];
              NSString *videoid;NSString *ylink;NSString *image; NSString *extrnLink;NSString *comments;
              
              comments = [dictDetails objectForKey:@"comments"];
              
              if([comments isEqualToString:@"yes"])
              {
                  // [self textResizeForButton];
                  //_btnComments.hidden = NO;
                  
                  
                  NSRange range = [self visibleRangeOfTextView:_txtVw];
                  
                  NSLog(@"range :%@",NSStringFromRange(range));
                  
                  _txtVw.textContainerInset = UIEdgeInsetsMake(0, 0, _btnComments.frame.size.height+50, 0);// top, left, bottom, right  //contentInset
                  
                  _btnComments.hidden = NO;
              }
              else
              {
                  _btnComments.hidden = YES;
              }
              
              ylink = [dictDetails objectForKey:@"youtube_link"];
              image = [dictDetails objectForKey:@"image"];
              
              extrnLink = [dictDetails objectForKey:@"external_link"];
              
              if([extrnLink isEqualToString:@""])
              {
                  _btnReadArticle.hidden=YES;
                  _lblReadArticle.hidden=YES;
              }
              NSURL *url = [NSURL URLWithString:extrnLink];
              
              NSURLRequest *request = [NSURLRequest requestWithURL:url];
              [self.webVw loadRequest:request];
              
              
              if([ylink isEqualToString:@""])
              {
                  [SVProgressHUD dismiss];
                  
                  imageVw = [[UIImageView alloc]init];
                  imageVw.frame = _playerVw.frame;
                  [imageVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:nil];
                  imageVw.backgroundColor = [UIColor whiteColor];
                  [self.view addSubview:imageVw];
                  return ;
              }
              
              NSString *newString1 = [ylink stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
              
              videoid = newString1;
              
              NSDictionary *playerVars = @{
                                           @"origin" :@"http://www.youtube.com",
                                           @"playsinline" : @1,
                                           @"autoplay" : @1,
                                           @"showinfo" : @0,
                                           @"rel" : @0,
                                           @"fs" : @1,
                                           @"modestbranding" : @1,
                                           };
              
              
              
              [_playerVw loadWithVideoId:videoid playerVars:playerVars];
              
              NSLog(@"playerView frame :%@",NSStringFromCGRect(self.playerVw.frame));
              
              
              
              
              [SVProgressHUD dismiss];
          });
      }];
    
    [dataTask resume];
    
}

-(void)updatebookmarkWS:(NSString *)articleid
{
    //bookmarks are now from server rather than local
    
    //http://listlinkz.com/ssc/webservice/update_bookmark.php?user_id=3&article_id=1
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSString *wUrl=[NSString stringWithFormat:@"%@update_bookmark.php?user_id=%@&article_id=%@",baseUrl,userid,articleid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
           // [self updatebookmarkWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
        });
    }];
    
    [dataTask resume];
    
}

#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == 10)
        return cArray.count;
        else
    return bArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 10)
    {
        if(IS_IPAD)
        {
            return 120;
        }
        else
            return 100;
    }
    else
     {
      if(IS_IPAD)
      {
        return 120;
      }
       else
        return 90;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
     if(tableView.tag == 10)
         return 10;
    else
    return 20;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 10)
    {
        static NSString *CellIdentifier =@"c";
        
        cTableViewCell *cell = (cTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        NSInteger section = indexPath.section;
        
        cell.userLbl.text = [[cArray objectAtIndex:section]valueForKey:@"user_name"];
        cell.dateLbl.text = [[cArray objectAtIndex:section]valueForKey:@"date"];
        cell.commentLbl.text = [[cArray objectAtIndex:section]valueForKey:@"comment"];
        
        
        cell.clipsToBounds = NO;
        cell.layer.masksToBounds = NO;
        
        return cell;
        
    }
    
    else
    {
    static NSString *CellIdentifier =@"more";
    
    moTableViewCell *cell = (moTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"moTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.cellTitle.text = [[bArray objectAtIndex:section]valueForKey:@"title"];
    
    //cell.cellImg = [[mArray objectAtIndex:section]valueForKey:@"image"];
    [cell.cellImg setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[bArray objectAtIndex:section]valueForKey:@"image"]]] placeholder:[UIImage imageNamed:@"logo"]];
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag == 10)
    {
    }
    else
    {
    NSString *idval = [NSString stringWithFormat:@"%@",[[bArray objectAtIndex:indexPath.section] valueForKey:@"id"]];
    
    currentIndex = indexPath.section;
    
     _detailHeadr.text = @"BookMark";
    
    [self detailWebservice:idval];
    
    }
   // [self.view addSubview:_detailVw];
    
    //[self playVideoD:indexPath.section];
    
   /*
    [self dismissViewControllerAnimated:YES completion:^{
        
        detailViewController *dVC = [[detailViewController alloc]init];
        [dVC getDictionaryDetailid:idval];
        dVC.isFrmMore=YES;
        [self.navigationController pushViewController:dVC animated:YES];
    }];
  */
     
    
}

-(void)playVideoD:(NSInteger)section
{
    dictDetails = [bArray objectAtIndex:section];
    
    
    _scrollArrowLbl.text = [dictDetails objectForKey:@"title"];
    
    
    NSString * htmlString = [dictDetails objectForKey:@"description"];
    htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    
    _txtVw.attributedText = attrStr;
    
    ////
    
    //////
    [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
    
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"rateArray"];
    }
    
    if ([arrRate count] == 0) {
        isbkmd = NO;
    }
    
    for (NSDictionary *dic in arrRate) {
        
        @try
        {
            
            NSLog(@"%@  == %@",[dictDetails objectForKey:@"id"],[dic objectForKey:@"id"]);
            
            //crashes here
            if ([[dictDetails objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]] && ([[dic objectForKey:@"userid"] isEqualToString:[[arrUser objectAtIndex:0]valueForKey:@"user_id"]]))
            {
                
                isbkmd = YES;
                
                break;
            }
            else {
                isbkmd = NO;
            }
            
        } @catch (NSException *exception)
        {
            
        }
    }
    
    if (isbkmd) {
        
        isbkmd = YES;
        
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateNormal];
        //change bookmark button image here
        
        
    }
    else {
        
        isbkmd = NO;
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
        //change bookmark image here
        
    }
    
    [imageVw removeFromSuperview];
    
    NSString *videoid;NSString *ylink;NSString *image; NSString *extrnLink;
    
    ylink = [dictDetails objectForKey:@"youtube_link"];
    image = [dictDetails objectForKey:@"image"];
    
    extrnLink = [dictDetails objectForKey:@"external_link"];
    
    if([extrnLink isEqualToString:@""])
    {
        _btnReadArticle.hidden=YES;
        _lblReadArticle.hidden=YES;
    }
    NSURL *url = [NSURL URLWithString:extrnLink];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webVw loadRequest:request];
    
    if([ylink isEqualToString:@""])
    {
        
        
        imageVw = [[UIImageView alloc]init];
        imageVw.frame = _playerVw.frame;
        [imageVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:nil];
        
        [self.view addSubview:imageVw];
        return ;
    }
    
    NSString *newString1 = [ylink stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
    
    videoid = newString1;
    
    NSDictionary *playerVars = @{
                                 @"origin" :@"http://www.youtube.com",
                                 @"playsinline" : @1,
                                 @"autoplay" : @1,
                                 @"showinfo" : @0,
                                 @"rel" : @0,
                                 @"fs" : @1,
                                 @"modestbranding" : @1,
                                 };
    
    
    
    [_playerVw loadWithVideoId:videoid playerVars:playerVars];
    
    NSLog(@"playerView frame :%@",NSStringFromCGRect(self.playerVw.frame));
    
    
    
}
#pragma mark button actions

- (IBAction)onBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onSearch:(id)sender
{
    //hidden
}
- (IBAction)homeActn:(id)sender
{
    // [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popToRootViewControllerAnimated:YES];
    
    UINavigationController *nav = (UINavigationController *)self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        [nav popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


# pragma mark Detail Vw

/*
 # pragma mark Swipe Gesture Delegate

-(void)swipeHandlerRight:(id)sender
{
    [self btnPrevActn:nil];
}

-(void)swipeHandlerLeft:(id)sender
{
    [self btnNxtActn:nil];
}
*/
-(void)forDetailBackReload
{
    bArray = [[NSArray alloc]init];
    
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        bArray = [plistDict objectForKey:@"rateArray"];
    }
    
    [self.bkTblVw reloadData];
}
# pragma mark YTPlayer Delegates


-(void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Playback started" object:self];
    [self.playerVw playVideo];
}
- (IBAction)playVideo:(id)sender {
    [self.playerVw playVideo];
}

- (IBAction)stopVideo:(id)sender {
    [self.playerVw stopVideo];
}



# pragma mark button actions

- (IBAction)onBackDetail:(id)sender
{
    
    [self bookmarkListWS];
    //[self forDetailBackReload];
    
    [_playerVw stopVideo];
    [imageVw removeFromSuperview];
    
    [_detailVw removeFromSuperview];
}
- (IBAction)onSearchDetail:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}

/*
- (IBAction)btnPrevActn:(id)sender
{
    NSString *action;
    action = @"previous";
    
    NSInteger i=1;
    [self nextPredetailWebservice:i:action];
}
- (IBAction)btnNxtActn:(id)sender
{
    NSString *action;
    action = @"next";
    
    NSInteger i=0;
    [self nextPredetailWebservice:i:action];
}
*/

- (IBAction)btnActionBookMark:(id)sender
{
    if([_btnBookMk.currentImage isEqual:[UIImage imageNamed:@"bookmark"]])
    {
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:(UIControlStateNormal)];
        
    }
    else
    {
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:(UIControlStateNormal)];
    }
    
    [self updatebookmarkWS:[[bArray objectAtIndex:currentIndex]valueForKey:@"id"]];
    
   /*
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"rateArray"];
    }
    
    
    
    
    NSString *dict = [dictDetails objectForKey:@"id"];
    for (NSDictionary *dic in arrRate) {
        
        NSLog(@"%@",[dic objectForKey:@"id"]);
        NSLog(@"%@",dict);
        
        if ([dict isEqualToString:[dic objectForKey:@"id"]]&& ([[[arrUser objectAtIndex:0]valueForKey:@"user_id"] isEqualToString:[dic valueForKey:@"userid"]]))
        {
            isbkmd = YES;
            break;
        }
        else
        {
            isbkmd = NO;
        }
        
    }
    
    
    
    if (!isbkmd) {
        
        
        isbkmd = YES;
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            arrRate = [plistDict valueForKey:@"rateArray"];
            if ([arrRate count] == 0)
            {
                arrRate = [NSMutableArray new];
                
                
                //       NSArray *keys = [dictDetails allKeys];
                //        NSArray * values = [dictDetails allValues];
                //                NSArray * homeArray;
                //
                //                for(int i =0 ; i<[keys count] ; i++)
                //                {
                //                    homeArray = @[ @{[keys objectAtIndex:i]: [values objectAtIndex:i]} ];
                //                }
                
                NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDetails];
                
                NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                [dictToSave removeObjectsForKeys:keysForNullValues];
                
                
                [dictToSave setObject:[[arrUser objectAtIndex:0]valueForKey:@"user_id"] forKey:@"userid"];
                
                NSInteger catid;
                catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"catTag"];
                
                [dictToSave setObject:[NSString stringWithFormat:@"%ld",(long)catid] forKey:@"catid"];
                
                [arrRate addObject:dictToSave];
                
                
                
            }
            else
            {
                NSMutableArray*arraySmpl;
                
                NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDetails];
                if ([arrRate isKindOfClass:[NSDictionary class]])
                {
                    NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
                    dict2= [dictToSave mutableCopy];
                    
                    [dictToSave setObject:dict2 forKey: @"test"];
                    
                    
                    NSMutableArray *ar = [[NSMutableArray alloc]init];
                    ar=[dictToSave objectForKey:@"test"];
                    
                    arraySmpl=[[NSMutableArray alloc]init];
                    [arraySmpl insertObject:ar atIndex:0];
                    arrRate = [arraySmpl mutableCopy];
                }
                else
                {
                    
                    
                    // [dictToSave setObject:[dictDetails objectForKey:@"id"] forKey:@"id"];
                    [dictToSave setObject:[[arrUser objectAtIndex:0]valueForKey:@"user_id"] forKey:@"userid"];
                    
                    NSInteger catid;
                    catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"catTag"];
                    
                    [dictToSave setObject:[NSString stringWithFormat:@"%ld",(long)catid] forKey:@"catid"];
                    
                    NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                    [dictToSave removeObjectsForKeys:keysForNullValues];
                    
                    [arrRate addObject:dictToSave];
                    
                    
                    
                }
                
                // [arrRate insertObject:dictToSave atIndex:0];
            }
            
            ///to remove <null> values from dict
            
            NSMutableDictionary *dict = [arrRate mutableCopy];
            if ([arrRate isKindOfClass:[NSDictionary class]])
            {
                if (!([arrRate count] == 0))
                {
                    @try {
                        NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                        [dict removeObjectsForKeys:keysForNullValues];
                        arrRate = dict;
                        
                    } @catch (NSException *exception) {
                        NSMutableDictionary *dict = [[arrRate objectAtIndex:0] mutableCopy];
                        
                        NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                        [dict removeObjectsForKeys:keysForNullValues];
                        
                        NSLog(@"dict :%@",dict);
                        
                        arrRate = dict;
                    }
                    
                }
            }
            ////
            
            [plistDict setValue:arrRate forKey:@"rateArray"];
            [plistDict writeToFile:finalPath atomically:YES];
            
        }
        
    }
    else
    {
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            
            int k =0;
            for (NSDictionary *dic in arrRate) {
                if ([dict isEqualToString:[dic objectForKey:@"id"]]&& ([[[arrUser objectAtIndex:0]valueForKey:@"user_id"] isEqualToString:[dic valueForKey:@"userid"]])) {
                    
                    [arrRate removeObjectAtIndex:k];
                    break;
                }
                k++;
            }
            [plistDict setValue:arrRate forKey:@"rateArray"];
            [plistDict writeToFile:finalPath atomically:YES];
        }
        
        
    }
    */
    
}
- (IBAction)btnActionShare:(id)sender
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    
    NSString *texttoshare =  [NSString stringWithFormat:@"%@  %@ %@",[dictDetails objectForKey:@"title"],[[arrSet objectAtIndex:0] valueForKey:@"article_share_text"],[[arrSet objectAtIndex:0] valueForKey:@"app_share_url"]];

    
    //NSString *texttoshare =  [NSString stringWithFormat:@"%@ %@",[dictDetails objectForKey:@"title"],[dictDetails objectForKey:@"youtube_link"]];
    NSArray *activityItems = @[texttoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    if ([activityVC respondsToSelector:@selector(popoverPresentationController)])
    {
        // iOS 8+
        UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
        
        presentationController.sourceView = self.view; // if button or change to self.view.
    }
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

- (IBAction)btnPrevActn:(id)sender
{
    NSString *action;
    action = @"previous";
    
   // [self nextPredetailWebservice:i:action];
    
    
    ////////
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"bkArray"];
        
    }
    
   
    
    if(currentIndex <= 0)
    {
        //currentMoreIndex = 0;
        
        [self.view makeToast:@"No more items in this category"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        //[SVProgressHUD dismiss];
        
        return;
    }
    currentIndex = currentIndex-1;
    
    _detailHeadr.text = @"BookMark";
    
    [self detailWebservice:[[arrRate objectAtIndex:currentIndex]valueForKey:@"id"]];
    
  /*  if(currentIndex <= 0)
    {
        //currentMoreIndex = 0;
        
        [self.view makeToast:@"No more items in this category"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        //[SVProgressHUD dismiss];
        
        return;
    }
    currentIndex = currentIndex-1;
    
    _detailHeadr.text = @"BookMark";
    
    [self playVideoD:currentIndex];
    //[self.view addSubview:_detailVw];
    */
    
}
- (IBAction)btnNxtActn:(id)sender
{
    NSString *action;
    action = @"next";
    
   // [self nextPredetailWebservice:i:action];
    
    ////////
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"bkArray"];
        
    }
    
    // [self nextPredetailWebservice:i:action];
    
    if(currentIndex >= ([bArray count]-1))
    {
        [self.view makeToast:@"No more items in this category"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        //[SVProgressHUD dismiss];
        
        return;
        //currentMoreIndex = 0;
    }
    
    currentIndex = currentIndex+1;
    
    _detailHeadr.text = @"BookMark";
    
    [self detailWebservice:[[arrRate objectAtIndex:currentIndex]valueForKey:@"id"]];
    
 /*   if(currentIndex >= ([bArray count]-1))
    {
        [self.view makeToast:@"No more items in this category"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        //[SVProgressHUD dismiss];
        
        return;
        //currentMoreIndex = 0;
    }
    
    currentIndex = currentIndex+1;
    
    _detailHeadr.text = @"BookMark";
    
    [self playVideoD:currentIndex];
    //[self.view addSubview:_detailVw];
  
  */
}

- (IBAction)readMoreActn:(id)sender
{
    [self.view addSubview:_readMoreVw];
}
- (IBAction)backRM:(id)sender
{
    [_readMoreVw removeFromSuperview];
}

-(NSRange)visibleRangeOfTextView:(UITextView *)textView {
    CGRect bounds = textView.bounds;
    UITextPosition *start = [textView characterRangeAtPoint:bounds.origin].start;
    UITextPosition *end = [textView characterRangeAtPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds))].end;
    return NSMakeRange([textView offsetFromPosition:textView.beginningOfDocument toPosition:start],
                       [textView offsetFromPosition:start toPosition:end]);
}

#pragma mark change textview frame to fit a button
-(void)textResizeForButton{
    
    CGRect buttonFrame = self.btnComments.frame;
    UIBezierPath *exclusivePath = [UIBezierPath bezierPathWithRect:buttonFrame];
    self.txtVw.textContainer.exclusionPaths = @[exclusivePath];
    
}

#pragma mark scroll view delegate

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = scrollView.contentOffset.y;
    
    NSString *comments;
    
    comments = [dictDetails objectForKey:@"comments"];
    
    
    
    
    if (scrollOffset == 0)
    {
        // then we are at the top
    }
    else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight)
    {
        if([comments isEqualToString:@"yes"])
        {
            // [self textResizeForButton];
            _btnComments.hidden = NO;
            
            
        }
        else
        {
            _btnComments.hidden = YES;
        }
    }
    else
    {
        _btnComments.hidden = YES;
    }
}

- (IBAction)btnCommentsAction:(id)sender
{
    _commentDetailHeader.text = [dictDetails objectForKey:@"title"];
    
    [self commentsListWS];
    
    [self.view addSubview:_commentDetailVw];
    
}

- (IBAction)backCommentsDetail:(id)sender
{
    [_commentDetailVw removeFromSuperview];
}

- (IBAction)commentSubmit:(id)sender
{
    [self.view endEditing:YES];
    [self commentWs];
}

-(void)commentsListWS
{
    //http://listlinkz.com/ssc/webservice/read_comments.php?article_id=2
    
    NSString *wUrl=[NSString stringWithFormat:@"%@read_comments.php?article_id=%@",baseUrl,[dictDetails objectForKey:@"id"]];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self commentsListWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cArray = [[NSArray alloc]init];
            cArray = [json objectForKey:@"comments"];
            
            if([cArray count]>0)
            {
                if([[cArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[cArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    cArray = [[NSArray alloc]init];
                    
                    [_commentsTblVw reloadData];
                    [SVProgressHUD dismiss];
                    return ;
                }
            }
            
            [_commentsTblVw reloadData];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
}

-(void)commentWs
{
    //http://listlinkz.com/ssc/webservice/write_comment.php?article_id=1&user_id=3&comment=test-comment
    [SVProgressHUD show];
    
    NSString *commentTf;
    
    commentTf = [_commentTxtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    if(commentTf.length > 0)
    {
        
    }
    else
    {
        [self.view makeToast:@"Enter Your Comment"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        
        [SVProgressHUD dismiss];
        return ;
    }
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSString *wUrl=[NSString stringWithFormat:@"%@write_comment.php?article_id=%@&user_id=%@&comment=%@",baseUrl,[dictDetails objectForKey:@"id"],userid,commentTf];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self commentWs];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *mArray = [json objectForKey:@"comment"];
            
            if([mArray count]>0)
            {
                if([[mArray objectAtIndex:0]objectForKey:@"success"])
                {
                    [self.view makeToast:@"Your comment is submitted and pending approval. It will be displayed here once approved."
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    _commentTxtVw.text = @"";
                    [SVProgressHUD dismiss];
                    return ;
                }
                
            }
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
    
}


@end
