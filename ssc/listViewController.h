//
//  listViewController.h
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;
@interface listViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollVw;
@property (strong, nonatomic) IBOutlet UILabel *scrollVwHdrLbl;
@property (strong, nonatomic) IBOutlet UITableView *listTblVw;
@property (strong, nonatomic) IBOutlet GADBannerView *adVw;
@property (strong, nonatomic) IBOutlet UIImageView *scrollRightArrowImg;
@property (strong, nonatomic) IBOutlet UIImageView *scrollLeftArrowImg;
@property (strong, nonatomic) IBOutlet UILabel *userInfoLbl;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;

@property (readonly, retain) NSMutableDictionary *arrays;

-(void)getDictionary:(NSDictionary *) dictHere;

@end
