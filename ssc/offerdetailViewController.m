//
//  detailViewController.m
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "offerdetailViewController.h"
#import "Constants.h"
#import "SVProgressHUD.h"
#import "UIImageView+JMImageCache.h"
#import <QuartzCore/QuartzCore.h>
#import "YTPlayerView.h"
#import "UIView+Toast.h"
#import "searchViewController.h"
#import "cTableViewCell.h"

#import "popUpVideoViewController.h"
#import "articleListViewController.h"

#import "loginViewController.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import "UIButton+WebCache.h"
#import "UIImageView+WebCache.h"

#import "homeViewController.h"
#import "interViewController.h"

@interface offerdetailViewController ()<UIWebViewDelegate>
{
    NSMutableDictionary *dictDetails;
    NSString *strID;
     UIImageView *imageVw;
    
    BOOL isbkmd;
    NSArray *arrUser;
    
    NSInteger currentSearchIndex;
    
    NSInteger currentMoreIndex;
    
    NSArray *cArray;
    
    NSMutableArray *mutArr;
    
    NSInteger indexe;
    
    NSMutableArray *gArray;
    
    int index;
    
    UIButton *customAdBanner;
}
@end

@implementation offerdetailViewController
@synthesize isFrmMore,isFrmSearch,isfrmBkMore,isFrmCSearch;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    NSString *userid;
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        userid = @"";
    }
    else
    {
        userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    }
    
     self.commentsTblVw.tableFooterView = [UIView new];
    
    currentSearchIndex = 0; currentMoreIndex = 0;
    
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"searchIndexPsth"])
    currentSearchIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"searchIndexPsth"];
    
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"moreArrayId"])
        currentMoreIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"moreArrayId"];
    
    
    
     _readMoreVw.frame = [UIScreen mainScreen].bounds;
    
    _commentDetailVw.frame = [UIScreen mainScreen].bounds;
    
    
  /*  gArray = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
        gArray = [plistDict objectForKey:@"articleBookmark"];
        
        if ([gArray count] == 0)
        {
            
        }
    }
    */
    gArray = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        gArray = [plistDict objectForKey:@"articleBookmark"];
        
    }
    
    NSString *admobId; NSString *interstitial; NSString *adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
    
    interstitial = [[arrSet objectAtIndex:0]valueForKey:@"admob_inter_id"];
    
    adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        
        [self.adVw loadRequest:[GADRequest request]];
    }
    else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
       // [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        //[self.view addSubview:customAdBanner];
        
        
    }
    
    //interstitial
    self.interstitial = [[GADInterstitial alloc]
                         initWithAdUnitID:interstitial];
    GADRequest *request = [GADRequest request];
    [self.interstitial loadRequest:request];
    
    
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerRight:)];
    [rightSwipe setDirection:(UISwipeGestureRecognizerDirectionRight)];
    //[self.view addGestureRecognizer:rightSwipe];
    
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandlerLeft:)];
    [leftSwipe setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    //[self.view addGestureRecognizer:leftSwipe];
    
    NSString *catName;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"catName"])
    catName =[[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
    
    if(isFrmMore)
    {
         _headerLbl.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"articleName"];
    }
    else
     _headerLbl.text = catName;
    //[[NSUserDefaults standardUserDefaults]objectForKey:@"HomeClickedCellName"];
    
    
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"detsilDidSelIndex"])
    {
       indexe = [[NSUserDefaults standardUserDefaults]integerForKey:@"detsilDidSelIndex"];
    }
    
    if(mutArr.count>0)
    {
        if(isFrmCSearch)
        {
            NSString *htmlString;// = @"<h1>Header</h1><h2>Subheader</h2><p>Some <em>text</em></p><img src='http://blogs.babble.com/famecrawler/files/2010/11/mickey_mouse-1097.jpg' width=70 height=100 />";
            NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                    initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                    options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                    documentAttributes: nil
                                                    error: nil
                                                    ];
           // textView.attributedText = attributedString;
            
          _txtVw.text = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"text"]];
        }
        else
        {
            NSString *htmlString;
            
            if([mutArr valueForKey:@"description"])
            htmlString = [NSString stringWithFormat:@"%@",[mutArr valueForKey:@"description"]];
            else if([mutArr valueForKey:@"text"])
                htmlString = [NSString stringWithFormat:@"%@",[mutArr valueForKey:@"text"]];
            
            NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                    initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                    options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                    documentAttributes: nil
                                                    error: nil
                                                    ];
           //  _txtVw.attributedText = attributedString;
            
            
            
        _txtVw.text = [NSString stringWithFormat:@"%@",htmlString];
            
            
            //NSString * htmlString = [dictDetails objectForKey:@"description"];
            htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
            
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            _txtVw.attributedText = attrStr;
            
            [_txtVw reloadInputViews];
        }
    }
    
    
    [_imgVwHeader sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[mutArr valueForKey:@"image"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    [_brandImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[mutArr valueForKey:@"brand_logo"]]] placeholderImage:[UIImage imageNamed:@"placeholder_empowerji_b"]];
    
    NSString *title = [NSString stringWithFormat:@"   %@",[mutArr valueForKey:@"title"]];
    
   
    
    NSString *lang = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    NSString *category = [languageBundle localizedStringForKey:@"category" value:@"" table:@"strings-english"];
    
    
    NSString *subTitle = [NSString stringWithFormat:@"  %@: %@",category,[mutArr valueForKey:@"category"]];
    
    _headerLbl2.text = title;
    
    _subHedrLbl.text = subTitle;
    
    NSString *header = [[NSUserDefaults standardUserDefaults]objectForKey:@"articlelistHeader"];
    _headerLbl.text = header;
    
    if(mutArr.count>0)
        _scrollArrowLbl.text = [NSString stringWithFormat:@"%@",[mutArr valueForKey:@"title"]];
    
    if(mutArr.count>0)
    {
        _titleLbl.text = [NSString stringWithFormat:@"%@",[mutArr valueForKey:@"title"]];
        
        NSString *langg = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:langg ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *by = [languageBundle localizedStringForKey:@"by" value:@"" table:@"strings-english"];
        
    }
    
    [_btnAvailOffer addTarget:self action:@selector(availOffer) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *theUrl;
    theUrl = [mutArr valueForKey:@"offer_url"];
    
    if([theUrl caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _btnAvailOffer.hidden = YES;
        
        
    }
    
    UIImage *originalImage = _imgVwHeader.image;
    double width = originalImage.size.width;
    double height = originalImage.size.height;
    double apect = width/height;
    
    double nHeight = 320.f/ apect;
    _imgVwHeader.frame = CGRectMake(_imgVwHeader.frame.origin.x , _imgVwHeader.frame.origin.y , _imgVwHeader.frame.size.width, nHeight);
    //_imgVwHeader.center = self.view.center;
    _imgVwHeader.image = originalImage;
    
    CGRect txtFrame = _txtVw.frame;
    CGRect hLblFrame = _headerLbl2.frame;
    CGRect shLblFrame = _subHedrLbl.frame;
    CGRect logoFrame = _brandImg.frame;
    
    hLblFrame.origin.y = _imgVwHeader.frame.origin.y + _imgVwHeader.frame.size.height;
    shLblFrame.origin.y = hLblFrame.origin.y + hLblFrame.size.height;
    logoFrame.origin.y = shLblFrame.origin.y;
    txtFrame.origin.y = shLblFrame.origin.y + shLblFrame.size.height;
    txtFrame.size.height = _refLbl.frame.origin.y - txtFrame.origin.y;
    
   _headerLbl2.frame = hLblFrame;
   _subHedrLbl.frame = shLblFrame;
    _brandImg.frame = logoFrame;
    _txtVw.frame = txtFrame;
    
    _btnAvailOffer.layer.cornerRadius = 5.0;
    _btnAvailOffer.layer.masksToBounds = YES;
    
    
    ////////
   // NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
   // NSData *data = [def objectForKey:@"settings"];
   // NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
  //  NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *strCount = [[arrSet objectAtIndex:0]valueForKey:@"popup_interval"];
    
    NSString *admobinter = [[arrSet objectAtIndex:0]valueForKey:@"admob_inter"];
    
    NSLog(@"%d",count % [strCount intValue]);
    
    if(count != 0 && count % [strCount intValue] == 0)
    {
        if([admobinter isEqualToString:@"Yes"])
        {
            
            if (self.interstitial.isReady) {
                [self.interstitial presentFromRootViewController:self];
            }
        }
        else if([admobinter caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
        {
            [self showInter];
        }
    }
    count++;
    
    
    
    if(arrSet.count > 0)
    {
        // "custom_banner_arr"
        
        //prefetch advw images and interstitial
        
        NSMutableArray *custom_banner_arr = [[NSMutableArray alloc]init];
        
        
        NSArray *custom_banner_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_banner_arr"];
        
        for(NSString *arr in custom_banner_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_banner"];
            [custom_banner_arr addObject:url];
        }
        
        // uint32_t rnd = arc4random_uniform([custom_banner_arr count]);
        
        index = arc4random_uniform(custom_banner_arr2.count);
        
        NSString *randomObject = [custom_banner_arr objectAtIndex:index];
        
        NSLog(@"rand:%u, rnd:%@",index,randomObject);
        
        NSLog(@"custom_banner url :%@",randomObject);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject forKey:@"custom_banner_url"];
        
        [customAdBanner sd_setImageWithURL:[NSURL URLWithString:randomObject] forState:UIControlStateNormal];
        
        /////
        NSMutableArray *custom_inter_arr = [[NSMutableArray alloc]init];
        
        NSArray *custom_inter_arr2 = [[arrSet objectAtIndex:0]valueForKey:@"custom_inter_arr"];
        
        for(NSString *arr in custom_inter_arr2)
        {
            NSString *url = [arr valueForKey:@"custom_inter"];
            [custom_inter_arr addObject:url];
        }
        
        // uint32_t rnd2 = arc4random_uniform([custom_inter_arr count]);
        
        // NSUInteger randomIndex = arc4random() % custom_inter_arr.count;
        
        //  index = arc4random_uniform(custom_inter_arr2.count);
        
        
        NSString *randomObject2 = [custom_inter_arr objectAtIndex:index];
        
        NSLog(@"custom_inter_arr url :%@",randomObject2);
        
        [[NSUserDefaults standardUserDefaults]setObject:randomObject2 forKey:@"custom_inter"];
        
        
        /////
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
     _readMoreBtn.layer.cornerRadius = _readMoreBtn.frame.size.height/2;
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    
    
    
    NSString *admobId,*adStatus;
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    if(arrSet.count >0)
    {
        admobId = [[arrSet objectAtIndex:0]valueForKey:@"admob_id"];
        
        adStatus = [[arrSet objectAtIndex:0]valueForKey:@"admob_status"];
    }
    
    _adVw.adSize = kGADAdSizeSmartBannerPortrait;
    self.adVw.adUnitID = admobId;
    self.adVw.rootViewController = self;
    
    if([adStatus isEqualToString:@"Yes"])
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        [self.adVw loadRequest:[GADRequest request]];
    }
  /*  else if([adStatus caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
    {
        CGRect frameH = _helpImg.frame;
        frameH.origin.y = _adVw.frame.origin.y - frameH.size.height;
        _helpImg.frame = frameH;
        
        CGRect frameB = _helpBtn.frame;
        frameB.origin.y = frameH.origin.y; //_adVw.frame.origin.y - frameB.size.height;
        _helpBtn.frame = frameB;
        
        [self.helpLbl setCenter:_helpBtn.center];
        
        //[self.adVw loadRequest:[GADRequest request]];
        
        NSString *custom_banner = [NSString stringWithFormat:@"%@",[[arrSet objectAtIndex:0]valueForKey:@"custom_banner"]];
        
        
       // UIButton *customAdBanner = [[UIButton alloc]initWithFrame:_adVw.frame];
        
        [customAdBanner addTarget:self action:@selector(customAdAction) forControlEvents:UIControlEventTouchUpInside];
        
        
     //   [customAdBanner sd_setImageWithURL:[NSURL URLWithString:custom_banner] forState:UIControlStateNormal];
        
        
        [self.view addSubview:customAdBanner];
        
    }
    */
    
    CGRect frame = _txtVw.frame;
    frame.origin.y = _titleLbl.frame.origin.y + _titleLbl.frame.size.height ;
    frame.size.height  = (frame.size.height - (_titleLbl.frame.origin.y));
   // _txtVw.frame = frame;
    
    CGRect btnFrame = _btnAvailOffer.frame;
    btnFrame.origin.y = _txtVw.frame.origin.y + _txtVw.contentSize.height + 10;
    _btnAvailOffer.frame = btnFrame;
    
    int hgt = [UIScreen mainScreen].bounds.size.height ;
    int bf = (btnFrame.size.height + btnFrame.origin.y);
    
    NSLog(@"%d %d",hgt,bf);
    
    if(hgt < bf)
    {
       // btnFrame.origin.y = [UIScreen mainScreen].bounds.size.height - btnFrame.size.height;
       // _btnAvailOffer.frame = btnFrame;
        
        NSLog(@"ythis");
    }
}

-(void)getDictionaryDetailid:(NSString *) idHere :(NSArray *) mutta
{
    strID = idHere;
    
    NSUserDefaults *defuser = [NSUserDefaults standardUserDefaults];
    NSData *datauser = [defuser objectForKey:@"userData"];
    NSArray *retrievedDictionaryuser = [NSKeyedUnarchiver unarchiveObjectWithData:datauser];
    
    arrUser = [[NSArray alloc] initWithArray:retrievedDictionaryuser];
    
    mutArr = [[NSArray alloc]init];
    mutArr = [mutta copy];
    
    
   // [self detailWebservice];
}

# pragma mark WebService Calls
static int count = 1;

-(void)detailWebservice
{
    [SVProgressHUD show];
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *strCount = [[arrSet objectAtIndex:0]valueForKey:@"popup_interval"];
    
    NSString *admobinter = [[arrSet objectAtIndex:0]valueForKey:@"admob_inter"];
    
    NSLog(@"%d",count % [strCount intValue]);
    
    if(count != 0 && count % [strCount intValue] == 0)
    {
        if([admobinter isEqualToString:@"Yes"])
        {
            
            if (self.interstitial.isReady) {
                [self.interstitial presentFromRootViewController:self];
            }
        }
        else if([admobinter caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
        {
            [self showInter];
        }
    }
    count++;
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&user_id=%@&lang_id=%ld",baseUrl,strID,userid,langidd];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&lang_id=%ld",baseUrl,strID,langidd];
    }
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
             [self detailWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
             dictDetails = [[json objectForKey:@"article"]objectAtIndex:0];
            
            
            _scrollArrowLbl.text = [dictDetails objectForKey:@"title"];
            
            _titleLbl.text = [dictDetails objectForKey:@"title"];
            
            _dateLbl.text = [dictDetails objectForKey:@"date"];
            
            NSString * htmlString = [dictDetails objectForKey:@"description"];
            htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
            
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            
            _txtVw.attributedText = attrStr;
            
            ////
            //////
            [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
            
          /*  NSMutableArray *arrRate = [[NSMutableArray alloc]init];
            NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            NSFileManager * fileManager = [NSFileManager defaultManager];
            //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                arrRate = [plistDict objectForKey:@"rateArray"];
            }
            
            if ([arrRate count] == 0) {
                isbkmd = NO;
            }
            
            for (NSDictionary *dic in arrRate) {
                
                @try
                {
                    
                    NSLog(@"%@  == %@",[dictDetails objectForKey:@"id"],[dic objectForKey:@"id"]);
                    
                    //crashes here
                    if ([[dictDetails objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]] && ([[dic objectForKey:@"userid"] isEqualToString:[[arrUser objectAtIndex:0]valueForKey:@"user_id"]]))
                    {
                        
                        isbkmd = YES;
                        
                        break;
                    }
                    else {
                        isbkmd = NO;
                    }
                    
                } @catch (NSException *exception)
                {
                    
                }
            }
           
           */
            
             if ([[dictDetails objectForKey:@"bookmark"] isEqualToString:@"yes"])
             {
                
                isbkmd = YES;
                
                [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateNormal];
                //change bookmark button image here
                
                
            }
            else {
                
                isbkmd = NO;
                [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
                //change bookmark image here
                
            }
            
            //////
            [imageVw removeFromSuperview];
            
           NSString *videoid;NSString *ylink;NSString *image; NSString *extrnLink; NSString *comments;
            
            comments = [dictDetails objectForKey:@"comments"];
            
            if([comments isEqualToString:@"yes"])
            {
               // [self textResizeForButton];
                //_btnComments.hidden = NO;
                
                
               NSRange range = [self visibleRangeOfTextView:_txtVw];
                
                NSLog(@"range :%@",NSStringFromRange(range));
                
               // _txtVw.contentInset = UIEdgeInsetsMake(0, 0, _btnComments.frame.size.height, 0);// top, left, bottom, right
                
                 _txtVw.textContainerInset = UIEdgeInsetsMake(0, 0, _btnComments.frame.size.height+50, 0);
                
                _btnComments.hidden = NO;
            }
            else
            {
                _btnComments.hidden = YES;
            }
            
            
           ylink = [dictDetails objectForKey:@"youtube_link"];
            image = [dictDetails objectForKey:@"image"];
            
            //external_link
            extrnLink = [dictDetails objectForKey:@"external_link"];
            
            if([extrnLink isEqualToString:@""])
            {
                _readMoreBtn.hidden=YES;
                _rmBackgroungLbl.hidden=YES;
            }
            NSURL *url = [NSURL URLWithString:extrnLink];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [self.webVw loadRequest:request];
            
            
            if([ylink isEqualToString:@""])
            {
                [SVProgressHUD dismiss];
                
                imageVw = [[UIImageView alloc]init];
                imageVw.frame = _playerVw.frame;
                [imageVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:nil];
                
                [self.view addSubview:imageVw];
                return ;
            }
            
            NSString *newString1 = [ylink stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
            
            videoid = newString1;
            
                NSDictionary *playerVars = @{
                                             @"origin" :@"http://www.youtube.com",
                                             @"playsinline" : @1,
                                             @"autoplay" : @1,
                                             @"showinfo" : @0,
                                             @"rel" : @0,
                                             @"fs" : @1,
                                             @"modestbranding" : @1,
                                             };
                
                
                
                [_playerVw loadWithVideoId:videoid playerVars:playerVars];
                
                NSLog(@"playerView frame :%@",NSStringFromCGRect(self.playerVw.frame));
                
           
            
            
            [SVProgressHUD dismiss];
            
          
            
        });
    }];
    
    [dataTask resume];
    
}


-(void)nextPredetailWebservice :(NSInteger)catid :(NSString*)action
{
    //http://listlinkz.com/ssc/webservice/article_detail.php?article_id=1720&cat_id=12&action=next
    
    [SVProgressHUD show];
    //count ++;
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *strCount = [[arrSet objectAtIndex:0]valueForKey:@"popup_interval"];
    
    NSString *admobinter = [[arrSet objectAtIndex:0]valueForKey:@"admob_inter"];
    
    NSLog(@"%d",count % [strCount intValue]);
    
    if(count != 0 && count % [strCount intValue] == 0)
    {
        if([admobinter isEqualToString:@"Yes"])
        {
            
            if (self.interstitial.isReady) {
                [self.interstitial presentFromRootViewController:self];
            }
        }
        else if([admobinter caseInsensitiveCompare:@"Custom"] == NSOrderedSame)
        {
            [self showInter];
        }
    }
    count++;
    
    //NSString *action; NSString *catid;
    
    catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"listid"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&user_id=%@&cat_id=%ld&action=%@",baseUrl,strID,userid,(long)catid,action];
    
    NSLog(@"URL :%@", wUrl);
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
      wUrl=[NSString stringWithFormat:@"%@article_detail.php?article_id=%@&cat_id=%ld&action=%@",baseUrl,strID,(long)catid,action];
    }
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            // [self detailWebservice:nil];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           // if([_listArray count]>0)
            {
                if([[[json objectForKey:@"article"]objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[[json objectForKey:@"article"]objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    [SVProgressHUD dismiss];
                    return ;
                }
            }
            
            dictDetails = [[json objectForKey:@"article"]objectAtIndex:0];
            
            
            strID = [dictDetails objectForKey:@"id"];
            
            _scrollArrowLbl.text = [dictDetails objectForKey:@"title"];
            
             _titleLbl.text = [dictDetails objectForKey:@"title"];
            
            NSString * htmlString = [dictDetails objectForKey:@"description"];
            htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
            htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
            
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            
            _txtVw.attributedText = attrStr;
            
            ////
            
            //////
            [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
            
         /*   NSMutableArray *arrRate = [[NSMutableArray alloc]init];
            NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
            NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
            NSFileManager * fileManager = [NSFileManager defaultManager];
            //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
            if([fileManager fileExistsAtPath:finalPath])
            {
                NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
                arrRate = [plistDict objectForKey:@"rateArray"];
            }
            
            if ([arrRate count] == 0) {
                isbkmd = NO;
            }
            
            for (NSDictionary *dic in arrRate) {
                
                @try
                {
                    
                    NSLog(@"%@  == %@",[dictDetails objectForKey:@"id"],[dic objectForKey:@"id"]);
                    
                    //crashes here
                    if ([[dictDetails objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]] && ([[dic objectForKey:@"userid"] isEqualToString:[[arrUser objectAtIndex:0]valueForKey:@"user_id"]]))
                    {
                        
                        isbkmd = YES;
                        
                        break;
                    }
                    else {
                        isbkmd = NO;
                    }
                    
                } @catch (NSException *exception)
                {
                    
                }
            }
          
          */
            
        if ([[dictDetails objectForKey:@"bookmark"] isEqualToString:@"yes"])
            {
                
                isbkmd = YES;
                
                [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateNormal];
                //change bookmark button image here
                
                
            }
            else {
                
                isbkmd = NO;
                [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
                //change bookmark image here
                
            }
            
            //////
            [imageVw removeFromSuperview];
            
            NSString *videoid;NSString *ylink;NSString *image;NSString *extrnLink;
            
            ylink = [dictDetails objectForKey:@"youtube_link"];
            image = [dictDetails objectForKey:@"image"];
            
            
            extrnLink = [dictDetails objectForKey:@"external_link"];
            
            if([extrnLink isEqualToString:@""])
            {
                _readMoreBtn.hidden=YES;
                _rmBackgroungLbl.hidden=YES;
            }
            else
            {
                _readMoreBtn.hidden=NO;
                _rmBackgroungLbl.hidden=NO;
            }
            NSURL *url = [NSURL URLWithString:extrnLink];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [self.webVw loadRequest:request];
            
            
            
            if([ylink isEqualToString:@""])
            {
                [SVProgressHUD dismiss];
                
                imageVw = [[UIImageView alloc]init];
                imageVw.frame = _playerVw.frame;
                [imageVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:nil];
                
                [self.view addSubview:imageVw];
                return ;
            }
            
            NSString *newString1 = [ylink stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
            
            videoid = newString1;
            
            NSDictionary *playerVars = @{
                                         @"origin" :@"http://www.youtube.com",
                                         @"playsinline" : @1,
                                         @"autoplay" : @1,
                                         @"showinfo" : @0,
                                         @"rel" : @0,
                                         @"fs" : @1,
                                         @"modestbranding" : @1,
                                         };
            
            
            
            [_playerVw loadWithVideoId:videoid playerVars:playerVars];
            
            NSLog(@"playerView frame :%@",NSStringFromCGRect(self.playerVw.frame));
            
           
            
            [SVProgressHUD dismiss];
        });
    }];
    
    [dataTask resume];
    
}

-(void)updatebookmarkWS:(NSString *)articleid
{
    //bookmarks are now from server rather than local
    
    //http://listlinkz.com/ssc/webservice/update_bookmark.php?user_id=3&article_id=1
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    NSString *wUrl=[NSString stringWithFormat:@"%@update_bookmark.php?user_id=%@&article_id=%@",baseUrl,userid,articleid];
    
    NSLog(@"URL :%@", wUrl);
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
      wUrl=[NSString stringWithFormat:@"%@update_bookmark.php?article_id=%@",baseUrl,articleid];
    }
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
     {
         NSDictionary *json;
         
         if(data ==nil)
         {
             // [self updatebookmarkWS];
             return ;
         }
         
         json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
         NSLog(@"json :%@", json);
         
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             
             
             
         });
     }];
    
    [dataTask resume];
    
}

-(void)favWS:(NSString*) gid :(NSString*) isSel
{
    //add_likes.php?id=4
    
    // action=yes/no
    
    NSString *wUrl=[NSString stringWithFormat:@"%@add_likes.php?id=%@&action=%@",baseUrl,gid,isSel];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            //[self favWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //[self listMWebservice];
            NSString *result = [[[json valueForKey:@"result"]objectAtIndex:0]valueForKey:@"likes"];
            
            mutArr = [mutArr mutableCopy];
            //[[[mutArr replaceObjectAtIndex:indexe]objectForKey:@"likes"] withObject:result];
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict = (NSDictionary *)[mutArr objectAtIndex:indexe];
            [newDict addEntriesFromDictionary:oldDict];
            [newDict setObject:result forKey:@"likes"];
            [mutArr replaceObjectAtIndex:indexe withObject:newDict];
            
            _countLbl.text = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"likes"]];
        });
    }];
    
    [dataTask resume];
}

-(void)clicksWS :(NSString *)type :(NSString *)cid
{
    
    //app.empowerji.com/webservice4/clicks.php?id=5&type=offer
    
    //e
    /*
     type=banner
     type=inter
     type=offer
     */
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    int r = arc4random_uniform(10000);
    
    
    NSString *wUrl=[NSString stringWithFormat:@"%@clicks.php?id=%@&type=%@",baseUrl,cid,type];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        
        NSDictionary *json;
        
        if(data ==nil)
        {
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
        });
    }];
    
    [dataTask resume];
}


# pragma mark Swipe Gesture Delegate

-(void)swipeHandlerRight:(id)sender
{
    [self btnPrevActn:nil];
}

-(void)swipeHandlerLeft:(id)sender
{
    [self btnNxtActn:nil];
}


# pragma mark YTPlayer Delegates


-(void)playerViewDidBecomeReady:(YTPlayerView *)playerView{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Playback started" object:self];
    [self.playerVw playVideo];
}
- (IBAction)playVideo:(id)sender {
    [self.playerVw playVideo];
}

- (IBAction)stopVideo:(id)sender {
    [self.playerVw stopVideo];
}



# pragma mark button actions

- (IBAction)onBack:(id)sender
{
    [imageVw removeFromSuperview];
    //isfrmDetail
    
    [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"isfrmDetail"];
    
    articleListViewController *listVC = [[articleListViewController alloc]initWithNibName:@"articleListViewController" bundle:nil];
    listVC.isfrmDetail = YES;
   // [self.navigationController pushViewController:listVC animated:YES];
    //[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onSearch:(id)sender
{
    searchViewController *seVC = [[searchViewController alloc]initWithNibName:@"searchViewController" bundle:nil];
    [self.navigationController pushViewController:seVC animated:YES];
}
- (IBAction)btnPrevActn:(id)sender
{
    NSString *action;
    action = @"previous";
    
    if(indexe == 0)
        indexe = mutArr.count-1;
    else
        indexe--;
    
    if(mutArr.count>0)
    {
        if(isFrmCSearch)
        {
         _txtVw.text = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"text"]];
        }
       // else
       // _txtVw.text = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"article"]];
        
        
        NSString *htmlString;
        
        if([[mutArr objectAtIndex:indexe]valueForKey:@"article"])
            htmlString = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"article"]];
        else if([[mutArr objectAtIndex:indexe]valueForKey:@"text"])
            htmlString = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"text"]];
        
        htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
        htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
        
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        
        _txtVw.attributedText = attrStr;
        
    }
    
    NSString *langg = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:langg ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    NSString *by = [languageBundle localizedStringForKey:@"by" value:@"" table:@"strings-english"];
    
    
    NSString *authr;
    if([[mutArr objectAtIndex:indexe]valueForKey:@"submitted_by"])
        authr = [[mutArr objectAtIndex:indexe]valueForKey:@"submitted_by"];
    
    
    
    _titleLbl.text = [NSString stringWithFormat:@"%@ \n %@",[[mutArr objectAtIndex:indexe]valueForKey:@"title"],[NSString stringWithFormat:@"%@ %@",by,authr]];
    
    //_titleLbl.text = [NSString stringWithFormat:@"%@ %@",by,authr];
    
    [_titleLbl setText:[_titleLbl.text uppercaseString]];
    
    _scrollArrowLbl.text = [[mutArr objectAtIndex:indexe]valueForKey:@"title"];
    
    
    //to hide the arrows
    if(indexe == 0)
    {
        _prevImg.hidden = YES;
        
        _btnPrev.hidden = YES;
    }
    else
    {
        _prevImg.hidden = NO;
        
        _btnPrev.hidden = NO;
    }
    
    if(indexe >= mutArr.count-1)
    {
        _nextImg.hidden = YES;
        
        _btnNext.hidden = YES;
    }
    else
    {
        _nextImg.hidden = NO;
        
        _btnNext.hidden = NO;
    }
    
    if([gArray containsObject:[[mutArr objectAtIndex:indexe]valueForKey:@"id"]])
    {
        [_favBtn setImage:[UIImage imageNamed:@"ico_star_selected_white"] forState:UIControlStateNormal];
    }
    else
    {
        [_favBtn setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
    }
    // _countLbl;
    
    _countLbl.text = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"likes"]];
    
    return;
    
    if(isFrmSearch)
    {
       // currentSearchIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"searchIndexPsth"];
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"searchArray"];
            
        }
        
       
        
        if(currentSearchIndex <= 0)
        {
            //currentSearchIndex = 0;
            
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
        }
        
         currentSearchIndex = currentSearchIndex-1;
        
       // [self getDictionaryDetailid:[[arrRate objectAtIndex:currentSearchIndex]valueForKey:@"id"]];
    }
    else if(isFrmMore)
    {
        
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"moreArray"];
            
        }
        
        
        
        if(currentMoreIndex <= 0)
        {
            //currentMoreIndex = 0;
            
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
        }
        currentMoreIndex = currentMoreIndex-1;
        
        //[self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
    }
    else if(isfrmBkMore)
    {
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"rateArray"];
            
        }
        
        
        
        if(currentMoreIndex <= 0)
        {
            //currentMoreIndex = 0;
            
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
        }
        currentMoreIndex = currentMoreIndex-1;
        
       // [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
    }
    else
    {
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"detailListArray"];
            
        }
        
        
        
        if(currentMoreIndex <= 0)
        {
            //currentMoreIndex = 0;
            
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
        }
        currentMoreIndex = currentMoreIndex-1;
        
       // [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
        
      //NSInteger i=1;
      //[self nextPredetailWebservice:i:action];
    }
}
- (IBAction)btnNxtActn:(id)sender
{
    NSString *action;
    action = @"next";
    
    if(indexe >= mutArr.count-1)
        indexe = 0;
    else
     indexe++;
    
    if(mutArr.count>0)
    {
        if(isFrmCSearch)
        {
         _txtVw.text = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"text"]];
        }
       // else
       // _txtVw.text = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"article"]];
        
        
        NSString *htmlString;
        
        if([[mutArr objectAtIndex:indexe]valueForKey:@"article"])
            htmlString = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"article"]];
        else if([[mutArr objectAtIndex:indexe]valueForKey:@"text"])
            htmlString = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"text"]];
        
        htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
        htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
        
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        
        _txtVw.attributedText = attrStr;
    }
    
    _scrollArrowLbl.text = [[mutArr objectAtIndex:indexe]valueForKey:@"title"];
    
    NSString *langg = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:langg ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    NSString *by = [languageBundle localizedStringForKey:@"by" value:@"" table:@"strings-english"];
    
    
    NSString *authr;
    if([[mutArr objectAtIndex:indexe]valueForKey:@"submitted_by"])
        authr = [[mutArr objectAtIndex:indexe]valueForKey:@"submitted_by"];
    
    
    
    _titleLbl.text = [NSString stringWithFormat:@"%@ \n %@",[[mutArr objectAtIndex:indexe]valueForKey:@"title"],[NSString stringWithFormat:@"%@ %@",by,authr]];
    
    //_titleLbl.text = [NSString stringWithFormat:@"%@ %@",by,authr];
    
    [_titleLbl setText:[_titleLbl.text uppercaseString]];
    
    
    //to hide the arrows
    if(indexe == 0)
    {
        _prevImg.hidden = YES;
        
         _btnPrev.hidden = YES;
    }
    else
    {
        _prevImg.hidden = NO;
        
        _btnPrev.hidden = NO;
    }
    
    if(indexe >= mutArr.count-1)
    {
        _nextImg.hidden = YES;
        
         _btnNext.hidden = YES;
    }
    else
    {
        _nextImg.hidden = NO;
        
         _btnNext.hidden = NO;
    }
    
    
    if([gArray containsObject:[[mutArr objectAtIndex:indexe]valueForKey:@"id"]])
    {
        [_favBtn setImage:[UIImage imageNamed:@"ico_star_selected_white"] forState:UIControlStateNormal];
    }
    else
    {
        [_favBtn setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
    }
    // _countLbl;
    
    _countLbl.text = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe]valueForKey:@"likes"]];
    
    return;
    
    if(isFrmSearch)
    {
       
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"searchArray"];
            
        }
        
        
        
        
        if(currentSearchIndex >= ([arrRate count]-1))
        {
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
            //currentSearchIndex = 0;
        }
        currentSearchIndex = currentSearchIndex+1;
        
       // [self getDictionaryDetailid:[[arrRate objectAtIndex:currentSearchIndex]valueForKey:@"id"]];
        //[self playVideoD:currentSearchIndex];
        
        
    }
    else if(isFrmMore)
    {
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"moreArray"];
            
        }
        
        
        
        if(currentMoreIndex >= ([arrRate count]-1))
        {
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
            //currentMoreIndex = 0;
        }
        
        currentMoreIndex = currentMoreIndex+1;
        
       // [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
    }
    else if(isfrmBkMore)
    {
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"rateArray"];
            
        }
        
        
        
        if(currentMoreIndex >= ([arrRate count]-1))
        {
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
            //currentMoreIndex = 0;
        }
        
        currentMoreIndex = currentMoreIndex+1;
        
       // [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
    }
    else
    {
        
        ////////
        NSMutableArray *arrRate = [[NSMutableArray alloc]init];
        NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
        NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
        NSFileManager * fileManager = [NSFileManager defaultManager];
        //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
            arrRate = [plistDict objectForKey:@"detailListArray"];
            
        }
        
        
        
        if(currentMoreIndex >= ([arrRate count]-1))
        {
            [self.view makeToast:@"No more items in this category"
                        duration:3.0
                        position:CSToastPositionBottom];
            
            [SVProgressHUD dismiss];
            
            return;
            //currentMoreIndex = 0;
        }
        
        currentMoreIndex = currentMoreIndex+1;
        
       // [self getDictionaryDetailid:[[arrRate objectAtIndex:currentMoreIndex]valueForKey:@"id"]];
        
      //NSInteger i=0;
      //[self nextPredetailWebservice:i:action];
    }
}
- (IBAction)btnActionBookMark:(id)sender
{
    if([_btnBookMk.currentImage isEqual:[UIImage imageNamed:@"bookmark"]])
    {
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:(UIControlStateNormal)];
    }
    else
    {
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:(UIControlStateNormal)];
    }
    
     [self updatebookmarkWS:strID];
     
   /* NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
         arrRate = [plistDict objectForKey:@"rateArray"];
    }
    
    
   
    
    NSString *dict = [dictDetails objectForKey:@"id"];
    for (NSDictionary *dic in arrRate) {
        
        NSLog(@"%@",[dic objectForKey:@"id"]);
        NSLog(@"%@",dict);
        
        if ([dict isEqualToString:[dic objectForKey:@"id"]]&& ([[[arrUser objectAtIndex:0]valueForKey:@"user_id"] isEqualToString:[dic valueForKey:@"userid"]]))
        {
            isbkmd = YES;
            break;
        }
        else
        {
            isbkmd = NO;
        }
        
    }
    
    
    
    if (!isbkmd) {
        
        
        isbkmd = YES;
        
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
              arrRate = [plistDict valueForKey:@"rateArray"];
            if ([arrRate count] == 0)
            {
                arrRate = [NSMutableArray new];
                
                
         //       NSArray *keys = [dictDetails allKeys];
        //        NSArray * values = [dictDetails allValues];
//                NSArray * homeArray;
//
//                for(int i =0 ; i<[keys count] ; i++)
//                {
//                    homeArray = @[ @{[keys objectAtIndex:i]: [values objectAtIndex:i]} ];
//                }
                
                NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDetails];
                
                NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                [dictToSave removeObjectsForKeys:keysForNullValues];
                
                
                [dictToSave setObject:[[arrUser objectAtIndex:0]valueForKey:@"user_id"] forKey:@"userid"];
                
                NSInteger catid;
                catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"catTag"];
                
                [dictToSave setObject:[NSString stringWithFormat:@"%ld",(long)catid] forKey:@"catid"];
                
                [arrRate addObject:dictToSave];
                
               
                
            }
            else
            {
                NSMutableArray*arraySmpl;
                
                NSMutableDictionary *dictToSave = [NSMutableDictionary dictionaryWithDictionary:dictDetails];
                if ([arrRate isKindOfClass:[NSDictionary class]])
                {
                    NSMutableDictionary *dict2 = [NSMutableDictionary dictionary];
                    dict2= [dictToSave mutableCopy];
                    
                    [dictToSave setObject:dict2 forKey: @"test"];
                    
                    
                    NSMutableArray *ar = [[NSMutableArray alloc]init];
                    ar=[dictToSave objectForKey:@"test"];
                    
                    arraySmpl=[[NSMutableArray alloc]init];
                    [arraySmpl insertObject:ar atIndex:0];
                    arrRate = [arraySmpl mutableCopy];
                }
                else
                {
                   
                    
                   // [dictToSave setObject:[dictDetails objectForKey:@"id"] forKey:@"id"];
                    [dictToSave setObject:[[arrUser objectAtIndex:0]valueForKey:@"user_id"] forKey:@"userid"];
                    
                    NSInteger catid;
                    catid = [[NSUserDefaults standardUserDefaults]integerForKey:@"catTag"];
                    
                    [dictToSave setObject:[NSString stringWithFormat:@"%ld",(long)catid] forKey:@"catid"];
                    
                    NSArray *keysForNullValues = [dictToSave allKeysForObject:[NSNull null]];
                    [dictToSave removeObjectsForKeys:keysForNullValues];
                    
                    [arrRate addObject:dictToSave];
                    
                   
                    
                }
                
                // [arrRate insertObject:dictToSave atIndex:0];
            }
            
            ///to remove <null> values from dict
            
            NSMutableDictionary *dict = [arrRate mutableCopy];
            if ([arrRate isKindOfClass:[NSDictionary class]])
            {
                if (!([arrRate count] == 0))
                {
                    @try {
                        NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                        [dict removeObjectsForKeys:keysForNullValues];
                        arrRate = dict;
                        
                    } @catch (NSException *exception) {
                        NSMutableDictionary *dict = [[arrRate objectAtIndex:0] mutableCopy];
                        
                        NSArray *keysForNullValues = [dict allKeysForObject:[NSNull null]];
                        [dict removeObjectsForKeys:keysForNullValues];
                        
                        NSLog(@"dict :%@",dict);
                        
                        arrRate = dict;
                    }
                    
                }
            }
            ////
            
            [plistDict setValue:arrRate forKey:@"rateArray"];
            [plistDict writeToFile:finalPath atomically:YES];
            
        }
       
    }
    else
    {
        if([fileManager fileExistsAtPath:finalPath])
        {
            NSMutableDictionary *plistDict = [[[NSDictionary alloc] initWithContentsOfFile:finalPath] mutableCopy];
            
            int k =0;
            for (NSDictionary *dic in arrRate) {
                if ([dict isEqualToString:[dic objectForKey:@"id"]]&& ([[[arrUser objectAtIndex:0]valueForKey:@"user_id"] isEqualToString:[dic valueForKey:@"userid"]])) {
                    
                    [arrRate removeObjectAtIndex:k];
                    break;
                }
                k++;
            }
            [plistDict setValue:arrRate forKey:@"rateArray"];
            [plistDict writeToFile:finalPath atomically:YES];
        }
        
        
    }
    */
}
- (IBAction)btnActionShare:(id)sender
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    
    NSString *texttoshare =  [NSString stringWithFormat:@"%@  %@ %@",[dictDetails objectForKey:@"title"],[[arrSet objectAtIndex:0] valueForKey:@"article_share_text"],[[arrSet objectAtIndex:0] valueForKey:@"app_share_url"]];
    
    NSArray *activityItems = @[texttoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    if ([activityVC respondsToSelector:@selector(popoverPresentationController)])
    {
        // iOS 8+
        UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
        
        presentationController.sourceView = self.view; // if button or change to self.view.
    }
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

- (IBAction)readMoreActn:(id)sender
{
   [self.view addSubview:_readMoreVw];
}
- (IBAction)backRM:(id)sender
{
    [_readMoreVw removeFromSuperview];
}
- (IBAction)homeAction:(id)sender
{
   [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)playVideoD:(NSInteger)section
{
    NSMutableArray *arrSear = [[NSMutableArray alloc]init];
    NSArray * pathss = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePaths = ([pathss count] > 0) ? [pathss objectAtIndex:0] : nil;
    NSString * finalPaths = [basePaths stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManagers = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManagers fileExistsAtPath:finalPaths])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPaths];
        arrSear = [plistDict objectForKey:@"searchArray"];
        
    }
    
    dictDetails = [arrSear objectAtIndex:section];
    
    
    _scrollArrowLbl.text = [dictDetails objectForKey:@"title"];
    
    _titleLbl.text = [dictDetails objectForKey:@"title"];
    
    NSString * htmlString = [dictDetails objectForKey:@"description"];
    htmlString =[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    htmlString =[NSString stringWithFormat:@"<font face=\"verdana\" size=\"4\" align=\"left\">%@<font>",htmlString];
    
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    
    _txtVw.attributedText = attrStr;
    
    ////
    
    //////
    [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
    
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"rateArray"];
    }
    
    if ([arrRate count] == 0) {
        isbkmd = NO;
    }
    
    for (NSDictionary *dic in arrRate) {
        
        @try
        {
            
            NSLog(@"%@  == %@",[dictDetails objectForKey:@"id"],[dic objectForKey:@"id"]);
            
            //crashes here
            if ([[dictDetails objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]] && ([[dic objectForKey:@"userid"] isEqualToString:[[arrUser objectAtIndex:0]valueForKey:@"user_id"]]))
            {
                
                isbkmd = YES;
                
                break;
            }
            else {
                isbkmd = NO;
            }
            
        } @catch (NSException *exception)
        {
            
        }
    }
    
    if (isbkmd) {
        
        isbkmd = YES;
        
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmarked"] forState:UIControlStateNormal];
        //change bookmark button image here
        
        
    }
    else {
        
        isbkmd = NO;
        [_btnBookMk setImage:[UIImage imageNamed:@"bookmark"] forState:UIControlStateNormal];
        //change bookmark image here
        
    }
    
    [imageVw removeFromSuperview];
    
    NSString *videoid;NSString *ylink;NSString *image;
    
    ylink = [dictDetails objectForKey:@"youtube_link"];
    image = [dictDetails objectForKey:@"image"];
    
    if([ylink isEqualToString:@""])
    {
        
        
        imageVw = [[UIImageView alloc]init];
        imageVw.frame = _playerVw.frame;
        [imageVw setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",image]] placeholder:nil];
        
        [self.view addSubview:imageVw];
        return ;
    }
    
    NSString *newString1 = [ylink stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@""];
    
    videoid = newString1;
    
    NSDictionary *playerVars = @{
                                 @"origin" :@"http://www.youtube.com",
                                 @"playsinline" : @1,
                                 @"autoplay" : @1,
                                 @"showinfo" : @0,
                                 @"rel" : @0,
                                 @"fs" : @1,
                                 @"modestbranding" : @1,
                                 };
    
    
    
    [_playerVw loadWithVideoId:videoid playerVars:playerVars];
    
    NSLog(@"playerView frame :%@",NSStringFromCGRect(self.playerVw.frame));
    
    
    
}

-(void)helpAction
{
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
    
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
}

-(void)favAction:(UIButton*)sender
{
    //[sender setImage:[UIImage imageNamed:@"ico-star-selected"] forState:UIControlStateNormal];
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        // notLoginAlert
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"notLoginAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
                lgVC.isfrmHome = YES;
                
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                [rvc presentViewController: lgVC animated: NO completion:nil];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     
                                 }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
        // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        //[vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [rvc presentViewController: alert animated: NO completion:nil];
    }
    else
    {
  
    NSString *gid = [NSString stringWithFormat:@"%@",[[mutArr objectAtIndex:indexe] valueForKey:@"id"]];
    
    NSMutableArray *arrRate = [[NSMutableArray alloc]init];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
    NSMutableDictionary *plistDict;
    if([fileManager fileExistsAtPath:finalPath])
    {
        plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
        arrRate = [plistDict objectForKey:@"articleBookmark"];
        
        arrRate = [arrRate mutableCopy];
        
        if ([arrRate count] == 0)
        {
            arrRate = [NSMutableArray new];
            
            
            [arrRate addObject:gid];
        }
        else
        {
            if([arrRate containsObject:gid])
            {
                //[arrRate removeObjectIdenticalTo:gid];
                [arrRate removeObject: gid];
            }
            else
                [arrRate addObject:gid];
        }
    }
    [plistDict setValue:arrRate forKey:@"articleBookmark"];
    [plistDict writeToFile:finalPath atomically:YES];
    
    
    gArray = [[NSMutableArray alloc]init];
    
    NSString *isSeleted;
    
    if([sender.currentImage isEqual:[UIImage imageNamed:@"ico_star_selected_white"]])
    {
        isSeleted = @"no";
         [sender setImage:[UIImage imageNamed:@"ico-star"] forState:UIControlStateNormal];
        
    }
    else
    {
        isSeleted = @"yes";
        [sender setImage:[UIImage imageNamed:@"ico_star_selected_white"] forState:UIControlStateNormal];
    }
    
    if([fileManager fileExistsAtPath:finalPath])
    {
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
        gArray = [plistDict objectForKey:@"articleBookmark"];
        
    }
    
    [self favWS:gid :isSeleted];
    }
}

-(void)customAdAction
{
    homeViewController *hvc = [[homeViewController alloc]init];
    [hvc customAdAction];
    
   /* NSString *theUrl;
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"])
        theUrl = [[NSUserDefaults standardUserDefaults]objectForKey:@"custom_banner_url"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    */
    
}
-(void)showInter
{
    interViewController*loginViewController = [[interViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    navController.navigationBar.hidden = YES;
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)availOffer
{
    NSString *cid;
    cid = [NSString stringWithFormat:@"%@",[mutArr valueForKey:@"id"]];
    
    [self clicksWS:@"offer" :cid];
    
    NSString *theUrl;
    theUrl = [mutArr valueForKey:@"offer_url"];
    
    if(theUrl)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
}


#pragma mark change textview frame to fit a button
-(void)textResizeForButton{
    
    CGRect buttonFrame = self.btnComments.frame;
    UIBezierPath *exclusivePath = [UIBezierPath bezierPathWithRect:buttonFrame];
    self.txtVw.textContainer.exclusionPaths = @[exclusivePath];
    
}

#pragma mark scroll view delegate

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = scrollView.contentOffset.y;
    
    NSString *comments;
    
    comments = [dictDetails objectForKey:@"comments"];
    
    
    
    
    if (scrollOffset == 0)
    {
        // then we are at the top
    }
    else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight)
    {
        if([comments isEqualToString:@"yes"])
        {
           // [self textResizeForButton];
            _btnComments.hidden = NO;
            
            
        }
        else
        {
            _btnComments.hidden = YES;
        }
    }
    else
    {
        _btnComments.hidden = YES;
    }
}

- (IBAction)btnCommentsAction:(id)sender
{
    _commentDetailHeader.text = [dictDetails objectForKey:@"title"];
    
    [self commentsListWS];
    
    [self.view addSubview:_commentDetailVw];
    
}

- (IBAction)backCommentsDetail:(id)sender
{
    [_commentDetailVw removeFromSuperview];
}

- (IBAction)commentSubmit:(id)sender
{
    [self.view endEditing:YES];
    [self commentWs];
}

-(void)commentsListWS
{
    //http://listlinkz.com/ssc/webservice/read_comments.php?article_id=2
    
    NSString *wUrl=[NSString stringWithFormat:@"%@read_comments.php?article_id=%@",baseUrl,[dictDetails objectForKey:@"id"]];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self commentsListWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cArray = [[NSArray alloc]init];
            cArray = [json objectForKey:@"comments"];
            
            if([cArray count]>0)
            {
                if([[cArray objectAtIndex:0]objectForKey:@"message"])
                {
                    [self.view makeToast:[[cArray objectAtIndex:0]objectForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                    
                    cArray = [[NSArray alloc]init];
                    
                     [_commentsTblVw reloadData];
                    [SVProgressHUD dismiss];
                    return ;
                }
            }
            
            [_commentsTblVw reloadData];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
}

-(void)commentWs
{
    //http://listlinkz.com/ssc/webservice/write_comment.php?article_id=1&user_id=3&comment=test-comment
    [SVProgressHUD show];
    
    NSString *commentTf;
    
    commentTf = [_commentTxtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    if(commentTf.length > 0)
    {
        
    }
    else
    {
        [self.view makeToast:@"Enter Your Comment"
                    duration:3.0
                    position:CSToastPositionBottom];
        
        
        [SVProgressHUD dismiss];
        return ;
    }
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    
    
        NSString *wUrl=[NSString stringWithFormat:@"%@write_comment.php?article_id=%@&user_id=%@&comment=%@",baseUrl,[dictDetails objectForKey:@"id"],userid,commentTf];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        wUrl=[NSString stringWithFormat:@"%@write_comment.php?article_id=%@&comment=%@",baseUrl,[dictDetails objectForKey:@"id"],commentTf];
    }
    
        NSLog(@"URL :%@", wUrl);
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = 100.0;
        sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary *json;
            
            if(data ==nil)
            {
                [self commentWs];
                return ;
            }
            
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"json :%@", json);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
               NSArray *mArray = [json objectForKey:@"comment"];
                
                if([mArray count]>0)
                {
                    if([[mArray objectAtIndex:0]objectForKey:@"success"])
                    {
                        [self.view makeToast:@"Your comment is submitted and pending approval. It will be displayed here once approved."
                                    duration:3.0
                                    position:CSToastPositionBottom];
                        
                        _commentTxtVw.text = @"";
                        [SVProgressHUD dismiss];
                        return ;
                    }
                    
                }
                
                [SVProgressHUD dismiss];
                
            });
        }];
        
        [dataTask resume];
        
    
}



#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return cArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 120;
    }
    else
        return 100;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier =@"c";
    
    cTableViewCell *cell = (cTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
    
    cell.userLbl.text = [[cArray objectAtIndex:section]valueForKey:@"user_name"];
    cell.dateLbl.text = [[cArray objectAtIndex:section]valueForKey:@"date"];
    cell.commentLbl.text = [[cArray objectAtIndex:section]valueForKey:@"comment"];
    
    
    cell.clipsToBounds = NO;
    cell.layer.masksToBounds = NO;
    
    return cell;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSRange)visibleRangeOfTextView:(UITextView *)textView {
    CGRect bounds = textView.bounds;
    UITextPosition *start = [textView characterRangeAtPoint:bounds.origin].start;
    UITextPosition *end = [textView characterRangeAtPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds))].end;
    return NSMakeRange([textView offsetFromPosition:textView.beginningOfDocument toPosition:start],
                       [textView offsetFromPosition:start toPosition:end]);
}

@end
