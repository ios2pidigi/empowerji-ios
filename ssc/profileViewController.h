//
//  profileViewController.h
//  ssc
//
//  Created by swaroop on 17/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface profileViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *myProfileVw;
@property (strong, nonatomic) IBOutlet UITextField *nameTF;
@property (strong, nonatomic) IBOutlet UITextField *emailTF;
@property (strong, nonatomic) IBOutlet UITextField *numTF;
@property (strong, nonatomic) IBOutlet UITextField *passwordTF;
@property (strong, nonatomic) IBOutlet UITableView *setTblVw;
@property (strong, nonatomic) IBOutlet UIView *changePassVw;
@property (strong, nonatomic) IBOutlet UITextField *cpTF;
@property (strong, nonatomic) IBOutlet UITextField *npTF;
@property (strong, nonatomic) IBOutlet UILabel *welcomeLbl;
@property (strong, nonatomic) IBOutlet UILabel *emailLbl;
@property (strong, nonatomic) IBOutlet UILabel *phoneLbl;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UITextField *cityTF;
@property (strong, nonatomic) IBOutlet UITextField *ageTF;
@property (strong, nonatomic) IBOutlet UITextField *languageTF;
@property (strong, nonatomic) IBOutlet UITextField *otherTF;

@property (strong, nonatomic) IBOutlet UITableView *langTblVw;

@property (strong, nonatomic) IBOutlet UITableView *ageTblVw;

@property (strong, nonatomic) IBOutlet UIButton *btnSelAge;
@property (strong, nonatomic) IBOutlet UIButton *btnSelLang;
@property (strong, nonatomic) IBOutlet UIButton *updateBtn;
@property (strong, nonatomic) IBOutlet UIButton *editBtn;
@property (strong, nonatomic) IBOutlet UIButton *changePassBtn;
@property (strong, nonatomic) IBOutlet UIButton *updateCPBtn;
@property (strong, nonatomic) IBOutlet UILabel *staticLbl;

@end
