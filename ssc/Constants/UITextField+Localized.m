//
//  UITextField+Localized.m
//  ssc
//
//  Created by sarath sb on 10/19/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "UITextField+Localized.h"
#import "Constants.h"

@implementation UITextField (Localized)

- (void)setLocalizeKey:(NSString*)key
{
    NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    NSBundle* languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
   // self.text = [languageBundle localizedStringForKey:key value:@"" table:@"strings-english"];
    
    self.placeholder = [languageBundle localizedStringForKey:key value:@"" table:@"strings-english"];
    
    //self.text = NSLocalizedString(key, nil);
}

@end
