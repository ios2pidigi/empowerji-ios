//
//  UILabel+Localized.m
//  ssc
//
//  Created by sarath sb on 10/17/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "UILabel+Localized.h"
#import "Constants.h"

@implementation UILabel (Localized)
- (void)setLocalizeKey:(NSString*)key
{
    NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
   
    NSBundle* languageBundle = [NSBundle bundleWithPath:languageFilepath];
 
    self.text = [languageBundle localizedStringForKey:key value:@"" table:@"strings-english"];//NSLocalizedString(key, nil);
}
@end
