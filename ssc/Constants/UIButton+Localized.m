//
//  UIButton+Localized.m
//  ssc
//
//  Created by sarath sb on 10/19/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "UIButton+Localized.h"
#import "Constants.h"

@implementation UIButton (Localized)

- (void)setLocalizeKey:(NSString*)key
{
    NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    NSBundle* languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    NSString *title;
    title = [languageBundle localizedStringForKey:key value:@"" table:@"strings-english"];
    
    [self setTitle:title forState:UIControlStateNormal];
    
   // self.titleLabel.text = NSLocalizedString(key, nil);
}
@end
