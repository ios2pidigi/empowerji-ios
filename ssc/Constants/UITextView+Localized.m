//
//  UITextView+Localized.m
//  ssc
//
//  Created by sarath sb on 10/24/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "UITextView+Localized.h"
#import "Constants.h"
@implementation UITextView (Localized)

- (void)setLocalizeKey:(NSString*)key
{
    NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    NSBundle* languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    // self.text = [languageBundle localizedStringForKey:key value:@"" table:@"strings-english"];
    
    //self.text = [languageBundle localizedStringForKey:key value:@"" table:@"strings-english"];
    
    //self.text = NSLocalizedString(key, nil);
}

@end
