//
//  UIButton+Localized.h
//  ssc
//
//  Created by sarath sb on 10/19/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (Localized)

@end

NS_ASSUME_NONNULL_END
