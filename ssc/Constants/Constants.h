//
//  Constants.h
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define baseUrl @"http://app.empowerji.com/webservice5/"
//http://app.empowerji.com/webservice4/

//http://app.empowerji.com/webservice2/
//http://app.empowerji.com/webservice/
//http://listlinkz.com/ssc/webservice/
//http://empowerji.com/app/webservice/
//http://app.empowerji.com/webservice3/

#define termsUrl @"http://empowerji.com/terms-and-conditions/"
#define privacyUrl @"http://empowerji.com/privacy-policy/"

#define IS_IPAD UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0].CGColor

#define gradientColor1 Rgb2UIColor(0, 61, 102)

#define gradientColor2 Rgb2UIColor(0, 148, 215)

#define btngradientColor2 Rgb2UIColor(160, 160, 160)

#define btngradientColor1 Rgb2UIColor(242, 242, 242)

#define helpBtnBG [UIColor colorWithRed:(0/255.0) green:(85/255.0) blue:(130/255.0) alpha:1]

//113, 165, 201
#define btnBorderColor Rgb2UIColor(113, 165, 201)

#define blueColorBtn [UIColor colorWithRed:(93/255.0) green:(139/255.0) blue:(194/255.0) alpha:1]


#define shadowColorCell [UIColor colorWithRed:(3/255.0) green:(69/255.0) blue:(113/255.0) alpha:1].CGColor
//3, 69, 113

#define cellBorderColor Rgb2UIColor(4, 92, 155)
//4, 92, 155


#define blueBgColor [UIColor colorWithRed:(1/255.0) green:(91/255.0) blue:(153/255.0) alpha:1].CGColor
//rgb(1, 91, 153)

#define errorMessage @"You need an active internet connection to use this feature"
//button gradience
//top #f4f4f4
//center #cacaca
//bottom #a0a0a0

#define analyticsID @"UA-128113237-1"
//UA-128113237-1 142904410262

#define oneSignalID @"5a9ee1f9-46c8-4ff4-8547-654b3b087f61"

#define langGlobal ( [[NSUserDefaults standardUserDefaults] objectForKey:@"language"] ? [[NSUserDefaults standardUserDefaults] objectForKey:@"language"] : @"en" )


//#define langGlobal @"en"
//hi
#endif /* Constants_h */
