//
//  UILabel+Localized.h
//  ssc
//
//  Created by sarath sb on 10/17/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Localized)

@end

NS_ASSUME_NONNULL_END
