//
//  UITextView+Localized.h
//  ssc
//
//  Created by sarath sb on 10/24/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (Localized)

@end

NS_ASSUME_NONNULL_END
