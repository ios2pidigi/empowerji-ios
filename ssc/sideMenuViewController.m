//
//  sideMenuViewController.m
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "sideMenuViewController.h"
#import "sideMenuTableViewCell.h"
#import "loginViewController.h"
#import "homeViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "profileViewController.h"
#import "bookmarkViewController.h"
#import "settingsViewController.h"
#import "myFeedViewController.h"
#import "connectivity.h"
#import "SVProgressHUD.h"
#import "popUpVideoViewController.h"

#import "notificationsViewController.h"
#import "languagesViewController.h"
#import "offersListingViewController.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#import <UserNotifications/UserNotifications.h>
#import "inAppViewController.h"

@interface sideMenuViewController ()
{
    NSMutableArray *arrDetails;
    NSMutableArray *imgArr;
    NSArray *arrSet;
    
    NSArray *aboutArr;
    NSArray *termsArr;
    
    UIView *aboutView;
    UIView *feedbackVw;
    UIView *termsVw;
    
    NSArray *staticPages;
    
    CGRect twtFrm;
    CGRect fbFrm;
    CGRect gFrm;
    CGRect instFrm;
    CGRect linFrm;
    
    UILabel *countLbl;
    
}
@end

@implementation sideMenuViewController
@synthesize isfrmSupport;

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
   // [self setStatusBarBackgroundColor:[UIColor colorWithRed:(153/255.0) green:(153/255.0) blue:(153/255.0) alpha:0.3]];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"settings"];
    NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    arrSet = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    //arrDetails = [[NSMutableArray alloc]initWithObjects:@"Home",@"Offers",@"My Profile",@"Bookmarks",@"Settings",@"My Selection",@"About Us",@"Send Message",@"Terms",@"Share",@"Rate Us",@"Logout", nil];
    
   NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    
    NSString *home = [languageBundle localizedStringForKey:@"home" value:@"" table:@"strings-english"];
    
    NSString *offers = [languageBundle localizedStringForKey:@"offersSide" value:@"" table:@"strings-english"];
    
   NSString *myProfile = [languageBundle localizedStringForKey:@"my_profile_txt" value:@"" table:@"strings-english"];
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
      myProfile = [languageBundle localizedStringForKey:@"login" value:@"" table:@"strings-english"];
    }
    
    NSString *notification = [languageBundle localizedStringForKey:@"notification" value:@"" table:@"strings-english"];
    
    
    NSString *aboutUs = [languageBundle localizedStringForKey:@"aboutUs" value:@"" table:@"strings-english"];
    
    NSString *text_share = [languageBundle localizedStringForKey:@"text_share" value:@"" table:@"strings-english"];
    
    NSString *rateUs = [languageBundle localizedStringForKey:@"rateUs" value:@"" table:@"strings-english"];
    
    NSString *terms = [languageBundle localizedStringForKey:@"terms" value:@"" table:@"strings-english"];
    
    NSString *logOut = [languageBundle localizedStringForKey:@"logOut" value:@"" table:@"strings-english"];
    
    NSString *changeLanguage = [languageBundle localizedStringForKey:@"changeLanguage" value:@"" table:@"strings-english"];
    
    NSString *purchasePlan = [languageBundle localizedStringForKey:@"purchasePlan" value:@"" table:@"strings-english"];
    
    NSString *restore = [languageBundle localizedStringForKey:@"restore" value:@"" table:@"strings-english"];
    
    
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        logOut = [languageBundle localizedStringForKey:@"changeLanguage" value:@"" table:@"strings-english"];
    }
    
    
    arrDetails = [[NSMutableArray alloc]initWithObjects:home,myProfile,purchasePlan,notification,/*offers,*/aboutUs,text_share,rateUs,terms,changeLanguage,logOut, nil];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        arrDetails = [[NSMutableArray alloc]initWithObjects:home,myProfile,purchasePlan,notification,/*offers,*/aboutUs,text_share,rateUs,terms,changeLanguage, nil];
    }
    
        
    imgArr = [[NSMutableArray alloc]initWithObjects:@"menu_ico_home",@"menu_ico_myprofile",@"icon_Purchase-plan",@"menu_ico_notif"/*,@"offer"*/,@"menu_ico_aboutus",@"menu_ico_share",@"menu_ico_rateus",@"menu_ico_terms",@"ico_changeLanguage",@"menu_ico_logout", nil];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
      imgArr = [[NSMutableArray alloc]initWithObjects:@"menu_ico_home",@"ico_logIn",@"icon_Purchase-plan",@"menu_ico_notif"/*,@"offer"*/,@"menu_ico_aboutus",@"menu_ico_share",@"menu_ico_rateus",@"menu_ico_terms",@"ico_changeLanguage", nil];
    }
    
    
    [_instaBtn addTarget:self action:@selector(instaAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_linkinBtn addTarget:self action:@selector(linkedInAction) forControlEvents:UIControlEventTouchUpInside];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:@"Side Menu"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
//}

[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTb:) name:@"reloadT" object:nil];

    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"removeCount"])
    {
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSInteger count = 0;
        
        if([userDefaults integerForKey:@"notificationCount"])
        {
            count = [userDefaults integerForKey:@"notificationCount"];
        }
        
        if (@available(iOS 10.0, *)) {
            [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
                
                NSLog(@"getDeliveredNotificationsWithCompletionHandler count %lu", (unsigned long)[notifications count]);
                
                if(notifications.count)
                {
                    NSLog(@"notif count : %lu",(unsigned long)notifications.count);
                    
                    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                    
                    NSInteger count = 0;
                    
                    count = notifications.count;
                    
                    [userDefaults setInteger:count forKey:@"notificationCount"];
                    
                    [userDefaults synchronize];
                    
                    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"removeCount"];
                    
                    //[[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];
                }
                else
                {
                    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                    
                    NSInteger count = 0;
                    
                    //count = notifications.count;
                    
                    [userDefaults setInteger:count forKey:@"notificationCount"];
                    
                    [userDefaults synchronize];
                    
                    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"removeCount"];
                }
                
            }];
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTestNotification:) name:@"backPop" object:nil];
    
}

- (void)reloadTb:(NSNotification *)notification
{
    [self viewDidLoad];
    [self.sideMenuTblVw reloadData];
}



-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    @try {
        
    
    
    NSLog(@"%@",[[arrSet objectAtIndex:0] valueForKey:@"fb"]);
    
    if([[[arrSet objectAtIndex:0] valueForKey:@"fb"] caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _faceBookImg.hidden = YES;
        
    }
    
    if([[[arrSet objectAtIndex:0] valueForKey:@"twitter"] caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        _twitterImg.hidden = YES;
    }
    
    if([[[arrSet objectAtIndex:0] valueForKey:@"mail"] caseInsensitiveCompare:@""]==NSOrderedSame)
    {
        NSLog(@"%@",[[arrSet objectAtIndex:0] valueForKey:@"mail"]);
        
        _googImg.hidden = YES;
    }
        
        if([[[arrSet objectAtIndex:0] valueForKey:@"linkedin"] caseInsensitiveCompare:@""]==NSOrderedSame)
        {
            NSLog(@"%@",[[arrSet objectAtIndex:0] valueForKey:@"linkedin"]);
            
            _linkinBtn.hidden = YES;
        }
        
        if([[[arrSet objectAtIndex:0] valueForKey:@"instagram"] caseInsensitiveCompare:@""]==NSOrderedSame)
        {
            NSLog(@"%@",[[arrSet objectAtIndex:0] valueForKey:@"instagram"]);
            
            _instaBtn.hidden = YES;
        }
        
    
    if(([[[arrSet objectAtIndex:0] valueForKey:@"fb"] caseInsensitiveCompare:@""]==NSOrderedSame) && ([[[arrSet objectAtIndex:0] valueForKey:@"twitter"] caseInsensitiveCompare:@""]==NSOrderedSame) && ([[[arrSet objectAtIndex:0] valueForKey:@"gplus"] caseInsensitiveCompare:@""]==NSOrderedSame))
    {
        _lblFollowus.hidden = YES;
    }
    
     if( self.aboutVw.window )
        {
            
            twtFrm = _twitterImg.frame;
            fbFrm = _faceBookImg.frame;
            gFrm = _googImg.frame ;
            instFrm = _instImg.frame;
            linFrm = _linkImg.frame;
            
        if(_twitterImg.isHidden)
        {
            _faceBookImg.frame = twtFrm;
            
            if(_faceBookImg.isHidden)
            {
                _googImg.frame = fbFrm;
                
                if(_googImg.isHidden)
                {
                    _instImg.frame = gFrm;
                    
                    if(_instImg.isHidden)
                    {
                        _linkImg.frame = instFrm;
                    }
                }
            }
        }
        else
        {
                if(_faceBookImg.isHidden)
                {
                    _googImg.frame = fbFrm;
                    
                    if(_googImg.isHidden)
                    {
                        _instImg.frame = gFrm;
                        
                        if(_instImg.isHidden)
                        {
                            _linkImg.frame = instFrm;
                        }
                    }
                }
                else
                {
                    if(_googImg.isHidden)
                    {
                        _instImg.frame = gFrm;
                        
                        if(_instImg.isHidden)
                        {
                            _linkImg.frame = instFrm;
                        }
                    }
                    else
                    {
                        if(_instImg.isHidden)
                        {
                            _linkImg.frame = instFrm;
                        }
                    }
                }
        }
        
        }
        
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = [UIScreen mainScreen].bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgAbout.layer insertSublayer:gradient atIndex:0];
    
    [_bgfeedBack.layer insertSublayer:gradient atIndex:0];
    
    [_bgTerms.layer insertSublayer:gradient atIndex:0];
    
    
    CGRect frame;
    frame = _shareVw.frame;
    frame.origin.y = _aboutTxtVw.frame.origin.y + _aboutTxtVw.contentSize.height;
    _shareVw.frame = frame;
    
    if(_shareVw.frame.origin.y >= ([UIScreen mainScreen].bounds.size.height -_shareVw.frame.size.height ))
    {
        CGRect frame;
        frame = _shareVw.frame;
        frame.origin.y = [UIScreen mainScreen].bounds.size.height -_shareVw.frame.size.height;
        _shareVw.frame = frame;
    }
        
    } @catch (NSException *exception)
    {
       // [self showMessage:errorMessage withTitle:nil];
    }
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //[_sideMenuTblVw reloadData];
}

-(void)reloadTblVw
{
    [self viewDidLoad];
    
}

#pragma mark Webservice Calls

-(void)aboutWS
{
    
    aboutArr = [[NSArray alloc]init];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@page.php?page_id=1&lang_id=%ld",baseUrl,(long)langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
           // [self aboutWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            aboutArr = [json objectForKey:@"page"];
            //page
            
            _titleAbout.text = [[aboutArr objectAtIndex:0] valueForKey:@"title"];
            
        });
    }];
    
    [dataTask resume];
    
}


-(void)termsWS
{
   
    
    termsArr = [[NSArray alloc]init];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@page.php?page_id=2&lang_id=%ld",baseUrl,(long)langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
           // [self termsWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            termsArr = [json objectForKey:@"page"];
            //page
            _titleTerms.text = [[termsArr objectAtIndex:0] valueForKey:@"title"];
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)feedBackWS
{
   //http://listlinkz.com/ssc/webservice/feedback_submit.php?user_id=1&feedback=message
    //user_id
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *feedTxtVwText = [_feedTxtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    if(feedTxtVwText.length>0)
    {
        
    }
    else
    {
        NSString *lang = langGlobal;
        
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"emptyFeedback" value:@"" table:@"strings-english"];
        
        
        [self.view makeToast:message
                    duration:1.0
                    position:CSToastPositionBottom];
        
        //[SVProgressHUD dismiss];
        return ;
    }
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@feedback_submit.php?user_id=%@&feedback=%@&lang_id=%ld",baseUrl,userid,feedTxtVwText,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self feedBackWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
           // [SVProgressHUD dismiss];
            
            NSArray *result = [[json objectForKey:@"result"]objectAtIndex:0];
            
            if([[result valueForKey:@"status"] isEqualToString:@"success"])
            {
                    [self.view makeToast:[result valueForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                
            }
            else
            {
                [self.view makeToast:@"Oops..Something went wrong"
                            duration:3.0
                            position:CSToastPositionBottom];
            }
            
            [self backFeed:nil];
            
        });
    }];
    
    [dataTask resume];
    
}

-(void)sendMessageWebservice
{
    //http://listlinkz.com/ssc/webservice/feedback_submit.php?user_id=1&feedback=message
    //user_id
    
    [self.feedBackVw makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    NSString *feedTxtVwText = [_feedTxtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    if(feedTxtVwText.length>0)
    {
        
    }
    else
    {
        NSString *lang = langGlobal;
        
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"messageEmpty" value:@"" table:@"strings-english"];
        
        
        [self.view makeToast:message
                    duration:1.0
                    position:CSToastPositionBottom];
        
        //[SVProgressHUD dismiss];
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           
                           [self.feedBackVw hideToastActivity];
                           
                       });
        
        return ;
    }
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@feedback_submit.php?user_id=%@&feedback=%@&lang_id=%ld",baseUrl,userid,feedTxtVwText,langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self sendMessageWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            _feedTxtVw.text = @"";
            // [SVProgressHUD dismiss];
            
            NSArray *result = [[json objectForKey:@"result"]objectAtIndex:0];
            
            if([[result valueForKey:@"status"] isEqualToString:@"success"])
            {
                [self.view makeToast:[result valueForKey:@"message"]
                            duration:3.0
                            position:CSToastPositionBottom];
                
            }
            else
            {
                [self.view makeToast:@"Oops..Something went wrong"
                            duration:3.0
                            position:CSToastPositionBottom];
            }
            
            // [self backFeed:nil];
            
            dispatch_async(dispatch_get_main_queue(),
                           ^{
                               
                               [self.feedBackVw hideToastActivity];
                               
                           });
            
        });
    }];
    
    [dataTask resume];
    
}
#pragma mark status bar color

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}


#pragma mark topMostController

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrDetails count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 80;
    }
    else
    return 50;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    NSInteger totalSections = [arrDetails count];
    if(section == (totalSections-1))
        return 80;
    else
        return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSInteger totalSections = [arrDetails count];
    if(section == (totalSections-1))
    {
       
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *helpTxt = [languageBundle localizedStringForKey:@"empowerjiApp" value:@"" table:@"strings-english"];
        
        UIButton *helpBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 15, tableView.frame.size.width - 40, 60)];
        
        if(IS_IPAD)
        {
            helpBtn.frame = CGRectMake(100, 15, tableView.frame.size.width - 200, 60);
        }
        
        [helpBtn setImage:[UIImage imageNamed:@"icon-play3.png"] forState:UIControlStateNormal];
        helpBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [helpBtn setTitle:helpTxt forState:UIControlStateNormal];
        helpBtn.titleLabel.font = [UIFont systemFontOfSize:20.0 weight:UIFontWeightBold];
        helpBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        helpBtn.clipsToBounds = YES;
        helpBtn.layer.cornerRadius = helpBtn.frame.size.height/2;
        helpBtn.backgroundColor = helpBtnBG;
        
        [helpBtn addTarget:self action:@selector(helpBtnAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 80)];
        view.backgroundColor = [UIColor clearColor];
        
        [view addSubview:helpBtn];
        
      /*  UIImageView *help = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
        help.image = [UIImage imageNamed:@"icon-play.png"];
        help.contentMode = UIViewContentModeScaleAspectFit;
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        NSString *helpTxt = [languageBundle localizedStringForKey:@"empowerjiApp" value:@"" table:@"strings-english"];
        
        UIButton *helpBtn = [[UIButton alloc]initWithFrame:CGRectMake(25, 0, tableView.frame.size.width, 50)];
        
        [helpBtn setTitle:helpTxt forState:UIControlStateNormal];
        helpBtn.titleLabel.font = [UIFont systemFontOfSize:20.0 weight:UIFontWeightBold];
        helpBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        
        [helpBtn addTarget:self action:@selector(helpBtnAction) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
        view.backgroundColor = [UIColor clearColor];
        
        [view addSubview:help];
        [view addSubview:helpBtn];
        */
        
        return view;
    }
    else
    {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier =@"sideMenu";
    
    sideMenuTableViewCell *cell = (sideMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"sideMenuTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSInteger section = indexPath.section;
   
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:(111/255.0) green:(127/255.0) blue:(127/255.0) alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];
    
    cell.sideMenuLbl.text = [arrDetails objectAtIndex:section];
    cell.sideMenuLbl.font = [UIFont systemFontOfSize:17.0 weight:UIFontWeightBold];
    cell.sideMenuImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imgArr objectAtIndex:section]]];
    
    
    
    if(section == 2)
    {
        [countLbl removeFromSuperview];
        
    countLbl = [[UILabel alloc]initWithFrame:CGRectMake(cell.sideMenuLbl.frame.origin.x + cell.sideMenuLbl.frame.size.width, cell.sideMenuLbl.frame.origin.y, cell.sideMenuLbl.frame.size.height, cell.sideMenuLbl.frame.size.height)];
    countLbl.backgroundColor = [UIColor redColor];
    countLbl.text = @"1";
    countLbl.textColor = [UIColor whiteColor];
    countLbl.textAlignment = NSTextAlignmentCenter;
    countLbl.layer.cornerRadius = countLbl.frame.size.height/2;
    countLbl.clipsToBounds = YES;
    
    if(IS_IPAD)
    {
        
    }
    else
    {
        CGRect countFrame = countLbl.frame;
        countFrame.origin.x = cell.sideMenuLbl.frame.origin.x + cell.sideMenuLbl.intrinsicContentSize.width + 2;
        countLbl.frame = countFrame;
        
    }
   
        NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSInteger count = 0;
        
        if([userDefaults integerForKey:@"notificationCount"])
        {
            count = [userDefaults integerForKey:@"notificationCount"];
        }
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;//count;
//[[self navigationController] tabBarItem].badgeValue = @"YourBadgeValue";
    
        countLbl.text = [NSString stringWithFormat:@"%ld",(long)count];
    
    if(section == 2)
        if(!([countLbl.text caseInsensitiveCompare:@"0"]==NSOrderedSame))
    [cell.contentView addSubview:countLbl];
    
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    NSMutableArray * mutArrayC = [[NSMutableArray alloc]init];
    
    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"userEmail"])
    {
        mutArrayC = [[NSMutableArray alloc]initWithObjects:@"Home",@"Sign-In",@"Purchase Plan",@"Notifications",@"About Us",@"Share",@"Rate Us",@"Terms",@"Change Language", nil];
    }
    else
    {
    mutArrayC = [[NSMutableArray alloc]initWithObjects:@"Home",@"My Profile",@"Purchase Plan",@"Notifications"/*,@"Offers"*/,@"About Us",@"Share",@"Rate Us",@"Terms",@"Change Language",@"Logout", nil];
    }
    
    NSString *nameString = [mutArrayC objectAtIndex:section];
    
    if([nameString caseInsensitiveCompare:@"home"] == NSOrderedSame )
    {
      //  [self dismissViewControllerAnimated:NO completion:nil];
        
        homeViewController *homeVC = [[homeViewController alloc]initWithNibName:@"homeViewController" bundle:nil];
       //[self.navigationController pushViewController:homeVC animated:YES];
        
        if(isfrmSupport)
        {
            UINavigationController *nav = (UINavigationController *)self.presentingViewController;
            [self dismissViewControllerAnimated:YES completion:^{
                [nav popViewControllerAnimated:YES];
            }];
        }
        else
        {
            UINavigationController *nav = (UINavigationController *)self.presentingViewController;
            [self dismissViewControllerAnimated:YES completion:
             ^{
                [nav popViewControllerAnimated:YES];
            }];
        }
        
       // [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if([nameString caseInsensitiveCompare:@"purchase plan"] == NSOrderedSame )
    {
       
       
        
        inAppViewController *inapp = [[inAppViewController alloc] init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:inapp];
        navController.navigationBar.hidden = YES;
        [self presentViewController:navController animated:YES completion:nil];
            
        
        
    }
   else if([nameString caseInsensitiveCompare:@"offers"] == NSOrderedSame )
    {
        offersListingViewController *loginViewController = [[offersListingViewController alloc] init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
        navController.navigationBar.hidden = YES;
        [self presentViewController:navController animated:YES completion:nil];
        
        return;
        
        offersListingViewController *ofrVC = [[offersListingViewController alloc]initWithNibName:@"offersListingViewController" bundle:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [pvc presentViewController: ofrVC animated: NO completion:nil];
        
    }
    else if([nameString caseInsensitiveCompare:@"my profile"] == NSOrderedSame )
    {
        [self dismissViewControllerAnimated:NO completion:^{
            
            profileViewController *proVC = [[profileViewController alloc]initWithNibName:@"profileViewController" bundle:nil];
            // [self.navigationController pushViewController:proVC animated:YES];
            proVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            proVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            UIViewController *top = [UIApplication sharedApplication].keyWindow.rootViewController;
            [top presentViewController:proVC animated:YES completion: nil];
            
        }];
        
        
        
    }
    else if([nameString caseInsensitiveCompare:@"bookmarks"] == NSOrderedSame )
    {
        NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
        
        [SVProgressHUD show];
        
        // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
        
        [[[NSURLSession sharedSession]
          dataTaskWithURL:[NSURL URLWithString:wUrl]
          completionHandler:^(NSData *urlData,
                              NSURLResponse *response,
                              NSError *error)
          {
              // handle response
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  if([urlData length] == 0)
                  {
                      NSLog(@"internet is not connected");
                      
                      
                      [self showMessage:errorMessage withTitle:nil];
                  }
                  else
                  {
                      NSLog(@"internet is connected");
                      
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            //connection unavailable
            
            [self showMessage:errorMessage withTitle:nil];
        }
        else
        {
            
        [self dismissViewControllerAnimated:NO completion:^{
            
            bookmarkViewController *bVC = [[bookmarkViewController alloc]initWithNibName:@"bookmarkViewController" bundle:nil];
            // [self.navigationController pushViewController:proVC animated:YES];
            bVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            bVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            UIViewController *top = [UIApplication sharedApplication].keyWindow.rootViewController;
            [top presentViewController:bVC animated:NO completion: nil];
            
        }];
            
        }
                      
                  }
                  dispatch_async(dispatch_get_main_queue(), ^{
                      //[self.view makeToastActivity:CSToastPositionCenter];
                      [SVProgressHUD dismiss];
                  });
                  
                  
              });
              
              
          }] resume];
    }
    else if([nameString caseInsensitiveCompare:@"settings"] == NSOrderedSame )
    {
        NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
        
        [SVProgressHUD show];
        
        // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
        
        [[[NSURLSession sharedSession]
          dataTaskWithURL:[NSURL URLWithString:wUrl]
          completionHandler:^(NSData *urlData,
                              NSURLResponse *response,
                              NSError *error)
          {
              // handle response
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  if([urlData length] == 0)
                  {
                      NSLog(@"internet is not connected");
                      
                      
                      [self showMessage:errorMessage withTitle:nil];
                  }
                  else
                  {
                      NSLog(@"internet is connected");
                      
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            //connection unavailable
            
            [self showMessage:errorMessage withTitle:nil];
        }
        else
        {
            
        [self dismissViewControllerAnimated:YES completion:^{
            
            settingsViewController *sVC = [[settingsViewController alloc]initWithNibName:@"settingsViewController" bundle:nil];
            // [self.navigationController pushViewController:proVC animated:YES];
            sVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            sVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            UIViewController *top = [UIApplication sharedApplication].keyWindow.rootViewController;
            [top presentViewController:sVC animated:NO completion: nil];
            
        }];
            
        }
        
                  }
                  
                  dispatch_async(dispatch_get_main_queue(), ^{
                      //[self.view makeToastActivity:CSToastPositionCenter];
                      [SVProgressHUD dismiss];
                  });
                  
                  
              });
              
              
          }] resume];
    }
    else if([nameString caseInsensitiveCompare:@"my selection"] == NSOrderedSame )
    {
        NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
        
        [SVProgressHUD show];
        
        // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
        
        [[[NSURLSession sharedSession]
          dataTaskWithURL:[NSURL URLWithString:wUrl]
          completionHandler:^(NSData *urlData,
                              NSURLResponse *response,
                              NSError *error)
          {
              // handle response
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  if([urlData length] == 0)
                  {
                      NSLog(@"internet is not connected");
                      
                      
                      [self showMessage:errorMessage withTitle:nil];
                  }
                  else
                  {
                      NSLog(@"internet is connected");
                      
        
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            //connection unavailable
            
            [self showMessage:errorMessage withTitle:nil];
        }
        else
        {
            
        [self dismissViewControllerAnimated:YES completion:^{
            
            myFeedViewController *mfVC = [[myFeedViewController alloc]initWithNibName:@"myFeedViewController" bundle:nil];
            // [self.navigationController pushViewController:proVC animated:YES];
            mfVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            mfVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            UIViewController *top = [UIApplication sharedApplication].keyWindow.rootViewController;
            [top presentViewController:mfVC animated:NO completion: nil];
            
        }];
            
        }
                  }
                  
                  dispatch_async(dispatch_get_main_queue(), ^{
                      //[self.view makeToastActivity:CSToastPositionCenter];
                      [SVProgressHUD dismiss];
                  });
                  
                  
              });
              
              
          }] resume];
    }
    else if([nameString caseInsensitiveCompare:@"about us"] == NSOrderedSame )
    {
        [self aboutWS];
       
        
        NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
        
        [SVProgressHUD show];
        
        // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
        
        [[[NSURLSession sharedSession]
          dataTaskWithURL:[NSURL URLWithString:wUrl]
          completionHandler:^(NSData *urlData,
                              NSURLResponse *response,
                              NSError *error)
          {
              // handle response
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  if([urlData length] == 0)
                  {
                      NSLog(@"internet is not connected");
                      
                      
                     // [self showMessage:errorMessage withTitle:nil];
                      
                      NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                      NSData *data = [def objectForKey:@"staticArrContents"];
                      staticPages = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                      
                      [self showAboutPageFromLocal];
                  }
                  else
                  {
                      NSLog(@"internet is connected");
                      
                      
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            //connection unavailable
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            NSData *data = [def objectForKey:@"staticArrContents"];
            staticPages = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            [self showAboutPageFromLocal];
            
        }
        else
        {
        NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        style.alignment = NSTextAlignmentJustified;
        style.firstLineHeadIndent = 10.0f;
        style.headIndent = 10.0f;
        style.tailIndent = -10.0f;
        
        // NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:lblText.text attributes:@{ NSParagraphStyleAttributeName : style}];
        
        
        NSString *htmlString=[[aboutArr objectAtIndex:0] valueForKey:@"page_content"];
        
      
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            // float widdth =  [UIScreen mainScreen].bounds.size.width-20 ;
            
            
            
            htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<html><head><style>img{max-width:50%%;height:auto !important;width:auto !important;};</style></head><style>body{font-family: 'SanFranciso'; font-size:22px;}</style>"]];//max-width: 100%; width: auto; height: auto;
        }
        else
        {
            
            htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: 'SanFranciso'; font-size:22px;}</style>"]];
        }
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        
        
        NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
        NSRange range = (NSRange){0,[newString length]};
        [newString enumerateAttribute:NSFontAttributeName inRange:range options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
            UIFont *replacementFont =  [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
            UIColor *color = [UIColor whiteColor];
            [newString addAttribute:NSForegroundColorAttributeName value:color range:range];
            // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
        }];
        
        
        _aboutTxtVw.attributedText= newString; //attributedString;
            
           // [_aboutTxtVw sizeToFit];
            
        
            _aboutTxtVw.showsVerticalScrollIndicator=NO;
            _aboutTxtVw.editable = NO;
            _aboutTxtVw.dataDetectorTypes = UIDataDetectorTypeLink;
            _aboutTxtVw.userInteractionEnabled=YES;
            _aboutTxtVw.selectable=YES;
        
        _aboutVw.frame = [UIScreen mainScreen].bounds;
            [self.view addSubview:_aboutVw];
        
        }
                      
                  }
                  
                  dispatch_async(dispatch_get_main_queue(), ^{
                      //[self.view makeToastActivity:CSToastPositionCenter];
                      [SVProgressHUD dismiss];
                  });
                  
                  
              });
              
              
          }] resume];

    }
    else if([nameString caseInsensitiveCompare:@"send message"] == NSOrderedSame )
    {
        _feedTxtVw.text = @"";
        _feedBackVw.frame = [UIScreen mainScreen].bounds;
        [self.view addSubview:_feedBackVw];
    }
    else if([nameString caseInsensitiveCompare:@"terms"] == NSOrderedSame )
    {
         [self termsWS];
        
        NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
        
        [SVProgressHUD show];
        
        // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
        
        [[[NSURLSession sharedSession]
          dataTaskWithURL:[NSURL URLWithString:wUrl]
          completionHandler:^(NSData *urlData,
                              NSURLResponse *response,
                              NSError *error)
          {
              // handle response
              dispatch_async(dispatch_get_main_queue(), ^{
                  
                  if([urlData length] == 0)
                  {
                      NSLog(@"internet is not connected");
                      
                      NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                      NSData *data = [def objectForKey:@"staticArrContents"];
                      staticPages = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                      
                      [self showTermsPageFromLocal];
                      
                     // [self showMessage:errorMessage withTitle:nil];
                  }
                  else
                  {
                      NSLog(@"internet is connected");
                      
        if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            //connection unavailable
            
            NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
            NSData *data = [def objectForKey:@"staticArrContents"];
            staticPages = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            [self showTermsPageFromLocal];
            
        }
        else
        {
            
        NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        style.alignment = NSTextAlignmentJustified;
        style.firstLineHeadIndent = 10.0f;
        style.headIndent = 10.0f;
        style.tailIndent = -10.0f;
        
        // NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:lblText.text attributes:@{ NSParagraphStyleAttributeName : style}];
        
        
        NSString *htmlString=[[termsArr objectAtIndex:0] valueForKey:@"page_content"];
        
        
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            // float widdth =  [UIScreen mainScreen].bounds.size.width-20 ;
            
            
            
            htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<html><head><style>img{max-width:50%%;height:auto !important;width:auto !important;};</style></head><style>body{font-family: 'SanFranciso'; font-size:22px;}</style>"]];//max-width: 100%; width: auto; height: auto;
        }
        else
        {
            
            htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: 'SanFranciso'; font-size:22px;}</style>"]];
        }
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        
        
        NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
        NSRange range = (NSRange){0,[newString length]};
        [newString enumerateAttribute:NSFontAttributeName inRange:range options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
            UIFont *replacementFont =  [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
            UIColor *color = [UIColor whiteColor];
            [newString addAttribute:NSForegroundColorAttributeName value:color range:range];
            // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
        }];
        
        
        _termsTxtVw.attributedText= newString;//attributedString;
        
        // [_aboutTxtVw sizeToFit];
        
        
        _termsTxtVw.showsVerticalScrollIndicator=NO;
        _termsTxtVw.editable = NO;
        _termsTxtVw.dataDetectorTypes = UIDataDetectorTypeLink;
        _termsTxtVw.userInteractionEnabled=YES;
        _termsTxtVw.selectable=YES;
        
        _termsVw.frame = [UIScreen mainScreen].bounds;
        [self.view addSubview:_termsVw];
            
        }
                      
                  }
                  dispatch_async(dispatch_get_main_queue(), ^{
                      //[self.view makeToastActivity:CSToastPositionCenter];
                      [SVProgressHUD dismiss];
                  });
                  
                  
              });
              
              
          }] resume];
    }
    else if([nameString caseInsensitiveCompare:@"share"] == NSOrderedSame )
    {
        NSString *texttoshare =  [NSString stringWithFormat:@"%@ %@",[[arrSet objectAtIndex:0] objectForKey:@"app_share_text"],[[arrSet objectAtIndex:0] objectForKey:@"app_share_url"]];
        UIImage *imagetoshare = [UIImage imageNamed:@"imgToShare"]; //this is your image to share
        NSArray *activityItems = @[texttoshare];//, imagetoshare];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        if ([activityVC respondsToSelector:@selector(popoverPresentationController)])
        {
            // iOS 8+
            UIPopoverPresentationController *presentationController = [activityVC popoverPresentationController];
            
            presentationController.sourceView = self.view; // if button or change to self.view.
        }
        activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
        [self presentViewController:activityVC animated:TRUE completion:nil];
    }
    else if([nameString caseInsensitiveCompare:@"rate us"] == NSOrderedSame )
    {
        NSString *theUrl = [[arrSet objectAtIndex:0] valueForKey:@"app_share_url"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    }
    else if([nameString caseInsensitiveCompare:@"logout"] == NSOrderedSame )
    {
        //[self dismissViewControllerAnimated:NO completion:nil];
        
        NSString *lang = langGlobal;
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"logoutAlert" value:@"" table:@"strings-english"];
        
        NSString *ok = [languageBundle localizedStringForKey:@"ok" value:@"" table:@"strings-english"];
        
        NSString *cancelA = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:message
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self logOut];
                
            });
        }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancelA style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
        {
            
         }];
        
        [alert addAction:okAction];
        [alert addAction:cancel];
        
       // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        //[vc presentViewController:alert animated:YES completion:nil];
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        [pvc presentViewController: alert animated: NO completion:nil];
    }
    else if([nameString caseInsensitiveCompare:@"Notifications"] == NSOrderedSame )
    {
        notificationsViewController *nVC = [[notificationsViewController alloc]initWithNibName:@"notificationsViewController" bundle:nil];
        
        self.definesPresentationContext = YES; //self is presenting view controller
       // mfVC.view.backgroundColor = [UIColor clearColor];
       // mfVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:nVC animated:NO completion:nil];
    }
    else if([nameString caseInsensitiveCompare:@"Sign-In"] == NSOrderedSame )
    {
        loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
        lgVC.isfrmHome = YES;
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        //[pvc presentViewController: lgVC animated: NO completion:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:lgVC animated:YES completion:nil];
        });
        
        /*
         loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
        lgVC.isfrmHome = YES;
       // self.definesPresentationContext = YES; //self is presenting view controller
        //[self presentViewController:lgVC animated:NO completion:nil];
        
        UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:lgVC];
        navigationController.navigationBarHidden=YES;
        
        [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
        */
    }

    else if([nameString caseInsensitiveCompare:@"change language"] == NSOrderedSame )
    {
        languagesViewController *lVC = [[languagesViewController alloc]initWithNibName:@"languagesViewController" bundle:nil];
        lVC.isFrmHome = YES;
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        //[pvc presentViewController: lVC animated: NO completion:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:lVC animated:YES completion:nil];
        });
        
       /* UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:lVC];
        navigationController.navigationBarHidden=YES;
        
        [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
        */
        
        /*loginViewController *lgVC = [[loginViewController alloc]initWithNibName:@"loginViewController" bundle:nil];
        
        self.definesPresentationContext = YES; //self is presenting view controller
        [self presentViewController:lgVC animated:NO completion:nil];
         */
    }
    
   else if([nameString caseInsensitiveCompare:@"restore"] == NSOrderedSame )
    {
        
        NSLog(@"restore btn clicked");
    }
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // Add your Colour.
    
    sideMenuTableViewCell *cell = (sideMenuTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    //[self setCellColor:[UIColor colorWithRed:(111/255.0) green:(127/255.0) blue:(127/255.0) alpha:1.0] ForCell:cell];  //highlight colour
}
- (void)setCellColor:(UIColor *)color ForCell:(UITableViewCell *)cell {
    cell.contentView.backgroundColor = color;
    cell.backgroundColor = color;
}

- (IBAction)menuBtn:(id)sender
{
    
}

-(void)deleteGuestOnLogin
{
    //listlinkz.com/liimr/webservice/remove_token.php?token=aaa&device_id=ccc
    
    NSString *wUrl;
    
    int randIntgr = arc4random_uniform(10000);
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"];
    
    token = [token stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    wUrl =[NSString stringWithFormat:@"%@delete_token.php?token=%@&device_id=%@&device=ios&counts=%d",baseUrl,token,currentDeviceId,randIntgr];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 60.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self deleteTokenOnLogoutWS];
            
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"token: %@ deleted",token);
        });
    }];
    
    [dataTask resume];
    
}

-(void)deleteTokenOnLogoutWS
{
    //listlinkz.com/liimr/webservice/remove_token.php?token=aaa&device_id=ccc
    
    NSString *wUrl;
    
    int randIntgr = arc4random_uniform(10000);
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"];
    
    token = [token stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *userid;
    userid = [[arrUser objectAtIndex:0]valueForKey:@"user_id"];
    
    wUrl =[NSString stringWithFormat:@"%@delete_token.php?user_id=%@&token=%@&device_id=%@&device=ios&counts=%d",baseUrl,userid,token,currentDeviceId,randIntgr];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 60.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
     {
         NSDictionary *json;
         
         if(data ==nil)
         {
             [self deleteTokenOnLogoutWS];
             
             return ;
         }
         
         json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
         NSLog(@"json :%@", json);
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             NSLog(@"token: %@ deleted",token);
         });
     }];
    
    [dataTask resume];
    
}


-(void)logOut
{
    //changed to stay in home screen after logout
    //loginViewController *lgVC = [[loginViewController alloc]init];
    
    //[[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
   /* UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if (types == UIRemoteNotificationTypeNone)
    {
        NSLog(@"none");
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
      NSLog(@"else");
    }
    
    */
    
    [self deleteTokenOnLogoutWS];
    
   /////to show language of guest upon logout
    NSString *langidGuest, *selLang;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"])
        langidGuest = [[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"];
    else
        langidGuest = @"5";
    
    NSInteger i = [langidGuest integerValue];
    switch (i) {
        case 0:
            selLang = @"en";
            break;
        case 1:
            selLang = @"en";
            break;
        case 2:
            selLang = @"hi";
            break;
        case 3:
            selLang = @"mr-IN";
            break;
        case 4:
            selLang = @"gu-IN";
            break;
        case 5:
            selLang = @"en";
            break;
        default:
            break;
    }
    if(selLang)
        [[NSUserDefaults standardUserDefaults] setObject:selLang forKey:@"language"];
   
    ///
    
    [self saveGuestTokenWS];
    
    homeViewController *lgVC = [[homeViewController alloc]initWithNibName:@"homeViewController" bundle:nil];
    lgVC.isFrmSignOut=YES;
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"supportClickedHereButton"];
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"keepgoingClickedHereButton"];
    
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:lgVC];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"keepmeloggedin"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userEmail"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
    
    navigationController.navigationBarHidden=YES;
}

-(void)saveGuestTokenWS
{
    int randIntgr = arc4random_uniform(10000);
    
    //[self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    // NSString *userid = [NSString stringWithFormat:@"%@",[[arrUser objectAtIndex:0] valueForKey:@"user_id"]];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"];
    
    token = [token stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    
    /* save_token.php?user_id=150&token=d0p_RE9zG_o:APA91bFPDu-tbWoeaSNZTTSkLtjelz7wYgJq6uL9XXkqqFubkvHq8mSP9pfCF4fZv9doazkWItnLP7Pk97mD8raacu_A8NAlHt2ZsSAL-46GWcQPLCzyk19Elp24OkKPY83b9HJK5GLB&device_id=9e9e11f98bb75e23&device=android
     */
    
    
    NSString *langid;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"])
        langid = [[NSUserDefaults standardUserDefaults]objectForKey:@"dummyLang"];
    else
        langid = @"5";
    
    //app.empowerji.com/webservice4/save_guest.php?&lang_id=5&device_id=7439aeb9fcd7aaa9
    
    NSString *wUrl=[NSString stringWithFormat:@"%@save_guest.php?token=%@&device_id=%@&device=ios&counts=%d&lang_id=%@",baseUrl,token,currentDeviceId,randIntgr,langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
   {
       NSDictionary *json;
       
       if(data ==nil)
       {
           [self saveGuestTokenWS];
           return ;
       }
       
       json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
       
       NSLog(@"json token save:%@", json);
       
       
       
       dispatch_async(dispatch_get_main_queue(), ^{
           
           
       });
   }];
    
    [dataTask resume];
    
}


-(void)playVideo
{
    //_videoVw.hidden = NO;
    
    NSString *url=@"https://www.youtube.com/embed/8NT-E-nrJ9M";
    
   // [self.webVw loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
  //  self.webVw.scrollView.bounces = NO;
  //  [self.webVw setMediaPlaybackRequiresUserAction:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark TouchEvent

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch ;
    touch = [[event allTouches] anyObject];
    
    
    if ([touch view] == self.view)
    {
        for (UIView * view in self.view.subviews){
            if ([view isKindOfClass:[UIImageView class]])
            {
                if(view.tag ==100)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification2" object:self];
                    
                    [self dismissViewControllerAnimated:NO completion:nil];
                }
            }
        }
    }
    
    
}
-(void)receiveTestNotification:(NSNotification *) notification
{
     [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)logoBtnClicked:(id)sender
{
    UINavigationController *nav = (UINavigationController *)self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        [nav popViewControllerAnimated:YES];
    }];
}

- (IBAction)backFeed:(id)sender
{
    [_feedBackVw removeFromSuperview];
}
- (IBAction)backAbout:(id)sender
{
    [_aboutVw removeFromSuperview];
}
- (IBAction)backTerms:(id)sender
{
   [_termsVw removeFromSuperview];
}
- (IBAction)submitFeedBak:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
              
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        
        [self showMessage:errorMessage withTitle:nil];
    }
    else
    {
      [self sendMessageWebservice];
    }
    
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
   // [self feedBackWS];
    
}
- (IBAction)homeActn:(id)sender
{
    //[self dismissViewControllerAnimated:NO completion:nil];
    
    UINavigationController *nav = (UINavigationController *)self.presentingViewController;
    [self dismissViewControllerAnimated:YES completion:^{
        [nav popViewControllerAnimated:YES];
    }];
}
- (IBAction)twitterBtnAction:(id)sender
{
    NSString *url = [[arrSet objectAtIndex:0] valueForKey:@"twitter"];
    
   /* NSURL *twitterURL = [NSURL URLWithString:@"twitter://user?screen_name="];
    if ([[UIApplication sharedApplication] canOpenURL:twitterURL])
        [[UIApplication sharedApplication] openURL:twitterURL];
    else
    */
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (IBAction)faceBookBtnAction:(id)sender
{
     NSString *url = [[arrSet objectAtIndex:0] valueForKey:@"fb"];
    
    /*NSURL *facebookURL = [NSURL URLWithString:@"fb://page/?id=318055708271372"];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL])
        [[UIApplication sharedApplication] openURL:facebookURL];
    else
     */
    if(url)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    //318055708271372
}
- (IBAction)googleplusBtnAction:(id)sender
{
     NSString *url = [[arrSet objectAtIndex:0] valueForKey:@"mail"];
    
    //mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!
    
    NSString *recipients = [NSString stringWithFormat:@"mailto:%@",url];
    
    NSString *body = @"";
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

-(void)instaAction
{
    NSString *url = [[arrSet objectAtIndex:0] valueForKey:@"instagram"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

-(void)linkedInAction
{
    NSString *url = [[arrSet objectAtIndex:0] valueForKey:@"linkedin"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}


-(void)showAboutPageFromLocal
{
    aboutArr = [staticPages objectAtIndex:0] ;
    
   // termsArr = [staticPages objectAtIndex:1] ;
     _titleAbout.text = [aboutArr valueForKey:@"title"];
    
    if([aboutArr count]>0)
    {
        {
            NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
            style.alignment = NSTextAlignmentJustified;
            style.firstLineHeadIndent = 10.0f;
            style.headIndent = 10.0f;
            style.tailIndent = -10.0f;
            
            // NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:lblText.text attributes:@{ NSParagraphStyleAttributeName : style}];
            
            
            NSString *htmlString=[aboutArr valueForKey:@"page_content"];
            
            
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                // float widdth =  [UIScreen mainScreen].bounds.size.width-20 ;
                
                
                
                htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<html><head><style>img{max-width:50%%;height:auto !important;width:auto !important;};</style></head><style>body{font-family: 'SanFranciso'; font-size:22px;}</style>"]];//max-width: 100%; width: auto; height: auto;
            }
            else
            {
                
                htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: 'SanFranciso'; font-size:22px;}</style>"]];
            }
            
            NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                    initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                    options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                    documentAttributes: nil
                                                    error: nil
                                                    ];
            
            
            NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
            NSRange range = (NSRange){0,[newString length]};
            [newString enumerateAttribute:NSFontAttributeName inRange:range options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
                UIFont *replacementFont =  [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
                UIColor *color = [UIColor whiteColor];
                [newString addAttribute:NSForegroundColorAttributeName value:color range:range];
                // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
            }];
            
            
            _aboutTxtVw.attributedText= newString; //attributedString;
            
            // [_aboutTxtVw sizeToFit];
            
            
            _aboutTxtVw.showsVerticalScrollIndicator=NO;
            _aboutTxtVw.editable = NO;
            _aboutTxtVw.dataDetectorTypes = UIDataDetectorTypeLink;
            _aboutTxtVw.userInteractionEnabled=YES;
            _aboutTxtVw.selectable=YES;
            
            _aboutVw.frame = [UIScreen mainScreen].bounds;
            [self.view addSubview:_aboutVw];
            
        }
    }
    
}

-(void)showTermsPageFromLocal
{
    termsArr = [staticPages objectAtIndex:1] ;
    
     _titleTerms.text = [termsArr valueForKey:@"title"];
    
    if([termsArr count]>0)
        
    {
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentJustified;
    style.firstLineHeadIndent = 10.0f;
    style.headIndent = 10.0f;
    style.tailIndent = -10.0f;
    
    // NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:lblText.text attributes:@{ NSParagraphStyleAttributeName : style}];
    
    
    NSString *htmlString=[termsArr valueForKey:@"page_content"];
    
    
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        // float widdth =  [UIScreen mainScreen].bounds.size.width-20 ;
        
        
        
        htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<html><head><style>img{max-width:50%%;height:auto !important;width:auto !important;};</style></head><style>body{font-family: 'SanFranciso'; font-size:22px;}</style>"]];//max-width: 100%; width: auto; height: auto;
    }
    else
    {
        
        htmlString = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: 'SanFranciso'; font-size:22px;}</style>"]];
    }
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    
    NSMutableAttributedString *newString = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
    NSRange range = (NSRange){0,[newString length]};
    [newString enumerateAttribute:NSFontAttributeName inRange:range options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
        UIFont *replacementFont =  [UIFont systemFontOfSize:16.0 weight:UIFontWeightSemibold];
        UIColor *color = [UIColor whiteColor];
        [newString addAttribute:NSForegroundColorAttributeName value:color range:range];
        // [newString addAttribute:NSFontAttributeName value:replacementFont range:range];
    }];
    
    
    _termsTxtVw.attributedText= newString;//attributedString;
    
    // [_aboutTxtVw sizeToFit];
    
    
    _termsTxtVw.showsVerticalScrollIndicator=NO;
    _termsTxtVw.editable = NO;
    _termsTxtVw.dataDetectorTypes = UIDataDetectorTypeLink;
    _termsTxtVw.userInteractionEnabled=YES;
    _termsTxtVw.selectable=YES;
    
    _termsVw.frame = [UIScreen mainScreen].bounds;
    [self.view addSubview:_termsVw];
        
    }
}

-(void)helpBtnAction
{
    popUpVideoViewController *popVC = [[popUpVideoViewController alloc]initWithNibName:@"popUpVideoViewController" bundle:nil];
    //[self.navigationController pushViewController:homeVC animated:YES];
   
    self.definesPresentationContext = YES; //self is presenting view controller
    popVC.view.backgroundColor = [UIColor clearColor];
    popVC.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:popVC animated:NO completion:nil];
    
    return;
    
    UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
    [pvc presentViewController:popVC animated: NO completion:nil];
}


# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
