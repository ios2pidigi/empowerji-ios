//
//  newsTableViewCell.h
//  ssc
//
//  Created by swaroop on 07/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface newsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *newsHdr;
@property (strong, nonatomic) IBOutlet UILabel *newsPrevw;
@property (strong, nonatomic) IBOutlet UIImageView *cellBgImg;

@end
