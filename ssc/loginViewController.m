//
//  loginViewController.m
//  ssc
//
//  Created by swaroop on 05/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "loginViewController.h"
#import "signupViewController.h"
#import "forgotPassViewController.h"
#import "homeViewController.h"
#import "Constants.h"
#import "SVProgressHUD.h"
#import "connectivity.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>

#import "GAI.h"
#import "GAIDictionaryBuilder.h"

#import "languagesViewController.h"
#import "sideMenuViewController.h"
#import "UIView+Toast.h"

@interface loginViewController ()
{
    NSString *langIdLogIn;
}
@end

@implementation loginViewController
@synthesize isfrmHome,isfrmInapp;

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  NSString *lang = [[NSUserDefaults standardUserDefaults] objectForKey:@"language"];
    
   // NSLog(@"language :%@",lang);
    
     NSString *lang = langGlobal;
    
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    
    languageBundle = [NSBundle bundleWithPath:languageFilepath];
    
    NSString *theString = lang;   // The one we want to switch on
    NSArray *items = @[@"other", @"no", @"hi",  @"mr-IN", @"gu-IN", @"en" ];
    NSInteger item = [items indexOfObject:theString];
    switch (item) {
        case 0:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 1:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 2:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 3:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 4:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        case 5:
            langIdLogIn = [NSString stringWithFormat:@"%ld",(long)item];
            break;
        default:
            break;
    }
    
    
   // langIdLogIn
    
  //  [[NSBundle mainBundle] loadNibNamed:@"loginViewController" owner:self options:nil];
    
    [SVProgressHUD setDefaultStyle:(SVProgressHUDStyleDark)];
    [SVProgressHUD setDefaultMaskType:(SVProgressHUDMaskTypeCustom)];
    
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    
    [SVProgressHUD setMaximumDismissTimeInterval:10.0];
    [SVProgressHUD setMinimumDismissTimeInterval:3];
    
    //[_showPassBtn setImage:[UIImage imageNamed:@"icon_visibilityoff.png"] forState:(UIControlStateNormal)];
    
    _showPassImg.image = [UIImage imageNamed:@"icon_visibilityoff"];
    
    [_keepMeSignedInBtn setImage:[UIImage imageNamed:@"checkbox.png"] forState:(UIControlStateNormal)];
    
    
    _emailTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _passTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    
    _emailTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _emailTF.layer.borderWidth= 1.0f;
    
    _passTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _passTF.layer.borderWidth= 1.0f;
    
    if(IS_IPAD)
    {
        //_showPassBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10,0,10);
        //UIEdgeInsetsMake(_showPassBtn.frame.size.height/2 - 62/2, _showPassBtn.frame.size.width/2 - 71/2, _showPassBtn.frame.size.height/2 - 62/2, _showPassBtn.frame.size.width/2 - 71/2);
    }
    
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"saveloginInfo"])
    {
        NSLog(@"userLoginInfo :%@ %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userLoginEmail"],[[NSUserDefaults standardUserDefaults]objectForKey:@"userLoginPassword"]);
        
        _emailTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userLoginEmail"];
        
        _passTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userLoginPassword"];
    }
    
    
    [FIRAnalytics logEventWithName:@"Pages"
                        parameters:@{
                                     @"name": @"login"
                                     
                                     }];
    
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:analyticsID];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"pages"
                                                          action:@"Login"
                                                           label:@"New"
                                                           value:nil] build]];
    
    
    [_btnLang addTarget:self action:@selector(langSel) forControlEvents:UIControlEventTouchUpInside];
    
    [_backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSignUP addTarget:self action:@selector(signUpAction:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnSignIn.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnSignIn.clipsToBounds = YES;
    _btnSignIn.layer.borderWidth = 1.0;
    
    _emailTF.layer.cornerRadius = 5.0;
    _passTF.layer.cornerRadius = 5.0;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if(isfrmHome)
    {
        _backBtn.hidden = NO;
        _backImg.hidden = NO;
        
    }
    else
    {
        _backBtn.hidden = YES;
        _backImg.hidden = YES;
    }
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
    UITouch *touch = [touches anyObject];
    if(![touch.view isKindOfClass:[UITextField class]])
    {
        [self.view endEditing:YES];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    _btnSignIn.layer.cornerRadius = _btnSignIn.frame.size.height/2;

    _btnSignUP.layer.cornerRadius = _btnSignUP.frame.size.height/2;
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
}

# pragma mark WebService Calls

-(void)loginWebservice
{
    [SVProgressHUD show];
    
    NSString *emailTf;
     emailTf = [_emailTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    // nameTf = [_nameTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSString *token;
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"])
    {
    token = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"];
    
    token = [token stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    NSInteger langid = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langid = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@sign_in.php?username=%@&password=%@&lang_id=%@&device_id=%@",baseUrl,emailTf,_passTF.text,langIdLogIn,currentDeviceId];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self loginWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *str = [[[json objectForKey:@"result"]objectAtIndex:0] valueForKey:@"login_status"];
          
            if([str caseInsensitiveCompare:@"success"] == NSOrderedSame )
            {
                NSString *support , *keepGoing;
                NSString *langid;
                
                //lang_tblvw
                langid = [NSString stringWithFormat:@"%@",[[[json objectForKey:@"result"]objectAtIndex:0] valueForKey:@"lang_id"]];
                
                [[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"langIDtoCat"];
                //save language selected locally
                NSString *selLang;
                
                NSInteger i = [langid integerValue];
                switch (i) {
                    case 0:
                        selLang = @"en";
                        break;
                    case 1:
                        selLang = @"en";
                        break;
                    case 2:
                        selLang = @"hi";
                        break;
                    case 3:
                        selLang = @"mr-IN";
                        break;
                    case 4:
                        selLang = @"gu-IN";
                        break;
                    case 5:
                        selLang = @"en";
                        break;
                    default:
                        break;
                }
                if(selLang)
                    [[NSUserDefaults standardUserDefaults] setObject:selLang forKey:@"language"];
                
                
                support = [[[json objectForKey:@"result"]objectAtIndex:0] valueForKey:@"support"];
                
                keepGoing = [[[json objectForKey:@"result"]objectAtIndex:0] valueForKey:@"keep_going"];
                
                
                if([support caseInsensitiveCompare:@"yes"]==NSOrderedSame)
                {
                   [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"supportClickedHereButton"];
                }
                if([keepGoing caseInsensitiveCompare:@"yes"]==NSOrderedSame)
                {
                    [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"keepgoingClickedHereButton"];
                }
                
                
                [[NSUserDefaults standardUserDefaults]setObject:_emailTF.text forKey:@"userEmail"];
                
                [[NSUserDefaults standardUserDefaults]setObject:_passTF.text forKey:@"passwordText"];
                
                NSLog(@"loginDetails :%@",[json objectForKey:@"result"]);
                
               
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:[json objectForKey:@"result"]] forKey:@"userData"];
                [def synchronize];
                
                //token for push services
  
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"])
                    [self saveTokenWS]; //save token
                
                if([_keepMeSignedInBtn.currentImage isEqual:[UIImage imageNamed:@"checkbox-checked.png"]])
                {
                   // [[NSUserDefaults standardUserDefaults]setObject:@"KMLG" forKey:@"keepmeloggedin"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:@"saveloginInfo" forKey:@"saveloginInfo"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:_passTF.text forKey:@"userLoginPassword"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:emailTf forKey:@"userLoginEmail"];
                }
                else
                {
                    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"keepmeloggedin"];
                    
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"saveloginInfo"];
                }
                
               ///////
                /////this is for showing feeds from "my selection" button upon Login(cat_ids are from server rather than from local)
                
               NSArray *chkArr = [[NSArray alloc]init];
                NSArray *arrstr = [[json objectForKey:@"result"]valueForKey:@"cat_ids"];
                
                NSString * str = [arrstr componentsJoinedByString:@""];
                
                chkArr = [str componentsSeparatedByString:@","];
                
                
                NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                NSFileManager * fileManager = [NSFileManager defaultManager];
                //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                if([fileManager fileExistsAtPath:finalPath])
                {
                    NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                    
                    [plistDict setValue:chkArr forKey:@"checkArray"];
                    [plistDict writeToFile:finalPath atomically:YES];
                }
             /////////////
                
            homeViewController *homeVC = [[homeViewController alloc]initWithNibName:@"homeViewController" bundle:nil];
           // [self.navigationController pushViewController:homeVC animated:YES];
               UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:homeVC];
                homeVC.isFrmLogin = YES;
                if(isfrmInapp)
                    homeVC.isFrmInAPP = YES;
                
                navigationController.navigationBarHidden=YES;
                
                [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
                
                
            }
            else
            {
                if(isfrmHome)
                {
                    [self.view makeToast:[[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"message"]
                                duration:3.0
                                position:CSToastPositionBottom];
                }
                else
                [self showMessage:[[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"message"]
                        withTitle:@"Error"];
            }
            
            [SVProgressHUD dismiss];
        });
    }];
    
    [dataTask resume];
    
}

//save token
-(void)saveTokenWS
{
    int randIntgr = arc4random_uniform(10000);
    
    //[self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *userid = [NSString stringWithFormat:@"%@",[[arrUser objectAtIndex:0] valueForKey:@"user_id"]];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"];
    
    token = [token stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
   
    /* save_token.php?user_id=150&token=d0p_RE9zG_o:APA91bFPDu-tbWoeaSNZTTSkLtjelz7wYgJq6uL9XXkqqFubkvHq8mSP9pfCF4fZv9doazkWItnLP7Pk97mD8raacu_A8NAlHt2ZsSAL-46GWcQPLCzyk19Elp24OkKPY83b9HJK5GLB&device_id=9e9e11f98bb75e23&device=android
     */
    
    
    NSString *langid;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"langIDtoCat"])
        langid = [[NSUserDefaults standardUserDefaults]objectForKey:@"langIDtoCat"];
    else
        langid = @"5";
    
    NSString *wUrl=[NSString stringWithFormat:@"%@save_token.php?user_id=%@&token=%@&device_id=%@&device=ios&counts=%d&lang_id=%@",baseUrl,userid,token,currentDeviceId,randIntgr,langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self saveTokenWS];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json token save:%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
        });
    }];
    
    [dataTask resume];
    
}


# pragma mark Button Actions

- (IBAction)showPassAction:(id)sender
{
    if([_showPassImg.image isEqual:[UIImage imageNamed:@"icon_visibilityoff"]])
    {
        _passTF.secureTextEntry = false;
        //[_showPassBtn setImage:[UIImage imageNamed:@"icon_visibilityon"] forState:(UIControlStateNormal)];
         _showPassImg.image = [UIImage imageNamed:@"icon_visibilityon"];
    }
    else
    {
        _passTF.secureTextEntry = true;
       // [_showPassBtn setImage:[UIImage imageNamed:@"icon_visibilityoff.png"] forState:(UIControlStateNormal)];
         _showPassImg.image = [UIImage imageNamed:@"icon_visibilityoff"];
    }
}

- (IBAction)keepMeLoggedinAction:(id)sender
{
    if([_keepMeSignedInBtn.currentImage isEqual:[UIImage imageNamed:@"checkbox.png"]])
    {
        [_keepMeSignedInBtn setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:(UIControlStateNormal)];
        
        // [[NSUserDefaults standardUserDefaults]setObject:@"KMLG" forKey:@"keepmeloggedin"];
        
    }
    else
    {
        [_keepMeSignedInBtn setImage:[UIImage imageNamed:@"checkbox.png"] forState:(UIControlStateNormal)];
    }
}
- (IBAction)signInAction:(id)sender
{
   [self.view endEditing:YES];
    
    if (!([_emailTF.text length] == 0)) {
        
        BOOL stricterFilter = YES;
        NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
        NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        BOOL myStringMatchesRegEx = [emailTest evaluateWithObject:_emailTF.text];
        
        myStringMatchesRegEx=YES;
        
        if (myStringMatchesRegEx) {
            if (!([_passTF.text length] == 0))
            {
                NSLog(@"loginWebService");
                
                NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
                
                [SVProgressHUD show];
                
                // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
                
                [[[NSURLSession sharedSession]
                  dataTaskWithURL:[NSURL URLWithString:wUrl]
                  completionHandler:^(NSData *urlData,
                                      NSURLResponse *response,
                                      NSError *error)
                  {
                      // handle response
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                          
                          
                          
                          
                          if([urlData length] == 0)
                          {
                              NSLog(@"internet is not connected");
                              
                              
                              [self showMessage:errorMessage withTitle:nil];
                          }
                          else
                          {
                              NSLog(@"internet is connected");
                              
                              
                if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                {
                    //connection unavailable
                    
                    [self showMessage:errorMessage withTitle:nil];
                }
                else
                {
                    //connection available
                    
                    [self loginWebservice];
                }
                
                          }
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              //[self.view makeToastActivity:CSToastPositionCenter];
                              [SVProgressHUD dismiss];
                          });
                          
                          
                      });
                      
                      
                  }] resume];

                
            }
            else
            {
                NSString *lang = langGlobal;
                
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                
                NSString *message = [languageBundle localizedStringForKey:@"passwordEmpty" value:@"" table:@"strings-english"];
                
                NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                
                
                [self showMessage:message withTitle:error];
                
               // [self showMessage:@"Please Enter your password" withTitle:@"Error"];
            }
        }
        else
        {
            NSString *lang = langGlobal;
            
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            
            NSString *message = [languageBundle localizedStringForKey:@"emailValid" value:@"" table:@"strings-english"];
            
            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
            
            
            [self showMessage:message withTitle:error];
            
           // [self showMessage:@"Please Enter a valid email id" withTitle:@"Error"];
        }
    }
    else
    {
        NSString *lang = langGlobal;
        
        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        
        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
        
        NSString *message = [languageBundle localizedStringForKey:@"emailEmptyLogin" value:@"" table:@"strings-english"];
        
        NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
        
        
        [self showMessage:message withTitle:error];
        
        //[self showMessage:@"Please Enter your email id" withTitle:@"Error"];
    }
    
   
}
- (IBAction)signUpAction:(id)sender
{
    [self.view endEditing:YES];
    
    _emailTF.text = @""; _passTF.text = @"";
    
    signupViewController *signupVC = [[signupViewController alloc]initWithNibName:@"signupViewController" bundle:nil];
   
    if(isfrmInapp)
    {
        signupVC.isfrmInapp = YES;
    }
    
    if(isfrmHome)
    {
        signupVC.isFrmHomeLogin = YES;
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        
        if([pvc isKindOfClass:[sideMenuViewController class]])
        {
          [self presentViewController: signupVC animated: NO completion:nil];
        }
        else
       // [pvc presentViewController: signupVC animated: NO completion:nil];
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:signupVC animated:YES completion:nil];
            });
        }
        
    }
    else
    {
        [self.navigationController pushViewController:signupVC animated:YES];
    }
    
}
- (IBAction)forgotPassAction:(id)sender
{
    [self.view endEditing:YES];
    
    forgotPassViewController *fpVC = [[forgotPassViewController alloc]initWithNibName:@"forgotPassViewController" bundle:nil];
    
    if(isfrmHome)
    {
        fpVC.isFrmHom = YES;
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        
        if([pvc isKindOfClass:[sideMenuViewController class]])
        {
            [self presentViewController: fpVC animated: NO completion:nil];
        }
        else
            [pvc presentViewController: fpVC animated: NO completion:nil];
    }
    else
    {
        
    [self.navigationController pushViewController:fpVC animated:YES];
    }
}

-(void)langSel
{
    [self.view endEditing:YES];
    
    languagesViewController *lVC = [[languagesViewController alloc]initWithNibName:@"languagesViewController" bundle:nil];
    
    if(isfrmHome)
    {
        lVC.isFrmHome = YES;
        
        UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
        UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
        
        if([pvc isKindOfClass:[sideMenuViewController class]])
        {
            [self presentViewController: lVC animated: NO completion:nil];
        }
        else
            [pvc presentViewController: lVC animated: NO completion:nil];
    }
    else
    {
       [self.navigationController pushViewController:lVC animated:YES];
    }
}

-(void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.returnKeyType == UIReturnKeyNext) {
        // tab forward logic here
        if(textField.tag==0)
        {
            [_emailTF resignFirstResponder];
            [_passTF becomeFirstResponder];
            return YES;
        }
        
    }
    else if (textField.returnKeyType == UIReturnKeyGo) {
       
            [self signInAction:nil];
            [_passTF resignFirstResponder];
            return YES;
    }
    
    return NO;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
    
}


# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
   // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
   // [vc presentViewController:alert animated:YES completion:nil];
    
    UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
    
    if([pvc isKindOfClass:[sideMenuViewController class]])
    {
        [self presentViewController: alert animated: NO completion:nil];
    }
    else
        [pvc presentViewController: alert animated: NO completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
