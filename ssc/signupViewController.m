//
//  signupViewController.m
//  ssc
//
//  Created by swaroop on 06/04/18.
//  Copyright © 2018 Pi Digi-Logical Solutions. All rights reserved.
//

#import "signupViewController.h"
#import "Constants.h"
#import "homeViewController.h"
#import "SVProgressHUD.h"
#import "connectivity.h"
#import "sideMenuViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface signupViewController ()
{
    NSMutableArray *langArr;
    
    NSArray *ageArr;
    
     NSString *langid;
}
@end

@implementation signupViewController
@synthesize isFrmHomeLogin,isfrmInapp;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self languagesWS];
    
    [_termsBtn setImage:[UIImage imageNamed:@"checkbox.png"] forState:(UIControlStateNormal)];
    
    [_btnSelAge addTarget:self action:@selector(ageSelAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnSelLang addTarget:self action:@selector(langSelAction) forControlEvents:UIControlEventTouchUpInside];
    
  //left padding to txtfield
    _nameTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _addTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _mobTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _passwrdTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
     _cityTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
     _ageTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
     _langTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
     _otherTF.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
    _nameTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _nameTF.layer.borderWidth= 1.0f;
    
    _addTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _addTF.layer.borderWidth= 1.0f;
    
    _mobTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _mobTF.layer.borderWidth= 1.0f;
    
    _passwrdTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _passwrdTF.layer.borderWidth= 1.0f;
    
    _cityTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _cityTF.layer.borderWidth= 1.0f;
    
    _ageTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _ageTF.layer.borderWidth= 1.0f;
    
    _langTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _langTF.layer.borderWidth= 1.0f;
    
    _otherTF.layer.borderColor=[[UIColor whiteColor]CGColor];
    _otherTF.layer.borderWidth= 1.0f;
    
    
    ageArr = [[NSArray alloc]initWithObjects:@"< 50 ",@"50 - 65",@"65 - 75",@"above 75", nil];
    
    [_ageTblVw reloadData];
    
    
    // [_showPassBtn setImage:[UIImage imageNamed:@"icon_visibilityoff.png"] forState:(UIControlStateNormal)];
    
    _showPassImg.image = [UIImage imageNamed:@"icon_visibilityoff"];
    
    if(IS_IPAD)
    {
        //_showPassBtn.imageEdgeInsets = UIEdgeInsetsMake(0,10,0,10);
    }
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    [self.bgImage addGestureRecognizer:tapRecognizer];
    
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"langidFromLanguages"] )
    langid = [[NSUserDefaults standardUserDefaults]objectForKey:@"langidFromLanguages"];
    
    [_showTerms addTarget:self action:@selector(showTermsAndPoliciesPop:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    
    //Add gradient to view
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _bgImage.bounds;
    
    
    // gradient.colors = @[(id)Rgb2UIColor(244, 244, 244), (id)Rgb2UIColor(202, 202, 202), (id)Rgb2UIColor(160, 160, 160)];
    
    gradient.colors = @[(id)gradientColor1, (id)gradientColor2];
    
    [_bgImage.layer insertSublayer:gradient atIndex:0];
    
    CGRect frame = _ageTblVw.frame;
    frame.size.height = _ageTblVw.contentSize.height;
    frame.origin.y = _ageTF.frame.origin.y - _ageTblVw.contentSize.height;
    _ageTblVw.frame = frame;
    
    CGRect frame2 = _langTblVw.frame;
    frame2.size.height = _langTblVw.contentSize.height;
    frame2.origin.y = _langTF.frame.origin.y - _langTblVw.contentSize.height;
    //_langTblVw.frame = frame2;
    
}

# pragma mark touch event handler
//to dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    
    UITouch *touch = [touches anyObject];
    if(![touch.view isKindOfClass:[UITextField class]])
    {
        [self.view endEditing:YES];
    }
}

- (void)hideKeyboard:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    NSLog(@"%ld", (long)view.tag);
    
    [self.view endEditing:YES];
    
    _langTblVw.hidden = YES;
    _ageTblVw.hidden = YES;
}


# pragma mark WebService Calls

-(void)signupWebservice
{
    //NSString *feedTxtVwText = [_feedTxtVw.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    //app.empowerji.com/webservice2/sign_up.php?full_name=test&email=test@mail.com&mobile_number=1111111111&password=123456&lang_id=5&city=cityname&age_group=50to55&language_other=malayalam
    
    [SVProgressHUD show];
    
    NSString *nameTf,*emailTf,*mobTf,*passTf ,*cityTf,*ageTf,*langTf,*otherTf;
    
     nameTf = [_nameTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    emailTf = [_addTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    mobTf = [_mobTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    passTf = [_passwrdTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    cityTf = [_cityTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    ageTf = [_ageTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    langTf = [_langTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    otherTf = [_otherTF.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString *langid = [[NSUserDefaults standardUserDefaults]objectForKey:@"langid"];
    //full_name=test&email=test@mail.com&mobile_number=1111111111&password=123456&lang_id=5&city=cityname&age_group=50to55&language_other=malayalam
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    NSString *wUrl=[NSString stringWithFormat:@"%@sign_up.php?full_name=%@&email=%@&mobile_number=%@&password=%@&lang_id=%@&city=%@&age_group=%@&language_other=%@&device_id=%@",baseUrl,nameTf,emailTf,mobTf,passTf,langid,cityTf,ageTf,otherTf,currentDeviceId];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 20.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSDictionary *json;
        
        if(data ==nil)
        {
            [self signupWebservice];
            return ;
        }
        
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"json :%@", json);
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *str = [[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"signup_status"];
            
            if([str caseInsensitiveCompare:@"success"] == NSOrderedSame )
            {
                NSString *userid = [[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"user_id"];
                
                [[NSUserDefaults standardUserDefaults]setObject:userid forKey:@"useridForToken"];
                
                NSString *catids = [[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"cat_ids"];
                
                [self.view endEditing:YES];
                
                [[NSUserDefaults standardUserDefaults]setObject:mobTf forKey:@"userMobileNum"];
                
                
                //save language selected locally
                NSString *selLang;
                
                NSInteger i = [langid integerValue];
                switch (i) {
                    case 0:
                        selLang = @"en";
                        break;
                    case 1:
                        selLang = @"en";
                        break;
                    case 2:
                        selLang = @"hi";
                        break;
                    case 3:
                        selLang = @"mr-IN";
                        break;
                    case 4:
                        selLang = @"gu-IN";
                        break;
                    case 5:
                        selLang = @"en";
                        break;
                    default:
                        break;
                }
                if(selLang)
                    [[NSUserDefaults standardUserDefaults] setObject:selLang forKey:@"language"];
                
                [[NSUserDefaults standardUserDefaults]setInteger:i forKey:@"languageIDMain"];
                ///////Auto Login upon successfull signup
                
                NSArray * userDetailsArray = [[NSArray alloc]init];
                userDetailsArray = @[ @{@"name": nameTf, @"user_id": userid, @"mobile_number": mobTf, @"cat_ids": catids}];
                
                NSLog(@"testArray :%@",userDetailsArray);
                
                
                NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                [def setObject:[NSKeyedArchiver archivedDataWithRootObject:userDetailsArray] forKey:@"userData"];
                [def synchronize];
                
                
                [[NSUserDefaults standardUserDefaults]setObject:@"KMLG" forKey:@"keepmeloggedin"];
                
               // [[NSUserDefaults standardUserDefaults]setObject:emailTf forKey:@"userEmail"];
                [[NSUserDefaults standardUserDefaults]setObject:mobTf forKey:@"userEmail"];
                
                [[NSUserDefaults standardUserDefaults]setObject:emailTf forKey:@"userEmail2"];
                
                
                [[NSUserDefaults standardUserDefaults]setObject:passTf forKey:@"passwordText"];
                
                
                ///////
                /////this is for showing feeds from "my selection" button upon SignUp(cat_ids are from server rather than from local)
                
                NSArray *chkArr = [[NSArray alloc]init];
                NSArray *arrstr = [[json objectForKey:@"result"]valueForKey:@"cat_ids"];
                
                NSString * str = [arrstr componentsJoinedByString:@""];
                
                chkArr = [str componentsSeparatedByString:@","];
                
                
                NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
                NSString * finalPath = [basePath stringByAppendingPathComponent:@"LocalData.plist"];
                NSFileManager * fileManager = [NSFileManager defaultManager];
                //NSMutableArray* myMutableArrayAgain = [NSMutableArray new];
                if([fileManager fileExistsAtPath:finalPath])
                {
                    NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:finalPath];
                    
                    [plistDict setValue:chkArr forKey:@"checkArray"];
                    [plistDict writeToFile:finalPath atomically:YES];
                }
                /////////////
                
                NSString *lang = langGlobal;
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                NSString *message = [languageBundle localizedStringForKey:@"signUpSuccAlert" value:@"" table:@"strings-english"];
                
                NSString *alertText2 = message;
                
                NSString *alertText = [[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"message"];
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:[NSString stringWithFormat:@"%@. %@",alertText,alertText2]
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
                    //push to login view and clear all the textfieldValues
                    
            _nameTF.text = @"";_addTF.text=@"";_mobTF.text=@"";_passwrdTF.text=@"";
            _cityTF.text = @"";_ageTF.text = @"";_langTF.text = @"";_otherTF.text = @"";
                    
                    [self.view endEditing:YES];
                    [self back:nil];
                    
                    
                    ///push to home (AutoLogin)
                    homeViewController *homeVC = [[homeViewController alloc]initWithNibName:@"homeViewController" bundle:nil];
                    // [self.navigationController pushViewController:homeVC animated:YES];
                    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:homeVC];
                    
                    if(isfrmInapp)
                    homeVC.isFrmInAPP = YES;
                    
                    navigationController.navigationBarHidden=YES;
                    
                    [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
                    /////
                    
                    
                }];
                [alert addAction:okAction];
               // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
              //  [vc presentViewController:alert animated:YES completion:nil];
                UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
                UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
                
                if([pvc isKindOfClass:[sideMenuViewController class]])
                {
                    [self presentViewController: alert animated: NO completion:nil];
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }
               // [pvc presentViewController: alert animated: NO completion:nil];
                
                //sent token from signup
                
                if([[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"])
                    [self saveTokenWS];
                
               // [self showMessage:[[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"signup_status"] withTitle:@"Success"];
            }
            else
                [self showMessage:[[[json objectForKey:@"result"]objectAtIndex:0]valueForKey:@"message"]
                        withTitle:@"Error"];
            
            [SVProgressHUD dismiss];
            
        });
    }];
    
    [dataTask resume];
    
}

//save token
-(void)saveTokenWS
{
    int randIntgr = arc4random_uniform(10000);
    
    //[self.view makeToastActivity:CSToastPositionCenter];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *data = [def objectForKey:@"userData"];
    NSArray *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSArray *arrUser = [[NSArray alloc] initWithArray:retrievedDictionary];
    
    NSString *userid = [NSString stringWithFormat:@"%@",[[arrUser objectAtIndex:0] valueForKey:@"techid"]];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsToken"];
    
    token = [token stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"useridForToken"])
        userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"useridForToken"]];
    
    NSString *langid;
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"langid"])
    langid = [[NSUserDefaults standardUserDefaults]objectForKey:@"langid"];
    else
        langid = @"5";
    
    NSString *wUrl=[NSString stringWithFormat:@"%@save_token.php?user_id=%@&token=%@&device_id=%@&device=ios&counts=%d&lang_id=%@",baseUrl,userid,token,currentDeviceId,randIntgr,langid];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
     {
         NSDictionary *json;
         
         if(data ==nil)
         {
             [self saveTokenWS];
             return ;
         }
         
         json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
         
         NSLog(@"json token save:%@", json);
         
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             
         });
     }];
    
    [dataTask resume];
    
}


-(void)languagesWS
{
    //app.empowerji.com/webservice2/languages.php
    
    NSInteger langidd = 5;
    if([[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"])
        langidd = [[NSUserDefaults standardUserDefaults]integerForKey:@"languageIDMain"];
    
    NSString *wUrl = [NSString stringWithFormat:@"%@languages.php?lang_id=%ld",baseUrl,(long)langidd];
    
    NSLog(@"URL :%@", wUrl);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 100.0;
    sessionConfig.requestCachePolicy=NSURLRequestReloadIgnoringCacheData;
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:wUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
   {
       NSDictionary *json;
       
       if(data ==nil)
       {
           //[self feedbackSubmitWS];
           return ;
       }
       
       json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
       
       NSLog(@"json :%@", json);
       
       
       dispatch_async(dispatch_get_main_queue(), ^{
           
           langArr = [json objectForKey:@"result"];
           
           langArr = [langArr mutableCopy];
           
           
           NSDictionary *dict;
           NSString *lang;
           NSString *langid;
           NSString *otherLanguage;
           NSString *language_preference;
           
           for ( int i = 0; i < langArr.count ; i++ )
           {
              langid  = [NSString stringWithFormat:@"%@",[[langArr objectAtIndex:i] valueForKey:@"id"]];
               
               
               if([langid caseInsensitiveCompare:@"0"]==NSOrderedSame)
               {
                   NSString *lang = langGlobal;
                   NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                   NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    language_preference = [languageBundle localizedStringForKey:@"language_preference" value:@"" table:@"strings-english"];
                   
                   otherLanguage = [languageBundle localizedStringForKey:@"other" value:@"" table:@"strings-english"];
                   
                   if(i == 0)
                   lang = [NSString stringWithFormat:@"%@",language_preference];
                   else
                    lang = [NSString stringWithFormat:@"%@",otherLanguage];
                   
                   dict = @{ @"id" : langid, @"language" : lang};
               }
               else
               {
                   lang = [NSString stringWithFormat:@"%@ (%@)",[[langArr objectAtIndex:i] valueForKey:@"reg_language"],[[langArr objectAtIndex:i] valueForKey:@"language"]];
                   
                   dict = @{ @"id" : langid, @"language" : lang};
               }
               
               
               [langArr replaceObjectAtIndex:i withObject:dict];
           }
           
           
           NSString *langIDFrmL = [[NSUserDefaults standardUserDefaults]objectForKey:@"langidFromLanguages"];
           
           for(int i=0; i< langArr.count ; i++)
           {
               NSString *langID = [[langArr objectAtIndex:i]valueForKey:@"id"];
            if([langIDFrmL caseInsensitiveCompare:langID]==NSOrderedSame)
               {
                   NSString *langText = [[langArr objectAtIndex:i]valueForKey:@"language"];
                  _langTF.text = langText;
               }
           }
           
           [_langTblVw reloadData];
           
           CGFloat heightt;
           if(IS_IPAD)
           {
               heightt  = 40;
               
               CGRect frame = _langTblVw.frame;
               frame.size.height = _langTblVw.contentSize.height;
               frame.origin.y = _langTF.frame.origin.y - _langTblVw.contentSize.height;
               _langTblVw.frame = frame;
           }
           else
           {
               heightt  = 25;
               
               CGRect frame = _langTblVw.frame;
               frame.size.height = heightt *langArr.count;
               frame.origin.y = _langTF.frame.origin.y - (heightt *langArr.count);
               _langTblVw.frame = frame;
           }
           
       });
   }];
    
    [dataTask resume];
}

# pragma mark Button Actions

- (IBAction)showPassAction:(id)sender
{
    if([_showPassImg.image isEqual:[UIImage imageNamed:@"icon_visibilityoff"]])
    {
        _passwrdTF.secureTextEntry = false;
       // [_showPassBtn setImage:[UIImage imageNamed:@"icon_visibilityon"] forState:(UIControlStateNormal)];
        _showPassImg.image = [UIImage imageNamed:@"icon_visibilityon"];
    }
    else
    {
        _passwrdTF.secureTextEntry = true;
        //[_showPassBtn setImage:[UIImage imageNamed:@"icon_visibilityoff.png"] forState:(UIControlStateNormal)];
        _showPassImg.image = [UIImage imageNamed:@"icon_visibilityoff"];
    }
}


- (IBAction)agreeTermsAction:(id)sender
{
    if([_termsBtn.currentImage isEqual:[UIImage imageNamed:@"checkbox.png"]])
    {
        [_termsBtn setImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:(UIControlStateNormal)];
        
        
    }
    else
    {
       // [_termsBtn setImage:[UIImage imageNamed:@"checkbox.png"] forState:(UIControlStateNormal)];
    }
}
- (IBAction)viewTermsPopUp:(id)sender
{
    
}

- (IBAction)createAccAction:(id)sender
{
    NSString *wUrl=[NSString stringWithFormat:@"https://app.empowerji.com/webservice/ping.php"];
    
    [SVProgressHUD show];
    
    // NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:wUrl]];
    
    [[[NSURLSession sharedSession]
      dataTaskWithURL:[NSURL URLWithString:wUrl]
      completionHandler:^(NSData *urlData,
                          NSURLResponse *response,
                          NSError *error)
      {
          // handle response
          dispatch_async(dispatch_get_main_queue(), ^{
            
              if([urlData length] == 0)
              {
                  NSLog(@"internet is not connected");
                  
                  
                  [self showMessage:errorMessage withTitle:nil];
              }
              else
              {
                  NSLog(@"internet is connected");
                  
                  
    if((!([_nameTF.text length] == 0)))
    {
        if (([_addTF.text length] != 0))
        {
        
        BOOL stricterFilter = YES;
        NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
        NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        BOOL myStringMatchesRegEx = [emailTest evaluateWithObject:_addTF.text];
        
        //myStringMatchesRegEx=YES;
        
        if (myStringMatchesRegEx) {
            
            
            if (!([_mobTF.text length] == 0))
            {
                BOOL valid;
                NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
                NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:_mobTF.text];
                valid = [alphaNums isSupersetOfSet:inStringSet];
                //if (!valid) // Not numeric
                    
                if(([_mobTF.text length] == 10) && valid)
                {
                    
                if (!([_passwrdTF.text length] == 0))
                {
                    if([_passwrdTF.text length] >= 6)
                    {
                        if(!([_cityTF.text length] == 0))
                        {
                            if(!([_ageTF.text length] == 0))
                            {
                        
                              if(!([_langTF.text length] == 0))
                              {
                                if([_langTF.text caseInsensitiveCompare:@"other"]!=NSOrderedSame)
                                    {
                                        
                        if([_termsBtn.currentImage isEqual:[UIImage imageNamed:@"checkbox-checked.png"]])
                        {
                            if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                            {
                                //connection unavailable
                                
                                [self showMessage:errorMessage withTitle:nil];
                            }
                            else
                            {
                                [self signupWebservice];
                            }
                        }
                        else
                        {
                            
                            NSString *lang = langGlobal;
                            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                            NSString *message = [languageBundle localizedStringForKey:@"agree" value:@"" table:@"strings-english"];
                             NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                            
                            [self showMessage:message withTitle:error];
                        }
                              }
                                  else
                                  {
                                      if([_langTF.text caseInsensitiveCompare:@"other"]==NSOrderedSame)
                                      {
                                          if(_otherTF.text.length > 0)
                                          {
                                              {
                                                  
                                                  if([_termsBtn.currentImage isEqual:[UIImage imageNamed:@"checkbox-checked.png"]])
                                                  {
                                                      if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                                                      {
                                                          //connection unavailable
                                                          
                                                          [self showMessage:errorMessage withTitle:nil];
                                                      }
                                                      else
                                                      {
                                                          [self signupWebservice];
                                                      }
                                                  }
                                                  else
                                                  {
                                                      NSString *lang = langGlobal;
                                                      NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                                                      NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                                                      NSString *message = [languageBundle localizedStringForKey:@"agree" value:@"" table:@"strings-english"];
                                                      NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                                                      
                                                      [self showMessage:message withTitle:error];
                                                      
                                                    //  [self showMessage:@"Please agree to Terms & Policies" withTitle:@"Error"];
                                                  }
                                              }
                                          }
                                          else
                                          {
                                              NSString *lang = langGlobal;
                                              NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                                              NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                                              NSString *message = [languageBundle localizedStringForKey:@"otherLangEmpty" value:@"" table:@"strings-english"];
                                              NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                                              
                                              [self showMessage:message withTitle:error];
                                              
                                           //[self showMessage:@"Please enter Other language" withTitle:@"Error"];
                                          }
                                      }
                                  }
                                }
                                else
                                {
                                    NSString *lang = langGlobal;
                                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                                    NSString *message = [languageBundle localizedStringForKey:@"selLangEmpty" value:@"" table:@"strings-english"];
                                    NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                                    
                                    [self showMessage:message withTitle:error];
                                    
                                  //[self showMessage:@"Please select language" withTitle:@"Error"];
                                }
                            }
                            else
                            {
                                NSString *lang = langGlobal;
                                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                                NSString *message = [languageBundle localizedStringForKey:@"ageEmpty" value:@"" table:@"strings-english"];
                                NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                                
                                [self showMessage:message withTitle:error];
                                // [self showMessage:@"Please select your age" withTitle:@"Error"];
                            }
                        }
                           
                        else
                        {
                            NSString *lang = langGlobal;
                            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                            NSString *message = [languageBundle localizedStringForKey:@"cityEmpty" value:@"" table:@"strings-english"];
                            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                            
                            [self showMessage:message withTitle:error];
                            
                          // [self showMessage:@"Please enter your city" withTitle:@"Error"];
                        }
                    }
                    else
                    {
                        NSString *lang = langGlobal;
                        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                        NSString *message = [languageBundle localizedStringForKey:@"passwordCharacter" value:@"" table:@"strings-english"];
                        NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                        
                        [self showMessage:message withTitle:error];
                        
                       // [self showMessage:@"Password should be atleast 6 characters" withTitle:@"Error"];
                    }
                    
                }
                else
                {
                    NSString *lang = langGlobal;
                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    NSString *message = [languageBundle localizedStringForKey:@"passwordEmpty" value:@"" table:@"strings-english"];
                    NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                    
                    [self showMessage:message withTitle:error];
                    
                   // [self showMessage:@"Please Enter your password" withTitle:@"Error"];
                }
               
            }
                else
                {
                    //numberValid
                    
                    NSString *lang = langGlobal;
                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    NSString *message = [languageBundle localizedStringForKey:@"numberValid" value:@"" table:@"strings-english"];
                    NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                    
                    [self showMessage:message withTitle:error];
                }
            }
            else
            {
                NSString *lang = langGlobal;
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                NSString *message = [languageBundle localizedStringForKey:@"mobEmpty" value:@"" table:@"strings-english"];
                NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                
                [self showMessage:message withTitle:error];
                
               // [self showMessage:@"Please Enter your mobile number" withTitle:@"Error"];
            }
            
            
       }
        else
        {
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *message = [languageBundle localizedStringForKey:@"emailValid" value:@"" table:@"strings-english"];
            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
            
            [self showMessage:message withTitle:error];
            
            //[self showMessage:@"Please Enter a valid email id" withTitle:@"Error"];
        }
       
     }
    else if (([_addTF.text length] == 0))
    {
        if (!([_mobTF.text length] == 0))
        {
            BOOL valid;
            NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
            NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:_mobTF.text];
            valid = [alphaNums isSupersetOfSet:inStringSet];
            //if (!valid) // Not numeric
            
        if(([_mobTF.text length] == 10) && valid)
        {
                
         if (!([_passwrdTF.text length] == 0))
            {
            if([_passwrdTF.text length] >= 6)
             {
              if(!([_cityTF.text length] == 0))
                {
                if(!([_ageTF.text length] == 0))
                {
                                
                if(!([_langTF.text length] == 0))
                {
                 if([_langTF.text caseInsensitiveCompare:@"other"]!=NSOrderedSame)
                    {
                        if([_termsBtn.currentImage isEqual:[UIImage imageNamed:@"checkbox-checked.png"]])
                        {
                            if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                            {
                                //connection unavailable
                                
                                [self showMessage:errorMessage withTitle:nil];
                            }
                            else
                            {
                                [self signupWebservice];
                            }
                        }
                        else
                        {
                            
                            NSString *lang = langGlobal;
                            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                            NSString *message = [languageBundle localizedStringForKey:@"agree" value:@"" table:@"strings-english"];
                            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                            
                            [self showMessage:message withTitle:error];
                        }
                        
                            }
                        else
                        {
                        if([_langTF.text caseInsensitiveCompare:@"other"]==NSOrderedSame)
                        {
                        if(_otherTF.text.length > 0)
                        {
                         {
                                                    
                         if([_termsBtn.currentImage isEqual:[UIImage imageNamed:@"checkbox-checked.png"]])
                            {
                                if ([[connectivity reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                                {
                                    //connection unavailable
                                    
                                    [self showMessage:errorMessage withTitle:nil];
                                }
                                else
                                {
                                    [self signupWebservice];
                                }
                            }
                        else
                        {
                            NSString *lang = langGlobal;
                            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                            NSString *message = [languageBundle localizedStringForKey:@"agree" value:@"" table:@"strings-english"];
                            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                            
                            [self showMessage:message withTitle:error];
                            
                            //  [self showMessage:@"Please agree to Terms & Policies" withTitle:@"Error"];
                            
                         }
                            }
                             }
                        else
                        {
                            NSString *lang = langGlobal;
                            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                            NSString *message = [languageBundle localizedStringForKey:@"otherLangEmpty" value:@"" table:@"strings-english"];
                            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                            
                            [self showMessage:message withTitle:error];
                            
                            //[self showMessage:@"Please enter Other language" withTitle:@"Error"];
                        }
                         }
                          }
                            }
                else
                    {
                                    NSString *lang = langGlobal;
                                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                                    NSString *message = [languageBundle localizedStringForKey:@"selLangEmpty" value:@"" table:@"strings-english"];
                                    NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                                    
                                    [self showMessage:message withTitle:error];
                                    
                                    //[self showMessage:@"Please select language" withTitle:@"Error"];
                                }
                            }
                            else
                            {
                                NSString *lang = langGlobal;
                                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                                NSString *message = [languageBundle localizedStringForKey:@"ageEmpty" value:@"" table:@"strings-english"];
                                NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                                
                                [self showMessage:message withTitle:error];
                                // [self showMessage:@"Please select your age" withTitle:@"Error"];
                            }
                        }
                        
                        else
                        {
                            NSString *lang = langGlobal;
                            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                            NSString *message = [languageBundle localizedStringForKey:@"cityEmpty" value:@"" table:@"strings-english"];
                            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                            
                            [self showMessage:message withTitle:error];
                            
                            // [self showMessage:@"Please enter your city" withTitle:@"Error"];
                        }
                    }
                    else
                    {
                        NSString *lang = langGlobal;
                        NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                        NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                        NSString *message = [languageBundle localizedStringForKey:@"passwordCharacter" value:@"" table:@"strings-english"];
                        NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                        
                        [self showMessage:message withTitle:error];
                        
                        // [self showMessage:@"Password should be atleast 6 characters" withTitle:@"Error"];
                    }
                    
                }
                else
                {
                    NSString *lang = langGlobal;
                    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                    NSString *message = [languageBundle localizedStringForKey:@"passwordEmpty" value:@"" table:@"strings-english"];
                    NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                    
                    [self showMessage:message withTitle:error];
                    
                    // [self showMessage:@"Please Enter your password" withTitle:@"Error"];
                }
                
            }
            else
            {
                //numberValid
                
                NSString *lang = langGlobal;
                NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
                NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
                NSString *message = [languageBundle localizedStringForKey:@"numberValid" value:@"" table:@"strings-english"];
                NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
                
                [self showMessage:message withTitle:error];
            }
        }
        else
        {
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *message = [languageBundle localizedStringForKey:@"mobEmpty" value:@"" table:@"strings-english"];
            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
            
            [self showMessage:message withTitle:error];
            
            // [self showMessage:@"Please Enter your mobile number" withTitle:@"Error"];
        }
        
            
            
            
    }
   
   
    }
        else
        {
            NSString *lang = langGlobal;
            NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
            NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
            NSString *message = [languageBundle localizedStringForKey:@"nameEmpty" value:@"" table:@"strings-english"];
            NSString *error = [languageBundle localizedStringForKey:@"error" value:@"" table:@"strings-english"];
            
            [self showMessage:message withTitle:error];
            
           // [self showMessage:@"Please Enter your name" withTitle:@"Error"];
        }
                  
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  //[self.view makeToastActivity:CSToastPositionCenter];
                  [SVProgressHUD dismiss];
              });
              
              
          });
          
          
      }] resume];
}

- (IBAction)back:(id)sender
{
    if(isFrmHomeLogin)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)ageSelAction
{
    _ageTblVw.hidden = NO;
    _langTblVw.hidden = YES;
}

-(void)langSelAction
{
    _ageTblVw.hidden = YES;
    _langTblVw.hidden = NO;
}

-(void)showTermsAndPoliciesPop:(UIButton*)sender
{
    NSString *lang = langGlobal;
    NSString *languageFilepath = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
    NSBundle *languageBundle = [NSBundle bundleWithPath:languageFilepath];
    NSString *terms = [languageBundle localizedStringForKey:@"terms" value:@"" table:@"strings-english"];
    
    NSString *policies = [languageBundle localizedStringForKey:@"policies" value:@"" table:@"strings-english"];
    
    
    NSString *cancel = [languageBundle localizedStringForKey:@"cancel" value:@"" table:@"strings-english"];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:terms style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        // Code for terms
        NSURL *url = [NSURL URLWithString:termsUrl];
        
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:NULL];
        }else{
            // Fallback on earlier versions
            [[UIApplication sharedApplication] openURL:url];
        }
        
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:policies style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //Code for policies
        
        NSURL *url = [NSURL URLWithString:privacyUrl];
        
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:NULL];
        }else{
            // Fallback on earlier versions
            [[UIApplication sharedApplication] openURL:url];
        }
        
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:cancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    popPresenter.sourceView = sender;
    popPresenter.sourceRect = sender.bounds; // You can set position of popover
    [self presentViewController:alert animated:TRUE completion:nil];
}

# pragma mark alertView

-(void)showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
    }];
    [alert addAction:okAction];
   // UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
   // [vc presentViewController:alert animated:YES completion:nil];
    
    UIViewController *rvc = [UIApplication sharedApplication].delegate.window.rootViewController;
    UIViewController *pvc = rvc.presentedViewController;  // you may need to loop through presentedViewControllers if you have more than one
    
    if([pvc isKindOfClass:[sideMenuViewController class]])
    {
        [self presentViewController: alert animated: NO completion:nil];
    }
    else
        [pvc presentViewController: alert animated: NO completion:nil];
}


#pragma mark - TextField Delegates

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag == 0)
    {
     [UIView beginAnimations:nil context:nil];
     [UIView setAnimationDuration:.3];
     [UIView setAnimationBeginsFromCurrentState:TRUE];
     self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -200., self.view.frame.size.width, self.view.frame.size.height);
    
     [UIView commitAnimations];
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag == 0)
    {
        
     [UIView beginAnimations:nil context:nil];
     [UIView setAnimationDuration:.3];
     [UIView setAnimationBeginsFromCurrentState:TRUE];
     self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +200., self.view.frame.size.width, self.view.frame.size.height);
    
     [UIView commitAnimations];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


#pragma mark UITableView methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag == 1)
        return langArr.count;
    else if(tableView.tag == 2)
        return ageArr.count;
    else
        return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
    {
        return 40;
    }
    else
        return 25;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;//cellSpacingHeight
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    
    
    NSInteger section = indexPath.section;
    
    if(tableView.tag == 1)
    {
    cell.textLabel.text = [[langArr objectAtIndex:section]valueForKey:@"language"];
    }
    else if(tableView.tag == 2)
    {
        cell.textLabel.text = [ageArr objectAtIndex:section];
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1)
    {
        //lang_tblvw
        NSString *langid = [NSString stringWithFormat:@"%@",[[langArr objectAtIndex:indexPath.section]valueForKey:@"id"]];
        
        [[NSUserDefaults standardUserDefaults]setObject:langid forKey:@"langid"];
        
        _langTF.text = [[langArr objectAtIndex:indexPath.section]valueForKey:@"language"];
        _langTblVw.hidden = YES;
        
        _ageTblVw.hidden = YES;
        
        if([langid caseInsensitiveCompare:@"0"]==NSOrderedSame && (indexPath.section !=0))
        {
            _otherTF.hidden = NO;
            
            [_otherTF becomeFirstResponder];
            
        }
        else
        {
            [_otherTF endEditing:YES];
            _otherTF.hidden = YES;
            
        }
    }
    else if(tableView.tag == 2)
    {
        //age_tblvw
        
        _ageTF.text = [ageArr objectAtIndex:indexPath.section];
        _ageTblVw.hidden = YES;
        
        _langTblVw.hidden = YES;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
